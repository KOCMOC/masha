<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/font-awesome.css',
        'css/bootstrap.min.css',
        'css/chosen.css',
        'css/fullcalendar.css',
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/all.js',
        'js/jquery.min.js',
        'js/jquery.touchSwipe.min.js',
        'js/moment.min.js',
        'js/fullcalendar.min.js',
        'js/locale-all.js',
        'js/chosen.jquery.js',
        'js/jquery.slideandswipe.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public function init()
    {
        parent::init();
        // resetting BootstrapAsset to not load own css files
        \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'css' => [],
            'js' => []
        ];
    }
}