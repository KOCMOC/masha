<?php

$this->title = 'FAQ';
?>

<ul>
<?php foreach ($questions as $item): ?>
    <li>
        <p><strong><?= nl2br($item->question) ?></strong></p>
        <p><i><?= nl2br($item->answer) ?></i></p>
    </li>
<?php endforeach; ?>
</ul>
