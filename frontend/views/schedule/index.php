<?php

/* @var $this yii\web\View */

$this->title = 'Расписание';

$nopay_order = false;
?>
<?php $course_ = $course;?>
<div id="calendar">

</div>

<select id="change_active" onchange="window.location.href='/schedule/'+$(this).val()">
<?php if($selected_course == 0): ?>
	<option value='0' selected>Выберите курс</option>
<?php endif; ?>
<?php foreach($user_courses as $user_course): if($user_course->course_group_id == 2) $nopay_order = $user_course; ?>
	<option value="<?=$user_course->id ?>"<?=($selected_course == $user_course->id)?' selected':'' ?>><?=$user_course->getCourseGroup()->title ?></option>
<?php endforeach; ?>
</select>

<?php if($nopay_order) switch($quest) {
	case '0':
		$sch = $nopay_order->getScheduleRelation()->orderBy('date')->one();
		if($sch) {
		?><br /><a href="/workout/<?=$sch->id ?>" class="buy-price-btn" style="width:auto;height:auto;padding:5px 15px;line-height:auto;position:static;margin-top:10px;">Попробовать бесплатную тренеровку!</a><?
		}
		break;
	case '1':
		$sch = $nopay_order->getScheduleRelation()->orderBy('date desc')->one();
		if($sch) {
		?><br /><a href="/workout/<?=$sch->id ?>" class="buy-price-btn" style="width:auto;height:auto;padding:5px 15px;line-height:auto;position:static;margin-top:10px;">Попробовать бесплатную тренеровку №2!</a><?
		}
		break;
	case '2':
		?><br /><a href="/course" class="buy-price-btn" style="width:auto;height:auto;padding:5px 15px;line-height:auto;position:static;margin-top:10px;">Купить курс и начать тренероваться!</a><?
		break;
	case '3':
		?><br /><a href="/workout" class="buy-price-btn" style="width:auto;height:auto;padding:5px 15px;line-height:auto;position:static;margin-top:10px;">Тренироваться!</a><?
		break;
} ?>

<?/*<div class="site-index">
    <section class="training_title row">
        Расписание занятий <span><?=\Yii::$app->formatter->asTime(strtotime(date('Y-m-d')), 'php:F Y');?></span>
    </section>
    <section id="tb">

        <select name="course" id="course">
            <option value="0">Все курсы</option>
            <?if(sizeof($user_courses) >0):?>
            <?foreach($user_courses as $user_course):?>
            <?if(!is_null($course)):?>
            <?if($user_course->course_group_id == $course):?>
            <option value="<?=$user_course->course_group_id?>" selected><?=$user_course->getCourseGroup()->title?></option>
            <?else:?>
            <option value="<?=$user_course->course_group_id?>"><?=$user_course->getCourseGroup()->title?></option>
            <?endif;?>
            <?else:?>
            <option value="<?=$user_course->course_group_id?>"><?=$user_course->getCourseGroup()->title?></option>
            <?endif;?>
            <?endforeach;?>
            <?endif;?>
        </select>
    </section>
    <section id="shedule">
    <?php
    #**************** Текущая неделя ****************
        $w_today = date('w');
        $w_today = ($w_today == 0)?7:$w_today;
        $week_arr = array();
        for($i=1;$i<=7;$i++) {
            $week_arr[] = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")-$w_today+$i, date("Y")));
        }
     #**************** End  Текущая неделя ****************

        $firstdate = strtotime('01.'.$month);
        $fmonday = (strtotime('monday 01.'.$month) == $firstdate)?strtotime('monday 01.'.$month):strtotime('last monday 01.'.$month);
        $lmonday = (strtotime('monday .'.date('t', $firstdate).'.'.$month) == $firstdate)?strtotime('monday '.date('t', $firstdate).'.'.$month):strtotime('next monday '.date('t', $firstdate).'.'.$month);

        $next_month = date("Y/m", strtotime('next month 01.'.$month));
        $last_month = date("Y/m", strtotime('last month 01.'.$month));

        $course = ($course !== false)?$course.'/':'';

        $day = 0;
        $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
        $day++;

        $arr_this_month = array();
        while($time < $lmonday) {
            if(date('n', $time) == $mon) {
                $arr_this_month[date('Y-m-d', $time)]=array();
            }
            $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
            $day++;
        }

        if(is_array($schedule) && count($schedule) > 0) {
           foreach ($schedule as $key=>$val){
               $arr_this_month[substr($val->date, 0,10)][] = $val;
           }
        }

      // print_R($arr_this_month);
        $day = 0;
        $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
        $day++;
        $exercise_list_arr = array();
        echo '<table class="course-schedule-table" cellspading="0" cellpadding="0"><tr>';
        while($time < $lmonday) {
            if(date('n', $time) != $mon) {
            	echo '<td data-date="'.date('Y-m-d', $time).'" class="c">'.date('d.m', $time).'</td>';
                //echo '<div class="c" date="'.date('Y-m-d', $time).'" style="border:2px solid #e5e5e5;width:40px;height:40px;float:left"></div>';
            } else {
                if(sizeof($arr_this_month[date('Y-m-d', $time)]) > 0) {
                	$scheduleStr = '';
                	$allDone = true;
                    foreach($arr_this_month[date('Y-m-d', $time)] as $current) {
                        if(in_array(date('Y-m-d', strtotime($current->date)), $week_arr)) {
                            $exercise_list_arr[] = $current;
                        }
                        $scheduleStr .= $current->getWorkout()->title.'<br>';
                        $scheduleStr .= \Yii::$app->formatter->asTime(strtotime($current['date']), 'php:H:i').'<br>';
                        $scheduleStr .= $current->getWorkout()->getCourse()->title.'<br>';
                        if($current->done == 0) {
                        	$allDone = false;
                        }
                    }
					$color = ($allDone === false)?'0f0':'e5e5e5';
					$color2 = ($allDone === false)?'0f0':'ccc';
                    if(in_array(date('Y-m-d', $time), $week_arr)) {
                        if(date('Y-m-d') == date('Y-m-d', $time)) {
                        	echo '<td data-date="'.date('Y-m-d', $time).'" class="c">'.date('d.m', $time).'</td>';
                            //echo '<div class="c" date="'.date('Y-m-d', $time).'" style="border:1px solid #'.$color2.';background:#'.$color.';width:40px;height:40px;float:left">'.date('d.m', $time).'<br>';
                        } else {
                        	echo '<td data-date="'.date('Y-m-d', $time).'" class="c">'.date('d.m', $time).'</td>';
                            //echo '<div class="c" date="'.date('Y-m-d', $time).'" style="border:1px solid #0f0;width:40px;height:40px;float:left">'.date('d.m', $time).'<br>';
                        }
                    } else {
                    	echo '<td data-date="'.date('Y-m-d', $time).'" class="c">'.date('d.m', $time).'</td>';
                        //echo '<div class="c" date="'.date('Y-m-d', $time).'" style="border:1px solid #000;width:40px;height:40px;float:left">'.date('d.m', $time).'<br>';
                    }
                    //echo $scheduleStr.'</div>';
                } else {
                    if(in_array(date('Y-m-d', $time), $week_arr)) {
                        if(date('Y-m-d') == date('Y-m-d', $time)) {
                        	echo '<td>'.date('d.m', $time).'</td>';
                           // echo '<div style="border:2px solid #0f0;background:#0f0;width:40px;height:40px;float:left">'.date('d.m', $time).'</div>';
                        } else {
                        	echo '<td class="c">'.date('d.m', $time).'</td>';
                            //echo '<div class="c" style="border:2px solid #0f0;width:40px;height:40px;float:left">'.date('d.m', $time).'</div>';
                        }
                    } else {
                    	echo '<td class="c">'.date('d.m', $time).'</td>';
                        //echo '<div class="c" style="border:2px solid #000;width:40px;height:40px;float:left">'.date('d.m', $time).'</div>';
                    }
                }
            }
            echo (date('w', $time) == 0)?'</tr><tr>':'';
            //echo (date('w', $time) == 0)?'<div style="clear:both"></div>':'';
            $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
            $day++;
        }
        echo '</tr></table>';
        echo '<div id="d_ex_list" style="margin: 20px 0;">';
        if(sizeof($exercise_list_arr) > 0) {
            foreach($exercise_list_arr as $current){
                echo '<div style="clear: both; width:950px; border:1px solid #000;float:left">'
                . '<div style="width:300px; float:left">'
                . \Yii::$app->formatter->asTime(strtotime($current->date), 'php:d F Y').', '
                . \Yii::$app->formatter->asTime(strtotime($current->date), 'php:D').'.'
                . '<br>'.$current->getWorkout()->title.'<br>'
                . \Yii::$app->formatter->asTime(strtotime($current->date), 'php:H:i').'<br>'
                . (($current->done == 1)?'Завершена':'<a href="/workout/'.$current->id.'">Перейти к тренировке</a>')
                . '</div>'
                . '<div style="border-left:1px solid #000;width:500px; float:left">'
                . $current->getWorkout()->exercise_list .'</div></div>';
            }
        }
        echo '</div>';
       // print_r($exercise_list_arr[0]->getWorkout()->exercise_list);
       // print_R($arr_courses);
    ?>
    </section>
    <section>
        <a href="/schedule/<?=$course.$last_month?>">Назад</a>
        <a href="/schedule/<?=$course.$next_month?>">Вперед</a>
    </section>
</div>
*/?>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.js" type="text/javascript"></script>
<script id="exTemplate" type="text/x-jquery-tmpl">
    <div style="margin: 20px 0;">
        <div style="clear: both; width:950px; border:1px solid #000;float:left">
            <div style="width:300px; float:left">
                ${date},${week_day}<br>
                ${title}<br>
                ${time}<br>
                <a href="/workout/${id}">Перейти к тренировке</a>
            </div>
            <div style="border-left:1px solid #000;width:500px; float:left">
                ${exercise_list}
            </div>
        </div>
    </div>
</script>

<script type="text/javascript">
$(document).ready(function(){

	$('#calendar').fullCalendar('addEventSource', [<?php foreach($schedule as $traning): ?>
			{
                id: <?=$traning->id ?>,
                className: '<?php
                if($traning->done) {
                	echo 'day-allready';
                } else {
                	echo (/*$traning->date >= date('Y-m-d 00:00:00')*/$traning->date < date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), date('d')+1, date('Y'))))?'day-important':'day-soon';
                }
                ?>',
                title: 'Repeating Event',
                description: '<?=$traning->getWorkout()->title ?><br /><?php if(/*$traning->date >= date('Y-m-d 00:00:00') and */$traning->date < date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), date('d')+1, date('Y')))) echo '<a href="/workout/'.$traning->id.'">Перейти</a>';?>',
                start:'<?=str_replace(' ', 'T', $traning->date) ?>'
            },
	<?php endforeach; ?>]);

    $('#course option').click(function(){
        var value = $(this).val();
        if(value == 0) {
            location.href = "/schedule/<?=$y?>/<?=$m?>"
        } else {
            location.href = "/schedule/" + value + "/<?=$y?>/<?=$m?>"
        }
    });

    $('#shedule td.c').click(function(){
        var current_date = $(this).data('date');
        $.ajax({
            beforeSend: function (){
                //showLoader();
            },
            type: 'POST',
            url: '/schedule/week',
            dataType: 'json',
            data: {'current_date':current_date, 'course':"<?=$course_?>"},
            success: function(data){
                if(data.status == 'ok') {
                    if(data.status == 'error') {
                        alert("Ошибка!\n"+data.errors.join("\n"));
                    } else {
                        $('#d_ex_list').html('');
                        if(data.schedule !== null) {
                        	d_sched = data.schedule;
                        	$('#exTemplate').tmpl(d_sched).appendTo('#d_ex_list');
                        }
                    }
                } else {
                    if(data.errors) {
                        if(data.errors.common) {
                            alert(data.errors.common);
                        }
                    }
                }
                //hideLoader();
            },
            error: function(){
                /*hideLoader();*/
                alert('Ошибка: Не удалось получить расписание на неделю.');
            }
        });
        return false;
        //alert($(this).attr('date'));
    });

});
</script>
<div id="popup" class="popup" style="display: none; position: absolute; width: 300px; height: 300px; z-index: 999; background: #fff; box-shadow: 0 0 3px rgba(0,0,0,0.5)"></div>
<div class="shadow"></div>