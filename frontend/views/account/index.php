<?php

use common\models\Course;
use yii\helpers\Html;
use kartik\form\ActiveForm;

$genders = Course::getGenderList();

$this->title = 'Аккаунт';
?>

<?php if(count($orders)): ?>
<div class="panel panel-account">
    <div class="panel-heading text-center">Активные курсы</div>
    <div class="panel-body">
        <?php foreach ($orders as $order):  ?>
        <div class="row">
            <div class="col-lg-4 order-details">
                <span class="order-title"><?=str_replace(['(',')'],['</span><br>',''],$order->getSellItem()->title) ?><br>
                <span><?=@$genders[$order->gender] ?></span>
            </div>
            <div class="col-lg-4 text-center">
                <div class="timer-count-down">
                    <?php
                    if(($order->expire - time()) > 0) {
                        $rem = $order->expire - time();
                        $day = floor($rem / 86400);
                        $hr  = floor(($rem % 86400) / 3600);
                        $min = floor(($rem % 3600) / 60);
                        $sec = ($rem % 60);
                        echo $day.':'.$hr.':'.$min.':'.$sec;
                    }
                    else {
                        echo '00:00:00:00';
                    }
                    ?>
                    </div>
                <div class="count-down-description">До конца курса осталось</div>
            </div>
            <div class="col-lg-4">
                <a class="btn-wm" href="#">Продлить курс</a>
                <a class="btn-wm" href="<?=yii\helpers\Url::toRoute('/schedule') ?>">Тренироваться</a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>

<div class="panel panel-account">
    <div class="panel-heading text-center">Регистрационные данные</div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'form-signup',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
            'options' => [
                'class' => 'user-anket',
            ],
        ]); ?>
        <div class="input-form-part">
            <?= Html::submitButton('Сохранить', ['class' => 'check-button', 'name' => 'change-name']) ?>
            <?= $form->field($model, 'fio')->textInput() ?>
            <?= $form->field($model, 'phone')->textInput(['readonly' => true]) ?>
            <?= $form->field($model, 'email')->textInput(['readonly' => true]) ?>
        </div>
        <div class="input-form-part">
            <?= Html::submitButton('Сохранить', ['class' => 'check-button', 'name' => 'change-password']) ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="panel panel-account">
    <div class="panel-heading text-center">Анкета</div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'avatar-change-form',
            'action' => '/account/change-avatar',
            'options' => ['enctype' => 'multipart/form-data'],
            'validateOnSubmit' => false,
            'enableAjaxValidation' => false,
        ]); ?>
            <div class="avatar-input">
                <img src="<?= (empty(Yii::$app->user->identity->photo) || Yii::$app->user->identity->photo == 'default.png') ? '/images/user_avatar.png' : Yii::$app->user->identity->photo?>" class="user-avatar">
                <?= $form->field($model, 'photo', ['options' => ['class' => 'hidden']])->fileInput(); ?>
                <a href="#" class="select-avatar-file">Загрузить фото</a>
            </div>
        <?php ActiveForm::end(); ?>

        <?php $form = ActiveForm::begin([
            'id' => 'user-anket-form',
            'action' => '/account/change-anket',
            'validateOnSubmit' => true,
            'enableAjaxValidation' => true,
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
            'options' => [
                'class' => 'user-anket',
            ],
        ]); ?>

        <?= $form->field($anketModel, 'gender')
            ->radioList([1 => 'муж.', 0 => 'жен.'], [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-checkbox\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>

        <?= $form->field($anketModel, 'growth')->textInput(['placeholder' => 'Ваш рост']) ?>
        <?= $form->field($anketModel, 'weight')->textInput(['placeholder' => 'Ваш вес']) ?>
        <?= $form->field($anketModel, 'breast_size')->textInput(['placeholder' => 'Ваш размер груди']) ?>
        <?= $form->field($anketModel, 'waist_size')->textInput(['placeholder' => 'Ваш размер талии']) ?>
        <?= $form->field($anketModel, 'hip_size')->textInput(['placeholder' => 'Ваш размер бедер']) ?>
        <div style="display: none">
            <?= $form->field($anketModel, 'operating_weight')->textInput(['placeholder' => 'Ваш рабочий вес', 'value' => '100']) ?>
        </div>
        <?= $form->field($anketModel, 'goal')->radioList([0 => 'похудеть', 1 => 'набрать вес', 2 => 'рельеф'], [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-checkbox\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>

        <div class="form-group text-center">
            <?= Html::submitButton('Сохранить', ['class' => 'btn-wm']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function(){

    $(document).on('click', '.select-avatar-file', function(){
        $('#accountform-photo').click();
        return false;
    });

    $("#accountform-photo").change(function (){
        $('a.select-avatar-file').addClass('now-loading');
        $('#avatar-change-form').submit();
    });

    $('#avatar-change-form').on('submit', function(e){
        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            url : '/account/change-avatar',
            type : 'POST',
            data : formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success : function(data) {
                $("#accountform-photo").val(null);

                if(data.status == 'success') {
                    $('.user-info-avatar').attr('src', data.photo);
                    $('.user-avatar').attr('src', data.photo);
                }

                $('a.select-avatar-file').removeClass('now-loading');
            }
        });
    });

});

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
