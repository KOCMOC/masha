<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Смена пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <p>Ваш новый пароль:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->label(false)->passwordInput(['autofocus' => true]) ?>

	            <div class="wrap">
	                <button class="button">
	                    СОХРАНИТЬ
	                    <img src="../landing/img/reg_icon.png" alt="#" />
	                </button>
	            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
