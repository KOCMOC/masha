<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Тренировки';
?>
<?php if(Yii::$app->session->hasFlash('inform')): ?><div><?= Yii::$app->session->getFlash('inform') ?></div><?php endif; ?>
<?php $form = ActiveForm::begin(['id' => 'delivery-form', 'action' => '']); ?>
	<div class="container clearfix">
	        <div class="title">
	            Запишись сейчас, и получи
	            <span>бесплатные занятия</span><br />
	            и скидку на полный курс!
	        </div>


            <?= $form->field($deliveryModel, 'fio')->textInput(['autofocus' => true, 'placeholder'=>'ФИО'])->label('') ?>

            <?= $form->field($deliveryModel, 'phone')->textInput(['autofocus' => true, 'placeholder'=>'Телефон'])->label('') ?>

            <?= $form->field($deliveryModel, 'email')->textInput(['autofocus' => true, 'placeholder'=>'E-mail'])->label('') ?>

                <div class="form-group">
                    <?= Html::submitButton('Записаться', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

	    </div>
	</div>
<?php ActiveForm::end(); ?>
