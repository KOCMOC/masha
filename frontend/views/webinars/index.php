<?php

use yii\widgets\LinkPager;

$this->title = 'Вебинары';

?>

<?php foreach ($models as $model): ?>

    <div class="news-item">
        <h3><a href="/webinars/<?= $model->id ?>"><?= $model->title ?></a></h3>
        <div class="text">
            <?php if(!empty($model->preview)): ?>
                <img src="<?= $model->preview ?>" style="float: left;width: 150px;margin-right: 10px; margin-bottom: 20px;">
            <?php endif; ?>
            <?= nl2br($model->short_text) ?>
        </div>
    </div>

<?php endforeach; ?>

<?php
echo LinkPager::widget([
    'pagination' => $pages,
]);
?>
