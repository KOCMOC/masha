<?php
$this->title = $model->title;
?>

<div>
<?= $model->video_content ?>
</div>

<div>
<?php if(empty($model->full_text)): ?>

    <?= $model->short_text ?>

<?php else: ?>

    <?= $model->full_text ?>

<?php endif;?>
</div>