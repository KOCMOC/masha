<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\OldAsset;
use common\widgets\Alert;
use yii\helpers\Url;

OldAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
 <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'style' => 'display:none'
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->email . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <div class="container">
         <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <div class="wrapper">
            <div id="navigation">
                <a href="#" class="close"></a>
                <ul>
                    <li><a href="/workout">Тренироваться</a></li>
                    <li><a href="/schedule">Расписание</a></li>
                    <li><a href="#">Достижения</a></li>
                    <li><a href="/diet">Питание</a></li>
                    <li><a href="/ticket">Задать вопрос</a></li>
                    <li><a href="/faq">FAQ</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li><a href="<?=Url::toRoute('course/') ?>">Курсы</a></li>
                    <li><a href="/webinars">Вебинары</a></li>
    				<?php if (!Yii::$app->user->isGuest): ?>
                    <li>
                        <a href="/account" class="profile_link">
                            <img src="<?=Yii::$app->urlManager->createAbsoluteUrl('/img/svg/9.svg')?>" alt="">
                            Мой профиль
                        </a>
                    </li>
                    <li>
                        <a href="/site/logout" class="profile_link">
                            <img src="<?=Yii::$app->urlManager->createAbsoluteUrl('/img/svg/9.svg')?>" alt="">
                            Выйти
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
            <header class="row">
                <a href="#navigation" class="nav_btn">
                    <img src="<?=Yii::$app->urlManager->createAbsoluteUrl('/img/nav_btn.png')?>" alt="">
                </a>
                <a href="#" class="asc_quest">
                    <img src="<?=Yii::$app->urlManager->createAbsoluteUrl('/img/asc.png')?>" alt="">
                </a>
                <h1>
                    <?=$this->title ?>
                </h1>
            </header>
            <div class="content-middle" style="position:relative; top:50px<?/*65px*/?>"><?= $content ?></div>
        </div>

    </div>
</div>
<footer class="footer">
    <div class="container"></div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
