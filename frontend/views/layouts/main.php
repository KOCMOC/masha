<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	    (function (d, w, c) {
	        (w[c] = w[c] || []).push(function() {
	            try {
	                w.yaCounter42952734 = new Ya.Metrika({
	                    id:42952734,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true,
	                    webvisor:true
	                });
	            } catch(e) { }
	        });

	        var n = d.getElementsByTagName("script")[0],
	            s = d.createElement("script"),
	            f = function () { n.parentNode.insertBefore(s, n); };
	        s.type = "text/javascript";
	        s.async = true;
	        s.src = "https://mc.yandex.ru/metrika/watch.js";

	        if (w.opera == "[object Opera]") {
	            d.addEventListener("DOMContentLoaded", f, false);
	        } else { f(); }
	    })(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/42952734" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="main-wrapper">
        <nav class="main-sidebar">

            <?php if(!Yii::$app->user->isGuest): ?>
            <!-- User info Start -->
            <div class="sidebar-user-info">
                <a href="/account"><img src="<?= (empty(Yii::$app->user->identity->photo) || Yii::$app->user->identity->photo == 'default.png') ? '/images/user_avatar.png' : Yii::$app->user->identity->photo?>" class="user-info-avatar"></a>
                <div class="user-info-fullname">
                    <a href="/account">
                <?php
                if(mb_stristr(Yii::$app->user->identity->fio, ' ')) {
                    echo '<span>' . mb_stristr(Yii::$app->user->identity->fio, ' ') . '</span><br>' . "\n";
                    echo str_replace(mb_stristr(Yii::$app->user->identity->fio, ' '), '', Yii::$app->user->identity->fio) . "\n";
                }
                else {
                    echo Yii::$app->user->identity->fio;
                }
                ?>
                    </a>
                </div>
            </div>
            <!-- User info End -->
            <?php endif; ?>

            <!-- Sidebar navigation Start -->
            <div class="sidebar-navigation">
                <?php
                $items = [
                    ['label' => 'Тренироваться', 'icon'=> 'workout', 'url' => '/workout'],
                    ['label' => 'Расписание', 'icon'=> 'calendar', 'url' => '/schedule'],
                    ['label' => 'Статьи', 'icon'=> 'achievements', 'url' => '/page/diet-articles'],
                    ['label' => 'Питание', 'icon'=> 'apple', 'url' => '/diet'],
                    ['label' => 'Задать вопрос', 'icon'=> 'tickets', 'url' => '/ticket'],
                    ['label' => 'FAQ', 'icon'=> 'faq', 'url' => '/faq'],
                    ['label' => 'Новости', 'icon'=> 'news', 'url' => '/news'],
                    ['label' => 'Курсы', 'icon'=> 'course', 'url' => '/course'],
                    ['label' => 'Вебинары', 'icon'=> 'webinars', 'url' => '/webinars'],
                    ['label' => 'Выход', 'icon'=> 'exit', 'url' => '/site/logout'],
                ];

                function isActive($item) {
                    if(mb_stristr($item['url'], Yii::$app->controller->id)) {
                        return ' active';
                    }
                }

                foreach ($items as $item):
                ?>
                <a href="<?= $item['url'] ?>" class="navigation-item <?= isActive($item) ?>">
                    <div class="nav-icon-<?= $item['icon'] ?>"></div>
                    <?= $item['label'] ?>
                </a>
                <?php
                endforeach;
                ?>
            </div>
            <!-- Sidebar navigation End -->

        </nav>
        <div class="main-container <?= Yii::$app->controller->id ?> <?= Yii::$app->controller->action->id ?>">
            <div class="page-title">
                <a href="#" class="open-sidebar ssm-toggle-nav"><img src="/images/open-sidebar.png"></a>
                <?= $this->title ?>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <a href="#" class="feedback-button" data-toggle="modal" data-target="#ticketModal"><img src="/images/feedback.png"></a>
                <?php endif; ?>
            </div>
            <div class="page-content">
                <?= $content ?>
            </div>
        </div>
    </div>

<!-- Ticket Modal -->
<div class="modal fade" id="ticketModal" tabindex="-1" role="dialog" aria-labelledby="ticketModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->render('@frontend/views/ticket/_ticket_form'); ?>
            </div>
        </div>
    </div>
</div>

<div class="ssm-overlay ssm-toggle-nav"></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
