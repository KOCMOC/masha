<?php
use rmrevin\yii\ulogin\ULogin;
use yii\bootstrap\ActiveForm;
use frontend\assets\AppAsset;
use yii\helpers\Html;

//AppAsset::register($this);

$this->registerJsFile('@web/landing/js/jquery.countdown.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/landing/js/jquery.mousewheel.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/landing/js/jquery.fancybox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/landing/js/jquery.jscrollpane.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/landing/js/slick.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/landing/js/all.js?v=1.7', ['depends' => [\yii\web\JqueryAsset::className()]]);

$loginModel = new \common\models\LoginForm();
$signUpModel = new \frontend\models\SignupForm();
$forgotModel = new \frontend\models\PasswordResetRequestForm();

$siteData = Yii::$app->params['siteData'];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	    (function (d, w, c) {
	        (w[c] = w[c] || []).push(function() {
	            try {
	                w.yaCounter42952734 = new Ya.Metrika({
	                    id:42952734,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true,
	                    webvisor:true
	                });
	            } catch(e) { }
	        });

	        var n = d.getElementsByTagName("script")[0],
	            s = d.createElement("script"),
	            f = function () { n.parentNode.insertBefore(s, n); };
	        s.type = "text/javascript";
	        s.async = true;
	        s.src = "https://mc.yandex.ru/metrika/watch.js";

	        if (w.opera == "[object Opera]") {
	            d.addEventListener("DOMContentLoaded", f, false);
	        } else { f(); }
	    })(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/42952734" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>WORKOUT MASHA - Онлайн фитнес-школа Марии Хлопниковой</title>
    <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <link rel="stylesheet" type="text/css" href="../landing/css/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="../landing/css/slick.css"/>
    <link rel="stylesheet" href="../landing/css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="../landing/css/jquery.jscrollpane.css"/>
    <link rel="stylesheet" href="../landing/css/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../landing/css/style.css" />
    <?php $this->head() ?>
    <?php if(1): ?>
    	<style type="text/css">
    		.section_15 .mask { height: 180px !important }
    		@media only screen and (max-width:1919px){ .section_15 .mask{bottom:-60px !important} }
    		@media only screen and (max-width:1365px){ .section_15 .mask{bottom:-75px !important} }
    		@media only screen and (max-width:1023px){ .section_15 .mask{bottom:-95px !important} }
    		@media only screen and (max-width:767px){ .section_15 .mask{bottom:-100px !important} }
    		.section_15 .mask:after,.section_15 .mask:before { height:170px }
    	</style>
    <?php endif; ?>
</head>
<body>
<?php $this->beginBody() ?>
<section class="header">
    <div class="mask"></div>
    <a href="#" class="logo clearfix">
        <img src="../landing/img/heart.png" alt="logo" />
        <div class="text">
            <div class="table">
                <h2>
                    workout masha
                </h2>
                <p>
                    ОНЛАЙН ФИТНЕС-ШКОЛА МАРИИ ХЛОПНИКОВОЙ
                </p>
            </div>
        </div>
    </a>
    <div class="info clearfix">
        <a href="#login_form" class="account clearfix fancybox">
            <img src="../landing/img/acc.png" alt="#" />
            <div class="text">
                <h4>
                    личный кабинет
                </h4>
                ВХОД/РЕГИСТРАЦИЯ
            </div>
        </a>
        <a href="https://www.instagram.com/workout_masha/" target="_blank" class="instagram clearfix">
            <img src="../landing/img/instagram.png" alt="" />
            <div class="text">
                <h4>
                    маша в инстаграмм!
                </h4>
                ПОДПИСЫВАЙСЯ!
            </div>
        </a>
    </div>
    <div class="pic">
        <img src="../landing/img/pic.png" alt="#" />
    </div>
    <div class="title">
        <p class="big_yellow">
            Создай фигуру
        </p>
        <p class="big_white">
            своей мечты
        </p>
        <p class="margin_top_17">
            вместе с Марией Хлопниковой!
        </p>
        <div class="wrap">
            <p class="letter_space_1_6">
                Зарегистрируйся <span> до 24.02.2017</span>
            </p>
            <p class="lineheight_16 letter_space_1_6">
                и получи скидку <span class="dots"></span><span class="big">10%</span>
            </p>
        </div>
        <div class="countdown clearfix">
            <div id="count" class="count"></div>
            <p class="m_left">
                До конца акции осталось
            </p>
            <a class="button fancybox" href="#popup_form">
                зарегистрироваться
                <img src="../landing/img/reg_icon.png" alt="#" />
            </a>
            <p class="m_left">8-800-550-03-37</p>
        </div>
    </div>
    <div class="girl"></div>
</section>
<section class="section_2">
    <h2>
        что мешает девушкам
    </h2>
    <p>
        иметь идеальную фигуру?
    </p>
    <div class="block">
        <ul>
            <li>
                <img src="../landing/img/number1.png" alt="1" />
                <div class="table">
                    На фитнес нет ни сил, ни времени!
                </div>
            </li>
            <li>
                <img src="../landing/img/number2.png" alt="2" />
                <div class="table">
                    Ты тренируешься, но жир не уходит!
                </div>
            </li>
            <li>
                <img src="../landing/img/number3.png" alt="3" />
                <div class="table">
                    Не всегда получается питаться правильно!
                </div>
            </li>
        </ul>
        <div class="img">
            <img src="../landing/img/arrow.png" alt="#" />
        </div>
        <p>
            Выход есть!
        </p>
    </div>
    <div class="girl"></div>
</section>
<section class="section_3">
    <h2>
        занимайся фитнесом
    </h2>
    <p>
        в онлайн фитнес-школе Марии Хлопниковой!
    </p>
    <div class="block">
        <ul>
            <li>
                <div class="img">
                    <img class="phone" src="../landing/img/section_3_item1.png" alt="#" />
                </div>
                <div class="table">
                    Тренировки проходят в твоем телефоне
                    или компьютере, в любой точке мира
                    24/7, в зале или дома
                </div>
            </li>
            <li>
                <div class="img">
                    <img class="program" src="../landing/img/section_3_item2.png" alt="#" />
                </div>
                <div class="table">
                    Ты получаешь индивидуальную программу
                    питания от профессионального диетолога!
                </div>
            </li>
            <li>
                <div class="img">
                    <img class="training" src="../landing/img/section_3_item3.png" alt="#" />
                </div>
                <div class="table">
                    Я и требовательные тренеры помогаем
                    тебе добиться результатов!
                </div>
            </li>
        </ul>
    </div>
    <div class="wrap">
        <a class="button clearfix fancybox" href="#popup_form">
            узнать цены
            <div class="img">
                <img src="../landing/img/reg_icon.png" alt="#" />
            </div>
        </a>
    </div>
    <div class="masters"></div>
</section>
<section class="section_4 clearfix">
    <h2>
        Ты всегда хотела:
    </h2>
    <div class="block">
        <ul>
            <li>
                <img src="../landing/img/check.png" alt="#" />
                <div class="text">
                    сбросить лишний вес
                </div>
            </li>
            <li>
                <img src="../landing/img/check.png" alt="#" />
                <div class="text">
                    точеные ножки
                </div>
            </li>
            <li>
                <img src="../landing/img/check.png" alt="#" />
                <div class="text">
                    накаченную попу
                </div>
            </li>
            <li>
                <img src="../landing/img/check.png" alt="#" />
                <div class="text">
                    красивую грудь
                </div>
            </li>
            <li>
                <img src="../landing/img/check.png" alt="#" />
                <div class="text">
                    плоский живот
                </div>
            </li>
            <li>
                <img src="../landing/img/check.png" alt="#" />
                <div class="text">
                    а еще забыть о стрессах, иметь отличное самочувствие, настроение и фонтан энергии!
                </div>
            </li>
        </ul>
        <h3>
            ДОБЕЙСЯ ГОЛОВОКРУЖИТЕЛЬНОГО
            УСПЕХА В МИРЕ МУЖЧИН!
        </h3>
        <p>
            Запишись в школу онлайн-фитнеса Марии Хлопниковой!
        </p>
    </div>
    <div class="girl"></div>
    <div class="mask">

    </div>
</section>
<section class="section_5">
    <h2>
        почему это эффективно?
    </h2>
    <div class="block">
        <div class="item">
            <div class="center">
                <div class="img">
                    <img class="calendar" src="../landing/img/sec_5_img1.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Сбалан&shy;сирован&shy;ные программы
                тренировок
            </div>
        </div>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img src="../landing/img/sec_5_img2.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Подходят для
                любого уровня
                подготовки
            </div>
        </div>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img src="../landing/img/sec_5_img3.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Более 300
                упражнений для
                дома и для зала
            </div>
        </div>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img src="../landing/img/sec_5_img4.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Все лучшее из
                классики,
                кроссфита и
                боди-фитнеса
            </div>
        </div>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img src="../landing/img/sec_5_img5.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Индивидуальная
                программа
                питания
                + мои рецепты!
            </div>
        </div>
    </div>
    <div class="list">
        <h3>
            Над твоей фигурой работают:
        </h3>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img class="form" src="../landing/img/sec_5_img6.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Сертифицирован&shy;ные
                тренеры
            </div>
        </div>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img class="doctor" src="../landing/img/sec_5_img7.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Профессиональный
                диетолог
            </div>
        </div>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img class="run" src="../landing/img/sec_5_img8.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Спортивный
                врач-консультант
            </div>
        </div>
        <div class="item">
            <div class="center">
                <div class="img">
                    <img class="results" src="../landing/img/sec_5_img9.png" alt="#" />
                </div>
            </div>
            <div class="text">
                Контроль
                за результатом!
            </div>
        </div>
    </div>
    <p>
        Ты заполняешь отчеты, которые просматривает тренер,
        требуя от тебя выкладываться на 100 %
    </p>
    <a href="#popup_form" class="button fancybox">
        узнать подробнее
        <img src="../landing/img/reg_icon.png" alt="#" />
    </a>
</section>
<section class="section_6">
    <h2>
        Почему <span>онлайн фитнес-школа
                Марии Хлопниковой лучше</span>,
        чем самостоятельные тренировки?
    </h2>
    <p>
        Сравни и убедись сама!
    </p>
    <div class="scroll-pane">
        <div class="table">
            <table>
                <thead>
                <tr>
                    <td></td>
                    <td>
                        Самостоятельно
                        + зал
                    </td>
                    <td>
                        Зал +
                        персональный
                        тренер
                    </td>
                    <td>
                        Онлайн школа
                        <?=$siteData['name'] ?>
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Эффективная
                        программа
                    </td>
                    <td>-</td>
                    <td>+</td>
                    <td>+</td>
                </tr>
                <tr>
                    <td>Программа питания
                        от профессионального
                        диетолога
                    </td>
                    <td>-</td>
                    <td>+/-</td>
                    <td>+</td>
                </tr>
                <tr>
                    <td>Контроль техники</td>
                    <td>-</td>
                    <td>+</td>
                    <td>+</td>
                </tr>
                <tr>
                    <td>Тренировки в зале</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                </tr>
                <tr>
                    <td>Тренировки дома и
                        в поездках
                    </td>
                    <td>+</td>
                    <td>-</td>
                    <td>+</td>
                </tr>
                <tr>
                    <td>Стоимость</td>
                    <td>-</td>
                    <td>
                        <div class="text">
                            от<br />
                            <span>15000 </span>в мес*
                        </div>
                    </td>
                    <td>
                        <div class="text">
                            от<br />
                            <span>1800 </span>в мес*
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Эффективность</td>
                    <td>
                        <img src="../landing/img/star1.png" alt="star" />
                    </td>
                    <td>
                        <img src="../landing/img/star2.png" alt="star" />
                        <img src="../landing/img/star2.png" alt="star" />
                        <img src="../landing/img/star2.png" alt="star" />
                        <img src="../landing/img/star2.png" alt="star" />
                        <img src="../landing/img/star2.png" alt="star" />
                    </td>
                    <td>
                        <img src="../landing/img/star3.png" alt="star" />
                        <img src="../landing/img/star3.png" alt="star" />
                        <img src="../landing/img/star3.png" alt="star" />
                        <img src="../landing/img/star3.png" alt="star" />
                        <img src="../landing/img/star3.png" alt="star" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="img_1"></div>
    <img class="img_2" src="../landing/img/tab_img2.png" alt="#" />
    <img class="img_3" src="../landing/img/tab_img3.png" alt="#" />
    <img class="img_4" src="../landing/img/tab_img4.png" alt="#" />
    <div class="mask"></div>
</section>
<section class="section_7">
    <h2>
        Начни заниматься прямо сейчас!
    </h2>
    <div class="block">
        <div class="item clearfix">
            <div class="img">
                <img src="../landing/img/number1.png" alt="1" />
            </div>
            <div class="text">
                <h3>
                    Зарегистрируйся в онлайн школе!
                </h3>
            </div>
        </div>
        <div class="item clearfix mb_51">
            <div class="img">
                <img src="../landing/img/number2.png" alt="2" />
            </div>
            <div class="text">
                <h3>
                    Выбирай нужный курс!
                </h3>
                Оплати подходящий тебе тариф, заполни анкету и получи индивидуальную программу тренировок,
                составленную специально для тебя!
            </div>
        </div>
        <div class="item clearfix mb_45">
            <div class="img">
                <img src="../landing/img/number3.png" alt="3" />
            </div>
            <div class="text pt_25">
                <h3>
                    Тренируйся!
                </h3>
                Выполняй упражнения, заполняй отчеты! По отчетам я буду следить за твоими результатами, и корректировать твою программу! Никаких поблажек
            </div>
        </div>
        <div class="item clearfix mb_40">
            <div class="img">
                <img src="../landing/img/number4.png" alt="4" />
            </div>
            <div class="text">
                <h3>
                    Питайся по программе!
                </h3>
                Твой ежедневный рацион и КЖБУ будет рассчитываться исходя из твоих целей!
            </div>
        </div>
        <div class="item clearfix">
            <div class="img">
                <img src="../landing/img/number5.png" alt="5" />
            </div>
            <div class="text">
                <h3>
                    Достигай результата и поддерживай его!
                </h3>
                Тренируйся по более продвинутому курсу!
            </div>
        </div>
    </div>
    <div class="wrap">
        <a href="#popup_form" class="button fancybox">
            узнать подробнее
            <img src="../landing/img/reg_icon.png" alt="#" />
        </a>
    </div>
    <div class="phone"></div>
</section>
<section class="section_8">
    <h2>
        ВСЕ МОИ УЧЕНИЦЫ ДОБИВАЮТСЯ РЕЗУЛЬТАТОВ!
        СМОЖЕШЬ И ТЫ!
    </h2>
    <div class="slider_wrap">
        <div class="slider">
            <div class="slide">
                <div class="img">
                    <img src="../landing/img/slide.png" alt="#" />
                </div>
            </div>
            <div class="slide">
                <div class="img">
                    <img src="../landing/img/slide.png" alt="#" />
                </div>
            </div>
            <div class="slide">
                <div class="img">
                    <img src="../landing/img/slide.png" alt="#" />
                </div>
            </div>
            <div class="slide">
                <div class="img">
                    <img src="../landing/img/slide.png" alt="#" />
                </div>
            </div>
        </div>
    </div>
    <div class="mask"></div>
    <div class="grid">
        <img src="../landing/img/grid.png" alt="#" />
    </div>
</section>
<section class="section_9">
    <h2>
        курс:
    </h2>
    <div class="text">
        ПОПА, ПРЕСС ДЛЯ ПРИНЦЕСС
    </div>
    <div class="block">
        Курс для дома и зала, рассчитан на максимально быстрые результаты по улучшению формы ягодиц, и созданию плоского живота.
    </div>
    <div class="mask"></div>
    <div class="img">
        <img src="../landing/img/section_9_img.png" alt="#" />
    </div>
    <div class="girl"></div>
</section>
<section class="section_10">
    <div class="wrap clearfix">
        <div class="wrap_t">
            <h2>
                курс:
            </h2>
            <div class="text">
                ЭКСПРЕСС ПОХУДЕНИЕ
            </div>
            <div class="slogan">
                Повернем весы вспять!
            </div>
        </div>
        <div class="block">
            Максимально эффективный комплекс упражнений как для дома, так и для для зала + программа питания, направленная на быстрое жиросжигание! Результат  каждый день виден на весах!
        </div>
    </div>
    <div class="mask"></div>
    <div class="img">
        <img src="../landing/img/section_10_img.png" alt="#" />
    </div>
    <div class="girl"></div>
</section>
<section class="section_11">
    <div class="wrap">
        <h2>
            курс:
        </h2>
        <div class="text">
            ПОДТЯНУТОЕ ТЕЛО
        </div>
        <div class="slogan">
            Четкий рельеф!
        </div>
        <div class="block">
            Курс для дома и для зала, рассчитанный на проработку всех главных мышц, формирующих сногсшибательную спортивную фигуру!
        </div>
    </div>
    <div class="mask"></div>
    <div class="img">
        <img src="../landing/img/section_11_img.png" alt="#" />
    </div>
    <div class="girl"></div>
</section>
<section class="section_12">
    <div class="wrap">
        <h2>
            курс:
        </h2>
        <div class="text">
            ВЕЛИКОЛЕПНАЯ ОСАНКА
        </div>
        <div class="slogan">
            СКОРО!
        </div>
    </div>
    <div class="mask"></div>
    <div class="img">
        <img src="../landing/img/section_12_img.png" alt="#" />
    </div>
    <div class="girl"></div>
</section>
<section class="section_13">
    <h2>
        ПРОФЕССИОНАЛЬНАЯ ПРОГРАММА ПИТАНИЯ
    </h2>
    <div class="block">
        <div class="item">
            <div class="img">
                <img src="../landing/img/section_13_img1.png" alt="#" />
            </div>
            <div class="text">
                Рассчитывает КЖБУ в зависимости от твоих целей
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="../landing/img/section_13_img2.png" alt="#" />
            </div>
            <div class="text">
                Эксклюзивные рецепты от Марии Хлопниковой
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="../landing/img/section_13_img3.png" alt="#" />
            </div>
            <div class="text">
                Контроль профессионального диетолога!
            </div>
        </div>
    </div>
    <div class="menu">
        <img src="../landing/img/section_13_menu.png" alt="menu" />
    </div>
    <div class="mask"></div>
</section>
<section class="section_14 clearfix">
    <h2>
        НАШИ ТРЕНЕРЫ:
    </h2>
    <div class="left">
        <div class="text">
            <h3>
                Мария Хлопникова
            </h3>
            тренерский стаж <span>2</span> года,
            более <span>350</span> довольных учеников.
            Читайте инстаграмм Маши!
        </div>
        <div class="img"></div>
    </div>
    <div class="right">
        <div class="item">
            <div class="img">
                <img src="../landing/img/star4.png" alt="#" />
            </div>
            <div class="text">
                <h4>
                    Настя Стриженко
                </h4>
                сертифицированный тренер,
                школа олимпийского резерва СДЮШР 234.
                Тренерский стаж <span>15</span> лет,
                более <span>1500</span> учеников,
                тренер по технике
            </div>
        </div>
        <div class="item mb_69">
            <div class="img">
                <img src="../landing/img/star4.png" alt="#" />
            </div>
            <div class="text">
                <h4>
                    Рома Иванов
                </h4>
                мастер спорта по пауэрлифтингу,
                тренерский стаж <span>5</span> лет,
                специалист по мотивации
            </div>
        </div>
        <div class="item mb_69">
            <div class="img">
                <img src="../landing/img/star4.png" alt="#" />
            </div>
            <div class="text">
                <h4>
                    Сергей Гордеев
                </h4>
                тренер международного класса по фитнесу, стаж <span>12</span> лет, сертификат GIA,
                автор программ. поддержка
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="../landing/img/star4.png" alt="#" />
            </div>
            <div class="text">
                <h4>
                    Барсукова Лидия Ивановна
                </h4>
                диетолог, высшее профильное образование, стаж более <span>20</span> лет, автор
                программ питания
            </div>
        </div>
    </div>
    <div class="mask"></div>
</section>
<section class="section_15">
    <div class="wrap_c">
        <h2>
            ИЗМЕНЕНИЯ В ТВОЕЙ ЖИЗНИ
            НАЧИНАЮТСЯ ЗДЕСЬ !
        </h2>
        <p class="letter_space_1_6">
            Зарегистрируйся <span> до 24.02.2017</span>
        </p>
        <p class="lineheight_16">
            и получи скидку <span class="dots"></span><span class="big">10%</span>
        </p>
        <div class="countdown clearfix">
            <div id="count" class="count"></div>
            <p>
                До конца акции осталось
            </p>
        </div>
    </div>
    <div class="form">
        <?php
        $form = ActiveForm::begin([
            'id' => 'signup-form',
            'action' => '/site/signup',
            'validateOnSubmit' => true,
            'enableAjaxValidation' => true,
        ]); ?>
        <div style="overflow: hidden">
        <?= $form->field($signUpModel, 'fio', ['options' => ['class' => 'form_group']])->label(false)->textInput(['placeholder' => 'Ваше имя']) ?>

        <?= $form->field($signUpModel, 'email', ['options' => ['class' => 'form_group']])->label(false)->textInput(['placeholder' => 'E-mail']) ?>

        <?= $form->field($signUpModel, 'phone', ['options' => ['class' => 'form_group']])->label(false)->textInput(['placeholder' => 'Телефон']) ?>
        </div>
            <div class="wrap">
                <button class="button">
                    ЗАРЕГИСТРИРОВАТЬСЯ
                    <img src="../landing/img/reg_icon.png" alt="#" />
                </button>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="girl"></div>
    <div class="mask"></div>
</section>
<section class="footer clearfix">
    <div class="left">
        <p>
            ©  2017 <a href="/page/offer" target="_blank">Публичная оферта</a>
        </p>
        <p>
            <?=$siteData['company'] ?>
        </p>
        <p>
            ОГРН <?=$siteData['ogrn'] ?>
        </p>
        <p>
            ИНН <?=$siteData['inn'] ?>
        </p>
        <p>
	        <?=$siteData['adress'] ?>
	    </p>
        <p>
            Тел.: <?=$siteData['phone'] ?>
	    </p>
        <p>
            Поддержка: <?=$siteData['email'] ?>
	    </p>
    </div>
</section>
<?php if(Yii::$app->session->hasFlash('info')): ?>
<div id="popup_flash" class="popup_form" class="fancybox"><?=Yii::$app->session->getFlash('info') ?></div>
<a href="#popup_flash" class="fancybox" id="popup_flash_link"></a>
<?php
$flash = <<< JS
    $(window).load(function() {
        $('#popup_flash_link').click();
    });
JS;
$this->registerJs($flash, yii\web\View::POS_READY);
?>
<?php endif; ?>
<div id="popup_thx" class="popup_thx">
    <h2>
        спасибо,
    </h2>
    <p>
        Ваша заявка принята,<br />
        мы свяжемся с вами в ближайшее время
    </p>
</div>
<div id="popup_form" class="popup_form">
    <div class="form">
        <?php
        $form = ActiveForm::begin([
            'id' => 'signup-form-popup',
            'action' => '/site/signup',
            'validationUrl' => '/site/signup-validate',
            'validateOnSubmit' => true,
            'enableAjaxValidation' => true,
        ]); ?>

            <?= $form->field($signUpModel, 'fio', ['options' => ['class' => 'form_group']])->label(false)->textInput(['placeholder' => 'Ваше имя']) ?>

            <?= $form->field($signUpModel, 'email', ['options' => ['class' => 'form_group']])->label(false)->textInput(['placeholder' => 'E-mail']) ?>

            <?= $form->field($signUpModel, 'phone', ['options' => ['class' => 'form_group']])->label(false)->textInput(['placeholder' => 'Телефон']) ?>

            <button class="button" type="submit">
                Зарегистрироваться
                <img src="../landing/img/reg_icon.png" alt="#" />
            </button>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div id="login_form" class="popup_form">
    <script src="//ulogin.ru/js/ulogin.js"></script>
    <div class="form">
        <?php

        $form = ActiveForm::begin([
            'id' => 'login-form',
            'action' => '/site/login',
            'validateOnSubmit' => true,
            'enableAjaxValidation' => true,
        ]); ?>

            <?= $form->field($loginModel, 'email')->label(false)->textInput(['placeholder' => 'E-mail']) ?>

            <?= $form->field($loginModel, 'password')->label(false)->passwordInput(['placeholder' => 'Пароль']) ?>

            <div class="form_group">
                <?php
                echo ULogin::widget([
                    // widget look'n'feel
                    'display' => ULogin::D_PANEL,

                    // required fields
                    'fields' => [ULogin::F_FIRST_NAME, ULogin::F_LAST_NAME, ULogin::F_EMAIL, ULogin::F_PHONE, ULogin::F_PHOTO],

                    // optional fields
                    //'optional' => [ULogin::F_BDATE],

                    // login providers
                    'providers' => [ULogin::P_VKONTAKTE, ULogin::P_ODNOKLASSNIKI, Ulogin::P_MAILRU, ULogin::P_FACEBOOK],

                    // login providers that are shown when user clicks on additonal providers button
                    'hidden' => [ULogin::P_TWITTER, ULogin::P_GOOGLE, ULogin::P_GOOGLEPLUS, ULogin::P_INSTAGRAM],

                    // where to should ULogin redirect users after successful login
                    'redirectUri' => ['site/ulogin'],

                    // optional params (can be ommited)
                    // force widget language (autodetect by default)
                    'language' => ULogin::L_RU,

                    // providers sorting ('relevant' by default)
                    'sortProviders' => ULogin::S_RELEVANT,

                    // verify users' email (disabled by default)
                    'verifyEmail' => '0',

                    // mobile buttons style (enabled by default)
                    'mobileButtons' => '0',
                ]);
                ?>
            </div>
            <input type="hidden" id="login-form-validation" name="validation" value="yes">
            <button class="button" type="submit">
                ВОЙТИ
                <img src="../landing/img/reg_icon.png" alt="#" />
            </button>
            <div class="form_group">
                <br>
                <a href="#popup_form" class="fancybox">Зарегистрироваться</a>
                <br /> <a href="#forgot_form" class="fancybox">Забыли пароль?</a>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div id="forgot_form" class="popup_form">
    <div class="form">
        <?php $form = ActiveForm::begin([
        	'id' => 'request-password-reset-form',
            'action' => '/site/request-password-reset',
            'validateOnSubmit' => true,
            'enableAjaxValidation' => true,
        ]); ?>

            <?= $form->field($forgotModel, 'email')->label(false)->textInput(['placeholder' => 'E-mail для восстановления']) ?>

            <div class="button" style="cursor:pointer">
                ОТПРАВИТЬ
                <img src="../landing/img/reg_icon.png" alt="#" />
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>