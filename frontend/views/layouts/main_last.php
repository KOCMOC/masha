<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    
    
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Тренировки',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Домой', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Регистрация', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выйти (' . Yii::$app->user->identity->email . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <div class="wrapper">
            <div class="col-md-4">
            <?php
                NavBar::begin([
                    'options' => [
                        'class' => 'navbar',
                    ],
                ]);
                $menuItems = [];
                if (!Yii::$app->user->isGuest) {
                    $menuItems[] = ['label' => 'Начать тренировку', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Мои результаты', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Расписание', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Тренер', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Питание', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'FAQ', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Анкета', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Курсы', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Вебинары', 'url' => ['/workout/index']];
                    $menuItems[] = ['label' => 'Аккаунт', 'url' => ['/workout/index']];
                } 
                echo Nav::widget([
                    'options' => ['class' => 'navbar-left nav'],
                    'items' => $menuItems,
                ]);
                NavBar::end();
                ?>
            </div>
            <div class="col-md-8"><?= $content ?></div>
        </div>
        
    </div>
</div>
    

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
