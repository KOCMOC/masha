<?php

use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = 'Курсы';

/*
<?php foreach($courses as $course): ?>
    <div><a href="/course/<?=$course->id ?>"><?=$course->title ?></a></div>
<?php endforeach; ?>
*/

?>

<?php
	if(Yii::$app->session->hasFlash('info')) {
		echo $this->render('_modal');
		$script = "
			$(document).ready(function(){
		    	$('#orderModal').modal('show');
		    	$('#orderModal').on('shown.bs.modal', function() {
		        	$('#orderModal .modal-body-course').html('<div class=\"preloader\">".str_replace("'", "\'", Yii::$app->session->getFlash('info'))."</div>');
		    	});
	    	});";
	    $this->registerJs($script, yii\web\View::POS_READY);
	}
?>

<div class="course-preview" id="course-1">
    <div class="course-details">
        <div class="course-word">Курс:</div>
        <div class="course-title">ПОПА, ПРЕСС ДЛЯ ПРИНЦЕСС</div>
        <div class="course-description">
            Курс для дома и зала, рассчитан на максимально быстрые результаты по улучшению формы ягодиц, и созданию плоского живота.
        </div>
        <a href="/course/one" class="course-order-btn">Подробнее</a>
    </div>

    <div class="course-bg-element"></div>
</div>

<div class="course-preview" id="course-2">
    <div class="course-details">
        <div class="course-word">Курс:</div>
        <div class="course-title">ЭКСПРЕСС ПОХУДЕНИЕ <span>Повернем весы вспять!</span></div>
        <div class="course-description">
            Максимально эффективный<br>
            комплекс упражнений как для дома<br>
            так и для для зала + программа<br>
            питания, направленный на быстрое<br>
            жиросжигание! Результат  каждый<br>
            день виден на весах!
        </div>
        <a href="/course/two" class="course-order-btn">Подробнее</a>
    </div>

    <div class="course-bg-element"></div>
</div>

<div class="course-preview" id="course-3">
    <div class="course-details">
        <div class="course-word">Курс:</div>
        <div class="course-title">Подтянутое тело <span>Четкий рельеф!</span></div>
        <div class="course-description">
            Курс для дома и для зала, расчитанный на проработку всех главных мышц, формирующих сногсшибательную спортивную фигуру!
        </div>
        <a href="/course/three" class="course-order-btn">Подробнее</a>
    </div>

    <div class="course-bg-element"></div>
</div>