<?php

	$this->title = 'Fit Champion';

?>
<div class="site-index" style="top:30px">
	<div style="width:282px;height:40px;line-height:40px;font-weight:bold;background:linear-gradient(to right, #ff9341, #ff3c4a);text-align:center;color:#fff;border-radius:10px 10px 0 0">
		<div style="background: url(/img/svg/calendar-arrow-left.svg) no-repeat; background-position:center; border-radius:10px 0 0 0; width:40px; height:40px;cursor:pointer; float:left;"></div>
		Октябрь 2016
		<div style="background: url(/img/svg/calendar-arrow-right.svg) no-repeat; background-position:center; border-radius:0 10px 0 0; width:40px; height:40px; cursor:pointer; float:right;"></div>
	</div>

    <section id="course-shedule">
    <?php
    /**************** Текущая неделя ****************/
        $w_today = date('w');
        $w_today = ($w_today == 0)?7:$w_today;
        $week_arr = array();
        for($i=1;$i<=7;$i++) {
            $week_arr[] = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")-$w_today+$i, date("Y")));
        }
     /**************** End  Текущая неделя ****************/

        $firstdate = strtotime('01.'.$month);
        $fmonday = (strtotime('monday 01.'.$month) == $firstdate)?strtotime('monday 01.'.$month):strtotime('last monday 01.'.$month);
        $lmonday = (strtotime('monday .'.date('t', $firstdate).'.'.$month) == $firstdate)?strtotime('monday '.date('t', $firstdate).'.'.$month):strtotime('next monday '.date('t', $firstdate).'.'.$month);

        $next_month = date("Y/m", strtotime('next month 01.'.$month));
        $last_month = date("Y/m", strtotime('last month 01.'.$month));

        $day = 0;
        $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
        $day++;

        $arr_this_month = array();
        while($time < $lmonday) {
            if(date('n', $time) == $mon) {
                $arr_this_month[date('Y-m-d', $time)]=array();
            }
            $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
            $day++;
        }

        /*if(is_array($schedule) && count($schedule) > 0) {
           foreach ($schedule as $key=>$val){
               $arr_this_month[substr($val->date, 0,10)][] = $val;
           }
        }*/

      // print_R($arr_this_month);
        $day = 0;
        $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
        $today_time = strtotime(date('d.m.Y'));  ;
        $day++;
        $exercise_list_arr = array();
        echo '<table class="course-schedule-table" cellspading="0" cellpadding="0"><tr>';
        while($time < $lmonday) {
        	$class = ($time >= $today_time)?'active':'';
            if(date('n', $time) != $mon) {
                echo '<td data-week="'.date('w', $time).'" data-time="'.$time.'" class="'.$class.'">'.date('d.m', $time).'</td>';
            } else {
                if(sizeof($arr_this_month[date('Y-m-d', $time)]) > 0) {
                	$scheduleStr = '';
                	$allDone = true;
                    foreach($arr_this_month[date('Y-m-d', $time)] as $current) {
                        if(in_array(date('Y-m-d', strtotime($current->date)), $week_arr)) {
                            $exercise_list_arr[] = $current;
                        }
                        $scheduleStr .= $current->getWorkout()->title.'<br>';
                        $scheduleStr .= \Yii::$app->formatter->asTime(strtotime($current['date']), 'php:H:i').'<br>';
                        $scheduleStr .= $current->getWorkout()->getCourse()->title.'<br>';
                        if($current->done == 0) {
                        	$allDone = false;
                        }
                    }
					$color = ($allDone === false)?'0f0':'e5e5e5';
					$color2 = ($allDone === false)?'0f0':'ccc';
                    if(in_array(date('Y-m-d', $time), $week_arr)) {
                        if(date('Y-m-d') == date('Y-m-d', $time)) {
                        	echo '<td data-week="'.date('w', $time).'" data-time="'.$time.'" class="'.$class.'">'.date('d.m', $time).'</td>';
                            //echo '<div class="course-day-schedule" date="'.date('Y-m-d', $time).'">'.date('d.m.Y', $time).'<br>';
                        } else {
	                        echo '<td data-week="'.date('w', $time).'" data-time="'.$time.'" class="'.$class.'">'.date('d.m', $time).'</td>';
                            //echo '<div class="course-day-schedule" date="'.date('Y-m-d', $time).'">'.date('d.m.Y', $time).'<br>';
                        }
                    } else {
                    	echo '<td data-week="'.date('w', $time).'" data-time="'.$time.'" class="'.$class.'">'.date('d.m', $time).'</td>';
                        //echo '<div class="course-day-schedule" date="'.date('Y-m-d', $time).'">'.date('d.m.Y', $time).'<br>';
                    }
                    echo $scheduleStr.'</div>';
                } else {
                    if(in_array(date('Y-m-d', $time), $week_arr)) {
                        if(date('Y-m-d') == date('Y-m-d', $time)) {
                        	echo '<td data-week="'.date('w', $time).'" data-time="'.$time.'" class="'.$class.'">'.date('d.m', $time).'</td>';
                            //echo '<div class="course-day-schedule">'.date('d.m.Y', $time).'</div>';
                        } else {                        	echo '<td data-week="'.date('w', $time).'" data-time="'.$time.'" class="'.$class.'">'.date('d.m', $time).'</td>';
                            //echo '<div class="course-day-schedule">'.date('d.m.Y', $time).'</div>';
                        }
                    } else {
                    	echo '<td data-week="'.date('w', $time).'" data-time="'.$time.'" class="'.$class.'">'.date('d.m', $time).'</td>';
                        //echo '<div class="course-day-schedule">'.date('d.m.Y', $time).'</div>';
                    }
                }
            }
            echo (date('w', $time) == 0)?'</tr><tr>':'';
            $time = strtotime(date('d.m.Y', $fmonday).' + '.$day.' day');
            $day++;
        }
        echo '</tr></table>';
    ?>
    </section>
    <form id="order-form" method="post">
    	<input type="hidden" name="selected_time">
    	<input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken() ?>">
    	<input type="submit" value="Заказать">
    </form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
    	<?php
    	$schstr = '';
    	$schdays = explode(',', $course->schedule_template);
    	if(sizeof($schdays) > 0) {    		foreach($schdays as $schday) {            	$schstr .= ',"wd'.$schday.'":true';    		}    	}
        $schstr = substr($schstr, 1);
    	?>
    	var schedule_template = {<?=$schstr ?>};

    	var selected_time = false;

		$('#course-shedule').delegate('.course-schedule-table td.active', 'click', function(){
			selected_time = $(this).data('time');
			cnt = 0;        	$(this).closest('.course-schedule-table').find('td').removeClass('trained').each(function(){        		if($(this).data('time') >= selected_time) {
                    if(cnt === 0) {                    	selected_time = $(this).data('time');                    }        			if(cnt < <?=$course->getWorkouts()->count() ?> && schedule_template['wd'+$(this).data('week')] && schedule_template['wd'+$(this).data('week')] === true) {                    	$(this).addClass('trained');
                    	cnt++;        			}        		}        	});		});
		$('#order-form').submit(function(){			if(selected_time !== false) {				$(this).find('input[name=selected_time]').val(selected_time);
				return true;			} else {				return false;			}		});
	});
</script>
