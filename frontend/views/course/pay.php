<?php

$this->title = 'Перейти к оплате счета';

?>
<form style="visibility:hidden" id="payment-form" action="<?php if($yandex['debug'] === true) echo yii\helpers\Url::toRoute('site/kassa'); elseif($yandex['is_test'] === true) echo "https://demomoney.yandex.ru/eshop.xml"; else echo "https://money.yandex.ru/eshop.xml"; ?>" method="post" style="display:none">
<input name="paymentType" value="" type="hidden">
<input name="shopId" value="<?=$yandex['shopId'] ?>" type="hidden"/>
<input name="scid" value="<?=$yandex['scid'] ?>" type="hidden"/>
<input name="sum" id="order-sum" value="<?=$amount ?>"/>
<input name="orderNumber" id="order-number" value="<?=$invoiceHash ?>"/>
<input name="customerNumber" value="<?=$user->id ?>"/>
<input name="cps_email" value="<?=$user->email ?>"/>
<input name="cps_phone" value="<?=$user->phone ?>"/>
<input name="shopSuccessUrl" value="<?=\yii\helpers\Url::toRoute(['course/pay', 'course_id'=>$course_id, 'transaction_id'=>$transaction_id], 'http') /*=$site_url ?>/course/pay/<?=$url /* сюда надо ебануть нормальный абсолютный урл */ ?>"/>
<input name="shopFailUrl" value="<?=\yii\helpers\Url::toRoute('course/', 'http').'?fail=payment' /*=$site_url ?>/course/pay/<?=$url /* сюда надо ебануть нормальный абсолютный урл */ ?>"/>
<input name="shopDefaultUrl" value="<?=\yii\helpers\Url::toRoute('course/', 'http') /*=$site_url ?>/course/pay/<?=$url /* сюда надо ебануть нормальный абсолютный урл */ ?>"/>
<?php if($yandex['debug'] === true): ?>
<input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>" />
<?php endif; ?>
<input type="submit" id="yaform-submit">
</form>


<div class="payment-order">
    <p><a href="#" class="go-pay">Переход к оплате</a></p>
</div>

<!--Недостаточно средств на счёте.<br />
<a id="pay" href="javascript:void(0)">пополнить баланс на <?/*=$amount */?>р.</a>
-->
<script type="text/javascript">
	$(document).ready(function(){
		$('.go-pay').on('click', function(){
		    $('#payment-form').submit();
		    return false;
        });
        $('.go-pay').trigger('click');
	});
</script>