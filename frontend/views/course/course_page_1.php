<?php
$this->title = $coursePages['one']['title'];

// Скрипты от видео плеера
$this->registerJsFile('http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js',  ['position' => yii\web\View::POS_END]);
$this->registerJsFile('http://vjs.zencdn.net/5.16.0/video.js',  ['position' => yii\web\View::POS_END]);
$this->registerCssFile('http://vjs.zencdn.net/5.16.0/video-js.css');

// Свойства курса
$course = [
    // Видео
    'video-preview' => '../images/course_1_video_preview.jpg',
    'video-url' => 'http://cache-fitchampions.cdnvideo.ru/fitchampions/29.01/final%20sound%20master/2.mp4',

    // Название и подзаголовок
    'title' => $coursePages['one']['name'],
    'sub-title' => 'оптимально для быстрого улучшения фигуры!',
    'description' => 'Курс для дома и зала, рассчитан на максимально быстрые результаты по улучшению формы ягодиц, и созданию плоского живота.'
];

?>

<div class="course-lp-wrapper">
    <!-- If you'd like to support IE8 -->
    <div class="course-video-preview">
        <video id="course-video" class="video-js" controls preload="auto" width="620" height="398" poster="<?= $course['video-preview'] ?>" data-setup="{}">
            <source src="<?= $course['video-url'] ?>" type='video/mp4'>
        </video>
    </div>

    <div class="course-title">
        <?= $course['title'] ?>
    </div>
    <div class="course-sub-title">
        <?= $course['sub-title'] ?>
    </div>

    <div class="course-decription">
        <div class="decription-content">
            <?= $course['description'] ?>
        </div>
    </div>

    <div class="dotted-line"></div>

    <div class="course-details-wrapper">
        <div class="course-features">
            <div clas="row">

                <div class="col-lg-6 feature-item">
                    <img src="/images/feature-icon-1.png">
                    <div class="feature-item-text">Насыщенная программа для зала или для дома</div>
                </div>

                <div class="col-lg-6 feature-item">
                    <img src="/images/feature-icon-2.png">
                    <div class="feature-item-text">Три уровня сложности - для новичков, для среднего уровня, и для
                        профессионалов
                    </div>
                </div>

                <div class="col-lg-6 feature-item">
                    <img src="/images/feature-icon-3.png">
                    <div class="feature-item-text">Продолжительность от 12 недель, от 2х до 4х занятий в неделю, по
                        45-60
                        минут
                    </div>
                </div>

                <div class="col-lg-6 feature-item">
                    <img src="/images/feature-icon-4.png">
                    <div class="feature-item-text">Специальная програма питания и уникальны рецепты от Маши</div>
                </div>

                <div class="col-lg-6 feature-item">
                    <img src="/images/feature-icon-5.png">
                    <div class="feature-item-text">Контроль персонального тренера</div>
                </div>

            </div>
        </div>
        <div class="course-bg-element"></div>
        <div class="course-results-title">Результаты</div>
        <div class="course-results-gallery">
            <!-- Carousel -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <div class="carousel-inner" role="listbox">
                    <div class="item text-center active">
                        <img class="first-slide img-responsive" src="../images/slider_rw_1.png">
                    </div>
                    <div class="item">
                        <img class="second-slide img-responsive" src="../images/slider_rw_2.png">
                    </div>
                    <div class="item">
                        <img class="third-slide img-responsive" src="../images/slider_rw_3.png">
                    </div>
                    <div class="item">
                        <img class="four-slide img-responsive" src="../images/slider_rw_4.jpg">
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div><!-- /.carousel -->
        </div>
        <div class="course-results">

        </div>
    </div>

    <div class="course-price">
        <div class="course-price-title">Купить курс</div>

        <div class="price-items">
            <div class="price-item price-item-type-vip">
                <div class="price-item-title">VIP</div>
                <ul>
                    <li>Персональный тренер</li>
                    <li>Персональный диетолог</li>
                    <li>Личный контроль Маши</li>
                    <li>Все вебинары включены</li>
                    <li>Все мероприятия включены</li>
                    <li>Расширенные возможности приложения</li>
                </ul>
                <div class="price-cost">
                    14900 Р/МЕС
                </div>
                <a href="#" data-course-id="<?=$coursePages['one']['course'] ?>" data-type="2" class="buy-price-btn">Купить</a>
            </div>
            <div class="price-item price-item-type-base">
                <div class="price-item-title">Базовый</div>
                <ul>
                    <li>Индивидуальная программа питания</li>
                    <li>Эффективная программа тренировок с планом увеличения нагрузок</li>
                    <li>Контроль отчетов и питания, с корректировками от тренера и диетолга</li>
                    <li>Базовые консультации специалистов</li>
                </ul>
                <div class="price-cost">
                    3300 Р/МЕС
                </div>
                <a href="#" data-course-id="<?=$coursePages['one']['course'] ?>" data-type="0" class="buy-price-btn">Купить</a>
            </div>
            <div class="price-item price-item-type-optimal">
                <div class="price-item-title">Оптимальный</div>
                <ul>
                    <li>Индивидуальная программа питания</li>
                    <li>Эффективная программа тренировок</li>
                    <li>Контроль отчетов и питания</li>
                    <li>Регулярная мотивация от тренера и диетолога</li>
                    <li>Консультации всех специалистов</li>
                </ul>
                <div class="price-cost">
                    5500 Р/МЕС
                </div>
                <a href="#" data-course-id="<?=$coursePages['one']['course'] ?>" data-type="1" class="buy-price-btn">Купить</a>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->render('_modal');

$script = <<< JS

$(document).ready(function(){

    var player = videojs('course-video');

    videojs('course-video').ready(function(){
        console.log(this.options()); //log all of the default videojs options

        // Store the video object
        var myPlayer = this, id = myPlayer.id();
        // Make up an aspect ratio
        var aspectRatio = 398/620;

        function resizeVideoJS(){
            var width = document.getElementById(id).parentElement.offsetWidth;
            myPlayer.width(width).height( width * aspectRatio );
        }

        // Initialize resizeVideoJS()
        resizeVideoJS();
        // Then on resize call resizeVideoJS()
        window.onresize = resizeVideoJS;
    });
});

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>