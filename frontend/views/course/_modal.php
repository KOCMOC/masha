<!-- Modal -->
<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel">
    <div class="modal-dialog order-modal-size" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-body-course">
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function(){

    $(document).on('click', '.buy-price-btn', function(){
        var course_id = $(this).data('course-id');
        var type = $(this).data('type');

        $('#orderModal').modal('show');
        $('#orderModal').on('shown.bs.modal', function() {
            $('#orderModal .modal-body-course').html('<div class="preloader">Loading...</div>');
            $('#orderModal .modal-body-course').load('/course/' + course_id + '?type='+type);
        });
        return false;
    });

    $(document).on('click', '.course-order-item-buy a', function(){
        var href = $(this).attr('href');
        $('#orderModal .modal-body-course').html('<div class="preloader">Loading...</div>');
        $('#orderModal .modal-body-course').load(href);
        return false;
    });

    $(document).on('click', '.course-order-item-buy a', function(){
        if($('#igree').is(':checked'))
            return true;
        else
            return false;
    });

});

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>