<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;

$this->title = 'Fit Champion';

?>
	<?php if($order): ?>

	<?php if($order->active == 1): ?>
		<?php  if($order->schedule_template == ''): ?>
        <div class="panel panel-account">
            <div class="panel-heading text-center">Анкета курса</div>
            <div class="panel-body">
            <?php
            	$growth = $orderModel->growth?$orderModel->growth:0;
            	$weight = $orderModel->weight?$orderModel->weight:0;
				$born = strtotime($orderModel->born);
			    $age = date('Y') - date('Y', $born);
			    if (date('md', $born) > date('md')) {
			  	    $age--;
			    }
			    if($age < 7 or $age > 70) $age = 0;
			    $born = $orderModel->born;
            	$gender = ($orderModel->gender == 2)?1:0;
            	$mother = $orderModel->mother;
            	$vegan = $orderModel->vegan;
            	$goal = $orderModel->goal;
            	$activity = 1;
            ?>
    <?php $form = ActiveForm::begin([
        'id' => 'order-course',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        'options' => [
            'role' => 'form',
            'class' => 'user-anket'
        ],
        'action' => Url::to(['course/order', 'id' => $course_group->id])
    ]);?>
    <?= $form->field($orderModel, 'fio')->textInput(['placeholder' => 'Фамилия Имя Отчество']) ?>
    <?= $form->field($orderModel, 'born')->textInput(['placeholder' => 'дд.мм.гггг', 'class' => 'handle', 'data-label' => 'born']) ?>
    <?= $form->field($orderModel, 'country')->textInput(['placeholder' => 'Страна']) ?>
    <?= $form->field($orderModel, 'city')->textInput(['placeholder' => 'Город']) ?>
    <?= $form->field($orderModel, 'insta')->textInput(['placeholder' => 'Ссылка на аккаунт']) ?>
    <?= $form->field($orderModel, 'vk')->textInput(['placeholder' => 'Ссылка на страницу']) ?>

	<?=$form->field($orderModel, 'gender')->radioList($genders, [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-checkbox handle\" data-label=\"gender\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>

    <?= $form->field($orderModel, 'growth')->textInput(['placeholder' => 'Ваш рост, см', 'class'=>'handle', 'data-label' => 'growth']) ?>
    <?= $form->field($orderModel, 'weight')->textInput(['placeholder' => 'Ваш вес, кг', 'class'=>'handle', 'data-label' => 'weight']) ?>
    <?= $form->field($orderModel, 'breast_size')->textInput(['placeholder' => 'Объем груди, см']) ?>
    <?= $form->field($orderModel, 'waist_size')->textInput(['placeholder' => 'Объем талии, см']) ?>
    <?= $form->field($orderModel, 'hip_size')->textInput(['placeholder' => 'Объем бедер, см']) ?>
    <?= $form->field($orderModel, 'most_hip_size')->textInput(['placeholder' => 'Объем бедра в самом широком месте']) ?>
    <div style="display: none">
        <?= $form->field($orderModel, 'operating_weight')->textInput(['placeholder' => 'Ваш рабочий вес', 'value' => '100']) ?>
    </div>
    <?= $form->field($orderModel, 'medical')->textArea(['placeholder' => 'Медицинские противопоказания']) ?>
	<?=$form->field($orderModel, 'activity')->DropDownList([
				'1' => 'сидячий образ жизни',
				'2' => '1-2 тренировки в неделю',
				'3' => '3-4 тренировки в неделю',
				'4' => '5-6 тренировок в неделю',
				'5' => '7 тренировок в неделю'
			], [
            	'data-label' => 'activity',
            	'class' => 'handle'
            ]); ?>
	<?=$form->field($orderModel, 'mother')->radioList([0=>'Нет', 1=>'Да'], [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-checkbox handle\" data-label=\"mother\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>
	<?=$form->field($orderModel, 'vegan')->radioList([0=>'Нет', 1=>'Да'], [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-checkbox\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>
    <?= $form->field($orderModel, 'goal')->radioList([0 => 'похудеть', 1 => 'набрать вес', 2 => 'рельеф'], [
        'item' => function ($index, $label, $name, $checked, $value) {
            $check = $checked ? ' checked="checked"' : '';
            return "<input class=\"css-checkbox handle\" data-label=\"goal\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
        }]); ?>

	<?=$form->field($orderModel, 'place')->radioList($places, [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-checkbox\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>
	<?=$form->field($orderModel, 'level')->radioList($levels, [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-checkbox\" type=\"radio\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>
	<?=$form->field($orderModel, 'weekdays')->checkboxList($orderModel->getWeekdays(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                $check = $checked ? ' checked="checked"' : '';
                return "<input class=\"css-calendar-checkbox\" type=\"checkbox\" id=\"{$name}_{$value}\" name=\"$name\" value=\"$value\"$check><label class=\"css-calendar-label\" for=\"{$name}_{$value}\">$label</label>";
            }]); ?>
            <div class="daykal">Ваша суточная норма калорий: <span style="font-weight:bold"></span><input type="hidden" name="kal" value="0"></div>
        <?= Html::submitButton('Выбрать', ['class' => 'go-to-payment', 'name' => 'order-button']) ?>
    <?php ActiveForm::end();?>
        </div>
    </div>
    	<?php else: ?>

	    <?php $form = ActiveForm::begin([
	        'id' => 'order-anket',
	        'type' => ActiveForm::TYPE_HORIZONTAL,
	        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
	        'options' => [
	            'role' => 'form',
	            'class' => 'user-anket'
	        ],
	        'action' => Url::to(['course/anket'])
	    ]);?>
	    <input type="hidden" name="order_id" value="<?=$order->id ?>">
	    <?php
   			$anket_fields = $order->getAnket();
   			if(sizeof($anket_fields) > 0) {
   				$growth = 0;
   				$weight = 0;
   				$age = 0;
   				$born = '';
   				$gender = 0;
   				$activity = 1;

   				$mother = 0;
   				$vegan = 0;

   				$uanket = $order->getUser()->getAnket(); $goal = ($uanket)?$uanket->goal:0;

   				foreach($anket_fields as $anket_field) {
   					$label = false;
   					$placeholder = $anket_field->title;
   					if(strpos($anket_field->title, 'Рост') > -1) {   						$growth = (int)$anket_field->value;
   						$label = 'growth';   					} elseif(strpos($anket_field->title, 'Вес') > -1) {   						$weight = (int)$anket_field->value;
   						$label = 'weight';   					} elseif(strpos($anket_field->title, 'Дата рождения') > -1) {
   						$born = strtotime($anket_field->title);
					    $age = date('Y') - date('Y', $born);
					    if (date('md', $born) > date('md')) {
					  	    $age--;
					    }
					    if($age < 7 or $age > 70) $age = 0;
	   					$label = 'born';
	   					$placeholder = 'дд.мм.гггг';
   					} elseif(strpos($anket_field->title, 'Пол женский') > -1) {                        $gender = $anket_field->value?1:0;
                        $label = 'gender';
   					} elseif(strpos($anket_field->title, 'уровень повседневной физической активности') > -1) {
   						$activity = (int)$anket_field->value;
   						if($activity < 1 or $activity > 5) $activity = 1;
   						$label = 'activity';
   					}/* elseif(strpos($anket_field->title, 'Кормите ли вы грудью') > -1) {
   						$activity = (int)$anket_field->value;
   						if($activity < 1 or $activity > 7) $activity = 0;
   						$label = 'mother';
   					} elseif(strpos($anket_field->title, 'Вы вегетарианец') > -1) {
   						$activity = (int)$anket_field->value;
   						if($activity < 1 or $activity > 7) $activity = 0;
   						$label = 'vegan';
   					}*/
                    if($label == 'activity') { ?>						<div class="form-group">
						<label class="control-label col-sm-5" style="padding-top:0;display:inline-block;vertical-align:middle;height:auto;float:none" for="useranket-growth" style="height:auto"><?=$anket_field->title ?></label>
						<div class="col-sm-6" style="display:inline-block;vertical-align:middle;float:none">
						<select class="form-control<?=($label !== false)?' handle':''?>"<?=($label !== false)?' data-label="'.$label.'"':'' ?> name="fields[<?=$anket_field->id ?>]">
							<option value="1"<?=($activity == 1)?' selected':''; ?>>сидячий образ жизни</option>
							<option value="2"<?=($activity == 2)?' selected':''; ?>>1-2 тренировки в неделю</option>
							<option value="3"<?=($activity == 3)?' selected':''; ?>>3-4 тренировки в неделю</option>
							<option value="4"<?=($activity == 4)?' selected':''; ?>>5-6 тренировок в неделю</option>
							<option value="5"<?=($activity == 5)?' selected':''; ?>>7 тренировок в неделю</option>
						</select>
						<div class="help-block"></div>
						</div>
						</div>
						<?php                    } else
                	switch ($anket_field->type) {
                		case '0': ?>
						<div class="form-group">
						<label class="control-label col-sm-5" style="padding-top:0;display:inline-block;vertical-align:middle;height:auto;float:none" for="useranket-growth" style="height:auto"><?=$anket_field->title ?></label>
						<div class="col-sm-6" style="display:inline-block;vertical-align:middle;float:none">
						<input type="text" class="form-control<?=($label !== false)?' handle':''?>"<?=($label !== false)?' data-label="'.$label.'"':'' ?> name="fields[<?=$anket_field->id ?>]" value="<?=$anket_field->value ?>" placeholder="<?=$placeholder ?>">
						<div class="help-block"></div>
						</div>
						</div>
						<?php
						break;
                		case '1': ?>
						<div class="form-group">
						<label class="control-label col-sm-5" style="padding-top:0;display:inline-block;vertical-align:middle;height:auto;float:none" for="useranket-growth" style="height:auto"><?=$anket_field->title ?></label>
						<div class="col-sm-6" style="display:inline-block;vertical-align:middle;float:none">
						<input type="text" class="form-control<?=($label !== false)?' handle':''?>"<?=($label !== false)?' data-label="'.$label.'"':'' ?> name="fields[<?=$anket_field->id ?>]" value="<?=$anket_field->value ?>" placeholder="<?=$placeholder ?>">
						<div class="help-block"></div>
						</div>
						</div>
						<?php
						break;
                		case '2': ?>
						<div class="form-group">
						<label class="control-label col-sm-7" style="padding-top:0;display:inline-block;vertical-align:middle;height:auto;float:none" for="useranket-growth" style="height:auto"><?=$anket_field->title ?></label>
						<div class="col-sm-1" style="display:inline-block;vertical-align:middle;float:none">
						<input type="checkbox" class="form-control<?=($label !== false)?' handle':''?>"<?=($label !== false)?' data-label="'.$label.'"':'' ?><?=($anket_field->value)?' checked':'' ?> onchange="$(this).parents('.form-group').find('input[type=hidden]').val(this.checked?1:0)">
						<input type="hidden" name="fields[<?=$anket_field->id ?>]" value="<?=$anket_field->value?1:0 ?>">
						<div class="help-block"></div>
						</div>
						</div>
						<?php
						break;
					}
   				}
   				echo Html::submitButton('Сохранить', ['class' => 'go-to-payment', 'name' => 'order-button']);
   			}
     	?>
     	<div class="daykal">Ваша суточная норма калорий: <span style="font-weight:bold"></span><input type="hidden" name="kal" value="0"></div>
	    <?php ActiveForm::end();?>
        <?php endif; ?>
     	<script type="text/javascript">
     		var growth = <?=$growth ?>, weight = <?=$weight ?>, age = <?=$age ?>, born = '<?=$born ?>', gender = '<?=$gender ?>', activity = <?=$activity ?>, goal = '<?=$goal ?>', mother = '<?=$mother ?>';
			var kal = Math.round(getKal(weight, growth, age, activity, gender, mother));
      		$('.daykal span').html((kal > 0)?kal:'Неверно заполнена анкета');
      		$('.daykal input').val(kal);
     		$(document).ready(function(){
            	$('.handle').change(function(){
            		var label = $(this).data('label');
            		if(label == 'growth') {
                    	growth = parseInt($(this).val());
                    	if(growth < 1) {
                    		console.log('Неверно заполнен рос');
                    		growth = 0;
                    	}
            		} else if(label == 'weight') {
                    	weight = parseInt($(this).val());
                    	if(weight < 1) {
                    		console.log('Неверно заполнен вес');
                    		weight = 0;
                    	}
            		} else if(label == 'born') {
                        born = $(this).val();
                        var now = new Date;
                        born = born.split('.');
                        if(born[0] > 0 && born[0] < 32 && born[1] > 0 && born[1] < 13 && born[2].length == 4) {
	                        age = now.getFullYear() - born[2];
	                        if((born[2]-1)+'.'+born['0'] > now.getMonth()+'.'+now.getDate()) age--;
	                    	if(age < 7 || age > 70) {
	                    		console.log('Неверно указан возраст');
	                    		age = 0;
	                    	}
                    	} else {
                    		console.log('Неверно заполнена дата рождения');
                    	}
            		} else if(label == 'activity') {
                    	activity = parseInt($(this).val());
                    	if(activity < 1 || activity > 5) {
                    		console.log('Неверно заполнена активность');
                    		activity = 1;
                    	}
            		} else if(label == 'gender') {
            			if($(this).attr('type') == 'radio') {            				gender = ($(this).val() == 2)?1:0;            			} else {
            				gender = this.checked;
            			}
            		} else if(label == 'mother') {
            			if($(this).attr('type') == 'radio') {
            				mother = $(this).val();
            			} else {
            				mother = this.checked;
            			}
            		} else if(label == 'goal') {
            			goal = $(this).val();
            		}

					var kal = Math.round(getKal(weight, growth, age, activity, gender, mother));
            		$('.daykal span').html((kal > 0)?kal:'Неверно заполнена анкета');
            		$('.daykal input').val(kal);

            	});
     		});
     		function getKal(weight, growth, age, activity, gender, mother) {
     			if(!weight || !growth || !age || !activity) {
     				return 0;
     			}
     			if(gender == 0) {
     				var kal = 66.5 + 13.7 * weight + 5 * growth - 6.8 * age;
     			} else {
     				var kal = 655 + 9.6 * weight + 1.8 * growth - 4.7 * age;
     			}
     			var koeff = 0;
     			if(activity == 1) {
     				koeff = 1.2;
     			} else if(activity == 2) {
     				koeff = 1.3;
     			} else if(activity == 3) {
     				koeff = 1.4;
     			} else if(activity == 4) {
     				koeff = 1.5;
     			} else if(activity == 5) {
     				koeff = 1.7;
     			}
     			kal *= koeff;
     			if(goal == '0') {
     				kal *= 0.8;
     			} else if(goal == '1') {
     				kal *= 1.2;
     			}
     			if(mother == 1) kal += 300;
     			return kal;
     		}
     	</script>
    <?php elseif($order->active == 2): ?>
    	<?php $this->title = 'Продление курса «'.$course_group->title.'»'; ?>
		<div class="modal" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel" style="display:block; position:static; overflow:visible; margin-bottom:70px">
		    <div class="modal-dialog order-modal-size" role="document" style="box-shadow:none; margin:0; width:100%">
		        <div class="modal-content" style="-webkit-box-shadow:none; box-shadow:none;">
		            <div class="modal-body">
		                <div class="modal-body-course">
		<?php
	    $sell_items_model = $course_group->getSellItems();
	    $sell_items_model->where(['type'=>$order->type]);
	    $sell_item = $sell_items_model->one();
	    $discounts = \common\models\Discount::find()->all();
	    if(sizeof($discounts) > 0):
	    ?>
	    <?php foreach($discounts as $discount): $q = (int)str_replace(' month', '', $discount->period); $amount = ($discount->discount > 0)?($sell_item->amount - $sell_item->amount * $discount->discount / 100) * $q:$sell_item->amount; ?>
	        <div class="course-order-item">
	            <div class="course-order-item-amount"><?php if($discount->discount > 0): ?><div>-<?=$discount->discount ?>%</div><?php endif; ?><span class="amount"><?=str_replace('.00', '', $amount) ?></span> Р</div>
	            <div class="course-order-item-title">
	                <?=$sell_item->title ?><br>
	                <?php /*if(isset($trigger['length'])): */?><!--<br><span><?/*=$trigger['length'] */?></span>--><?php /*endif; */?>
	                <span><?=$discount->title ?></span>
	            </div>
	            <div class="course-order-item-buy"><a href="/course/extend/<?=$sell_item->id ?>/<?=$order->id ?>?discount=<?=$discount->id ?>&promo=" onclick="$(this).attr('href', ($(this).attr('href') + $('#promocode_field').val()));">Купить</a></div>
	        </div>
		<?php endforeach; ?>
	    <div class="order-footer">
	        <lable><input type="checkbox" id="igree" checked> я согласен с <a href="/page/offer" target="_blank">правилами проекта</a><input id="promocode_field" data-ro="0" type="text" placeholder="Промокод" style="float:right;border-radius:7px;border:1px solid #ffba00; padding:10px 5px;"><div id="promo_message" style="float:right;clear:both"></div></lable>
	    </div>
	    <?php endif; ?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		<?php
		$script = "
		$('.main-container.course.view .page-content').css({padding:'0 40px', background:'none', overflow:'visible'});
		$(document).ready(function(){

		    $(document).on('click', '.course-order-item-buy a', function(){
		        var href = $(this).attr('href');
		        $('#orderModal .modal-body-course').html('<div class=\"preloader\">Loading...</div>');
		        $('#orderModal .modal-body-course').load(href);
		        return false;
		    });

		    $(document).on('click', '.course-order-item-buy a', function(){
		        if($('#igree').is(':checked'))
		            return true;
		        else
		            return false;
		    });

		});

		";

		$this->registerJs($script, yii\web\View::POS_READY);
		?>
    <?php elseif($order->active == 3): ?>
		<?php
		$script = "
		window.location.href='".Url::toRoute(['/course/extend', 'order_id'=>$order->id])."';

		";

		$this->registerJs($script, yii\web\View::POS_READY);
		?>
	<?php endif; ?>

	<?php
	$transactions = $order->getTransactions()->where(['status'=>\common\models\Finance_transaction::Status_progress])->andWhere('amount < 0')->orderBy('created_at')->all();
	if(sizeof($transactions) > 0):
    ?>
		<h4>У вас есть неоплаченные счета:</h4>
        <table class="table">
        <?php foreach($transactions as $transaction): ?>
            <tr>
                <td>
                    <strong><?=substr($transaction->comment, 0, strpos($transaction->comment, ',')) ?></strong><br>
                    <small>Дата: <?=date('d.m.Y', $transaction->created_at) ?></small>
                </td>
                <td><?=$transaction->amount*-1 ?> руб.</td>
                <td>
                    <a class="go-to-payment" href="/course/payto/<?=$course_group->id ?>/<?=$transaction->id ?>">Оплатить</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
    <?php endif; ?>

	<?php else: ?>

    <h4 class="course-order-title">Оплата курса «<?=$course_group->title ?>»</h4>
	<?php
    $sell_items_model = $course_group->getSellItems();
    if(isset($_GET['type']))
    	$sell_items_model->where(['type'=>$_GET['type']]);
    $sell_item = $sell_items_model->one();
    $discounts = \common\models\Discount::find()->all();
    if(sizeof($discounts) > 0):
    ?>
    <?php foreach($discounts as $discount): $q = (int)str_replace(' month', '', $discount->period); $amount = ($discount->discount > 0)?($sell_item->amount - $sell_item->amount * $discount->discount / 100) * $q:$sell_item->amount; ?>
        <div class="course-order-item">
            <div class="course-order-item-amount"><?php if($discount->discount > 0): ?><div>-<?=$discount->discount ?>%</div><?php endif; ?><span class="amount"><?=str_replace('.00', '', $amount) ?></span> Р</div>
            <div class="course-order-item-title">
                <?=$sell_item->title ?><br>
                <?php /*if(isset($trigger['length'])): */?><!--<br><span><?/*=$trigger['length'] */?></span>--><?php /*endif; */?>
                <span><?=$discount->title ?></span>
            </div>
            <div class="course-order-item-buy"><a href="/course/buy/<?=$sell_item->id ?>?discount=<?=$discount->id ?>&promo=" onclick="$(this).attr('href', ($(this).attr('href') + $('#promocode_field').val()));">Купить</a></div>
        </div>
	<?php endforeach; ?>
    <div class="order-footer">
        <lable><input type="checkbox" id="igree" checked> я согласен с <a href="/page/offer" target="_blank">правилами проекта</a><input id="promocode_field" data-ro="0" type="text" placeholder="Промокод" style="float:right;border-radius:7px;border:1px solid #ffba00; padding:10px 5px;"><div id="promo_message" style="float:right;clear:both"></div></lable>
    </div>
    <?php endif; ?>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
	$('#promocode_field').keyup(function(){		console.log($(this).val());
		if($(this).data('ro') == '0' && $(this).val().toLowerCase() == 'workoutmasha') {			$(this).data('ro', 1);
			$(this).attr('readonly', 'readonly');
			$('#promo_message').html('Промокод активирован. Скидка 10%');        	$('span.amount').each(function(){        		var amount = Math.round(parseInt($(this).html())*0.9);
        		$(this).html(amount);        	});		}
		if($(this).data('ro') == '0' && $(this).val().toLowerCase() == 'masha20%') {
			$(this).data('ro', 1);
			$(this).attr('readonly', 'readonly');
			$('#promo_message').html('Промокод активирован. Скидка 20%');
        	$('span.amount').each(function(){
        		var amount = Math.round(parseInt($(this).html())*0.8);
        		$(this).html(amount);
        	});
		}
		if($(this).data('ro') == '0' && $(this).val().toLowerCase() == 'manechka') {
			$(this).data('ro', 1);
			$(this).attr('readonly', 'readonly');
			$('#promo_message').html('Промокод активирован. Скидка 15%');
        	$('span.amount').each(function(){
        		var amount = Math.round(parseInt($(this).html())*0.85);
        		$(this).html(amount);
        	});
		}
		if($(this).data('ro') == '0' && $(this).val().toLowerCase() == 'mashavip') {
			$(this).data('ro', 1);
			$(this).attr('readonly', 'readonly');
			$('#promo_message').html('Промокод активирован. Скидка 30%');
        	$('span.amount').each(function(){
        		var amount = Math.round(parseInt($(this).html())*0.7);
        		$(this).html(amount);
        	});
		}
		if($(this).data('ro') == '0' && $(this).val().toLowerCase() == 'newlife') {
			$(this).data('ro', 1);
			$(this).attr('readonly', 'readonly');
			$('#promo_message').html('Промокод активирован. Скидка 30%');
        	$('span.amount').each(function(){
        		var amount = Math.round(parseInt($(this).html())*0.7);
        		$(this).html(amount);
        	});
		}
		if($(this).data('ro') == '0' && $(this).val() == 'Masha50%') {
			$(this).data('ro', 1);
			$(this).attr('readonly', 'readonly');
			$('#promo_message').html('Промокод активирован. Скидка 50%');
        	$('span.amount').each(function(){
        		var amount = Math.round(parseInt($(this).html())*0.5);
        		$(this).html(amount);
        	});
		}
		if($(this).data('ro') == '0' && $(this).val() == 'жизньпрекрасна') {
			$(this).data('ro', 1);
			$(this).attr('readonly', 'readonly');
			$('#promo_message').html('Промокод активирован. Скидка 30%');
        	$('span.amount').each(function(){
        		var amount = Math.round(parseInt($(this).html())*0.7);
        		$(this).html(amount);
        	});
		}	});
});
</script>