<?php

/* @var $this yii\web\View */

$this->title = $workout->title;

?>
<div class="site-index">
<?php if ($schedule):
        $sleep_arr = array();
        $set_arr = array();
        $report_arr = array();
        $js_sleep_arr = '[';
        $js_set_arr = '[';
        $js_ex_arr = '[';
        $excntr = 0;
        $routscnt = 0;
        $timer = 0;
?>
    <section class="training_title row">
        Тренировка №<?=$workout->number ?>: <?=$workout->title ?>
    </section>
    <div class="training_content row">
	<?php foreach($schedule_exercises as $schedule_exercise):
		if($schedule_exercise->type == \common\models\Schedule_exercise::Exercise):
			$exercise = $schedule_exercise->getExercise();
			$exid = ($exercise->id_tab == 2)?$exercise->id_related:$exercise->id;
	?>
    <?php
    	//$exercise = $exercises->where(['number'=>$number])->one();
    	$excntr++;
    	$ex_scheme = '';
        $tr_routs = $schedule_exercise->getScheduleRoutines('set')->all();
        $js_ex_arr .= '{id:'.$exercise->id.', title:"'.str_replace('"', '&quot;', $exercise->title).'", tr:[';
        $report_arr[$schedule_exercise->number] = '';
    if($tr_routs) {
        $timer = 0;
        $set_counter = 0;
        $set_str = '';
        $sleep_arr[$excntr] = array();
        $js_sleep_arr .= '[';
        $set_arr[$excntr] = array();

        //    if($excntr > 1) echo '<section class="ex_sepor row"></section>';
    ?>
    <section class="training_body row" id="excntr<?=$excntr ?>" <?=($excntr > 1)?'style="height:100px"':'' ?>>

        <?/*<p>
            <strong>Упражнение <?=$excntr ?>:</strong> <?php echo $exercise->title;?>.
        </p>*/?>
	    <?php foreach($tr_routs as $rout){
    		$routscnt++;
	        $timer += $rout->sleep;
	        $set_counter ++;
	        $sleep_arr[$excntr][] = $rout->sleep;
	        $js_sleep_arr .= $rout->sleep.',';
	        $js_ex_arr .= '{sleep:'.$rout->sleep.', alert_proc:"'.$rout->alert_proc.'", alert_sleep:"'.$rout->alert_sleep.'", sets:';
	        $set_str .= $rout->set.' подход:';
	        $sets = $rout->getScheduleSets();

	        $report_arr[$schedule_exercise->number] .= '<div>'.$rout->set.' подход:';
	        $defaults = array();

	        if($sets) {
	            foreach($sets as $set) {
	            	if(!is_numeric($reports['set'.$set->id]->default_val)){	            		$_def = str_replace(',', '.', trim(str_replace(@$userGlobalKeys[$exid], @$userGlobalVals[$exid], $reports['set'.$set->id]->default_val)));
	            		$defaults['set'.$set->id] = 0;
	            		if($_def == '' or in_array(substr($_def, 0), array('+', '-', '*', '/'))) {} else {
		            		//echo('$defaults["set'.$set->id.'"] = '.$_def.';');
	                    	@eval('$defaults["set'.$set->id.'"] = '.$_def.';');
	            	    }
	            	} else {	            		$defaults['set'.$set->id] = $reports['set'.$set->id]->default_val;	            	}
	                $set_attr = $set->getSet_attribute();
	                $set_str .= ' ' . $set_attr->name.' '.$defaults['set'.$set->id].($set_attr->unit?' '.$set_attr->unit:'');
	                $report_arr[$schedule_exercise->number] .= ' '.$set_attr->name.' '.'<span><input type="text" value="'.$defaults['set'.$set->id].'" name="report[set'.$set->id.']"></span>'. ($set_attr->unit?' '.$set_attr->unit:'');
	            }
	            $set_arr[$excntr][] = $set_str;
	            $js_set_arr .= '"'.$set_str.'",';
	        }
	        $js_ex_arr .= '"'.$set_str.'"},';
	        $ex_scheme .= '<input type="checkbox" onclick="return false" id="training_checkbox_'.$schedule_exercise->number.'_'.$rout->set.'">&nbsp;'.$set_str.'<br />';
	        $set_str = '';
	        $report_arr[$schedule_exercise->number] .= '</div>';
    	}
	    $js_sleep_arr = substr($js_sleep_arr, 0, strlen($js_sleep_arr)-1).'],';
	   	$js_set_arr = substr($js_set_arr, 0, strlen($js_set_arr)-1).'],';
    ?>
    <?php }
    ?>
        <div class="exercise-video"><?=str_replace('controls="controls"', 'controls="controls" preload="metadata"', $exercise->video) ?></div>
	   	<div class="exercise-scheme">
	   		<strong>Схема выполнения:</strong><br>
	   		<?=$ex_scheme ?>
	   	</div>
        <div><strong>Описание</strong><br><?=$exercise->description ?></div>
    	<?php
    		$exFields = $exercise->getFields()->all();
    		foreach($exFields as $exField): ?>
   		<div>
   			<strong><?=$exField->title ?></strong>
	   		<?=$exField->value ?>
	   	</div>
    		<?php endforeach; ?>
	   	</section>
    <?php
		    	$js_ex_arr = substr($js_ex_arr, 0, strlen($js_ex_arr)-1).']},';
		    /* конец упражнения, если упражнение */
		    elseif($schedule_exercise->type == \common\models\Schedule_exercise::Set):

			$_set = $schedule_exercise->getSet();
	?>
    <?php
    	//$exercise = $exercises->where(['number'=>$number])->one();
    	$excntr++;

?>
    <section class="training_body row" id="excntr<?=$excntr ?>" <?=($excntr > 1)?'style="height:100px"':'' ?>>
<?

    	$exercises = $_set->getExercises('number')->all();
        $js_ex_arr .= '{id:'.$_set->id.', title:"'.str_replace('"', '&quot;', $_set->title).'", tr:[{sleep:'.$_set->sleep.', alert_proc:"'.$_set->alert_proc.'", alert_sleep:"'.$_set->alert_sleep.'", sets:';

		$report_arr[$schedule_exercise->number] = '';
		$set_str_arr = array();
        foreach($exercises as $exercise) {
        	$exe = $exercise->getExercise();
        	$exid = ($exe->id_tab == 2)?$exe->id_related:$exe->id;
        	?>
        <div class=""><?=$exe->title ?></div>
		<div class="exercise-video"><?=str_replace('controls="controls"', 'controls="controls" preload="metadata"', $exe->video) ?></div>
		<div><strong>Описание</strong><?=$exe->description ?></div>
    	<?php
    		$exFields = $exe->getFields()->all();
    		foreach($exFields as $exField): ?>
   		<div>
   			<strong><?=$exField->title ?></strong>
	   		<?=$exField->value ?>
	   	</div>
    		<?php endforeach; ?>
        	<?
        	$schedule_routine = $exercise->getScheduleRoutines('set')->one();
        	if($schedule_routine) {
        		$sets = $schedule_routine->getScheduleSets();
        		$set_str = $exercise->number.'. '.$exe->title;
        		$report_arr[$schedule_exercise->number] .= '<div>'. $exercise->number.'. '.$exe->title;
		        if($sets) {
		            foreach($sets as $set) {
		            	if(!is_numeric($reports['set'.$set->id]->default_val)){
		            		$_def = str_replace(',', '.', trim(str_replace(@$userGlobalKeys[$exid], @$userGlobalVals[$exid], $reports['set'.$set->id]->default_val)));
		            		$defaults['set'.$set->id] = 0;
		            		if($_def == '' or in_array(substr($_def, 0), array('+', '-', '*', '/'))) {} else {
			            		//echo('$defaults["set'.$set->id.'"] = '.$_def.';');
		                    	@eval('$defaults["set'.$set->id.'"] = '.$_def.';');
		            	    }
		            	} else {
		            		$defaults['set'.$set->id] = $reports['set'.$set->id]->default_val;
		            	}
		                $set_attr = $set->getSet_attribute();
		                $set_str .= ' ' . $set_attr->name.' '.$defaults['set'.$set->id].($set_attr->unit?' '.$set_attr->unit:'');
		                $report_arr[$schedule_exercise->number] .= ' '.$set_attr->name.' '.'<span><input type="text" value="'.$defaults['set'.$set->id].'" name="report[set'.$set->id.']"></span>'. ($set_attr->unit?' '.$set_attr->unit:'');
		            }
		            array_push($set_str_arr, $set_str);
		        }
		        $report_arr[$schedule_exercise->number] .= '</div>';
	        }
        }
        $ex_scheme = /*'<input type="checkbox" id="training_checkbox_'.$schedule_exercise->number.'_'.$rout->set.'" style="display:none">'.*/implode('<br />', $set_str_arr);
        $set_str_arr = array($_set->title);
        $js_ex_arr .= '"'.implode('<br />', $set_str_arr).'"}]},';
?>
	   	<div class="exercise-scheme">
	   		<strong>Схема выполнения:</strong><br>
	   		<?=$ex_scheme ?>
	   	</div>
	</section>
<?

		    /* конец сета, ели сет */
	    	endif;

    	endforeach;
        $js_sleep_arr = substr($js_sleep_arr, 0, strlen($js_sleep_arr)-1).']';
    	$js_set_arr = substr($js_set_arr, 0, strlen($js_set_arr)-1).']';
        $js_ex_arr = substr($js_ex_arr, 0, strlen($js_ex_arr)-1).']';
	    //echo $js_sleep_arr;
	    //echo $js_set_arr;
    ?>
    </div>
    <div class="bottom">
	    <section class="progress row">
	        <div class="progress_bar">
	            <?/*php $progress_width = 100/$routscnt;*/?>
	            <div class="progress" style="width: <?/*=$progress_width*/?>0%"></div>
	        </div>
	        <div class="row table">
	            <div class="description">
	                <span style="display:none"><span id="sps_do_now">Сейчас делаете </span><span id="sp_do_now"></span>&nbsp;</span>
	                <span id="rest">Отдыхаем...</span>
	                <span id="comment" style="display:block;line-height:16px;padding-bottom:5px"></span>
	            </div>
	            <div class="counter" id="timer" style="display:none">
	                 00:<span class="min">00</span>
	            </div>
	        </div>
	    </section>
	    <div class="row">
	        <form id="frm_do">
			    <div style="" class="arrow-left"></div>
	            <div class="next-btn-grey"><div style="position:absolute;width:240px;z-index:1;cursor:pointer" id="do_exe" counter="0" exercise="1" set="1">Начать</div><a href="#next" class="next_btn active"></a></div>
	            <div style="<?=$excntr<2?'visibility:hidden':'' ?>" class="arrow-right"></div>
	            <div id="ch"></div>
	        </form>
    	</div>
    </div>
    <div id="mfrm_real" class="modalDialog">
        <div>
            <form id="frm_real" method="post">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close">×</button>
                <h4 class="modal-title">Отчет об упражнении</h4>
              </div>
              <div class="modal-body">
              	<div id="report-fields"></div>
                <input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>" />
                </div>
                <div class="modal-footer">
                    <?/*<button type="button" class="close">Закрыть</button>*/?>
                    <button type="submit" class="btn btn-primary">Готово!</button>
                </div>
            </div>
            </form>
        </div>
    </div>
	<?php
	$schedule_report_fields = $schedule->getWorkoutReport();
	if(sizeof($schedule_report_fields) > 0):
	?>
    <div id="mfrm_report" class="modalDialog">
        <div>
            <form id="frm_report" method="post">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close">×</button>
                <h4 class="modal-title">Отчет по тренировке</h4>
              </div>
              <div class="modal-body">
                <?php foreach($schedule_report_fields as $schedule_report_field):
                	$workout_report_field = $schedule_report_field->getField();
	                switch($workout_report_field->type) {
	                	case \common\models\Workout_report_field::Text:
	                	    echo '<div>'.$workout_report_field->title.'&nbsp;<input type="text" name="fields['.$schedule_report_field->id.']"></div>';
	                		break;
	                	case \common\models\Workout_report_field::Textarea:
		                	echo '<div>'.$workout_report_field->title.'&nbsp;<textarea name="fields['.$schedule_report_field->id.']"></textarea></div>';
	                		break;
	                	case \common\models\Workout_report_field::Checkbox:
	                		echo '<div>'.$workout_report_field->title.'&nbsp;<input type="hidden" name="fields['.$schedule_report_field->id.']" value="Нет"><input type="checkbox" name="fields['.$schedule_report_field->id.']" value="Да"></div>';
	                		break;
	                }
	            ?>
                <?php endforeach; ?>
                <input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>" />
                </div>
                <div class="modal-footer">
                    <?/*<button type="button" class="close">Закрыть</button>*/?>
                    <button type="submit" class="btn btn-primary">Готово!</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <?php else: ?>
    <form id="frm_report" method="post" style="display:none">
    	<input type="hidden" name="fields" value="1"><input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>" />
    </form>
    <?php endif; ?>
<?php endif;?>
</div>
<audio id="spoons" preload="auto">
        <source src="/audio/spoons.mp3"></source>
        <source src="/audio/spoons.ogg"></source>
</audio>
<?php foreach($report_arr as $number=>$report): ?>
<div id="exercise_report_<?=$number ?>" style="display:none"><?=$report ?></div>
<?php endforeach; ?>

<?php if(!is_null($exercise)):?>
<script type="text/javascript">
if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}
String.prototype.toHHMMSS = function (hours) {
	hours = (hours)?hours:false;
    var sec_num = parseInt(this, 10); // don't forget the second param
    if(hours !== false)
    	var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours !== false && hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return (hours!==false?hours+':':'')+minutes+':'+seconds;
}

$(document).ready(function(){
    //$('#timer').html('<?=$timer?>');
    var ex_arr = <?=str_replace(array(chr(10), chr(13)), '', $js_ex_arr) ?>; console.log(ex_arr);
    //$('#timer').html(ex_arr[0].tr[0].sleep.toString().toHHMMSS());
    //$('#timer').html('00:00');
    $('#timer').hide();
    var workout_timer = 0, tmp_workout_time = 0;
    $('header h1').html(ex_arr[0].title);
    <?/*var sleep_arr = [<?=implode(',', $sleep_arr) ?>];
    var set_arr = ["<?=implode('","', $set_arr)?>"];*/?>
    var counter = parseInt($('#do_exe').attr('counter'));
    var from = ex_arr[0].tr[0].sleep;
    var audioif = 6;
    var to = 1;//from - sleep_arr[counter-1];

    var current_exercise = 1, focused_exercise = 1, width = 0;
    var progress_width = 100/ex_arr[current_exercise-1].tr.length;
    var routscnt = ex_arr[current_exercise-1].tr.length;
    $('#report-fields').html($('#exercise_report_'+current_exercise).html());
    $('#sp_do_now').html('Приступаете к выполнению упражнения');
    $('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});

    $('#do_exe').attr('disabled', false);

    $(window).resize(function(){
    	var baseWidth = $('.site-index').width();
    	console.log(baseWidth);
    	$('.training_content').css({width:(baseWidth*<?=$excntr ?>)});
    	$('.training_body').each(function(){
    		$(this).css({width: baseWidth});
    	});
    	//если мобилка mobile
    	<?php /*if($is_mobile): ?>
    	if($(window).outerWidth() > $(window).outerHeight()) {
            $('header').hide();
            $('#excntr'+focused_exercise+' .exercise-video, .bottom').addClass('horisont');
    	} else {
    		$('header').show();
    		$('#excntr'+focused_exercise+' .exercise-video, .bottom').removeClass('horisont');
    	}
    	<?php endif; */ ?>
    });

    $(window).trigger('resize');
	$('video').each(function(){
		var $_this = $(this);
		if($_this.data('sound') != 'yes')
			$_this.get(0).muted = true;
		$_this.get(0).loop = true;
		$_this.attr('playsinline','');
		$_this.attr('webkit-playsinline','');
		$_this.click(function(){
			var $_this = $(this).get(0);			if($_this.paused === true){				$_this.play();
			} else {				$_this.pause();
			}		});
	});
	$('#excntr1 .exercise-video video').get(0).play();

    var arrow_block = false;

    $('.arrow-right').click(function(){
    	if(arrow_block === false) {
    		arrow_block = true;
	    	var listedMargin = parseInt($('.training_content').css('margin-left').replace('px', ''));
	    	listedMargin -= parseInt($('#excntr'+focused_exercise).css('width').replace('px', ''));
	    	var oldEx = focused_exercise;
	    	focused_exercise++;
	    	$('#excntr'+focused_exercise+' .exercise-video video').get(0).play();
	    	$('#excntr'+oldEx+' .exercise-video video').each(function(){$(this).get(0).pause()});
	    	$('header h1').html(ex_arr[focused_exercise-1].title);
	    	$('#excntr'+focused_exercise).css({height:'auto'});
	    	$('.training_content').animate({
	    		'margin-left': listedMargin
	    	}, 300, function(){
	    		$('#excntr'+oldEx).css({height: '100px'});
	    		arrow_block = false;
	    	});
	    	$('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});
	    	if(parseInt(focused_exercise) >= parseInt(<?=$excntr ?>))$(this).css({visibility:'hidden'});
	    	$('.arrow-left').css({visibility:'visible'});
	  	}
    });

    $('.arrow-left').click(function(){
    	if(arrow_block === false) {    		arrow_block = true;
	    	var listedMargin = parseInt($('.training_content').css('margin-left').replace('px', ''));
	    	listedMargin += parseInt($('#excntr'+focused_exercise).css('width').replace('px', ''));
	    	var oldEx = focused_exercise;
	    	focused_exercise--;
	    	$('#excntr'+focused_exercise+' .exercise-video video').get(0).play();
	    	$('#excntr'+oldEx+' .exercise-video video').each(function(){$(this).get(0).pause()});
	    	$('header h1').html(ex_arr[focused_exercise-1].title);
	    	$('#excntr'+focused_exercise).css({height:'auto'});
	    	$('.training_content').animate({
	    		'margin-left': listedMargin
	    	}, 300, function(){
	    		$('#excntr'+oldEx).css({height: '100px'});
	    		arrow_block = false;
	    	});
	    	$('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});
	    	if(focused_exercise <= 1)$(this).css({visibility:'hidden'});
	    	$('.arrow-right').css({visibility:'visible'});
	    }
    });

    $('#do_exe').click(function(){
        if(!$('#do_exe').attr('disabled')) {
        	$('#comment, #sp_do_now').html('');
        	if($('#do_exe').attr('counter') == -1) return false;
            if(focused_exercise != current_exercise) {
		    	var listedMargin = parseInt($('#excntr'+focused_exercise).css('width').replace('px', '')) * -(current_exercise - 1);
		    	//$('#excntr'+focused_exercise+' .exercise-video video').get(0).pause();
		    	var oldEx = focused_exercise;
		    	focused_exercise = current_exercise;
		    	$('header h1').html(ex_arr[focused_exercise-1].title);
		    	$('#excntr'+focused_exercise).css({height:'auto'});
		    	$('.training_content').animate({
		    		'margin-left': listedMargin
		    	}, 300, function(){
		    		$('#excntr'+oldEx).css({height: '100px'});
		    	});
		    	if(parseInt(focused_exercise) >= parseInt(<?=$excntr ?>))$('.arrow-right').css({visibility:'hidden'});else $('.arrow-right').css({visibility:'visible'});
		    	if(parseInt(focused_exercise) <= 1)$('.arrow-left').css({visibility:'hidden'}); else $('.arrow-left').css({visibility:'visible'});
            }
        	if($('#do_exe').attr('counter') == 0) {
        		console.log('Начали выполнение');
        		tmp_workout_time = Math.floor(Date.now() / 1000);
        		$('#sp_do_now').html(ex_arr[current_exercise-1].tr[0].sets).parent().show();
        		$('#comment').html(ex_arr[current_exercise-1].tr[0].alert_proc);
        		$('#timer').html(ex_arr[current_exercise-1].tr[0].sleep.toString().toHHMMSS()); // здесь таймер на подход
        		$('#do_exe').html('Сделал 1 подход');
        	    $('#do_exe').attr('counter', 1);
        	    $('#rest').html('');
        	    $('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});

        		return false;
        	}
        	$('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});

            if($('#do_exe').attr('counter') < routscnt) {
            	console.log('Начали отдых');
            	workout_timer += Math.floor(Date.now() / 1000) - tmp_workout_time;
            	tmp_workout_time = 0;
	            $('#do_exe').attr('disabled', true);
	            $('.next_btn').removeClass('active');
	            $('.next_btn').css({width:'27px', 'border-bottom-right-radius':0, 'border-top-right-radius':0});
	            $('.next_btn').animate({width:'240px', 'border-bottom-right-radius':'27px', 'border-top-right-radius':'27px'}, from*1000, function(){$('.next_btn').addClass('active')});

            	$('#timer').show().html(ex_arr[current_exercise-1].tr[parseInt($('#do_exe').attr('counter')-1)].sleep.toString().toHHMMSS()); // Здесь таймер на отдых
            	$('#comment').html(ex_arr[current_exercise-1].tr[parseInt($('#do_exe').attr('counter')-1)].alert_sleep);
            	$('#training_checkbox_'+current_exercise+'_'+$('#do_exe').attr('counter')).attr('checked', true);
                $('#do_exe').attr('counter', parseInt($('#do_exe').attr('counter'))+1);
                if($('#do_exe').attr('counter') > routscnt) {
                    //$('#do_exe').attr('counter', '-1');
                } else {
                    $('#do_exe').val('Отдыхаю');
                }
                if (width < 100){
                    width = width + progress_width;
                }
                $('.progress_bar .progress').css('width', width+'%');
                /*$('#ch').append(
                    $(document.createElement('input')).attr({
                        id:     'ch_do_exe_' + parseInt($('#do_exe').attr('counter')-1)
                        ,type:  'checkbox'
                        ,set:   parseInt($('#do_exe').attr('counter')-1)
                        ,checked: true
                    }).hide()
                );

                $('#ch').append(
                    $(document.createElement('label')).attr({
                        for:    'ch_do_exe_' + parseInt($('#do_exe').attr('counter')-1)
                    }).html('&nbsp;&nbsp;' + parseInt($('#do_exe').attr('counter')-1) + '&nbsp;подход&nbsp;&nbsp;').hide()
                 );*/

                $('#ch input').attr('disabled', true);

				$('#sp_do_now').parent().hide();
				$('#rest').show();
				$('#do_exe').html('Пропустить отдых');
				$('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});
                run_timer();
            } else {
               	console.log(routscnt+' '+($('#do_exe').attr('counter')-1));
               	//current--;
               	//$('#timer').html(current.toString().toHHMMSS());
               	$('#mfrm_real').show();
               	$('#do_exe').attr('disabled', true).attr('counter', '-1').html('Заполнение отчёта');
                $('#rest').html('Приступаем к следующему упражнению').show();
                $('#comment').html('');
            }
        } else {
        	current = 1;
    		$('.next_btn').stop(true, true);
    		var audio = $("#spoons")[0];
    		audio.pause();
    		audio.currentTime = 0;
        }
    });

    $('.progress_bar .progress').css('width', width+'%');

    /*$('#ch').delegate('input', 'change', function(){
        from = 0;
        var st = sleep_arr.length-1, fn = parseInt($(this).attr('set'))-1;
        for(var i=st; i>=fn; i--) {
            from += parseInt(sleep_arr[i]);
        }
        to = from - sleep_arr[parseInt($(this).attr('set'))-1];
        from = parseInt(sleep_arr[i]);
        to = 0;
        $('#timer').html(from.toHHMMSS());
        $('#do_exe').val('Сделал ' + $(this).attr('set') + ' подход!').show().attr('counter', $(this).attr('set'));

        $('#ch label:gt('+(parseInt($(this).attr('set'))-1)+'), #ch label:eq('+(parseInt($(this).attr('set'))-1)+')').remove();
        $('#ch input:gt('+(parseInt($(this).attr('set'))-1)+'), #ch input:eq('+(parseInt($(this).attr('set'))-1)+')').remove();
    });*/

    $('#frm_real').submit(function () {
        var data = $('#frm_real').serializeArray();
        $.ajax({
            beforeSend: function (){
                $('#frm_real .error').remove();
                //showLoader();
            },
            type: 'POST',
            url: '/workout/report/<?=$schedule->id ?>',
            dataType: 'json',
            data: data,
            success: function(data){
                if(data.status == 'ok') {
                    if(data.status == 'error') {
                         alert("Ошибка!\n"+data.errors.join("\n"));
                    } else {
                    		if(current_exercise < parseInt(<?=$excntr ?>)) {
	                    	    current_exercise++;
							    progress_width = 100/ex_arr[current_exercise-1].tr.length;
							    routscnt = ex_arr[current_exercise-1].tr.length;
							    $('#report-fields').html($('#exercise_report_'+current_exercise).html());
							    $('#sp_do_now').html(ex_arr[current_exercise-1].tr[0].sets);
							    //$('#comment').html(ex_arr[current_exercise-1].tr[0].alert);

								counter = 1; width = 0;
							    from = ex_arr[current_exercise - 1].tr[0].sleep;
							    $('#do_exe').attr('counter', 0).attr('exercise', current_exercise).attr('set', 1).attr('disabled', false).html('Начать');
							    $('.progress .progress_bar .progress').css('width', 0);

						    	var listedMargin = parseInt($('#excntr'+focused_exercise).css('width').replace('px', '')) * -(current_exercise - 1);
						    	var oldEx = focused_exercise;
				    	    	$('#excntr'+current_exercise+' .exercise-video video').get(0).play();
						    	$('#excntr'+oldEx+' .exercise-video video').each(function(){$(this).get(0).pause()});
						    	focused_exercise = current_exercise;
						    	$('header h1').html(ex_arr[focused_exercise-1].title);
						    	$('#excntr'+focused_exercise).css({height:'auto'});
						    	$('.training_content').animate({
						    		'margin-left': listedMargin
						    	}, 300, function(){
						    		$('#excntr'+oldEx).css({height: '100px'});
						    	});
						    	$('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});
						    	if(parseInt(focused_exercise) >= parseInt(<?=$excntr ?>))$('.arrow-right').css({visibility:'hidden'});else $('.arrow-right').css({visibility:'visible'});
						    	$('.arrow-left').css({visibility:'visible'});

						    } else {
						    	//если последнее упражнение в трене
						    	<?php if($schedule_report_fields): ?>
                                $('#mfrm_report').show();
                                <?php else: ?>
                                $('#frm_report').submit();
                                <?php endif; ?>
						    }

					    	$('#mfrm_real').hide();
					    	$('#rest').html('');


                    }
                } else {
                    if(data.errors) {
                        if(data.errors.common) {
                            alert(data.errors.common);
                        }
                        if(data.errors.sets) {
                            for(var i in data.errors.sets) {
                                $('#frm_real input[name="report['+i+']"]').parent().prepend('<div class="error" style="color:#f00;position:absolute;display:inline-block;margin-top:20px;padding:3px;background:#fff;border: 1px solid #f00">'+data.errors.sets[i]+'</div>');
                            }
                        }
                    }
                }
                //hideLoader();
            },
            error: function(){
                /*hideLoader();*/
                alert('Ошибка: Не удалось отправить отчет.');
            }
        });
        return false;
    });

    $('#frm_real .close').on('click', function(){
        $('#mfrm_real').hide();
        return false;
    });

    function run_timer(){
        current = from;
        stopdot = to;
        //from = to;
        //to = from - 1;
        timer = setInterval(function () {
            if(current > stopdot) {
                current--;
                $('#timer').html(current.toString().toHHMMSS());
                if(current == audioif) {
	                var audio = $("#spoons")[0];
					audio.play();
				}
            } else {
                if(current == to && $('#do_exe').attr('counter')-1 == routscnt){
                	console.log(routscnt+' '+($('#do_exe').attr('counter')-1));
                	current--;
                	$('#timer').html(current.toString().toHHMMSS());
                	$('#mfrm_real').show();
                	$('#do_exe').attr('disabled', true).attr('counter', '-1');
                    $('#rest').html('Приступаем к следующему упражнению');
                    $('#comment').html('');
                    /*$('#ch').append(
                        $(document.createElement('input')).attr({
                            id:     'btn_next_exe'
                            ,type:  'button'
                            ,value:  'Следующее'
                            ,class: 'btn btn-primary'
                        })
                    );*/
                    //$('#btn_next_exe').on('click', function(){

                    //});

                } else {
		            var d_exercise = parseInt($('#do_exe').attr('exercise')), d_set = parseInt($('#do_exe').attr('set'));
		            if(d_set >= ex_arr[d_exercise-1].tr.length) {
		            	d_set = 0;
		            	$('#do_exe').attr('set', 1);
		            	$('#do_exe').attr('exercise', d_exercise+1);
		            } else {
		            	d_exercise--;
		            	$('#do_exe').attr('set', d_set+1);
		            }
	            	from = ex_arr[d_exercise].tr[d_set].sleep;
	            	$('#timer').html(from.toString().toHHMMSS()).hide();//стоп отдых
	                $('#do_exe').text('Сделал ' + $('#do_exe').attr('set') + ' подход!');
	                $('#sp_do_now').html(ex_arr[d_exercise].tr[d_set].sets).parent().show();
	                $('#comment').html(ex_arr[d_exercise].tr[d_set].alert_proc);
	                $('.training_content').css({'margin-bottom':$('.bottom').height(),'margin-top':$('header').height()-20});
	                $('#rest').hide();
	                $('#do_exe, #ch input').attr('disabled', false);
	                console.log('Начали '+$('#do_exe').attr('set')+' подход');
	                tmp_workout_time = Math.floor(Date.now() / 1000);
                }
                clearInterval(timer);
            }
        },1000);
    }
    $('header').addClass('topped');
    $(window).scroll(function(){
    	var headerHeight = 14 + $('header').height();
    	if($(window).scrollTop() > headerHeight) {
	    	$('header').removeClass('topped');
    	} else {
    		$('header').addClass('topped');
    	}
    	if(!$('header').hasClass('topped')) {
	    	if($(window).scrollTop() >= scrollCurrent) {
	    		$('header').hide();
	    	} else {
	    		$('header').show();
	    	}
    	}
    	scrollCurrent = $(window).scrollTop();
    });
});
</script>
<?php endif;?>