<?php

/* @var $this yii\web\View */

$this->title = $workout->title;

?>
<style type="text/css">
	.opacitive {opacity:0.1}
	.ex-active {background:#eee}
</style>
<div class="site-index" style="overflow:auto;margin-top:-15px">
	<div class="training_content row">
		<div class="exercise-video" style="position:fixed;z-index:10;width:100%;max-width:960px;"></div>
		<div class="exercises" style="position:relative;">
<?php if ($schedule):
        $sleep_arr = array();
        $set_arr = array();
        $report_arr = array();
        $js_sleep_arr = '[';
        $js_set_arr = '[';
        $js_ex_arr = '[';
        $excntr = 0;
        $routscnt = 0;
?>
	<?php foreach($schedule_exercises as $schedule_exercise):
		if($schedule_exercise->type == \common\models\Schedule_exercise::Exercise): /* если упражнение */
			$exercise = $schedule_exercise->getExercise();
	?>
<?php

    	$excntr++;
    	$ex_scheme = '';
        $tr_routs = $schedule_exercise->getScheduleRoutines('set')->all();
        $js_ex_arr .= '{id:'.$exercise->id.', title:"'.str_replace('"', '&quot;', $exercise->title).'", video:"'.str_replace('"', '\\"', $exercise->video).'", tr:[';
        $report_arr[$schedule_exercise->number] = '';
    if($tr_routs) {
        $timer = 0;
        $set_counter = 0;
        $set_str = '';
        $sleep_arr[$excntr] = array();
        $js_sleep_arr .= '[';
        $set_arr[$excntr] = array();

		foreach($tr_routs as $rout){
    		$routscnt++;
	        $timer += $rout->sleep;
	        $set_counter ++;
	        $sleep_arr[$excntr][] = $rout->sleep;
	        $js_sleep_arr .= $rout->sleep.',';
	        $js_ex_arr .= '{sleep:'.$rout->sleep.', alert_proc:"'.$rout->alert_proc.'", alert_sleep:"'.$rout->alert_sleep.'", sets:';
	        $set_str .= $rout->set.' подход:';
	        $sets = $rout->getScheduleSets();

	        $report_arr[$schedule_exercise->number] .= $rout->set.' подход:';
	        $defaults = array();

	        if($sets) {
	            foreach($sets as $set) {
	            	if(!is_numeric($reports['set'.$set->id]->default_val)){
                    	eval('$defaults["set'.$set->id.'"] = "'.str_replace($globalKeys, $globalVals, str_replace(@$userGlobalKeys[$exercise->id], @$userGlobalVals[$exercise->id], $reports['set'.$set->id]->default_val)).'";');
	            	} else {
	            		$defaults['set'.$set->id] = $reports['set'.$set->id]->default_val;
	            	}
	                $set_attr = $set->getSet_attribute();
	                $set_str .= ' ' . $set_attr->name.' '.$defaults['set'.$set->id].($set_attr->unit?' '.$set_attr->unit:'');
	                $report_arr[$schedule_exercise->number] .= ' '.$set_attr->name.' '.'<span><input type="text" value="'.$defaults['set'.$set->id].'" name="report[set'.$set->id.']"></span>'. ($set_attr->unit?' '.$set_attr->unit:'');
	            }
	            $set_arr[$excntr][] = $set_str;
	            $js_set_arr .= '"'.$set_str.'",';
	        }
	        $js_ex_arr .= '"'.$set_str.'"},';
	        $ex_scheme .= '<input type="checkbox" id="training_checkbox_'.$schedule_exercise->number.'_'.$rout->set.'">&nbsp;'.$set_str.'<br />';
	        $set_str = '';
	        $report_arr[$schedule_exercise->number] .= '<br>';
    	}
	    $js_sleep_arr = substr($js_sleep_arr, 0, strlen($js_sleep_arr)-1).'],';
	   	$js_set_arr = substr($js_set_arr, 0, strlen($js_set_arr)-1).'],';
	}
		$js_ex_arr = substr($js_ex_arr, 0, strlen($js_ex_arr)-1).']},';
		$ex_content = '<div id="exercise_'.$schedule_exercise->id.'" class="training_body ex-description" style="display:none;position:absolute;width:100%;min-height:100%;max-width:960px;background:#fff;z-index:1;padding:20px">
			<div><strong>'.$exercise->title.'</strong></div>
		   	<div class="exercise-scheme">
		   		<strong>Схема выполнения:</strong><br>
		   		'.$ex_scheme.'
		   	</div>
			<div><strong>Описание</strong><br>'.$exercise->description.'</div>';
    		$exFields = $exercise->getFields()->all();
    		foreach($exFields as $exField){    			$ex_content .= '<div>
   			<strong>'.$exField->title.'</strong>'.$exField->value.'</div>';
    		}
		$ex_content .'</div>';
		$exercise_content[] = $ex_content;
		?>
		<div class="exercise" style="border: 1px solid #e5e5e5;" number="<?=$excntr ?>" data-ex-id="<?=$schedule_exercise->id ?>">
			<img src="http://www.art-djem.ru/upload/medialibrary/61c/zwzamkwsln.jpg" class="img_video" video='<?=str_replace("'", "\'", $exercise->video) ?>' height="100px" style="cursor:pointer;float:left">
			<div style="float:left; padding: 3px 10px;">
				<?=$ex_scheme ?>
			</div>
			<div style="float:right; padding:10px;">
				<?=$exercise->title ?>
				<br />
				<img src="http://vanzzo.net/wp-content/uploads/2014/12/Checkbox.png" height="30px" class="show-me">
			</div>
			<div style="clear:both"></div>
		</div>
		<?php else: /* если сет */ ?>

		<?php endif; ?>
	<?php endforeach; ?>
	<?php

	$js_ex_arr = substr($js_ex_arr, 0, strlen($js_ex_arr)-1).']';

	?>
<?php endif;?>
			</div>
		</div>
	</div>
	<?=implode('', $exercise_content) ?>
</div>
<script type="text/javascript">
	var js_ex_arr = <?=$js_ex_arr ?>;
	$(document).ready(function(){
		$('header').addClass('opacitive');  		$('header').css({position:'fixed', 'z-index':20, top:0});
  		$('header h1').css({'font-size':'0.7em'});
  		$('.content-middle').css({top:0});

  		$('header h1').click(function(){  			$('header').toggleClass('opacitive');  		});

  		$('header h1').append('<span> - '+js_ex_arr[0].title+'</span>');
  		$('.exercise-video').html(js_ex_arr[0].video);
  		$('.exercises, .ex-description').css({top:$('.exercise-video').height()});
  		$('.exercise:first').addClass('ex-active');

  		$('.exercise .img_video').click(function(){
  			$('.exercise').removeClass('ex-active');  			$('.exercise-video').html($(this).attr('video'));
	  		$('.exercises, .ex-description').css({top:$('.exercise-video').height()});
  			$(this).parent().addClass('ex-active');
  			$('header h1 span').remove();
  			$('header h1').append('<span> - '+js_ex_arr[parseInt($(this).parent().attr('number'))-1].title+'</span>');
  			//alert('Приступили к упражнению id '+$(this).parent().data('ex-id'));  		});

  		$('.show-me').click(function(){  			$('#exercise_'+$(this).parent().parent().data('ex-id')).show();  		});	});
</script>