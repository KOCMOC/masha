<?php

/* @var $this yii\web\View */

$this->title = 'Начать тренировку';

?>
<div class="site-index">
<?php if(is_null($exercise)):?>
    Вы закончили упражнения! 
<?php else:?>
<?php if (sizeof($exercise) > 0):?>   
    <?php 
        $tr_routs = $exercise->getTrainingRoutines()->where(['id_user'=>Yii::$app->user->identity->id])->all();
        if($tr_routs) {
            $timer = 0;
            $set_count = 0;
            $sleep_arr = array();?>
            <div>Отдых <div id="timer"></div> сек.</div>
            <form id="frm_do"> 
                <input type="button" id="do_exe" counter="1" value="Сделал 1 подход!">
                <div id="ch"></div>
            </form>
            <p><b><?php echo $exercise['title']?></b></p>
            <p><?php echo $exercise['description_cut']?></p>
            <?php echo '<p><b>Схема тренировки:</b></p>';
            foreach($tr_routs as $rout){
                $timer += $rout->sleep; 
                $set_counter ++;
                $sleep_arr[] = $rout->sleep;
                echo $rout->set.' подход:';
                $sets = $rout->getSets();
                if($sets) {
                    foreach($sets as $set) {
                        $set_attr = $set->getSet_attribute();
                        echo ' '.$set_attr->name.' '.$set->default_val.($set_attr->unit?' '.$set_attr->unit:'');
                    }
                    echo '<br>';
                }
            }?>
            <div id="mfrm_real" class="modal fade">
            <div class="modal-dialog">
                <form id="frm_real" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Отчет об упражнении</h4>
                  </div>
                  <div class="modal-body">
                  <?php
                      foreach($tr_routs as $rout){                 
                      echo $rout->set.' подход:';
                      $sets = $rout->getSets();
                      if($sets) {
                          foreach($sets as $set) {
                              $set_attr = $set->getSet_attribute();
                              echo ' '.$set_attr->name.' '.'<span><input type="text" value="'.$set->default_val.'" name="report['.$set->id.']"></span>'. ($set_attr->unit?' '.$set_attr->unit:'');
                          }
                          echo '<br>';
                      }
                  }?>
                  <input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>" />
                  </div></form>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Готово!</button>
                  </div>
              </div>
            </div>
          </div>
        <?php }
    ?>
<p><?php //echo $exercise['description']?></p>
<?php endif;?>
<?php endif;?>
</div>

<?php if(!is_null($exercise)):?>
<script type="text/javascript">
$(document).ready(function(){
    $('#timer').html('<?=$timer?>');
    var sleep_arr = [<?=implode(',', $sleep_arr) ?>];
    var counter = parseInt($('#do_exe').attr('counter'));
    var from = <?=$timer?>;
    var to = from - sleep_arr[counter-1];
    $('#do_exe').attr('disabled', false); 
    
    $('#do_exe').click(function(){
        $('#do_exe').attr('disabled', true); 
        if($('#do_exe').attr('counter') <= <?=$set_counter?>) {
            $('#do_exe').attr('counter', parseInt($('#do_exe').attr('counter'))+1);
            if($('#do_exe').attr('counter') > <?=$set_counter?>) {
                $('#do_exe').hide();                 
            } else {
                $('#do_exe').val('Отдыхаю'); 
            }
            
            $('#ch').append(
                $(document.createElement('input')).attr({
                    id:     'ch_do_exe_' + parseInt($('#do_exe').attr('counter')-1)
                    ,type:  'checkbox'
                    ,set:   parseInt($('#do_exe').attr('counter')-1)
                    ,checked: true
                })
            );
     
            $('#ch').append(
                $(document.createElement('label')).attr({
                    for:    'ch_do_exe_' + parseInt($('#do_exe').attr('counter')-1)
                }).html('&nbsp;&nbsp;' + parseInt($('#do_exe').attr('counter')-1) + '&nbsp;подход&nbsp;&nbsp;')
             );
     
            $('#ch input').attr('disabled', true);
            
            run_timer();           
        }
    });
    
    $('#ch').delegate('input', 'change', function(){
        from = 0;
        var st = sleep_arr.length-1, fn = parseInt($(this).attr('set'))-1;
        for(var i=st; i>=fn; i--) {
            from += parseInt(sleep_arr[i]);
        }
        to = from - sleep_arr[parseInt($(this).attr('set'))-1]; 
        $('#timer').html(from);
        $('#do_exe').val('Сделал ' + $(this).attr('set') + ' подход!').show().attr('counter', $(this).attr('set'));
        $('#ch label:gt('+(parseInt($(this).attr('set'))-1)+'), #ch label:eq('+(parseInt($(this).attr('set'))-1)+')').remove();
        $('#ch input:gt('+(parseInt($(this).attr('set'))-1)+'), #ch input:eq('+(parseInt($(this).attr('set'))-1)+')').remove();
    });
    
    $('#frm_real').submit(function () {
        var data = $('#frm_real').serializeArray();
        $.ajax({
            beforeSend: function (){
                $('#frm_real .error').remove();
                //showLoader();
            },
            type: 'POST',
            url: '/workout/report',
            dataType: 'json',
            data: data,
            success: function(data){
                if(data.status == 'ok') {
                    if(data.status == 'error') {
                         alert("Ошибка!\n"+data.errors.join("\n"));
                    } else {
                        location.href = '/workout/exercise/<?=$exercise->order + 1?>';
                    }
                } else {
                    if(data.errors) {
                        if(data.errors.common) {
                            alert(data.errors.common);
                        } 
                        if(data.errors.sets) {
                            for(var i in data.errors.sets) {
                                $('#frm_real input[name="report['+i+']"]').parent().prepend('<div class="error" style="color:#f00;position:absolute;display:inline-block;margin-top:20px;padding:3px;background:#fff;border: 1px solid #f00">'+data.errors.sets[i]+'</div>');
                            }
                        }
                    }
                }
                //hideLoader();
            },
            error: function(){
                /*hideLoader();*/
                alert('Ошибка: Не удалось отправить отчет.');
            }   
        }); 
        return false;
    }); 
   
    function run_timer(){ 
        current = from;
        stopdot = to;
        from = to; 
        to = from - sleep_arr[parseInt($('#do_exe').attr('counter'))-1];
        timer = setInterval(function () {
            if(current > stopdot) {
                current--;
                $('#timer').html(current);
            } else {
                $('#do_exe').val('Сделал ' + $('#do_exe').attr('counter') + ' подход!');
                $('#do_exe, #ch input').attr('disabled', false);
                if(current == 0){
                    $('#ch').append(
                        $(document.createElement('input')).attr({
                            id:     'btn_next_exe'
                            ,type:  'button'
                            ,value:  'Следующее'
                            ,class: 'btn btn-primary'
                        })
                    );
                    $('#btn_next_exe').on('click', function(){
                        $('#mfrm_real').modal('show');
                    });
                }
                clearInterval(timer);
            }
        },1000);
    }
});
</script>
<?php endif;?>