<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Задать вопрос';

?>
<div class="ticket-details">
    <div class="ticket-status">
        <?php switch ($model->status) {
            case(\common\models\Ticket::STATUS_NEW):
                echo '<span class="glyphicon glyphicon-star" aria-hidden="true"></span> Новый';
                break;
            case(\common\models\Ticket::STATUS_VIEWERD):
                echo '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Неотвечено';
                break;
            case(\common\models\Ticket::STATUS_ANSWERED):
            case(\common\models\Ticket::STATUS_NEW_ANSWER):
                echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Отвечено';
                break;
            case(\common\models\Ticket::STATUS_NEW_ANSWER_VIEWED):
                echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Просмотрено';
                break;
            case(\common\models\Ticket::STATUS_CLOSED):
                echo 'Закрыт ';
                break;
        } ?>
    </div>
    <div class="ticket-main-title">
        Мои диалоги
    </div>
    <?php if(count($model->ticketMessages)): ?>
    <div class="ticket-messages">
        <?php foreach ($model->ticketMessages as $message): ?>

            <div class="ticket-message <?= ($model->user_id == $message->user_id) ? 'user' : 'admin'?>">
                <div class="ticket-user">
                    <?php if($model->user_id == $message->user_id): ?>
                        <img src="<?= (empty(Yii::$app->user->identity->photo) || Yii::$app->user->identity->photo == 'default.png') ? '/images/user_avatar.png' : Yii::$app->user->identity->photo?>" class="ticket-user-avatar">
                    <?php else: ?>
                        <img src="/images/admin_avatar.png" class="ticket-user-avatar">
                    <?php endif; ?>
                </div>
                <div class="ticket-user-name">
                    <span class="user-name"><?php if($model->user_id == $message->user_id): ?>
                        <?= $model->user->fio; ?>
                    <?php else: ?>
                        Администрация
                        <?php endif; ?></span>
                    <span class="date-time" title="<?= $message->creation_datetime ?>"><?= date("H:i",strtotime($message->creation_datetime)) ?></span>
                </div>
                <div class="ticket-message-content">
                    <?=nl2br($message->message) ?>
                </div>
                <?php if(count($message->attachments)): ?>
                <div class="ticket-attachments">
                    <?php foreach ($message->attachments as $file): ?>
                        <a href="<?= $file['url'] ?>" target="_blank"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> <?= $file['filename'] ?></a>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>

        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>

<div class="ticket-message-form">
    <?php $form = ActiveForm::begin([
        'id' => 'ticket-message-form',
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>
    <div class="ticket-message-fields">
        <?= $form->field($formModel, 'message')
            ->label(false)
            ->textarea(['placeholder' => 'Сообщение','rows' => '3']) ?>
        <?= $form->field($formModel, 'attachmentFiles[]', [
            'template' => '<div class="file-attachments"><a href="#">Прикрепить документ</a> <div class="attachments"></div>{input}{hint}{error}</div>'
        ])
            ->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
    </div>
    <?= Html::submitButton('Отправить', ['class' => 'btn-send-ticket']) ?>
    <?php ActiveForm::end(); ?>
</div>