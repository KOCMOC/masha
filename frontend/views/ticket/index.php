<?php

$this->title = 'Задать вопрос';

?>
<div class="ticket-main-title">
    Мои диалоги
</div>
<?php if(count($tickets)): ?>

    <?php foreach ($tickets as $ticket): ?>
        <a href="/ticket/<?= $ticket->id ?>" class="ticket-item<?=($ticket->status == \common\models\Ticket::STATUS_NEW) ? ' new' : '' ?>">
            <div class="ticket-item-id">
                <?= $ticket->id ?>
            </div>
            <div class="ticket-item-role">
                <div class="ticket-item-role-icon">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <span class="ticket-item-role-subtitle"><?= $ticket->roleName ?></span>
                </div>
                <div class="ticket-item-role-title">
                    <span><?= $ticket->roleName ?></span>
                </div>
            </div>
            <div class="ticket-item-title">
                <div>
                    <?= $ticket->title ?><br>
                    <span>
                        <?php switch ($ticket->status) {
                            case(\common\models\Ticket::STATUS_NEW):
                                echo '<span class="glyphicon glyphicon-star" aria-hidden="true"></span> Новый';
                                break;
                            case(\common\models\Ticket::STATUS_VIEWERD):
                                echo '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Неотвечено';
                                break;
                            case(\common\models\Ticket::STATUS_ANSWERED):
                            case(\common\models\Ticket::STATUS_NEW_ANSWER):
                                echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Отвечено';
                                break;
                            case(\common\models\Ticket::STATUS_NEW_ANSWER_VIEWED):
                                echo '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Просмотрено';
                                break;
                            case(\common\models\Ticket::STATUS_CLOSED):
                                echo 'Закрыт ';
                                break;
                        } ?>
                    </span>
                </div>
            </div>
        </a>
    <?php endforeach; ?>

<?php else: ?>
    <div class="alert alert-success" role="alert">
        Вы пока не задали ни одного вопроса
    </div>
<?php endif; ?>

<?php echo $this->render('_ticket_form'); ?>
