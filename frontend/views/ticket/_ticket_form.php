<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use frontend\models\TicketForm;

$ticketForm = new TicketForm();

?>
<div class="ticket-form">
    <?php $form = ActiveForm::begin([
        'id' => 'ticket-from',
        'action' => '/ticket',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>

    <?= $form->field($ticketForm, 'role')->dropDownList($ticketForm->roles) ?>

    <?= $form->field($ticketForm, 'message', ['template' => '<div class="col-md-12">{input}{hint}{error}</div>'])
        ->label(false)
        ->textarea(['placeholder' => 'Сообщение','rows' => '6']) ?>

    <?= $form->field($ticketForm, 'attachmentFiles[]', [
        'template' => '<div class="col-md-12 file-attachments"><a href="#">Прикрепить документ</a> <div class="attachments"></div>{input}{hint}{error}</div>'
    ])
        ->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <div class="form-group text-center">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-send-ticket']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS

$(document).ready(function(){
    
    $(document).on('click', '.file-attachments a', function(){
        $("#ticketform-attachmentfiles").click();
        return false;
    });
    
    $("#ticketform-attachmentfiles").change(function (){
        var files = this.files,
        html = '';
        $.each(files, function(index, file) {
            html+= '<span><span class="filename">' + file.name + '</span> <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></span> ';
        });
        
        $('.file-attachments .attachments').html(html);
    });
    
    
});

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>