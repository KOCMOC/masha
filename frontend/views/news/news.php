<?php
$this->title = $model->title;
?>

<?php if(!empty($model->preview)): ?>
    <img src="<?= $model->preview ?>" style="float: left;width: 150px;margin-right: 10px; margin-bottom: 20px;">
<?php endif; ?>

<?php if(empty($model->full_text)): ?>

    <?= $model->short_text ?>

<?php else: ?>

    <?= $model->full_text ?>

<?php endif;?>
