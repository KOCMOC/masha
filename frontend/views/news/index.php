<?php

use yii\widgets\LinkPager;

$this->title = 'Новости';

$user = \common\models\User::findOne(Yii::$app->user->identity->getId());
if(!is_null($user)) {
    $nopay_order = $user->getOrders()->where(['course_group_id'=>2])->one();
    if(!is_null($nopay_order))
        $first_nopay_schedule = $nopay_order->getScheduleRelation()->orderBy('date')->one();
    else
        $first_nopay_schedule = null;
}

if(is_null($first_nopay_schedule))
    $href = '#';
else
    $href = '/workout/'.$first_nopay_schedule->id;

?>
<div class="news-index-title">
    Начни заниматься прямо сейчас!
</div>

<div class="news-list">
    <div class="news-list-item">
        <div class="news-list-item-number">1.</div>
        <div class="news-list-title">Выбери нужный курс!</div>
        <div class="news-list-content">
            Оплати подходящий тебе тариф, заполни анкету и получи индивидуальную программу тренировок, рассчитанную специально для тебя!
            <p><a href="/course">Выбрать курс</a></p>
            <p><a href="<?= $href ?>">Попробовать бесплатную тренировку</a></p>
        </div>
    </div>
    <div class="news-list-item">
        <div class="news-list-item-number">2.</div>
        <div class="news-list-title">Тренируйся!</div>
        <div class="news-list-content">
            Выполняй упражнения, заполняй отчеты! По отчетам я буду следить за твоими результатами,
            и корректировать твою программу! Никаких поблажек на пути к цели!
        </div>
    </div>
    <div class="news-list-item">
        <div class="news-list-item-number">3.</div>
        <div class="news-list-title">Питайся по программе!</div>
        <div class="news-list-content">
            Твой ежедневный рацион и КЖБУ будет рассчитываться исходя из твоих целей!
        </div>
    </div>
    <div class="news-list-item">
        <div class="news-list-item-number">4.</div>
        <div class="news-list-title">Достигай результат и поддерживай его!</div>
        <div class="news-list-content">
            Тренируйся по более продвинутому курсу!
        </div>
    </div>
</div>

<div class="action-news-block">
    <div class="action-news-block-title">Акция!</div>
    <div class="action-news-block-description">Скидка на все курсы 10 %</div>
    <div class="action-news-block-subtitle">только до 23 февраля!</div>

    <div class="action-news-block-code">
        Промокод:<br>
        <span>workoutmasha</span>
    </div>

    <div class="action-news-block-text">
        Введи этот промокод при<br>
        покупке абонемента<br>
        <a href="/course">Купить курс по акции</a>
    </div>
</div>

<?php /*foreach ($models as $model): */?><!--

    <div class="news-item">
        <h3><a href="/news/<?/*= $model->id */?>"><?/*= $model->title */?></a></h3>
        <div class="text">
            <?php /*if(!empty($model->preview)): */?>
                <img src="<?/*= $model->preview */?>" style="float: left;width: 150px;margin-right: 10px; margin-bottom: 20px;">
            <?php /*endif; */?>
            <?/*= nl2br($model->short_text) */?>
        </div>
    </div>

<?php /*endforeach; */?>

--><?php
/*echo LinkPager::widget([
    'pagination' => $pages,
]);
*/?>
