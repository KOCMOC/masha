<?php
$this->title = $page->title;

$next_week = $week + 1;
$next_week = ($next_week > 4)?1:$next_week;

$d_day = $day;
$w_week = $week;

$days = array();
$days[$w_week.'_'.$d_day] = 'Сегодня';
$d_day++;
if($d_day > 7) {	$d_day = 1;
	$w_week++;
	if($w_week > 4) {		$w_week = 1;	}}
$days[$w_week.'_'.$d_day] = 'Завтра';
$d_day++;
if($d_day > 7) {
	$d_day = 1;
	$w_week++;
	if($w_week > 4) {
		$w_week = 1;
	}
}
$days[$w_week.'_'.$d_day] = date('d.m.Y', strtotime('+2 day'));
$d_day++;
if($d_day > 7) {
	$d_day = 1;
	$w_week++;
	if($w_week > 4) {
		$w_week = 1;
	}
}
$days[$w_week.'_'.$d_day] = date('d.m.Y', strtotime('+3 day'));
$d_day++;
if($d_day > 7) {
	$d_day = 1;
	$w_week++;
	if($w_week > 4) {
		$w_week = 1;
	}
}
$days[$w_week.'_'.$d_day] = date('d.m.Y', strtotime('+4 day'));
$d_day++;
if($d_day > 7) {
	$d_day = 1;
	$w_week++;
	if($w_week > 4) {
		$w_week = 1;
	}
}
$days[$w_week.'_'.$d_day] = date('d.m.Y', strtotime('+5 day'));
$d_day++;
if($d_day > 7) {
	$d_day = 1;
	$w_week++;
	if($w_week > 4) {
		$w_week = 1;
	}
}
$days[$w_week.'_'.$d_day] = date('d.m.Y', strtotime('+6 day'));
?>

<div style="text-align:center">
<a href="/diet/<?=$course[0].'_'.$course[1].'_'.$week.'_m' ?>" style="<?=($url == $course[0].'_'.$course[1].'_'.$week.'_m')?'font-weight:bold;':'' ?>float:left;display:block;padding:10px;border:1px solid #e5e5e5;border-radius:10px;color: #424242">Список продуктов<br />текущая неделя</a>
<a href="/diet/<?=$course[0].'_'.$course[1].'_'.$next_week.'_m' ?>" style="<?=($url == $course[0].'_'.$course[1].'_'.$next_week.'_m')?'font-weight:bold;':'' ?>float:right;display:block;padding:10px;border:1px solid #e5e5e5;border-radius:10px;color: #424242">Список продуктов<br />следующая неделя</a>
<a href="/diet/_1" style="<?=($url == '_1')?'font-weight:bold;':'' ?>display:inline-block;padding:10px;border:1px solid #e5e5e5;border-radius:10px;color: #424242">Рекомендации по питанию</a>
</div>
<div style="clear:both"></div>
<?php foreach($days as $d_day=>$d_title): ?>
<a href="/diet/<?=$course[0].'_'.$course[1].'_'.$d_day ?>" style="<?=($url == $course[0].'_'.$course[1].'_'.$d_day or !$url and $d_title == 'Сегодня')?'font-weight:bold;':'' ?>font-size:12px;float:left;display:block;padding:7px;margin:3px;border:1px solid #e5e5e5;border-radius:10px;color: #424242"><?=$d_title ?></a>
<?php endforeach; ?>
<div>
	<?=$page->content ?>
</div>

