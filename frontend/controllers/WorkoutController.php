<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Set;
use common\models\User_global_field;

/**
 * Workout controller
 */
class WorkoutController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'report'],
                'rules' => [
                    [
                        'actions' => ['index', 'report'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($schedule_id = false, $number = 1)
    {

    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);

		$gototransaction = $user->getFinanceTransactions()
								->leftJoin('finance_transaction as invoice', 'invoice.parent = finance_transaction.id and invoice.subject_type='.\common\models\Finance_transaction::Subject_invoice)
								->where([
									'finance_transaction.status'=>\common\models\Finance_transaction::Status_progress,
									'finance_transaction.subject_type'=>\common\models\Finance_transaction::Subject_order,
									'invoice.status'=>\common\models\Finance_transaction::Status_approved
								])
								->one();
		if($gototransaction and $gototransaction->amount * -1 <= $user->getFinanceAccount()->balance) {
			$gotoorder = \common\models\Order::findOne($gototransaction->subject_id);
			if($gotoorder) {
        		return $this->redirect('/course/pay/'.$gotoorder->course_group_id.'/'.$gototransaction->id);
        	}
		}

    	$interface = Yii::$app->request->get('interface');
    	$this->layout = 'old_main.php';
    	if($schedule_id === false) {
    		$inactive_order = $user->getOrders()->where(['active' => 0])->one();
    		if($inactive_order) {    			return $this->redirect('/course/'.$inactive_order->course_group_id);    		}

    		$orders = $user->getOrders()->where(['active' => 1])->andWhere('expire > :time', ['time' => time()])->all();
    		if(sizeof($orders) > 0) {
    			$schedule = false;    			foreach($orders as $order) {
    				if($schedule === false) {    					$schedule = $order->getScheduleRelation()->where(['done' => 0])->one();
    					if($schedule) {    						if($schedule->date > date('Y-m-d H:i:s')) {    						/* ХУЙ ЕГО ЗНАЕТ, ДОПИСАТЬ! */    						}    						break;    					}
    				}    			}
    			if(!$schedule) {
    				$anket_order = $user->getOrders()->where(['active' => 1, 'schedule_template' => ''])->one();
    				if($anket_order) {    					return $this->redirect('/course/'.$anket_order->course_group_id);    				}    				return $this->redirect('/schedule');    			}    		} else {   				$anket_order = $user->getOrders()->where(['active' => 1, 'schedule_template' => ''])->one();
   				if($anket_order) {
   					return $this->redirect('/course/'.$anket_order->course_group_id);
   				} else {   					return $this->redirect('/course');   				}    		}    	}
    	if($schedule_id > 0) {
	    	$schedule = \common\models\Schedule::findOne($schedule_id);
    	}
        if($schedule) {

        	if($schedule->getOrder()->active == 2) {        		return $this->redirect('/course/'.$schedule->getOrder()->course_group_id);        	}

        	$fields = Yii::$app->request->post('fields');
        	if($fields) {        		if(is_array($fields) and sizeof($fields) > 0) {	        		foreach($fields as $field_id=>$field_value) {	                	$field = \common\models\Schedule_report::findOne($field_id);
	                	if($field) {	                		$field->value = $field_value;
	                		if($field->save()) {
	                			if($field->related_anket_field > 0) {		                			$order_anket_field = $field->getRelatedAnketField();
	                				if($order_anket_field) {
	                					$order_anket_field->value = $field_value;	                					$order_anket_field->save();	                				}
	                			}	                		}	                	}	        		}
        		}

                $schedule->done = 1;
         		$schedule->save();

         		if($schedule->workout_id == 2) {                	$user = $schedule->getOrder()->getUser();
                	if($user and $user->quest == 0) {                		$user->quest = 1;
                		$user->save();
                		$p_schedule = $schedule->getOrder()->getScheduleRelation()->orderBy('date desc')->one();
			    		if($p_schedule) {
			    			$p_schedule->prepare();
			    		}                	}         		}
         		if($schedule->workout_id == 6) {
                	$user = $schedule->getOrder()->getUser();
                	if($user and $user->quest == 1) {
                		$user->quest = 2;
                		$user->save();
                	}
         		}
         		# хардкод

                $this->redirect('/schedule');
        	}

			// тоже надо понять, может ли человек по этому итему расписания заниматься - поставить проверку и проверить его done
        	$exercises = $schedule->getExercises('number');
        	/*if($number > $exercises->count()) {                $schedule->done = 1;
         		$schedule->save();
                $this->redirect('/schedule');
                return;        	}*/

			$detect = new \common\models\Mobile_Detect();

        	$reportArr = $globalKeys = $globalVals = $userGlobalKeys = $userGlobalVals = [];
        	$reports = $schedule->getReports();
        	foreach($reports as $report) {        		$reportArr['set'.$report->id_set] = $report;
        		$reportArr['set'.$report->id_set]->default_val = str_replace(',', '.', $reportArr['set'.$report->id_set]->default_val);// надо эту хуйню сделать в админке!!        	}
        	$userGlobals = $schedule->getOrder()->getGlobalFields();
            foreach($userGlobals as $userGlobal) {
            	if(!isset($userGlobalKeys[$userGlobal->id_exercise])) {
            		$userGlobalKeys[$userGlobal->id_exercise] = [];
            		$userGlobalVals[$userGlobal->id_exercise] = [];
            	}            	$userGlobalKeys[$userGlobal->id_exercise][] = $userGlobal->alias;//getGlobal_field()->alias;
            	$userGlobalVals[$userGlobal->id_exercise][] = $userGlobal->value;            }
            /*$globals = \common\models\Global_field::find()->asArray()->all();
            foreach($globals as $global) {            	$globalKeys[] = $global['alias'];
            	$globalVals[] = $global['default']?$global['default']:'до отказа';            }*/

         	$index_template = ($interface and $interface == 'small')?'index_small':'index';
            return $this->render($index_template, ['schedule' => $schedule, 'workout' => $schedule->getWorkout(), 'schedule_exercises' => $exercises->all(), 'reports' => $reportArr, /*'globalKeys' => $globalKeys, 'globalVals' => $globalVals,*/ 'userGlobalKeys' => $userGlobalKeys, 'userGlobalVals' => $userGlobalVals, 'is_mobile' => $detect->isMobile()]);
        } else {
            return $this->render('index', ['schedule' => false]);
        }
    }

    public function actionReport($schedule_id)
    {
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = array('status'=>'ok', 'errors'=>[]);

            // тут поставить проверку соответствует ли данное schedule этому юзеру и проверить у schedule done
            ### !!! ###

            $schedule = \common\models\Schedule::findOne($schedule_id);
        	$reportArr = [];
        	$reports = $schedule->getReports();
        	foreach($reports as $report) {
        		$reportArr['set'.$report->id_set] = $report;
        	}
            $reportData = Yii::$app->request->post('report');
            if(is_array($reportData) and sizeof($reportData) > 0) {            	$globals_changed = [];
            	$user_globals_prot = $schedule->getOrder()->getGlobalFields();
            	$user_globals = array();
	            foreach($user_globals_prot as $user_global_prot) {
	            	if(!isset($user_globals[$user_global_prot->id_exercise])) {	            		$user_globals[$user_global_prot->id_exercise] = array();	            	}
	            	array_push($user_globals[$user_global_prot->id_exercise], $user_global_prot);
	            }
            	$done = true;
            	$exercise = false;
                foreach($reportData as $set_id=>$real_val) {
                    $report = $reportArr[$set_id];
                    if(isset($report)) {
                    	if($exercise === false) {                    		$sh_tr_r = $report->getScheduleSet()->getScheduleTraining_routine();
                    		$exercise = ($sh_tr_r->type == \common\models\Schedule_training_routine::WorkoutRelation)?$sh_tr_r->getScheduleExercise():$sh_tr_r->getScheduleSetExercise();
                    		$exercise_id = ($exercise->getExercise()->id_tab == 2)?$exercise->getExercise()->id_related:$exercise->getExercise()->id;                    	}
                        $report->real_val = $real_val;
                        if($report->save()) {
                        	if(is_numeric($report->default_val)) {
	                        	if($report->default_val > $real_val) {	                        		$done = false;	                        	}
                        	} else {
                        		if(isset($user_globals[$exercise_id]) and sizeof($user_globals[$exercise_id]) > 0)
								foreach($user_globals[$exercise_id] as $user_global) {
									if(strpos(trim($report->default_val), $user_global->alias) > -1) {
                                        $user_global->value = $report->real_val;
                                        if($user_global->save()) {                                        	$globals_changed[$user_global->alias] = $user_global;                                        }
									}
								}                        	}
                        } else {
                            $errors = $report->errors;
                            $response['status'] = 'error';
                            $response['errors']['sets'][$set_id] = implode($errors['real_val']);
                        }
                    } else {
                        $response['status'] = 'error';
                        $response['errors']['sets'][$set_id] = 'Поле '.$set_id.' не найдено';
                    }
                }
                if($exercise !== false and ($exercise->trigger_yes or $exercise->trigger_no)) {
                	$userGlobalsArr = $globals_changed;
                	$userGlobals = $user_globals[$exercise_id];
                	foreach($userGlobals as $userGlobal) {
                		if(!in_array($userGlobal->alias, $userGlobalsArr)) {                			$userGlobalsArr[$userGlobal->alias] = $userGlobal;
                		}                	}
                	if(sizeof($userGlobalsArr) > 0) {
                		$filled_trigger = false;
                		if($done === true and $exercise->trigger_yes){
                			$filled_trigger = $exercise->trigger_yes;
						}
						if($done === false and $exercise->trigger_no){
                            $filled_trigger = $exercise->trigger_no;
						}
						if($filled_trigger !== false) {
							$trigger_saver = [];							foreach($userGlobalsArr as $ugalias=>$ugval) {
								if(strpos($filled_trigger, $ugalias) > -1) {
									$filled_trigger = str_replace($ugalias, '$ugval->value', $filled_trigger);
									eval($filled_trigger.';');
                                    $ugval->save();
									$globals_changed[$ugalias] = $ugval->value;
								}
							}						}                	}                }


            } else {
                $response['status'] = 'error';
                $response['errors']['common'] = 'Отчёт не был передан на обработку';
            }
        }
        return $response;
    }
}
