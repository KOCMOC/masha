<?php
namespace frontend\controllers;

use common\models\UserAnket;
use yii\base\DynamicModel;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Order;
use common\models\User;
use frontend\models\AccountForm;
use yii\web\UploadedFile;
use yii\web\Response;
use Yii;

/**
 * Ticket controller
 */
class AccountController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(Yii::$app->controller->action->id == 'change-avatar') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
    	$user = User::findOne(Yii::$app->user->identity->id);

    	if($user) {
    		$orders = $user->getOrders()->where(['active'=>1])->andWhere(['!=', 'course_group_id', '2'])->all();

            $anketModel = UserAnket::findOne(['user_id' => Yii::$app->user->identity->getId()]);
            if(is_null($anketModel))
                $anketModel = new UserAnket();

    		$model = new AccountForm;
    		$model->init();

	        if(Yii::$app->request->post()) {
		        if ($model->load(Yii::$app->request->post()) and $model->validate()) {
		            if ($user = $model->save()) {
		            	Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
		            } else {
		            	Yii::$app->session->setFlash('error', 'Не удалось сохранить изменения.');
		            }
		        }
	        }

	        return $this->render('index', [
	            'orders' => $orders,
	            'user' => $user,
	            'model' => $model,
                'anketModel' => $anketModel,
	        ]);
    	}
    }

    public function actionChangeAvatar()
    {
        if(!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new AccountForm();

        if (Yii::$app->request->isPost) {
            $model->photo = UploadedFile::getInstance($model, 'photo');

            $photo = $model->upload();

            if ($photo) {
                return [
                    'status' => 'success',
                    'photo' => $photo
                ];
            }
            else {
                return [
                    'status' => 'error'
                ];
            }
        }

    }

    public function actionChangeAnket()
    {
        $model = UserAnket::findOne(['user_id' => Yii::$app->user->identity->getId()]);
        if(is_null($model))
            $model = new UserAnket();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->isNewRecord)
                $model->user_id = Yii::$app->user->identity->getId();

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось сохранить изменения.');
            }
            //return $this->refresh();
            return $this->redirect('/account');
        }
    }
}