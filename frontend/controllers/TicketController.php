<?php
namespace frontend\controllers;

use common\models\Ticket;
use common\models\TicketMessage;
use frontend\models\TicketForm;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Ticket controller
 */
class TicketController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $tickets = Ticket::find()->where(['user_id' => Yii::$app->user->identity->getId()])->all();

        $ticketForm = new TicketForm();

        if ($ticketForm->load(Yii::$app->request->post()) && $ticketForm->validate()) {

            $ticketForm->attachmentFiles = UploadedFile::getInstances($ticketForm, 'attachmentFiles');

            if($ticket = $ticketForm->createTicket()) {
                $this->redirect(['ticket/view', 'id' => $ticket->id]);
            }
        }

        return $this->render('index', [
            'tickets' => $tickets,
            'ticketForm' => $ticketForm,
        ]);
    }

    public function actionView($id)
    {
        $model = Ticket::findOne(['id' => $id, 'user_id' => Yii::$app->user->identity->getId()]);

        if(is_null($model)) {
            throw new NotFoundHttpException();
        }

        $formModel = new TicketMessage();

        if($model->status == Ticket::STATUS_ANSWERED) {
            $model->status = Ticket::STATUS_NEW_ANSWER_VIEWED;
            $model->save();
        }

        if ($formModel->load(Yii::$app->request->post())) {

            $formModel->ticket_id = $id;
            $formModel->user_id = Yii::$app->user->identity->getId();
            $formModel->creation_datetime = date("Y-m-d H:i:s");
            $formModel->read_datetime = date("Y-m-d H:i:s");

            if($formModel->validate()) {

                $formModel->attachmentFiles = UploadedFile::getInstances($formModel, 'attachmentFiles');

                if ($formModel->save()) {

                    $model->status = Ticket::STATUS_NEW;
                    $model->save();

                    $formModel->attachment = $formModel->upload();
                    $formModel->save();
                }
            }

            $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('ticket', [
            'model' => $model,
            'formModel' => $formModel,
        ]);
    }
}