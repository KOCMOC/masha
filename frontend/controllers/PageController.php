<?php
namespace frontend\controllers;

use common\models\Faq;
use common\models\Page;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Ticket controller
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex($url)
    {
        $page = Page::findOne(['url' => $url]);

        if(is_null($page)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('page', [
            'page' => $page,
        ]);
    }
}