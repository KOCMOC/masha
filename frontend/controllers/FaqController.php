<?php
namespace frontend\controllers;

use common\models\Faq;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Ticket controller
 */
class FaqController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $questions = Faq::find()->all();

        return $this->render('index', [
            'questions' => $questions,
        ]);
    }
}