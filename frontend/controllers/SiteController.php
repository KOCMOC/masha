<?php
namespace frontend\controllers;

use common\models\Finance_account;
use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\EmailCollectForm;
use rmrevin\yii\ulogin\AuthAction;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup', 'ulogin'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'ulogin' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'uloginSuccessCallback'],
                'errorCallback' => function($data){
                    \Yii::error($data['error']);
                },
            ]
        ];
    }

	public function beforeAction($action)
    {
        if ($action->id == 'error' and Yii::$app->user->isGuest)
            $this->layout = 'landing.php';

        if($action->id == 'ulogin') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/news');
        }

		$this->layout = '/landing';

        $deliveryModel = new EmailCollectForm();
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = array('status'=>'ok', 'message'=>'');
	        if ($deliveryModel->load(Yii::$app->request->post())) {
	        	if($deliveryModel->collect() === true) {
	        		//$deliveryModel = new EmailCollectForm();
	        		//$response['message'] = 'Запрос успешно отправлен';
	        	} else {
	        		$response['message'] = ($deliveryModel->errorMessage)?$deliveryModel->errorMessage:'Не удалось отправить запрос';
	        		$response['status'] = 'error';
	        	}
	        }
	        return $response;
        }

    	return $this->render('index', ['deliveryModel' => $deliveryModel]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if(Yii::$app->params['is_production'] === true) {
            if(Yii::$app->request->get('hidden') != 'login') {
                return $this->goHome();
            }
        }

        if(!Yii::$app->request->isAjax) {
            return $this->goHome();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                if(Yii::$app->request->post('validation') == 'no') {
	                if($model->login()) {
	                    return $this->redirect(['/schedule']);
	                }
                }
            }
        }

        return ActiveForm::validate($model);
    }

    public function uloginSuccessCallback($attributes)
    {
        if(Yii::$app->params['is_production'] === true) {
            if(Yii::$app->request->get('hidden') != 'login') {
                return $this->goHome();
            }
        }

        $user = User::findOne(['email' => $attributes['email']]);
        if(is_null($user) and Yii::$app->params['setNopayCourse'] === true) {

            $user = new User();
            $user->email = $attributes['email'];
            $user->fio = $attributes['first_name'] . ' ' . $attributes['last_name'];
            $user->phone = $attributes['phone'];
            $user->photo = $attributes['photo'];
            $user->status = User::STATUS_ACTIVE;
            $user->setPassword($user->generatePassword());
            $user->generateAuthKey();

            // Set free trainer
            $user->setTrainer();

            $returnUser = $user->save() ? $user : null;
            if($returnUser) {
                $account = new Finance_account();
                $account->user_id = $returnUser->id;
                $account->balance = 0;
                $account->status = 1;
                $account->save();

                /* снова ёбаный хардкод */
                $today = date('w');
                $aftertomorrow = date('w', strtotime('2 day'));
                $expire = strtotime('2 day');

                $order = new \common\models\Order();
                $order->course_group_id = 2;
                $order->promocode = 0;
                $order->level = 1;
                $order->gender = 2;
                $order->place = 1;
                $order->discount = 0;
                $order->user_id = $returnUser->id;
                $order->type = 0;
                $order->schedule_template = $today.','.$aftertomorrow;
                $order->active = 1;
                $order->amount = 0;
                $order->expire = $expire;

                if($order->save()) {
                    $order->refresh();
                    $course = \common\models\Course::findOne(3);
                    $course->order($order, $returnUser->id);
                }

                Yii::$app->user->login($user, 0);

                /* хардкод когда-нибудь кончается */
                #но это, блядь, не наш случай
            }
        }
        else {
            Yii::$app->user->login($user, 0);
        }

        return $this->goHome();
    }

    public function actionTest()
    {		/*$mailer = Yii::$app->get('mail');
		$mailer->compose()
			->setTo('magaanal@mail.ru')
			->setFrom([Yii::$app->params['adminEmail']=>Yii::$app->params['adminEmail']])
			->setSubject('Регистрация на проекте '.Yii::$app->params['siteData']['name'])
			->setHtmlBody('<hr>lalalala<hr>')
			->send();*/

			/*$headers = "From: " . strip_tags(Yii::$app->params['adminEmail']) . "\r\n";
			$headers .= "Reply-To: ". strip_tags(Yii::$app->params['adminEmail']) . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
			mail('magaanal@mail.ru', 'Тестовое письмо', '<hr>lalalala<hr>', $headers);
			die('asd'); */    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignupValidate()
    {
        if(!Yii::$app->request->isAjax) {
            return $this->goHome();
        }

        $model = new SignupForm();
        $request = Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        if(Yii::$app->params['is_production'] === true) {
            if(Yii::$app->request->get('hidden') != 'signup') {
                return $this->goHome();
            }
        }

        //if (Yii::$app->params['is_production'] == true) {return $this->goHome();} //если показывать только ленд

        if(!Yii::$app->request->isAjax) {
            return $this->goHome();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $user = $model->signup();

                // TODO Добавлять в список рассылки

                // TODO Подключить mandrill for mailchimp
				$headers = "From: " . strip_tags(Yii::$app->params['adminEmail']) . "\r\n";
				$headers .= "Reply-To: ". strip_tags(Yii::$app->params['adminEmail']) . "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
				mail($user->email, 'Регистрация на проекте '.Yii::$app->params['siteData']['name'], $this->renderPartial('/mail/registration', ['promoCode'=>false, 'siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$model->password]), $headers);
                /*$mailer = Yii::$app->get('mail');
		        $mailer->compose()
		            ->setTo($user->email)
		            ->setFrom([Yii::$app->params['adminEmail']=>Yii::$app->params['adminEmail']])
		            ->setSubject('Регистрация на проекте '.Yii::$app->params['siteData']['name'])
		            ->setHtmlBody($this->renderPartial('/mail/registration', ['promoCode'=>false, 'siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$model->password]))
		            ->send();
		            */

                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return ActiveForm::validate($model);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                return '{"passwordresetrequestform-email":["Ссылка для восстановления пароля успешно отправлена."]}';
            } else {
                return '{"passwordresetrequestform-email":["Не удалось отправить ссылку на этот E-mail. Попробуйте позже."]}';
            }

        } else {			return '{"passwordresetrequestform-email":["Извините, невозможно восстановить пароль для этого E-mail."]}';        }

        /*return $this->renderPartial('requestPasswordResetToken', [
            'model' => $model,
        ]);*/
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
    	$this->layout = '/landing';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            Yii::$app->session->setFlash('info', $e->getMessage());
            return $this->actionIndex();
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('info', 'Новый пароль был сохранён.');

            return $this->goHome();
        }

        Yii::$app->session->setFlash('info', $this->renderPartial('resetPassword', ['model' => $model]));
        return $this->actionIndex();
    }

    public function actionKassa()
    {
        echo 'Здесь страница Яндекс Кассы<br>Переданы параметры<br><pre>';
        $data = Yii::$app->request->post();
        unset($data['_csrf']);
        print_r($data);
        die();
    }
}
