<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\Course;

/**
 * Course controller
 */
class CourseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

    	if(Yii::$app->request->get('fail')) {    		Yii::$app->session->setFlash('info', 'Не удалось провести платёж');    	}

    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);

		$gototransaction = $user->getFinanceTransactions()
								->leftJoin('finance_transaction as invoice', 'invoice.parent = finance_transaction.id and invoice.subject_type='.\common\models\Finance_transaction::Subject_invoice)
								->where([
									'finance_transaction.status'=>\common\models\Finance_transaction::Status_progress,
									'finance_transaction.subject_type'=>\common\models\Finance_transaction::Subject_order,
									'invoice.status'=>\common\models\Finance_transaction::Status_approved
								])
								->one();
		if($gototransaction and $gototransaction->amount * -1 <= $user->getFinanceAccount()->balance) {
			$gotoorder = \common\models\Order::findOne($gototransaction->subject_id);
			if($gotoorder) {
        		return $this->redirect('/course/pay/'.$gotoorder->course_group_id.'/'.$gototransaction->id);
        	}
		}

		$courses = \common\models\Course_group::find()->all();

        return $this->render('index', [
            'courses' => $courses,
        ]);
    }

    public function actionView($id)
    {
    	$course_group = \common\models\Course_group::findOne($id);
    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);

        if($course_group) {
        	$order = \common\models\Order::find()->where(['user_id'=>\Yii::$app->user->identity->id, 'course_group_id'=>$course_group->id])->orderBy(['expire' => SORT_DESC])/*->andWhere(['!=', 'active', 2])*/->one();

	    	//$transactions = \common\models\Finance_transaction::find()->where(['user_id'=>\Yii::$app->user->identity->id, 'subject_id'=>$course_group->id, 'active'=>0])->all();



    	/*if($course) {

	    	$order = \common\models\Order::find()->where(['user_id'=>\Yii::$app->user->identity->id, 'course_id'=>$course->id, 'active'=>1])->one();
	    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);

    		$fio = Yii::$app->request->post('fio');
    		if($fio) {
    			$growth = Yii::$app->request->post('growth');
    			$user->fio = $fio;
    			$user->growth = $growth;
    			$user->save();

    			$fields = Yii::$app->request->post('fields');

    			$anket_fields = $order->getAnket();
				if(sizeof($anket_fields) > 0)
				foreach($anket_fields as $anket_field) {
					if($anket_field->type !== 2) {
						if(isset($fields['anket_'.$anket_field->id])) {
							$anket_field->value = $fields['anket_'.$anket_field->id];
							$anket_field->save();
						}
					} else {
						if(isset($fields['anket_'.$anket_field->id])) {
							$anket_field->value = 1;
							$anket_field->save();
						} else {
							$anket_field->value = 0;
							$anket_field->save();
						}
					}
				}

    		}*/

	        $levels = \common\models\Course_level::find()->asArray()->all();
	        $levelsArray = [];
	        if(sizeof($levels) > 0) {
	        	foreach($levels as $level){
					$levelsArray[$level['id']] = $level['title'];
	        	}
	        }

	        if(Yii::$app->request->isAjax) {
                return $this->renderAjax('course', [
                    'course_group' => $course_group,
                    'order' => $order,
                    'user' => $user,
                    'orderModel' => new \frontend\models\CourseOrderForm(),
                    'genders' => Course::getGenderList(),
                    'places' => Course::getPlaceList(),
                    'levels' => $levelsArray
                ]);
            }
            else {
                return $this->render('course', [
                    'course_group' => $course_group,
                    'order' => $order,
                    'user' => $user,
                    'orderModel' => new \frontend\models\CourseOrderForm(),
                    'genders' => Course::getGenderList(),
                    'places' => Course::getPlaceList(),
                    'levels' => $levelsArray
                ]);
            }
    	}
    }

    public function actionOrder($id)
    {
		$course_group = \common\models\Course_group::findOne($id);

		if($course_group) {
        	$order = \common\models\Order::find()->where(['user_id'=>\Yii::$app->user->identity->id, 'course_group_id'=>$course_group->id])->andWhere(['!=', 'active', 2])->one();
            if($order) {
            	$orderModel = new \frontend\models\CourseOrderForm();
		        if ($orderModel->load(Yii::$app->request->post()) && $orderModel->validate()) {
		            if($orderModel->order($order)) {
		                return $this->redirect('/schedule');
		            } else {		            	print_R($orderModel->errors); die();		            }
		        }
            }
		}

		$selected_time = Yii::$app->request->post('selected_time');
        if($selected_time) {
        	if($course->order($selected_time))
        		$this->redirect('/schedule/');
        } else {
	        $y = (true or is_null($year))?date('Y'):$year;
	        $m = (true or is_null($month))?date('m'):$month;

	        $mon = date("n", strtotime('01.'.$m.'.'.$y));
	        $month = $m.'.'.$y;

	    	return $this->render('order', [
	    		'course' => $course,
	            'm' => $m,
	            'y' => $y,
	            'mon' => $mon,
	            'month' => $month
	    	]);
	    }
    }

    public function actionBuy($sale_item_id)
    {
		$sale_item = \common\models\Sell_item::findOne($sale_item_id);
		$discount = Yii::$app->request->get('discount');

		$promo = Yii::$app->request->get('promo');
		$promoc = false;
		if(strtolower($promo) == 'workoutmasha') {			$promoc = 1;		} elseif($promo == 'Lij0X7s2') {			$promoc = 2;		} elseif(strtolower($promo) == 'mashavip') {
			$promoc = 3;
		} elseif(strtolower($promo) == 'newlife') {
			$promoc = 4;
		} elseif($promo == 'Masha50%') {			$promoc = 5;		} elseif($promo == 'manechka') {
			$promoc = 6;
		} elseif($promo == 'masha20%') {
			$promoc = 7;
		} elseif($promo == 'жизньпрекрасна') {			$promoc = 8;		}

        if($sale_item) {
        	if($sale_item->getOrders()->where(['user_id'=>\Yii::$app->user->identity->id])->count() == 0) {
        		$sale_item->buy($discount, $promoc);
        	}
        	$order = $sale_item->getOrders()->where(['user_id'=>\Yii::$app->user->identity->id])->one();
        	if($order) {        		$transaction = $order->getTransactions()->one();
        		if($transaction) {           			return $this->actionPay($transaction->id, $order->course_group_id);        		}        	}

        	return $this->actionView($sale_item->course_group_id);
        }
    }

    public function actionExtend($sale_item_id=false, $order_id)
    {
		$order = \common\models\Order::findOne($order_id);
    	if($sale_item_id === false) {			if($order->active == 3)	{
		        $course = \common\models\Course::find()->where([/*'gender'=>$order->gender, */'level'=>$order->level, 'place'=>$order->place, 'course_group_id'=>$order->course_group_id])->one();
				$course->extend($order);

		        return $this->redirect(\yii\helpers\Url::toRoute(['/schedule/index', 'course'=>$order->id]));
	        }    	} else {
			$sale_item = \common\models\Sell_item::findOne($sale_item_id);
			$discount = Yii::$app->request->get('discount');

			$promo = Yii::$app->request->get('promo');
			$promoc = false;
			if(strtolower($promo) == 'workoutmasha') {
				$promoc = 1;
			} elseif($promo == 'Lij0X7s2') {
				$promoc = 2;
			} elseif(strtolower($promo) == 'mashavip') {
				$promoc = 3;
			} elseif(strtolower($promo) == 'newlife') {
				$promoc = 4;
			} elseif($promo == 'MashaVip50%') {
				$promoc = 5;
			} elseif($promo == 'manechka') {
				$promoc = 6;
			} elseif($promo == 'masha20%') {
				$promoc = 7;
			}

	        if($sale_item) {
	        	if($order->active == 2) {
	        		$sale_item->buy($discount, $promoc, $order);
	        	}

	        	if($order) {
	        		$transaction = $order->getTransactions()->orderBy(['created_at' => SORT_DESC])->one();
	        		if($transaction and $transaction->status == 0) {
	           			return $this->actionPay($transaction->id, $order->course_group_id);
	        		}
	        	}


	        	//return $this->actionView($sale_item->course_group_id);
	        }
	    }
    }

    public function actionSchedule() {

    }

    public function actionAnket() {
    	$order = \common\models\Order::findOne(Yii::$app->request->post('order_id'));
    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);
    	$fields = Yii::$app->request->post('fields');    	if($user and $order and is_array($fields) and sizeof($fields) > 0) {
	    	$anket_fields = $order->getAnket();
	    	$user_anket = $user->getAnket();
	    	if(!$user_anket) {	    		$user_anket = new \common\models\User_anket;
	    		$user_anket->user_id = \Yii::$app->user->identity->id;	    	}
	    	if(is_array($anket_fields) and sizeof($anket_fields) > 0){
		    	foreach($anket_fields as $anket_field) {
	            	$anket_field->value = $fields[$anket_field->id];
	            	$anket_field->save();	            	if($anket_field->alias) {
	            		if($anket_field->alias == 'gender') {	            			$fields[$anket_field->id] = ($fields[$anket_field->id])?0:1;	            		}
	            		$alias = $anket_field->alias;	            		$user_anket->$alias = $fields[$anket_field->id];	            	}		    	}
	    	}
	    	$user_anket->kal = Yii::$app->request->post('kal');
	    	if($user_anket->day == 0) $user_anket->day = 1;
	    	if($user_anket->week == 0) $user_anket->week = 1;
	    	$user_anket->status = 1;
	    	$user_anket->save();
	    	/*print_R(Yii::$app->request->post('fields'));
	    	die('!!!');*/
    	}
    	return $this->redirect('/account');
    }

    public function actionPayto($transaction_id, $course_id) {		return $this->actionPay($transaction_id, $course_id, true);    }

    public function actionPay($transaction_id, $course_id, $layout = false) {
    	$transaction = \common\models\Finance_transaction::findOne($transaction_id);
    	if($transaction) {
			if($transaction->commit()) {				// todo сюда въебнуть $course->extend(вся_хуйня);
				return $this->actionView($course_id);
				//return $this->redirect('/course/'.$course_id);
			} else {
				$invoice_transaction = $transaction->getChild();
				if($invoice_transaction->subject_type == \common\models\Finance_transaction::Subject_invoice) {
					if($invoice = \common\models\Finance_invoice::findOne($invoice_transaction->subject_id)) {
						$user = $invoice->getUser();
						/*echo $invoice_transaction->amount;

						echo $user->email;
						echo $user->phone;
						echo ' '.$invoice->user_id.' ';
						die('dd');*/
						$yandex = Yii::$app->params['yandex'];
						$level = 1;
						$site_url = Yii::$app->params['siteData']['url'];
						if($layout === true) {					    	return $this->render('pay', [
					    		'url' => $course_id.'/'.$transaction_id,
					    		'yandex' => $yandex,
					    		'amount' => $invoice->amount,
					    		'site_url' => $site_url,
					    		'invoiceHash' => $invoice->hash,
					    		'user' => $user,
					    		'course_id' => $course_id,
					    		'transaction_id' => $transaction_id
					    	]);						} else {
					    	return $this->renderPartial('pay', [
					    		'url' => $course_id.'/'.$transaction_id,
					    		'yandex' => $yandex,
					    		'amount' => $invoice->amount,
					    		'site_url' => $site_url,
					    		'invoiceHash' => $invoice->hash,
					    		'user' => $user,
					    		'course_id' => $course_id,
					    		'transaction_id' => $transaction_id
					    	]);
				    	}
			    	}
		    	}
			}
    	} else {
    		die('Не удалось найти счёт');
    	}
    }

    public function actionOne()
    {
    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);
    	if($user->getOrders()->where(['course_group_id'=>4, 'active'=>2])->count() > 0) {    		return $this->actionView(4);    	}
        return $this->render('course_page_1', ['coursePages' => Yii::$app->params['coursePages']]);
    }

    public function actionTwo()
    {
    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);
    	if($user->getOrders()->where(['course_group_id'=>3, 'active'=>2])->count() > 0) {
    		return $this->actionView(3);
    	}
        return $this->render('course_page_2', ['coursePages' => Yii::$app->params['coursePages']]);
    }

    public function actionThree()
    {
    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);
    	if($user->getOrders()->where(['course_group_id'=>5, 'active'=>2])->count() > 0) {
    		return $this->actionView(5);
    	}
        return $this->render('course_page_3', ['coursePages' => Yii::$app->params['coursePages']]);
    }

}
