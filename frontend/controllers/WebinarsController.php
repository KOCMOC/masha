<?php

namespace frontend\controllers;

use Yii;
use common\models\Webinar;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class WebinarsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all Webinar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Webinar::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();


        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
        ]);
    }


    public function actionView($id)
    {
        $model = Webinar::findOne(['id' => $id]);
        if(is_null($model)) {
            throw new NotFoundHttpException();
        }

        return $this->render('webinar', [
            'model' => $model
        ]);
    }
}
