<?php
namespace frontend\controllers;

use common\models\Diet;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Ticket controller
 */
class DietController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex($url = null)
    {
    	$user = \common\models\User::findOne(\Yii::$app->user->identity->id);
    	$course = false;
    	if($user){    		$user_anket = $user->getAnket();
    		if($user_anket and $user_anket->kal > 0 and $user_anket->status != 0) {
    			if($user_anket->vegan == 1 and $user_anket->mother == 1) {    				$page = Diet::find()->where(['<', 'min', $user_anket->kal])->andWhere(['>', 'max', $user_anket->kal])->andWhere(['vegan'=>$user_anket->vegan])->andWhere(['like', 'url', '%_'.$user_anket->week.'_'.$user_anket->day, false])->one();    			} else {		    		$page = Diet::find()->where(['<', 'min', $user_anket->kal])->andWhere(['>', 'max', $user_anket->kal])->andWhere(['vegan'=>$user_anket->vegan, 'mother'=>$user_anket->mother])->andWhere(['like', 'url', '%_'.$user_anket->week.'_'.$user_anket->day, false])->one();
				}
				if($page) {
					$course = explode('_', $page->url);
				}
				$week = $user_anket->week;
				$day = $user_anket->day;
            } else {            	$page = null;
            }
    	}

		if($url) {
			$page = Diet::findOne(['url' => $url]);
		}

        if(is_null($page)) {
            return $this->render('index');
        }

        $course = ($course)?$course:explode('_', $page->url);

        if(!$week and !$day) {        	$week = 1;
        	$day = 1;        }

        return $this->render('page', [
            'page' => $page,
            'course' => $course,
            'day' => $day,
            'week' => $week,
            'url' => $url
        ]);
    }
}