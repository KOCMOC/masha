<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Workout;
use common\models\Order;

/**
 * Workout controller
 */
class ScheduleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'week'],
                'rules' => [
                    [
                        'actions' => ['index', 'week'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($course = null, $month = null, $year = null)
    {
        $y = is_null($year)?date('Y'):$year;
        $m = is_null($month)?date('m'):$month;

        $mon = date("n", strtotime('01.'.$m.'.'.$y));
        $month = $m.'.'.$y;

		$user = \common\models\User::findOne(\Yii::$app->user->identity->id);

		$gototransaction = $user->getFinanceTransactions()
								->leftJoin('finance_transaction as invoice', 'invoice.parent = finance_transaction.id and invoice.subject_type='.\common\models\Finance_transaction::Subject_invoice)
								->where([
									'finance_transaction.status'=>\common\models\Finance_transaction::Status_progress,
									'finance_transaction.subject_type'=>\common\models\Finance_transaction::Subject_order,
									'invoice.status'=>\common\models\Finance_transaction::Status_approved
								])
								->one();
		if($gototransaction and $gototransaction->amount * -1 <= $user->getFinanceAccount()->balance) {
			$gotoorder = \common\models\Order::findOne($gototransaction->subject_id);
			if($gotoorder) {        		return $this->redirect('/course/pay/'.$gotoorder->course_group_id.'/'.$gototransaction->id);
        	}		}

		if($course) {
			$user_course = \common\models\Order::findOne($course);
			if($user_course and $user_course->active == 1 and $user_course->user_id == $user->id) {				$user->course = $course;
				$user->save();
			}		} else {			//��� �����, ��� ��� �� ��� �� ��� ���� ��� ��� ��� �����, ������ ��� ��� else		}

        $schedule = \common\models\Schedule::find()
                ->where(['user_id'=>\Yii::$app->user->identity->id])
                ->andwhere(['order_id'=>$user->course]);

        /*if(is_numeric($course) and $course > 0) {
            $schedule->joinWith('parent_workout')
                     ->andwhere(['course_id'=>$course]);
        } else {*/
            $course = false;
            $schedule->joinWith('parent_workout');
        //}

        $schedule->joinWith('parent_order');
        /*$schedule = $schedule->andwhere(['and', "MONTH(`date`)= $m", "YEAR(`date`) = $y"])
                ->all();*/
        /*####$schedule = $schedule->andwhere(['between', 'date', '2017-03-01', '2017-04-01'])
        		->all();###*/
        $schedule = $schedule->all();
        $user_courses = \common\models\Order::find()
                ->where(['user_id'=>$user->id])
                ->andwhere(['active'=>1])
                ->all();

        return $this->render('index', [
            'm' => $m,
            'y' => $y,
            'mon' => $mon,
            'month' => $month,
            'course' => $course,
            'schedule' => $schedule,
            'user_courses' => $user_courses,
            'selected_course' => $user->course,
            'quest' => $user->quest
        ]);
    }

    public function actionWeek()
    {
        if(Yii::$app->request->isAjax) {
           Yii::$app->response->format = Response::FORMAT_JSON;
           $response = array('status'=>'ok', 'errors'=>[], 'schedule'=>[]);
           $current_date = Yii::$app->request->post('current_date');
           $course = Yii::$app->request->post('course');
           if($current_date) {
                $date_arr = explode('-', $current_date);
                $w_today = date('w', strtotime($current_date));
                $w_today = ($w_today == 0)?7:$w_today;
                $week_arr = array();
                for($i=1;$i<=7;$i++) {
                    $week_arr[] = date('Y-m-d', mktime(0, 0, 0, date("m", strtotime($current_date))  , date("d", strtotime($current_date))-$w_today+$i, date("Y", strtotime($current_date))));
                }

                $schedule = \common\models\Schedule::find()
			                ->joinWith('parent_order')
			                ->where(['user_id'=>\Yii::$app->user->identity->id]);

                if(is_numeric($course) and $course > 0) {
                    $schedule->joinWith('parent_workout')
                             ->andwhere(['course_id'=>$course]);
                } else {
                    $course = false;
                    $schedule->joinWith('parent_workout');
                }

                $schedule = $schedule->andwhere(['between', "STR_TO_DATE(`date`, '%Y-%m-%d')", $week_arr[0], $week_arr[6]])
                    ->orderBy('`date`')
                    ->all();
                $schedule_arr = array();
                foreach($schedule as $key => $current) {
                    $schedule_arr[$key]['id'] = $current->id;
                    $schedule_arr[$key]['date'] = \Yii::$app->formatter->asTime(strtotime($current->date), 'php:d F Y');
                    $schedule_arr[$key]['week_day'] = \Yii::$app->formatter->asTime(strtotime($current->date), 'php:D');
                    $schedule_arr[$key]['time'] = \Yii::$app->formatter->asTime(strtotime($current->date), 'php:H:i');
                    $schedule_arr[$key]['title'] = $current->getWorkout()->title;
                    $schedule_arr[$key]['exercise_list'] = $current->getWorkout()->exercise_list;
                }
            }
            $response['schedule'] = $schedule_arr;
            return $response;
        }
    }

}
