<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => ''
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

            	'course/<id:\d+>' => 'course/view',
            	'course/buy/<sale_item_id:\d+>' => 'course/buy',
            	'course/extend/<sale_item_id:\d+>/<order_id:\d+>' => 'course/extend',
            	'course/extend/<order_id:\d+>' => 'course/extend',
            	'course/order/<id:\d+>' => 'course/order',
            	'course/pay/<course_id:\d+>/<transaction_id:\d+>' => 'course/pay',
            	'course/payto/<course_id:\d+>/<transaction_id:\d+>' => 'course/payto',
                'workout/<schedule_id:\d+>' => 'workout/index',
                'workout/<schedule_id:\d+>/<number:\d+>' => 'workout/index',
                'workout/report/<schedule_id:\d+>' => 'workout/report',
                'schedule/<course:\d+>' => 'schedule/index',
                'schedule/<year:\d+>/<month:\d+>' => 'schedule/index',
                'schedule/<course:\d+>/<year:\d+>/<month:\d+>' => 'schedule/index',
                'page/<url:[-a-zA-Z0-9_]*>' => 'page/index',
                'diet/<url:\w+>' => 'diet/index',

                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        /*'request' => [
            'baseUrl' => ''
        ]*/
    ],
    'params' => $params,
];
