<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;

/**
 * ContactForm is the model behind the contact form.
 */
class AccountForm extends Model
{
    public $fio;
    public $phone;
    public $email;
    public $password;
    //public $growth;
    public $photo;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'phone', 'email'], 'required'],
            [['email'], 'email'],
            ['fio', 'string', 'length' => [2, 255]],
            ['phone', 'string', 'max' => 20],
            //['growth', 'number','min'=>1, 'message' => 'Рост должен быть числом'],

			['password', 'string', 'min' => 6],
            //['password_confirm', 'compare', 'skipOnEmpty' => false, 'compareAttribute' => 'password', 'message'=>'Пароли не совпадают']

            [['photo'], 'safe'],
            [['photo'], 'file',  'skipOnEmpty' => false, 'extensions' => 'jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'Ваше ФИО',
            'phone' => 'Телефон',
            'phone' => 'E-mail',
            //'growth' => 'Рост',
            'password' => 'Пароль',
            //'password_confirm' => 'Подтвердите пароль'
        ];
    }

    public function init()
    {
    	$user = User::findOne(\Yii::$app->user->identity->getId());
    	$this->fio = $user->fio;
    	$this->phone = $user->phone;
    	$this->email = $user->email;
    	//$this->growth = $user->growth;
    	$this->password = '';
    	//$this->password_confirm = '';
    }

    public function upload()
    {
        if ($this->validate()) {
            $file = '/public/photo/' . Yii::$app->security->generateRandomString() . '.' . $this->photo->extension;
            if($this->photo->saveAs(Yii::getAlias('@frontend/web' . $file))){
                Image::thumbnail(Yii::getAlias('@frontend/web' . $file), 123, 123)->save(Yii::getAlias(Yii::getAlias('@frontend/web' . $file)), ['quality' => 90]);
            }
            else
                return false;

            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
            if(is_null($user))
                return false;
            else {
                if(file_exists(Yii::getAlias('@frontend/web' . $user->photo)) && is_file(Yii::getAlias('@frontend/web' . $user->photo)))
                    unlink(Yii::getAlias('@frontend/web' . $user->photo));

                $user->photo = $file;
                return ($user->save()) ? $user->photo : false;
            }
        } else {
            return false;
        }
    }

    public function save()
    {
    	$this->errorMessage = '';
    	$user = User::findOne(\Yii::$app->user->identity->getId());
    	$user->fio = $this->fio;
    	$user->phone = $this->phone;
    	//$user->growth = $this->growth;
    	if($this->password) {
    		$user->setPassword($this->password);
    	}
    	if($user->save()) {
            $this->password = '';
          	//$this->password_confirm = '';
    		return true;
    	}
    }

}
