<?php

namespace frontend\models;

use common\models\Ticket;
use common\models\TicketMessage;
use common\models\UserRole;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class TicketForm extends Model
{
    public $role;
    public $message;
    public $attachmentFiles;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'message'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role' => 'Написать',
            'message' => 'Сообщение',
        ];
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return ArrayHelper::map(UserRole::find()->all(), 'id', 'name');
    }

    /**
     * @return bool|Ticket
     */
    public function createTicket()
    {
        $ticket = new Ticket();
        $ticket->role = $this->role;
        $ticket->title = rtrim(mb_substr(strip_tags($this->message), 0, 250), "!,.-");
        $ticket->status = Ticket::STATUS_NEW;
        $ticket->user_id = Yii::$app->user->identity->getId();
        $ticket->creation_datetime = date("Y-m-d H:i:s");
        $ticket->read_datetime = date("Y-m-d H:i:s");

        if($ticket->save()) {
            $ticketMessage = new TicketMessage();
            $ticketMessage->user_id = Yii::$app->user->identity->getId();
            $ticketMessage->ticket_id = $ticket->id;
            $ticketMessage->message = $this->message;
            $ticketMessage->creation_datetime = date("Y-m-d H:i:s");
            $ticketMessage->read_datetime = date("Y-m-d H:i:s");

            if($ticketMessage->save()) {
                $ticketMessage->attachmentFiles = $this->attachmentFiles;
                $ticketMessage->attachment = $ticketMessage->upload();
                $ticketMessage->save();
                return $ticket;
            }
        }

        return false;
    }
}
