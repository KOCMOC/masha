<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Delivery;

/**
 * ContactForm is the model behind the contact form.
 */
class EmailCollectForm extends Model
{
    public $fio;
    public $email;
    public $phone;
    public $errorMessage;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['fio', 'email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['fio', 'string', 'length' => [2, 255]],
            ['phone', 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'Ф И О',
            'phone' => 'Телефон',
            'email' => 'E-mail'
        ];
    }

    public function collect()
    {
    	/*$f = fopen('emails.log');
    	fwrite($f, );*/

    	$this->errorMessage = '';    	$delivery = new Delivery();
    	$delivery->fio = $this->fio;
    	$delivery->email = $this->email;
    	$delivery->phone = $this->phone;
    	if($delivery->save()) {    		return true;    	} else {
    		foreach($delivery->errors as $error) {    			$this->errorMessage .= implode('<br />', $error).'<br />';    		}    		return false;    	}    }
}
