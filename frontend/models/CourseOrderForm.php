<?php

namespace frontend\models;

use common\models\UserAnket;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CourseOrderForm extends Model
{

	public $fio;
	public $born;
	public $country;
	public $city;
	public $insta;
	public $vk;
    public $gender;
    public $growth;
    public $weight;
    public $mother;
    public $vegan;
    public $goal;
    public $breast_size;
    public $waist_size;
    public $hip_size;

    public $most_hip_size;
    public $medical;
    public $activity;

    public $operating_weight; // рудимент

    public $level;
    public $place;
    public $weekdays;

    private $_userAnket;

    public function init()
    {
        $this->_userAnket = UserAnket::findOne(['user_id' => Yii::$app->user->identity->getId()]);
        if(is_null($this->_userAnket)) {
            $this->_userAnket = new UserAnket();
            $this->_userAnket->user_id = Yii::$app->user->identity->getId();
        }

        $this->fio = $this->_userAnket->fio;
        $this->born = date('d.m.Y', strtotime($this->_userAnket->born));
        $this->country = $this->_userAnket->country;
        $this->city = $this->_userAnket->city;
        $this->insta = $this->_userAnket->insta;
        $this->vk = $this->_userAnket->vk;
        $this->vegan = $this->_userAnket->vegan;
        $this->mother = $this->_userAnket->mother;

        $this->gender = $this->_userAnket->gender;
        $this->growth = $this->_userAnket->growth;
        $this->weight = $this->_userAnket->weight;
        $this->breast_size = $this->_userAnket->breast_size;
        $this->waist_size = $this->_userAnket->waist_size;
        $this->hip_size = $this->_userAnket->hip_size;
        $this->operating_weight = $this->_userAnket->operating_weight;
        $this->goal = $this->_userAnket->goal;

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender', 'level', 'weekdays', 'born', 'growth', 'weight', 'breast_size', 'waist_size', 'hip_size', 'operating_weight', 'goal', 'mother', 'vegan'], 'required'],
            [['gender', 'level', 'place', 'growth', 'weight', 'breast_size', 'waist_size', 'hip_size', 'operating_weight', 'goal', 'mother', 'vegan'], 'integer'],
            ['weekdays', 'in', 'range' => [1,2,3,4,5,6,0], 'allowArray' => true],
            [['country', 'city', 'insta', 'vk', 'fio'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'fio' => 'ФИО',
			'born' => 'Дата рождения',
			'country' => 'Страна',
			'city' => 'Город',
			'insta' => 'Аккаунт в инстаграм',
			'vk' => 'Страница в ВК',
			'mother' => 'Кормите ли вы грудью?',
			'vegan' => 'Вы вегетарианец?',
			'city' => 'Город',

			'most_hip_size' => 'Объем бедра в самом широком месте, см',
			'medical' => 'Медицинские противопоказания для занятия спортом',
			'activity' => 'Оцените Ваш уровень повседневной физической активности',

            'gender' => 'Пол',
            'level' => 'Уровень',
            'place' => 'Место',
            'weekdays' => 'Тренировки',

            // User anket
            'growth' => 'Рост, см',
            'weight' => 'Вес, кг',
            'breast_size' => 'Объём груди, см',
            'waist_size' => 'Объём талии, см',
            'hip_size' => 'Объём бёдер, см',
            'operating_weight' => 'Рабочий вес',
            'goal' => 'Цель',
        ];
    }

    public function getWeekdays() {
    	return [
    		'1' => 'Пн',
    		'2' => 'Вт',
    		'3' => 'Ср',
    		'4' => 'Чт',
    		'5' => 'Пт',
    		'6' => 'Сб',
    		'0' => 'Вс'
    	];
    }

    public function order($order)
    {

    	$this->_userAnket->fio = $this->fio;
    	$this->_userAnket->born = date('Y-m-d', strtotime($this->born));
    	$this->_userAnket->country = $this->country;
    	$this->_userAnket->city = $this->city;
    	$this->_userAnket->insta = $this->insta;
    	$this->_userAnket->vk = $this->vk;
        $this->_userAnket->gender = $this->gender;
        $this->_userAnket->growth = $this->growth;
        $this->_userAnket->weight = $this->weight;
        $this->_userAnket->mother = $this->mother;
        $this->_userAnket->vegan = $this->vegan;
        $this->_userAnket->breast_size = $this->breast_size;
        $this->_userAnket->waist_size = $this->waist_size;
        $this->_userAnket->hip_size = $this->hip_size;
        $this->_userAnket->operating_weight = $this->operating_weight;
        $this->_userAnket->goal = $this->goal;
        if(Yii::$app->request->post('kal') > 0) {
	        $this->_userAnket->kal = Yii::$app->request->post('kal');
	        $this->_userAnket->day = 1;
	        $this->_userAnket->week = 1;
	        $this->_userAnket->status = 1;
        }
        $this->_userAnket->save();

    /*public $most_hip_size;
    public $medical;
    public $activity;  */

        $order->level = $this->level;
        $order->gender = $this->gender;
        $order->place = $this->place;
        $order->schedule_template = implode(',', $this->weekdays);
        if($order->save()) {
        	if($order->expire > 0) {
            	$course = \common\models\Course::find()->where([/*'gender'=>$order->gender, */'level'=>$order->level, 'place'=>$order->place, 'course_group_id'=>$order->course_group_id])->one();
            	if($course) {
            		$course->order($order);
            	}
        	}
        	return true;
        } else {
        	return false;
        }
    }
}
