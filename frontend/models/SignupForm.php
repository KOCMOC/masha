<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\Finance_account;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $fio;
    public $email;
    public $phone;
    public $password;
    //public $password_confirm;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['fio', 'trim'],
            ['fio', 'required'],
            //['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот имя уже есть'],
            ['fio', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот емейл уже используется'],

            ['phone', 'trim'],
            ['phone', 'required'],
            ['phone', 'string', 'max' => 25],

            /*['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'message'=>'Пароли не совпадают']*/
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'Ф И О',
            'phone' => 'Телефон',
            'email' => 'E-mail'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->email = $this->email;
        $user->fio = $this->fio;
        $user->phone = $this->phone;
        $user->status = User::STATUS_ACTIVE;
        $this->password = $user->generatePassword();
        $user->setPassword($this->password);
        $user->generateAuthKey();

        // Set free trainer
        $user->setTrainer();

        $returnUser = $user->save() ? $user : null;

        if($returnUser) {
        	$account = new Finance_account();
            $account->user_id = $returnUser->id;
            $account->balance = 0;
            $account->status = 1;
            $account->save();
        	if(Yii::$app->params['setNopayCourse'] === true) {
	            /* снова ёбаный хардкод */
	            $today = date('w');
				$aftertomorrow = date('w', strtotime('2 day'));
				$expire = strtotime('2 day');

				$order = new \common\models\Order();
				$order->course_group_id = 2;
				$order->promocode = 0;
				$order->level = 1;
				$order->gender = 2;
				$order->place = 1;
				$order->discount = 0;
				$order->user_id = $returnUser->id;
				$order->type = 0;
				$order->schedule_template = $today.','.$aftertomorrow;
				$order->active = 1;
				$order->amount = 0;
				$order->expire = $expire;

				if($order->save()) {
					$order->refresh();
					$course = \common\models\Course::findOne(3);
					$course->order($order, $returnUser->id);
				}
				/* хардкод когда-нибудь кончается */
				#но это, блядь, не наш случай
			}
        }

        return $returnUser;
    }
}
