<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }
                    /*Yii::$app->get('mail');
                        $mailer->compose()
                            ->setTo($this->email)
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminEmail']])
                            ->setSubject('Тестовая тема')
                            ->setHtmlBody('Тестовое<hr>контенто')
                            //->setHtmlBody($this->renderPartial('/mail/registration', ['siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$password]))
							->send(); */
        $mailer = Yii::$app->get('mail');
        $mailer->htmlLayout = '@common/mail/layouts/html';
        $mailer->textLayout = '@common/mail/layouts/text';
        return  $mailer
        	->compose(
                ['html' => '@common/mail/passwordResetToken-html', 'text' => '@common/mail/passwordResetToken-text', 'htmlLayout' => '@common/mail/layouts/html' , 'textLayout' => '@common/mail/layouts/text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['adminEmail']=>Yii::$app->params['adminEmail']])
            ->setTo($this->email)
            ->setSubject('Смена пароля для ' . Yii::$app->params['siteData']['name'])
            ->send();
    }
}
