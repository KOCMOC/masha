$(document).ready(function(){
    $('.fancybox').fancybox();
    $(document).delegate('#delivery-form', 'submit', function () {
        var flag = true;
        var name = $('#emailcollectform-fio');
        var phone = $('#emailcollectform-phone');
        var email = $('#emailcollectform-email');

        if (name.val() === '') {
            name.addClass('error');
            flag = false;
        } else {
            name.removeClass('error');
        }
        if (phone.val() === '') {
            phone.addClass('error');
            flag = false;
        } else {
            phone.removeClass('error');
        }
        if (email.val() === '') {
            email.addClass('error');
            flag = false;
        } else {
            email.removeClass('error');
        }

        if (flag) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function (r) {
                    if(r.status == 'ok') {
	                    $('#emailcollectform-fio').val('');
	                    $('#emailcollectform-phone').val('');
	                    $('#emailcollectform-email').val('');
                    	var msg = 'Ваш запрос<br>успешно отправлен!';
                    } else {
                    	var msg = r.message;
                    }
                    $.fancybox('<h2 class="text-success no-margin">'+msg+'</h2>');
                    setTimeout(function () {
                        $.fancybox.close(true);
                    }, 5000);
                }
            });
        }
        return false;
    });
});
