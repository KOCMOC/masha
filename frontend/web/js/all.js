    var todayDate = false;
    var YM = false;
    var YESTERDAY = false;
    var TODAY = false;
    var TOMORROW = false;
$(function() {

    todayDate = moment().startOf('day');
    YM = todayDate.format('YYYY-MM');
    YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
    TODAY = todayDate.format('YYYY-MM-DD');
    TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

    $('#calendar').fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        locale: 'ru',
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        navLinks: true,
        events: [],
        allDayDefault: false,
        dayClick: function(date, jsEvent, view) {

            //console.log('Clicked on: ' + date.format());
            //console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

            var offset = $(this).offset();
            if($(this).attr('data-day')){
                $('#popup').css({"top" : offset.top + $(this).outerHeight()/2, "left" : offset.left + $(this).outerWidth()/2,
                    "right" : offset.left + $(this).width + 30, bottom: offset.top + $(this).height + 30})
                    .html("<div class='time'>"+$(this).attr('data-time')+"</div><div class='day'>"+$(this).attr('data-day')+"</div><div class='"+$(this).attr('data-icon')+"'></div><div class='description'>"+$(this).attr('data-description')+"</div>").show();
            }else{
                $('#popup').css({"top" : offset.top + $(this).outerHeight()/2, "left" : offset.left + $(this).outerWidth()/2,
                    "right" : offset.left + $(this).width + 30, bottom: offset.top + $(this).height + 30})
                    .html("<br />На текущий день событий не запланировано").show();
            }
            $('.shadow').show();
            //console.log('Current view: ' + view.name);
        },
        eventClick: function(date, jsEvent, view) {

            //console.log('Clicked on: ' + date.format());
            //console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

            var offset = $(this).offset();
            if($(this).attr('data-day')){
                $('#popup').css({"top" : offset.top + $(this).outerHeight()/2, "left" : offset.left + $(this).outerWidth()/2,
                    "right" : offset.left + $(this).width + 30, bottom: offset.top + $(this).height + 30})
                    .html("<div class='time'>"+$(this).attr('data-time')+"</div><div class='day'>"+$(this).attr('data-day')+"</div><div class='"+$(this).attr('data-icon')+"'></div><div class='description'>"+$(this).attr('data-description')+"</div>").show();
            }else{
                $('#popup').css({"top" : offset.top + $(this).outerHeight()/2, "left" : offset.left + $(this).outerWidth()/2,
                    "right" : offset.left + $(this).width + 30, bottom: offset.top + $(this).height + 30})
                    .html("<br />На текущий день событий не запланировано").show();
            }
            $('.shadow').show();
            //console.log('Current view: ' + view.name);
        },
        eventAfterRender: function(event, element, view) {
            var itemDate = event._start._i.substring(0, 10);
            var itemDay = new Date(itemDate).getDate();
            var itemTime = event._start._i.substring(11).split(':');
            itemTime.pop();
            itemTime = itemTime.join(':');
            element.attr({
            	"data-date": itemDate,
                "data-title": event.title,
                "data-description": event.description,
                "data-day": itemDay,
                "data-time": itemTime,
                "data-icon": event.className
            });
            element.find('.fc-time').text(''/*itemTime*/);
            $('.fc td.fc-day[data-date="'+itemDate+'"]').addClass(event.className + '').attr({
                "data-title": event.title,
                "data-description": event.description,
                "data-day": itemDay,
                "data-time": itemTime,
                "data-icon": event.className
            });
        }
    });
});
$(document).ready(function(){
    $('body').on('click', '.burger', function(){
        $('.sidebar').toggleClass('active');
        $('.shadow').show();
        return false;
    });
    $('body').on('click', '.sidebar .nav_header .burg, .shadow', function(){
        $('.sidebar').toggleClass('active');
        $('.shadow').hide();
        return false;
    });

    $('body').on('click', '.shadow', function(){
        $('.popup').hide();
        return false;
    });

    setTimeout(function(){
        $('.fc-day.fc-today').append('<div class="today">&nbsp;</div>');
        $('.today').css('height', $('.fc-day.fc-today').height()-2);
    },500);
    $(window).resize(function(){
        setTimeout(function(){
            $('.today').remove();
            $('.fc-day.fc-today').append('<div class="today">&nbsp;</div>');
            $('.today').css('height', $('.fc-day.fc-today').height()-2);
        },200);
    });
});

var scrollCurrent = 0;
$(document).ready(function(){
	scrollCurrent = $(window).scrollTop();
});

// Мобильное меню
$(document).ready(function() {
    $('nav.main-sidebar').css('height', $('.main-wrapper').height());
    $('nav.main-sidebar').slideAndSwipe();
});