var scroller;
$(document).ready(function() {
    //scroller = $('.scroll-pane').jScrollPane();
    $('.slider').slick();
    $('.fancybox').fancybox();
    $('.count').countdown({
        date: new Date('24 Feb 2017').valueOf(),//24 * 60 * 60 * 1000 + new Date().valueOf(),
        render: function(data) {
            $(this.el).html("<div>" + this.leadingZeros(data.days, 2) + ":</div><div>" + this.leadingZeros(data.hours, 2) + ":</div><div>" + this.leadingZeros(data.min, 2) + ":</div><div>" + this.leadingZeros(data.sec, 2) + "</div>");
        }
    });

    $('body').on('beforeSubmit', 'form#login-form', function () {

        var form = $(this);

        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                // do something with response
            }
        });
        return false;
    });

    $('body').on('beforeSubmit', 'form#signup-form, form#signup-form-popup', function () {

        var form = $(this);

        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                // do something with response
            }
        });
        return false;
    });

    $('body').on('change', 'form#login-form input.form-control', function() {    	$('#login-form-validation').val('yes');    });

    $('body').on('mousedown', 'form#login-form button[type=submit]', function() {
    	$('#login-form-validation').val('no');
    });

    /*$('body').on('submit', 'form', function () {
        var flag = true;
        var name = $(this).find('[name="name"]');
        var phone = $(this).find('[name="phone"]');
        var country = $(this).find('[name="country"]');

        if (name.val() === '') {
            name.addClass('input_error');
            flag = false;
        } else {
            name.removeClass('input_error');
        }
        if (phone.val() === '') {
            phone.addClass('input_error');
            flag = false;
        } else {
            phone.removeClass('input_error');
        }
        if (phone.val() === '') {
            country.addClass('input_error');
            flag = false;
        } else {
            country.removeClass('input_error');
        }

        if (true) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'text',
                success: function (r) {
                    name.val('');
                    phone.val('');
                    country.val('');
                    $.fancybox(' <div id="popup_thx" class="popup_thx" style="display: block"> <h2>спасибо,</h2> <p>Ваша заявка принята,<br /> мы свяжемся с вами в ближайшее время</p> </div>');
                    setTimeout(function () {
                        $.fancybox.close(true);
                    }, 5000);
                }
            });
        }
        return false;
    });*/
});
$(window).resize(function(){
    //scroller.data('jsp').reinitialise();
});