<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use \common\models\Order;
use \common\models\Schedule;
use \common\models\User;
use \common\models\UserAnket;

class DailyController extends Controller
{
	public function actionSchedule() {
        $schedules = Schedule::find()->where(['date'=>date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), date('d'), date('Y')))])->all();

		if(sizeof($schedules) > 0) {
	        foreach($schedules as $schedule) {
	        	echo $schedule->id.chr(10);        		$schedule->prepare();	        }
        }
    }

    public function actionSch($id = false) {    	if($id !== false) {    		$schedule = Schedule::findOne($id);
    		if($schedule) {    			$schedule->prepare();    		}    	}    }

    public function actionNopayschedule($user_id) {
		$user = User::findOne($user_id);
		if($user) {	    	$today = date('w');
			$aftertomorrow = date('w', strtotime('2 day'));
			$expire = strtotime('2 day');

			$order = new \common\models\Order();
			$order->course_group_id = 2;
			$order->promocode = 0;
			$order->level = 1;
			$order->gender = 2;
			$order->place = 1;
			$order->discount = 0;
			$order->user_id = $user->id;
			$order->type = 0;
			$order->schedule_template = $today.','.$aftertomorrow;
			$order->active = 1;
			$order->amount = 0;
			$order->expire = $expire;

			if($order->save()) {
				echo $user_id.' - ok'.chr(10);
				$order->refresh();
				$course = \common\models\Course::findOne(3);
				$course->order($order, $user->id);
			} else {				echo $user_id.' - fuck'.chr(10);			}
		}    }

    public function actionPerm() {    	$users = User::find()->where(['!=', 'id', '1'])->orderBy('created_at')->all();
    	foreach($users as $user) {    		if(!$user->getOrders()->where(['course_group_id'=>2])->count()) {            	$this->actionNopayschedule($user->id);    		}    	}    }

    public function actionUserdayup() {    	$ankets = UserAnket::find()->where(['>', 'kal', '0'])->andWhere(['status'=>1])->all();
    	foreach($ankets as $anket) {    		$anket->day++;
    		if($anket->day > 7) {    			$anket->day = 1;
    			$anket->week++;
    			if($anket->week > 4) {    				$anket->week = 1;    			}    		}
    		$anket->save();    	}    }

    public function actionSchs($date = false) {    	$date = ($date)?$date:date('Y-m-d');
   		$schedules = Schedule::find()
   						->select('schedule.*, report.id repid, count(1) reps')
   						->leftJoin('report', 'report.id_schedule = schedule.id')
   						->where(['date'=>$date])
   						->groupBy('schedule.id')
   						->having(['reps'=>1, 'repid'=>null])
   						->all();
   		if($schedules and is_array($schedules) and sizeof($schedules) > 0) {
	   		foreach($schedules as $schedule) {
	   			echo $schedule->id.chr(10);	   			$schedule->prepare();	   		}
   		}    }

    public function actionExp($date = false) {    	$date = ($date)?$date:date('Y-m-d');
    	$udate = strtotime($date);
    	$orders = Order::find()
    				->where(['active'=>1])
    				->andWhere(['!=', 'course_group_id', 2]) // ������ �������
    				->andWhere(['<', 'expire', $udate])
    				->all();
   		if($orders and is_array($orders) and sizeof($orders) > 0) {
	   		foreach($orders as $order) {
	   			echo $order->id.chr(10);
	   			$user = $order->getUser();
	   			$order->active = 2;
	   			$order->save();
	   			$user_orders = $user->getOrders()
	   								->where(['active'=>1])
	   								->andWhere(['!=', 'course_group_id', '2']) // ����� �������
	   								->count();
            	if($user_orders == 0) {            		$anket = $user->getAnket();
            		if($anket) {
	            		$anket->status = 0;
	                	$anket->save();
                	}            	}

	   		}
   		}    }

    public function actionOrd($order_id) {
    	$order = Order::findOne($order_id);
    	if($order) {    		$course = \common\models\Course::find()->where(['course_group_id'=>$order->course_group_id, 'level'=>$order->level, 'place'=>$order->place])->one();

	    	$user = $order->getUser();

	    	if($course and $order and $user) {
	    		$course->order($order->id, $user->id);
	    	}
	    }    }
}