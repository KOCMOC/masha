<?php
namespace console\controllers;

use yii\console\Controller;
use console\models\MailChimp;
use common\models\MailingList;
use common\models\Order;
use common\models\User;

/**
 * Cron controller
 */
class CronController extends Controller
{
    public function actionMailchimp() {
        $mailchimp = new MailChimp(\Yii::$app->params['mailchimp']['apiKey']);

        $mailing_list = [];
        $mailing_list_all = MailingList::find()->all();
        foreach ($mailing_list_all as $item) {
            $mailing_list[$item->user_id] = $item;
        }

        $user_list_all = User::find()->where(['status' => User::STATUS_ACTIVE])->all();

        foreach ($user_list_all as $user) {
            $md5_hash = md5(mb_strtolower($user->email));

            if(!isset($mailing_list[$user->id])) {
                $mailing_list_user = new MailingList();
                $mailing_list_user->user_id = $user->id;
                $mailing_list_user->email = $user->email;
                $mailing_list_user->save();
            } else {
                $mailing_list_user = $mailing_list[$user->id];
            }

            $orders = Order::find()
                ->where(['!=', 'course_group_id', 2])
                ->andWhere(['>', 'expire', time()])
                ->andWhere(['user_id' => $user->id])
                ->andWhere(['active' => 1])
                ->all();

            $orders_expired = Order::find()
                ->where(['!=', 'course_group_id', 2])
                ->andWhere(['<=', 'expire', time()])
                ->andWhere(['user_id' => $user->id])
                ->andWhere(['!=', 'active', 0])
                ->all();

            if(!$mailing_list_user->is_active and !empty($orders)) {
                // Добавление в active список и удаление из expired, если нужно

                if($mailing_list_user->is_expired) {
                    $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_expired'], $md5_hash);
                    $mailing_list_user->is_expired = 0;
                }
                if($mailing_list_user->is_subscriber) {
                    $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_allsubscribers'], $md5_hash);
                    $mailing_list_user->is_subscriber = 0;
                }

                $doc = $mailchimp->new_list_member(\Yii::$app->params['mailchimp']['list_id_actual'], $user->email);
                $mailing_list_user->is_active = 1;
            }

            if(!$mailing_list_user->is_expired && !empty($orders_expired) && empty($orders)) {
                // Добавление в expired список и удаление из active, если нужно

                if($mailing_list_user->is_active) {
                    $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_actual'], $md5_hash);
                    $mailing_list_user->is_active = 0;
                }
                if($mailing_list_user->is_subscriber) {
                    $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_allsubscribers'], $md5_hash);
                    $mailing_list_user->is_subscriber = 0;
                }

                $doc = $mailchimp->new_list_member(\Yii::$app->params['mailchimp']['list_id_expired'], $user->email);
                $mailing_list_user->is_expired = 1;
            }

            if(
                !$mailing_list_user->is_subscriber
                and !$mailing_list_user->is_active
                and !$mailing_list_user->is_expired
                and empty($orders_expired)
                and empty($orders)
            ) {
                $doc = $mailchimp->new_list_member(\Yii::$app->params['mailchimp']['list_id_allsubscribers'], $user->email);
                $doc = json_decode($doc, 1);
                $mailing_list_user->is_subscriber = 1;
            }

            $mailing_list_user->save();
        }

        // Обрабатываем удаленных пользователей

        $user_list_deleted = User::find()->where(['status' => User::STATUS_DELETED])->all();

        foreach($user_list_deleted as $user) {
            $md5_hash = md5(mb_strtolower($user->email));

            $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_allsubscribers'], $md5_hash);
            $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_actual'], $md5_hash);
            $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_expired'], $md5_hash);
        }

        $user_list = [];
        foreach ($user_list_all as $user) {
            $user_list[$user->id] = $user;
        }
        foreach ($mailing_list as $user_id => $item) {
            if(!isset($user_list[$user_id])) {
                $email = $item->email;
                $md5_hash = md5(mb_strtolower($email));

                $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_allsubscribers'], $md5_hash);
                $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_actual'], $md5_hash);
                $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_expired'], $md5_hash);
            }
        }
    }

    public function actionMailingfix() {
        $mailchimp = new MailChimp(\Yii::$app->params['mailchimp']['apiKey']);

        $users = MailingList::find()
            ->where(['is_active' => 1])
            ->orWhere(['is_expired' => 1])
            ->all();

        foreach ($users as $user) {
            $md5_hash = md5(mb_strtolower($user->email));
            $mailchimp->delete_member_from_list(\Yii::$app->params['mailchimp']['list_id_allsubscribers'], $md5_hash);
        }
    }
}