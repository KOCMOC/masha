<?php

use yii\db\Schema;
use yii\db\Migration;

class m161012_112005_exercises_to_course_customisation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%course_training_routine}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_relation' => $this->integer()->unsigned()->notNull(),
            'type' => $this->smallInteger(1)->unsigned()->notNull(),
            'set' => $this->smallInteger()->unsigned()->notNull(),
            'sleep' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('{{%course_set}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_set_attr' => $this->integer()->unsigned()->notNull(),
            'id_c_tr_rout' => $this->integer()->unsigned()->notNull(),
            'default_val' => $this->string(10)->notNull()
        ], $tableOptions);

		return true;
    }

    public function down()
    {
        $this->dropTable('{{%course_training_routine}}');

        $this->dropTable('{{%course_set}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
