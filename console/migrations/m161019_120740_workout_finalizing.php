<?php

use yii\db\Schema;
use yii\db\Migration;

class m161019_120740_workout_finalizing extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%workout_report_field}}', [
            'id' => $this->primaryKey()->unsigned(),
            'workout_id' => $this->integer()->unsigned()->notNull(),
            'title' => $this->string(255)->notNull(),
            'type' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%schedule_report}}', [
            'id' => $this->primaryKey()->unsigned(),
            'schedule_id' => $this->integer()->unsigned()->notNull(),
            'workout_report_field_id' => $this->string(255)->notNull(),
            'value' => $this->text()
        ], $tableOptions);

        $this->createTable('{{%workout_report}}', [
            'id' => $this->primaryKey()->unsigned(),
            'workout_id' => $this->integer()->unsigned()->notNull(),
            'workout_report_field_id' => $this->string(255)->notNull()
        ], $tableOptions);

		$this->addColumn('{{%schedule}}', 'workout_time', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `done`');
		$this->addColumn('{{%schedule}}', 'workout_calories', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `workout_time`');

        return true;

    }

    public function down()
    {
        $this->dropTable('{{%workout_report_field}}');
        $this->dropTable('{{%schedule_report}}');
        $this->dropTable('{{%workout_report}}');

        $this->dropColumn('{{%schedule}}', 'workout_time');
        $this->dropColumn('{{%schedule}}', 'workout_calories');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
