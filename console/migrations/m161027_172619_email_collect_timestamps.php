<?php

use yii\db\Schema;
use yii\db\Migration;

class m161027_172619_email_collect_timestamps extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%delivery}}', 'created_at', Schema::TYPE_INTEGER . '(11) NOT NULL AFTER email');
		$this->addColumn('{{%delivery}}', 'updated_at', Schema::TYPE_INTEGER . '(11) NOT NULL AFTER created_at');

		return true;
    }

    public function down()
    {
        $this->dropColumn('{{%delivery}}', 'created_at');
        $this->dropColumn('{{%delivery}}', 'updated_at');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
