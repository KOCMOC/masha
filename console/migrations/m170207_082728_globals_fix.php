<?php

use yii\db\Schema;
use yii\db\Migration;

class m170207_082728_globals_fix extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%global_field}}', 'id_course', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id`');

    	return true;
    }

    public function down()
    {
        $this->dropColumn('{{%global_field}}', 'id_course');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
