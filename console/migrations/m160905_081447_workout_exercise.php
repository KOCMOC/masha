<?php

use yii\db\Migration;

class m160905_081447_workout_exercise extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%workout_exercise}}', [
            'id' => $this->primaryKey()->unsigned(),
            'workout_id' => $this->integer()->unsigned()->notNull(),
            'exercise_id' => $this->integer()->unsigned()->notNull(),
            'order' => $this->integer()->unsigned()->notNull(),
        ], $tableOptions); 
    }

    public function down()
    {
       $this->dropTable('{{%workout_exercise}}');
    }
}
