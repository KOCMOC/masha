<?php

use yii\db\Schema;
use yii\db\Migration;

class m161018_081152_exercise_alert extends Migration
{
    public function up()
    {
		$this->addColumn('{{%training_routine}}', 'alert_proc', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');
		$this->addColumn('{{%course_training_routine}}', 'alert_proc', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');
		$this->addColumn('{{%schedule_training_routine}}', 'alert_proc', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');

		$this->addColumn('{{%workout_set}}', 'alert_proc', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');
		$this->addColumn('{{%schedule_workout_set}}', 'alert_proc', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');


		$this->addColumn('{{%training_routine}}', 'alert_sleep', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');
		$this->addColumn('{{%course_training_routine}}', 'alert_sleep', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');
		$this->addColumn('{{%schedule_training_routine}}', 'alert_sleep', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');

		$this->addColumn('{{%workout_set}}', 'alert_sleep', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');
		$this->addColumn('{{%schedule_workout_set}}', 'alert_sleep', Schema::TYPE_STRING . '(255) DEFAULT "" AFTER sleep');

		return true;
    }

    public function down()
    {
        $this->dropColumn('{{%training_routine}}', 'alert_proc');
        $this->dropColumn('{{%course_training_routine}}', 'alert_proc');
        $this->dropColumn('{{%schedule_training_routine}}', 'alert_proc');

        $this->dropColumn('{{%workout_set}}', 'alert_proc');
        $this->dropColumn('{{%schedule_workout_set}}', 'alert_proc');


        $this->dropColumn('{{%training_routine}}', 'alert_sleep');
        $this->dropColumn('{{%course_training_routine}}', 'alert_sleep');
        $this->dropColumn('{{%schedule_training_routine}}', 'alert_sleep');

        $this->dropColumn('{{%workout_set}}', 'alert_sleep');
        $this->dropColumn('{{%schedule_workout_set}}', 'alert_sleep');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
