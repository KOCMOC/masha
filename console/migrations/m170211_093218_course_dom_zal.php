<?php

use yii\db\Schema;
use yii\db\Migration;

class m170211_093218_course_dom_zal extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%course}}', 'place', Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0 AFTER gender');
    	$this->addColumn('{{%order}}', 'place', Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0 AFTER gender');

    	$this->addColumn('{{%user_global_field}}', 'title', Schema::TYPE_STRING . '(255) NOT NULL DEFAULT "" AFTER id_global_field');
    	$this->addColumn('{{%user_global_field}}', 'alias', Schema::TYPE_STRING . '(10) NOT NULL DEFAULT "" AFTER title');

    	return true;
    }

    public function down()
    {
        $this->dropColumn('{{%course}}', 'place');
        $this->dropColumn('{{%order}}', 'place');

        $this->dropColumn('{{%user_global_field}}', 'title');
        $this->dropColumn('{{%user_global_field}}', 'alias');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
