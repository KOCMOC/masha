<?php

use yii\db\Migration;

class m161230_021709_ticket extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ticket}}', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->notNull(),
            'role' => $this->smallInteger(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'creation_datetime' => $this->dateTime()->notNull(),
            'read_datetime' => $this->dateTime()->notNull(),
            'evaluation' => $this->smallInteger(),
        ], $tableOptions);

        $this->createTable('{{%ticket_message}}', [
            'id' => $this->primaryKey()->unsigned(),
            'ticket_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'creation_datetime' => $this->dateTime()->notNull(),
            'read_datetime' => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->createIndex('FK_ticket_message', '{{%ticket_message}}', 'ticket_id');
        $this->addForeignKey(
            'FK_ticket_message', '{{%ticket_message}}', 'ticket_id', '{{%ticket}}', 'id', 'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropTable('{{%ticket_message}}');
        $this->dropTable('{{%ticket}}');
    }
}
