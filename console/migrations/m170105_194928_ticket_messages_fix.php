<?php

use yii\db\Migration;

class m170105_194928_ticket_messages_fix extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%ticket}}', 'description');
        $this->dropColumn('{{%ticket_message}}', 'title');
        $this->renameColumn('{{%ticket_message}}', 'description', 'message');
        $this->addColumn('{{%ticket_message}}', 'attachment', $this->text());
    }

    public function down()
    {
        echo "m170105_194928_ticket_messages_fix cannot be reverted.\n";

        return false;
    }
}
