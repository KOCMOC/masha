<?php

use yii\db\Migration;

class m160823_080140_set_attribute extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%set_attribute}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255)->notNull(),
            'unit' => $this->string(10)->notNull()
        ], $tableOptions); 
    }

    public function down()
    {
       $this->dropTable('{{%set_attribute}}');
    }
}
