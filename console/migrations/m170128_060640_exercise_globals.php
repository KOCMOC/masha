<?php

use yii\db\Schema;
use yii\db\Migration;

class m170128_060640_exercise_globals extends Migration
{
    public function up()
    {
		$this->addColumn('{{%user_global_field}}', 'id_order', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id`');

		$this->addColumn('{{%course_global_field}}', 'id_exercise', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id_course`');
		$this->addColumn('{{%course_global_field}}', 'default', Schema::TYPE_STRING . '(10) NOT NULL DEFAULT "" AFTER `id_global_field`');

		$this->addColumn('{{%global_field}}', 'id_exercise', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id`');

    	return true;
    }

    public function down()
    {
        $this->dropColumn('{{%user_global_field}}', 'id_order');

        $this->dropColumn('{{%course_global_field}}', 'id_exercise');
        $this->dropColumn('{{%course_global_field}}', 'default');

        $this->dropColumn('{{%global_field}}', 'id_exercise');

        return true;
    }

}
