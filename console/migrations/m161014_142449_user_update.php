<?php

use yii\db\Schema;
use yii\db\Migration;

class m161014_142449_user_update extends Migration
{
    public function up()
    {

		$this->addColumn('{{%user}}', 'fio', Schema::TYPE_STRING . '(10) AFTER email');

		return true;

    }

    public function down()
    {

    	$this->dropColumn('{{%user}}', 'fio');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
