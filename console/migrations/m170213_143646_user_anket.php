<?php

use yii\db\Migration;

class m170213_143646_user_anket extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_anket}}', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->notNull(),
            'level' => $this->smallinteger(2),
            'gender' => $this->smallinteger(2),
            'growth' => $this->integer(),
            'weight' => $this->integer(),
            'breast_size' => $this->integer(),
            'waist_size' => $this->integer(),
            'hip_size' => $this->integer(),
            'operating_weight' => $this->integer(),
            'goal' => $this->string(),
            'training_days' => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user_anket}}');
    }
}
