<?php

use yii\db\Migration;

class m160823_081329_set extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%set}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_set_attr' => $this->integer()->unsigned()->notNull(),
            'id_tr_rout' => $this->integer()->unsigned()->notNull(),
            'default_val' => $this->string(10),
            'real_val' => $this->string(10),
        ], $tableOptions); 
    }

    public function down()
    {
       $this->dropTable('{{%set}}');
    }
}
