<?php

use yii\db\Schema;
use yii\db\Migration;

class m161015_120951_course_schedue_template extends Migration
{
    public function up()
    {
		$this->addColumn('{{%course}}', 'schedule_template', Schema::TYPE_STRING . '(255) AFTER description');

        return true;
    }

    public function down()
    {

        $this->dropColumn('{{%course}}', 'schedule_template');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
