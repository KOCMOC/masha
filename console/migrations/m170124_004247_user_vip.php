<?php

use yii\db\Migration;

class m170124_004247_user_vip extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'is_vip', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'is_vip');
    }
}
