<?php

use yii\db\Schema;
use yii\db\Migration;

class m170107_171937_course_update extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

		$this->addColumn('{{%set}}', 'trigger', Schema::TYPE_STRING . '(20) NOT NULL DEFAULT "" AFTER default_val');
		$this->addColumn('{{%course_set}}', 'trigger', Schema::TYPE_STRING . '(20) NOT NULL DEFAULT "" AFTER default_val');
		$this->addColumn('{{%schedule_set}}', 'trigger', Schema::TYPE_STRING . '(20) NOT NULL DEFAULT "" AFTER default_val');

		return true;
    }

    public function down()
    {
        $this->dropColumn('{{%set}}', 'trigger');
        $this->dropColumn('{{%course_set}}', 'trigger');
        $this->dropColumn('{{%schedule_set}}', 'trigger');

        return true;
    }

}
