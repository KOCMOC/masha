<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m170213_165252_user_anket_fix extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%user_anket}}', 'level');
        $this->dropColumn('{{%user_anket}}', 'training_days');

        $this->addColumn('{{%order_anket}}', 'alias', Schema::TYPE_STRING);
        $this->addColumn('{{%course_anket}}', 'alias', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->addColumn('{{%user_anket}}', 'level', Schema::TYPE_STRING);
        $this->addColumn('{{%user_anket}}', 'training_days', Schema::TYPE_STRING);

        $this->dropColumn('{{%order_anket}}', 'alias');
        $this->dropColumn('{{%course_anket}}', 'alias');
    }
}
