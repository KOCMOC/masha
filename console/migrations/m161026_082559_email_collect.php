<?php

use yii\db\Migration;

class m161026_082559_email_collect extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%delivery}}', [
            'id' => $this->primaryKey()->unsigned(),
            'fio' => $this->string(255)->notNull(),
            'phone' => $this->string(20),
            'email' => $this->string(255)->notNull()
        ], $tableOptions);

        return true;
    }

    public function down()
    {
		$this->dropTable('{{%delivery}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
