<?php

use yii\db\Schema;
use yii\db\Migration;

class m170121_173544_global_fields_fixing extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%exercise}}', 'id_related', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id_tab`');

    	$this->addColumn('{{%user_global_field}}', 'id_exercise', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id_user`');

    	return true;
    }

    public function down()
    {
        $this->dropColumn('{{%exercise}}', 'id_related');

        $this->dropColumn('{{%user_global_field}}', 'id_exercise');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
