<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m170325_145413_anket_status extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_anket}}', 'status', Schema::TYPE_SMALLINT . "(1) NOT NULL DEFAULT 0 AFTER `week`");

        return true;
    }

    public function down()
    {
        $this->dropColumn('{{%user_anket}}', 'status');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
