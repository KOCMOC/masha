<?php

use yii\db\Schema;
use yii\db\Migration;

class m161025_122909_user_anketa extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

    	$this->addColumn('{{%user}}', 'born', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `fio`');
    	$this->addColumn('{{%user}}', 'growth', Schema::TYPE_SMALLINT . '(6) UNSIGNED NOT NULL DEFAULT 0 AFTER `born`');

    	$this->addColumn('{{%workout_report_field}}', 'related_anket_field', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `type`');
    	$this->addColumn('{{%schedule_report}}', 'related_anket_field', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `workout_report_field_id`');

        $this->createTable('{{%course_anket}}', [
            'id' => $this->primaryKey()->unsigned(),
            'course_id' => $this->integer()->unsigned()->notNull(),
            'title' => $this->string(255)->notNull(),
            'type' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('{{%order_anket}}', [
            'id' => $this->primaryKey()->unsigned(),
            'order_id' => $this->integer()->unsigned()->notNull(),
            'title' => $this->string(255)->notNull(),
            'type' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            'value' => $this->string(255)
        ], $tableOptions);

        return true;
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'born');
        $this->dropColumn('{{%user}}', 'growth');

        $this->dropColumn('{{%workout_report}}', 'related_anket_field');
        $this->dropColumn('{{%schedule_report}}', 'related_anket_field');

        $this->dropTable('{{%course_anket}}');
        $this->dropTable('{{%order_anket}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
