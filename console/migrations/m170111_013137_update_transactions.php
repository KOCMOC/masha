<?php

use yii\db\Schema;
use yii\db\Migration;

class m170111_013137_update_transactions extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%finance_transaction}}', 'parent', Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0 AFTER id');
		$this->addColumn('{{%finance_invoice}}', 'hash', Schema::TYPE_STRING . '(32) NOT NULL DEFAULT "" AFTER status');

    	return true;
    }

    public function down()
    {
        $this->dropColumn('{{%finance_transaction}}', 'parent');
        $this->dropColumn('{{%finance_invoice}}', 'hash');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
