<?php

use yii\db\Migration;

class m160823_082243_exercise extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%exercise}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255)->notNull(),
            'description_cut' => $this->string(512),
            'description' => $this->text(),
            'order' => $this->integer()->unsigned()->notNull()
        ], $tableOptions); 
    }

    public function down()
    {
       $this->dropTable('{{%exercise}}');
    }
}
