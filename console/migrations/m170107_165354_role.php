<?php

use yii\db\Migration;

class m170107_165354_role extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_role}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255)->notNull(),
        ], $tableOptions);

        $role = new \common\models\UserRole();
        $role->name = 'Техническая поддержка';
        $role->save();
    }

    public function down()
    {
        $this->dropTable('{{%user_role}}');
    }
}
