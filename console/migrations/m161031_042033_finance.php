<?php

use yii\db\Schema;
use yii\db\Migration;

class m161031_042033_finance extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

		$this->createTable('{{%finance_invoice}}', [
		    'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned()->notNull(),
		    'updated_by' => $this->integer()->unsigned()->notNull(),
		    'amount' => $this->decimal(10, 2)->notNull(),
		    'method' => $this->smallInteger(1)->notNull(),
		    'status' => $this->smallInteger(1)->notNull(),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createTable('{{%finance_transaction}}', [
		    'id' => $this->primaryKey()->unsigned(),
		    'user_id' => $this->integer()->notNull(),
		    'subject_id' => $this->integer(),
		    'subject_type' => $this->smallInteger(1),
		    'trigger_id' => $this->integer()->notNull(),
		    'amount' => $this->decimal(10, 2)->notNull(),
		    'comment' => $this->string(255),
		    'status' => $this->smallInteger(1),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createTable('{{%finance_transaction_trigger}}', [
		    'id' => $this->primaryKey()->unsigned(),
		    'trigger' => $this->text()->notNull(),
		    'comment' => $this->string(255),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createTable('{{%finance_account}}', [
		    'id' => $this->primaryKey()->unsigned(),
		    'user_id' => $this->integer()->unsigned()->notNull(),
		    'balance' => $this->decimal(10, 2),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
		], $tableOptions);


		$this->createTable('{{%sell_item}}', [
		    'id' => $this->primaryKey()->unsigned(),
		    'course_group_id' => $this->integer()->unsigned()->notNull(),
		    'type' => $this->smallInteger(1)->notNull(),
		    'title' => $this->string(255)->notNull(),
		    'amount' => $this->decimal(10, 2),
		    'trigger' => $this->text()->notNull(),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createTable('{{%auth_log}}', [
		    'id' => $this->primaryKey()->unsigned(),
		    'user_id' => $this->integer()->unsigned()->notNull(),
		    'ip' => $this->string(15)->notNull(),
		    'created_at' => $this->integer()->notNull(),
		], $tableOptions);

    	$this->addColumn('{{%finance_account}}', 'status', Schema::TYPE_SMALLINT . '(1) NOT NULL AFTER balance');

		return true;
    }

    public function down()
    {
        $this->dropTable('{{%finance_invoice}}');
        $this->dropTable('{{%finance_transaction}}');
        $this->dropTable('{{%finance_transaction_trigger}}');
        $this->dropTable('{{%finance_account}}');
        $this->dropTable('{{%sell_item}}');
        $this->dropTable('{{%auth_log}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
