<?php

use yii\db\Schema;
use yii\db\Migration;

class m161016_182337_schedule_course extends Migration
{
    public function up()
    {
		$this->renameColumn('{{%schedule_exercise}}', 'workout_id', 'schedule_id');

		$this->addColumn('{{%schedule_exercise}}', 'type', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `exercise_id`');
		//$this->addColumn('{{%schedule_exercise}}', 'number', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `type`');
		$this->dropColumn('{{%schedule_exercise}}', 'order_id');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%schedule_training_routine}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_relation' => $this->integer()->unsigned()->notNull(),
            'type' => $this->smallInteger(1)->unsigned()->notNull(),
            'set' => $this->smallInteger()->unsigned()->notNull(),
            'sleep' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('{{%schedule_set}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_set_attr' => $this->integer()->unsigned()->notNull(),
            'id_s_tr_rout' => $this->integer()->unsigned()->notNull(),
            'default_val' => $this->string(10)->notNull()
        ], $tableOptions);

        $this->createTable('{{%schedule_workout_set}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255)->notNull(),
            'sleep' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('{{%schedule_set_exercise}}', [
            'id' => $this->primaryKey()->unsigned(),
            'set_id' => $this->integer()->unsigned()->notNull(),
            'exercise_id' => $this->integer()->unsigned()->notNull(),
            'number' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        return true;

    }

    public function down()
    {

    	$this->renameColumn('{{%schedule_exercise}}', 'schedule_id', 'workout_id');

    	$this->dropColumn('{{%schedule_exercise}}', 'type');
    	//$this->dropColumn('{{%schedule_exercise}}', 'number');
        $this->addColumn('{{%schedule_exercise}}', 'order_id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AFTER exercise_id');

        $this->dropTable('{{%schedule_training_routine}}');
        $this->dropTable('{{%schedule_set}}');
        $this->dropTable('{{%schedule_workout_set}}');
        $this->dropTable('{{%schedule_set_exercise}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
