<?php

use yii\db\Migration;

class m170121_215514_user_trainer_relation extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'is_trainer', $this->boolean());
        $this->addColumn('{{%user}}', 'trainer_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'is_trainer');
        $this->dropColumn('{{%user}}', 'trainer_id');
    }
}
