<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m170125_144119_is_trainer_to_ticket_role extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user}}', 'is_trainer', Schema::TYPE_INTEGER);
        $this->renameColumn('{{%user}}', 'is_trainer', 'tickets_role');
    }

    public function down()
    {
        $this->renameColumn('{{%user}}', 'tickets_role', 'is_trainer');
        $this->alterColumn('{{%user}}', 'is_trainer', Schema::TYPE_BOOLEAN);
    }
}
