<?php

use yii\db\Schema;
use yii\db\Migration;

class m161002_101713_restructurisation2 extends Migration
{
    public function up()
    {
    	$this->renameTable('workout_exercise', 'schedule_exercise');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%workout_exercise}}', [
            'id' => $this->primaryKey()->unsigned(),
            'workout_id' => $this->integer()->unsigned()->notNull(),
            'exercise_id' => $this->integer()->unsigned()->notNull()
        ], $tableOptions);

        $this->addColumn('{{%workout_exercise}}', 'number', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `exercise_id`');

		return true;

    }

    public function down()
    {

    	$this->dropTable('{{%workout_exercise}}');

        $this->renameTable('schedule_exercise', 'workout_exercise');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
