<?php

use yii\db\Schema;
use yii\db\Migration;

class m170207_153426_set_trigger extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%set_exercise}}', 'trigger_yes', Schema::TYPE_TEXT . ' AFTER `number`');
    	$this->addColumn('{{%set_exercise}}', 'trigger_no', Schema::TYPE_TEXT . ' AFTER `trigger_yes`');
    	$this->addColumn('{{%schedule_set_exercise}}', 'trigger_yes', Schema::TYPE_TEXT . ' AFTER `number`');
    	$this->addColumn('{{%schedule_set_exercise}}', 'trigger_no', Schema::TYPE_TEXT . ' AFTER `trigger_yes`');

    	return true;
    }

    public function down()
    {
        $this->dropColumn('{{%set_exercise}}', 'trigger_yes');
        $this->dropColumn('{{%set_exercise}}', 'trigger_no');
        $this->dropColumn('{{%schedule_set_exercise}}', 'trigger_yes');
        $this->dropColumn('{{%schedule_set_exercise}}', 'trigger_no');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
