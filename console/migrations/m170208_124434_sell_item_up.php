<?php

use yii\db\Schema;
use yii\db\Migration;

class m170208_124434_sell_item_up extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%discount}}', [
            'id' => $this->primaryKey()->unsigned(),
        	'title' => $this->string(20),
            'period' => $this->string(20),
            'discount' => $this->smallinteger(2)->unsigned()->notNull()
        ], $tableOptions);

        $this->addColumn('{{%order}}', 'discount', Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0 AFTER course_group_id');
        $this->addColumn('{{%order}}', 'promocode', Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0 AFTER discount');

    	return true;
    }

    public function down()
    {
        $this->dropColumn('{{%order}}', 'discount');
        $this->dropColumn('{{%order}}', 'promocode');

    	return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
