<?php

use yii\db\Migration;

class m160823_074840_training_routine extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%training_routine}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_exercise' => $this->integer()->unsigned()->notNull(),
            'id_user' => $this->integer()->unsigned()->notNull(),
            'set' => $this->smallInteger()->unsigned()->notNull(),
            'sleep' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
        ], $tableOptions); 
    }

    public function down()
    {
       $this->dropTable('{{%training_routine}}');
    }
}
