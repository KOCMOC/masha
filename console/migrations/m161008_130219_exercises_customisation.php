<?php

use yii\db\Schema;
use yii\db\Migration;

class m161008_130219_exercises_customisation extends Migration
{
    public function up()
    {

        $this->addColumn('{{%exercise}}', 'video', Schema::TYPE_TEXT . ' NOT NULL AFTER `description_cut`');

        $this->addColumn('{{%set}}', 'default_val', Schema::TYPE_STRING . '(10) NOT NULL DEFAULT "" AFTER `id_tr_rout`');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%exercise_fields}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_exercise' => $this->integer()->unsigned()->notNull(),
            'title' => $this->string(255)->notNull(),
            'value' => $this->text(),
        ], $tableOptions);

		return true;

    }

    public function down()
    {

        $this->dropColumn('{{%exercise}}', 'video');

        $this->dropColumn('{{%set}}', 'default_val');

        $this->dropTable('{{%exercise_fields}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
