<?php

use yii\db\Migration;

class m170107_174157_faq extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%faq}}', [
            'id' => $this->primaryKey()->unsigned(),
            'question' => $this->text()->notNull(),
            'answer' => $this->text()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%faq}}');
    }
}
