<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m170219_191829_user_order_anket_com extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_anket}}', 'fio', Schema::TYPE_STRING . " NOT NULL DEFAULT '' AFTER `user_id`");
        $this->addColumn('{{%user_anket}}', 'born', Schema::TYPE_TIMESTAMP . " NULL AFTER `fio`");
        $this->addColumn('{{%user_anket}}', 'country', Schema::TYPE_STRING . " NOT NULL DEFAULT '' AFTER `born`");
        $this->addColumn('{{%user_anket}}', 'city', Schema::TYPE_STRING . " NOT NULL DEFAULT '' AFTER `country`");
        $this->addColumn('{{%user_anket}}', 'insta', Schema::TYPE_STRING . " NOT NULL DEFAULT '' AFTER `city`");
        $this->addColumn('{{%user_anket}}', 'vk', Schema::TYPE_STRING . " NOT NULL DEFAULT '' AFTER `insta`");

        return true;
    }

    public function down()
    {
    	$this->dropColumn('{{%user_anket}}', 'fio');
        $this->dropColumn('{{%user_anket}}', 'born');
        $this->dropColumn('{{%user_anket}}', 'country');
        $this->dropColumn('{{%user_anket}}', 'city');
        $this->dropColumn('{{%user_anket}}', 'insta');
        $this->dropColumn('{{%user_anket}}', 'vk');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
