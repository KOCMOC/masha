<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m170517_063043_startdate extends Migration
{
    public function up()
    {
        $this->addColumn('{{%order}}', 'datestart', Schema::TYPE_INTEGER . "(11) NOT NULL DEFAULT 0 AFTER `amount`");

        return true;
    }

    public function down()
    {
        $this->dropColumn('{{%order}}', 'datestart');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
