<?php

use yii\db\Migration;

class m160901_071922_workout extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%workout}}', [
            'id' => $this->primaryKey()->unsigned(),
            'course_id' => $this->integer()->unsigned()->notNull(),
            'description' => $this->text(),
            'exercise_list' => $this->text(),
            'title' => $this->string(255)->notNull(),
        ], $tableOptions); 
    }

    public function down()
    {
       $this->dropTable('{{%workout}}');
    }
}
