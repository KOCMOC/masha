<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m170214_090248_user_schedule_nav extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%user}}', 'course', Schema::TYPE_INTEGER.'(11) NOT NULL DEFAULT 0 AFTER status');
        $this->addColumn('{{%user}}', 'quest', Schema::TYPE_SMALLINT.'(1) NOT NULL DEFAULT 0 AFTER course');

    	return true;
    }

    public function down()
    {

        $this->dropColumn('{{%user}}', 'course');
        $this->dropColumn('{{%user}}', 'quest');

    	return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
