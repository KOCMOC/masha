<?php

use yii\db\Schema;
use yii\db\Migration;

class m161002_181315_workout_sets extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%workout_exercise}}', 'type', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `exercise_id`');

        $this->createTable('{{%workout_set}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255)->notNull(),
            'sleep' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('{{%set_exercise}}', [
            'id' => $this->primaryKey()->unsigned(),
            'set_id' => $this->integer()->unsigned()->notNull(),
            'exercise_id' => $this->integer()->unsigned()->notNull(),
            'number' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->addColumn('{{%workout}}', 'number', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `exercise_list`');

        return true;
    }

    public function down()
    {
        $this->dropColumn('{{%workout_exercise}}', 'type');

        $this->dropTable('{{%workout_set}}');

        $this->dropTable('{{%set_exercise}}');

        $this->dropColumn('{{%workout}}', 'number');

        return true;

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
