<?php

use yii\db\Schema;
use yii\db\Migration;

class m170118_182919_tabs_i_trashclean extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%exercise_tab}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255)
        ], $tableOptions);

        $this->addColumn('{{%exercise}}', 'id_tab', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 1 AFTER `id`');

        $this->dropColumn('{{%exercise}}', 'order');
        $this->dropColumn('{{%training_routine}}', 'id_user');

        return true;
    }

    public function down()
    {
    	$this->dropTable('{{%exercise_tab}}');

        $this->dropColumn('{{%exercise}}', 'id_tab');

		$this->addColumn('{{%exercise}}', 'order', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id`');
		$this->addColumn('{{%training_routine}}', 'id_user', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id`');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
