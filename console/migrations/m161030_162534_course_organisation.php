<?php

use yii\db\Schema;
use yii\db\Migration;

class m161030_162534_course_organisation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

		$this->addColumn('{{%order}}', 'amount', Schema::TYPE_DECIMAL . '(10,2) DEFAULT "0.00" AFTER active');
		$this->addColumn('{{%order}}', 'expire', Schema::TYPE_INTEGER . ' NOT NULL AFTER amount');
		$this->addColumn('{{%order}}', 'created_at', Schema::TYPE_INTEGER . ' NOT NULL AFTER expire');
		$this->addColumn('{{%order}}', 'updated_at', Schema::TYPE_INTEGER . ' NOT NULL AFTER created_at');
		$this->addColumn('{{%user}}', 'phone', Schema::TYPE_STRING . '(20) NOT NULL AFTER email');

		$this->createTable('{{%course_group}}', [
			'id' => $this->primaryKey()->unsigned(),
			'title' => $this->string(255)->notNull(),
			'description' => $this->text()
		], $tableOptions);

		$this->addColumn('{{%course}}', 'course_group_id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AFTER id');
		$this->addColumn('{{%course}}', 'level', Schema::TYPE_SMALLINT . '(3) NOT NULL AFTER course_group_id');
		$this->addColumn('{{%course}}', 'gender', Schema::TYPE_SMALLINT . '(1) NOT NULL AFTER level');

		$this->renameColumn('{{%order}}', 'course_id', 'course_group_id');
		$this->addColumn('{{%order}}', 'level', Schema::TYPE_SMALLINT . '(3) NOT NULL AFTER course_group_id');
		$this->addColumn('{{%order}}', 'gender', Schema::TYPE_SMALLINT . '(1) NOT NULL AFTER level');
		$this->addColumn('{{%order}}', 'type', Schema::TYPE_SMALLINT . '(1) NOT NULL AFTER gender');
		$this->addColumn('{{%order}}', 'schedule_template', Schema::TYPE_STRING . '(15) NOT NULL AFTER type');

		$this->createTable('{{%course_level}}', [
			'id' => $this->primaryKey()->unsigned(),
			'title' => $this->string(255)->notNull()
		], $tableOptions);

		return true;
    }

    public function down()
    {
        $this->dropColumn('{{%order}}', 'amount');
        $this->dropColumn('{{%order}}', 'expire');
        $this->dropColumn('{{%order}}', 'created_at');
        $this->dropColumn('{{%order}}', 'updated_at');
        $this->dropColumn('{{%user}}', 'phone');

        $this->dropTable('{{%course_group}}');

        $this->dropColumn('{{%course}}', 'course_group_id');
        $this->dropColumn('{{%course}}', 'level');
        $this->dropColumn('{{%course}}', 'gender');

		$this->renameColumn('{{%order}}', 'course_group_id', 'course_id');
        $this->dropColumn('{{%order}}', 'level');
        $this->dropColumn('{{%order}}', 'gender');
        $this->dropColumn('{{%order}}', 'type');

        $this->dropTable('{{%course_level}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
