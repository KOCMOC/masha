<?php

use yii\db\Migration;

class m160901_074023_schedule extends Migration
{
   public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%schedule}}', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'workout_id' => $this->integer()->unsigned()->notNull(),
            'date' => $this->timestamp(),
            
        ], $tableOptions); 
    }

    public function down()
    {
       $this->dropTable('{{%schedule}}');
    }
}
