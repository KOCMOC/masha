<?php

use yii\db\Migration;

class m170115_175938_webinars extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%webinar}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string()->notNull(),
            'preview' => $this->string(),
            'short_text' => $this->text()->notNull(),
            'full_text' => $this->text(),
            'video_content' => $this->text(),
            'date' => $this->date()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%webinar}}');
    }
}
