<?php

use yii\db\Migration;

class m170130_075848_socialnoetwork_auth_details extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%user}}', 'photo', $this->string());

        $this->dropColumn('{{%user}}', 'username');

        $this->createTable('{{%user_networks}}', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->notNull(),
            'network' => $this->string()->notNull(),
            'identity' => $this->string()->notNull(),
            'uid' => $this->string()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'photo');
        $this->addColumn('{{%user}}', 'username', $this->string());
    }
}
