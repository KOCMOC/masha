<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m170218_165538_user_anket_calories extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_anket}}', 'kal', Schema::TYPE_SMALLINT . "(4) NOT NULL DEFAULT 0 AFTER `goal`");
        $this->addColumn('{{%user_anket}}', 'vegan', Schema::TYPE_SMALLINT . "(4) NOT NULL DEFAULT 0 AFTER `kal`");
        $this->addColumn('{{%user_anket}}', 'mother', Schema::TYPE_SMALLINT . "(4) NOT NULL DEFAULT 0 AFTER `vegan`");
        $this->addColumn('{{%user_anket}}', 'day', Schema::TYPE_SMALLINT . "(1) NOT NULL DEFAULT 0 AFTER `mother`");
        $this->addColumn('{{%user_anket}}', 'week', Schema::TYPE_SMALLINT . "(1) NOT NULL DEFAULT 0 AFTER `day`");

        $this->addColumn('{{%diet}}', 'min', Schema::TYPE_SMALLINT . "(4) NOT NULL DEFAULT 0 AFTER `url`");
        $this->addColumn('{{%diet}}', 'max', Schema::TYPE_SMALLINT . "(4) NOT NULL DEFAULT 0 AFTER `min`");
        $this->addColumn('{{%diet}}', 'vegan', Schema::TYPE_SMALLINT . "(1) NOT NULL DEFAULT 0 AFTER `max`");
        $this->addColumn('{{%diet}}', 'mother', Schema::TYPE_SMALLINT . "(1) NOT NULL DEFAULT 0 AFTER `vegan`");

        return true;
    }

    public function down()
    {
        $this->dropColumn('{{%user_anket}}', 'kal');
        $this->dropColumn('{{%user_anket}}', 'vegan');
        $this->dropColumn('{{%user_anket}}', 'mother');

        $this->dropColumn('{{%diet}}', 'min');
        $this->dropColumn('{{%diet}}', 'max');
        $this->dropColumn('{{%diet}}', 'vegan');
        $this->dropColumn('{{%diet}}', 'mother');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
