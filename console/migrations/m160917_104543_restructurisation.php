<?php

use yii\db\Schema;
use yii\db\Migration;

class m160917_104543_restructurisation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

		$this->addColumn('{{%exercise}}', 'workout_id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id`');
		$this->addColumn('{{%exercise}}', 'number', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `description`');

		$this->dropColumn('{{%training_routine}}', 'sleep'); // �������
		$this->addColumn('{{%training_routine}}', 'sleep', Schema::TYPE_SMALLINT . '(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `set`');

		$this->dropColumn('{{%set}}', 'default_val');
		$this->dropColumn('{{%set}}', 'real_val');
		//$this->dropColumn('{{%set}}', 'id_user');
		//$this->dropColumn('{{%set}}', 'sleep');

		$this->renameColumn('{{%schedule}}', 'user_id', 'order_id');
		$this->addColumn('{{%schedule}}', 'done', Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0 AFTER `date`');

		$this->renameColumn('{{%workout_exercise}}', 'order', 'order_id');
		$this->addColumn('{{%workout_exercise}}', 'number', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `order_id`');

        $this->createTable('{{%report}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_schedule' => $this->integer()->unsigned()->notNull(),
            'id_set' => $this->integer()->unsigned()->notNull(),
            'default_val' => $this->string(10),
            'real_val' => $this->string(10)
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropColumn('{{%exercise}}', 'workout_id');
        $this->dropColumn('{{%exercise}}', 'number');

        $this->dropColumn('{{%training_routine}}', 'sleep');

        $this->addColumn('{{%set}}', 'default_val', Schema::TYPE_STRING . '(10) AFTER id_tr_rout');
        $this->addColumn('{{%set}}', 'real_val', Schema::TYPE_STRING . '(10) AFTER default_val');
        $this->addColumn('{{%set}}', 'id_user', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AFTER real_val');
        $this->addColumn('{{%set}}', 'sleep', Schema::TYPE_SMALLINT . '(5) UNSIGNED NOT NULL DEFAULT 0 AFTER id_user');

		$this->renameColumn('{{%schedule}}', 'order_id', 'user_id');
		$this->dropColumn('{{%schedule}}', 'done');

		$this->dropTable('{{%report}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
