<?php

use yii\db\Schema;
use yii\db\Migration;

class m170116_164443_global_fields extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

    	$this->alterColumn('{{%user}}', 'fio', Schema::TYPE_STRING . '(255)');

        $this->createTable('{{%global_field}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255),
            'alias' => $this->string(10),
            //'type' => $this->integer()->unsigned()->notNull() // раз поля расчётные, убираем type
        ], $tableOptions);

        $this->createTable('{{%course_global_field}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_course' => $this->integer()->unsigned()->notNull(),
            'id_global_field' => $this->integer()->unsigned()->notNull()
        ], $tableOptions);

        $this->createTable('{{%order_global_field}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_order' => $this->integer()->unsigned()->notNull(),
            'id_global_field' => $this->integer()->unsigned()->notNull()
        ], $tableOptions);

        $this->createTable('{{%user_global_field}}', [
            'id' => $this->primaryKey()->unsigned(),
            'id_user' => $this->integer()->unsigned()->notNull(),
            'id_global_field' => $this->integer()->unsigned()->notNull(),
            'value' => $this->string(10)
        ], $tableOptions);

        $this->addColumn('{{%workout_exercise}}', 'trigger_yes', Schema::TYPE_TEXT . ' AFTER `number`');
        $this->addColumn('{{%workout_exercise}}', 'trigger_no', Schema::TYPE_TEXT . ' AFTER `trigger_yes`');
        $this->addColumn('{{%schedule_exercise}}', 'trigger_yes', Schema::TYPE_TEXT . ' AFTER `number`');
        $this->addColumn('{{%schedule_exercise}}', 'trigger_no', Schema::TYPE_TEXT . ' AFTER `trigger_yes`');

        $this->addColumn('{{%course_set}}', 'to_global', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `trigger`');
        $this->addColumn('{{%schedule_set}}', 'to_global', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `trigger`');

        $this->addColumn('{{%workout}}', 'first', Schema::TYPE_SMALLINT . '(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `title`');

        $this->alterColumn('{{%set}}', 'default_val', Schema::TYPE_STRING . '(50)');
        $this->alterColumn('{{%course_set}}', 'default_val', Schema::TYPE_STRING . '(50)');

    	return true;
    }

    public function down()
    {
		$this->dropTable('{{%global_field}}');
		$this->dropTable('{{%course_global_field}}');
		$this->dropTable('{{%order_global_field}}');
		$this->dropTable('{{%user_global_field}}');

		$this->dropColumn('{{%workout_exercise}}', 'trigger_yes');
		$this->dropColumn('{{%workout_exercise}}', 'trigger_no');
		$this->dropColumn('{{%schedule_exercise}}', 'trigger_yes');
		$this->dropColumn('{{%schedule_exercise}}', 'trigger_no');

    	return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
