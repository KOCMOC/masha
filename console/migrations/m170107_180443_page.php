<?php

use yii\db\Migration;

class m170107_180443_page extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string()->notNull(),
            'url' => $this->string()->notNull()->unique(),
            'content' => $this->text()->notNull(),
        ], $tableOptions);

        $page = new \common\models\Page();
        $page->url = 'diet';
        $page->title = 'Питание';
        $page->content = 'Информация о питании';
        $page->save();
    }

    public function down()
    {
        $this->dropTable('{{%page}}');
    }
}
