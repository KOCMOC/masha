<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mailing_list`.
 */
class m170324_091839_create_mailing_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mailing_list', [
            'user_id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull()->defaultValue(""),
            'is_subscriber' => $this->integer(1)->notNull()->defaultValue(0),
            'is_active' => $this->integer(1)->notNull()->defaultValue(0),
            'is_expired' => $this->integer(1)->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mailing_list');
    }
}
