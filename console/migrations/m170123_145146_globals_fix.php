<?php

use yii\db\Schema;
use yii\db\Migration;

class m170123_145146_globals_fix extends Migration
{
    public function up()
    {
    	$this->addColumn('{{%global_field}}', 'default', Schema::TYPE_STRING . '(10) NOT NULL DEFAULT "" AFTER `alias`');

    	$this->alterColumn('{{%schedule_set}}', 'default_val', Schema::TYPE_STRING . '(50)');
    	$this->alterColumn('{{%report}}', 'default_val', Schema::TYPE_STRING . '(50)');

		return true;
    }

    public function down()
    {
        $this->dropColumn('{{%global_field}}', 'default');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
