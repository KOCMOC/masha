<?php

namespace console\models;

class MailChimp
{
    protected $api_key;

    public function __construct($api_key) {
        $this->api_key = $api_key;
    }

    public function call($method, $params, $additional_opts = []) {
        usleep(100000); // 0.1s
        $dc = "us14";
        $url = "https://$dc.api.mailchimp.com/3.0/$method";
        $post = json_encode($params);
        $credentials = "any:".$this->api_key;

        $ch = curl_init($url);
        $opts = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_USERPWD => $credentials,
            CURLOPT_CONNECTTIMEOUT => 100,
            CURLOPT_TIMEOUT => 100
        ];
        curl_setopt_array($ch, $opts);
        curl_setopt_array($ch, $additional_opts);

        $doc = curl_exec($ch);
        if($doc) {
            $res = json_decode($doc, 1);
            if(isset($res['id'])) {
                $msg = "Method: $method, post: $post, response: OK";
                $this->log($msg);
            } else {
                $msg = "Method: $method, post: $post, response: $doc";
                $this->log($msg);
            }
        } else {
            $msg = "Method: $method, post: $post, response: curl error - " . curl_error($ch);
            $this->log($msg);
        }
        curl_close($ch);
        return $doc;
    }

    public function log($msg) {
        $msg = date("Y-m-d H:i:s") . " -- " . $msg . "\n";
        file_put_contents("console/runtime/logs/mailchimp_log", $msg, FILE_APPEND);
    }

    public function new_list_member($list_id, $email) {
        $md5_hash = md5(mb_strtolower($email));

        $data = [
            "email_address" => $email,
            "status" => "subscribed"
        ];

        $opts = [
            CURLOPT_CUSTOMREQUEST => "PUT"
        ];

        return $this->call("lists/$list_id/members/$md5_hash", $data, $opts);
    }

    public function delete_member_from_list($list_id, $uid) {
        $data = [
            "status" => "unsubscribed"
        ];
        $opts = [
            CURLOPT_CUSTOMREQUEST => "PATCH"
        ];

        $this->call("lists/$list_id/members/$uid", $data, $opts);
    }
}