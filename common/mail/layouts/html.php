<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  </head>
<body>
    <?php $this->beginBody() ?>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
      <tbody>
        <tr>
          <td align="center" bgcolor="#ffffff" style="background-color:#ffffff;">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="width:600px;">
              <tr>
                <td style="background:#f1f2ec;">
                  <a href="http://workoutmasha.ru" style="display:block;">
                    <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/1273de6d-6606-4947-b856-cb6083e13e3d.jpg" alt="WORKOUT MASHA">
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                	<?= $content ?>
				</td>
              </tr>
              <tr>
                <td>
                  <table cellpadding="0" cellspacing="0" width="100%" border="0" style="height:224px;max-width:600px;background:#000000;">
                    <tr>
                      <td height="32" style="vertical-align:top;">
                        <div style="height:32px;line-height:32px;font-size:30px;"> </div>
                      </td>
                    </tr>
                    <tr>
                      <td height="82" style="vertical-align:top;">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="max-width:600px;">
                          <tr>
                            <td width="30">

                            </td>
                            <td align="left">
                              <a href="http://workoutmasha.ru/">
                                <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/9924c88a-21d2-46ce-9f31-706b3dea5385.png" alt="Войти">
                              </a>
                            </td>
                            <td>
                              <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                  <td>

                                  </td>
                                  <td width="161" align="right">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                      <tr>
                                        <td align="left">
                                          <a href="https://vk.com/club138868954">
                                            <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/2678fac2-ce70-48d8-b09c-283c0f67246c.png" alt="VK">
                                          </a>
                                        </td>
                                        <td align="center">
                                          <a href="https://www.instagram.com/wm_fitness_school/">
                                            <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/75774088-8811-4d98-b5ec-b6b9a8b0fb99.png" alt="INSTAGRAM">
                                          </a>
                                        </td>
                                        <td align="right">
                                          <a href="#">
                                            <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/5d8961eb-8d65-47c8-ba02-0cc4797fef86.png" alt="FACEBOOK">
                                          </a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td width="30">

                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style="vertical-align:top;">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="max-width:600px;">
                          <tr>
                            <td width="30">

                            </td>
                            <td>
                              <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/24e123f7-3e68-46b7-821d-06713a2408d7.png" alt="СОЗДАЙ ФИГУРУ СВОЕЙ МЕЧТЫ">
                            </td>
                            <td align="right">
                              <a href="#">
                                <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/9c01c30d-e43f-4868-ab01-361d05dc136e.png" alt="WWW.WORKOUTMASHA.RU">
                              </a>
                            </td>
                            <td width="30">

                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="78" align="center">
                  <p style="font:13px/15px Arial, sans-serif;color:#999999;">
                    Вы подписаны на рассылку <span style="font-weight: bold">workout_masha</span>, если вы желаете больше не получать от нас письма<br>
                    перейдите <a href="#" style="color:#efa20a;">по этой ссылке</a>, пожалуйста
                  </p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>