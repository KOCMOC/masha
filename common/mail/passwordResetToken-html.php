<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
				  <table cellpadding="0" cellspacing="0" width="100%" border="0" style="height:auto;padding-bottom:80px;max-width:600px;background:#f1f2ec;">
                    <tr>
                      <td height="60" style="vertical-align:top;text-align:center;font:35px/37px Arial, sans-serif;">
                        Восстановление пароля!
                        <img src="https://gallery.mailchimp.com/da2bf67dbf481c274c160e669/images/7788c82c-9a28-44d7-b243-377145e41d9f.png" alt="smile">
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align:center;font:16px/24px Arial, sans-serif;vertical-align:top;">
                        <p style="margin:0 0 24px;">
                          Для восстановления пароля перейди по ссылке
                        </p>
                        <p style="margin:0;">
                          <?= Html::a(Html::encode($resetLink), $resetLink, ['style'=>'font-size:20px;color:#0000ff']) ?>
                        </p>
                      </td>
                    </tr>
                  </table>
