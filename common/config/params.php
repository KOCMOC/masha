<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'mailchimp' => [
        'apiKey' => 'db155c86b51dd98741d48f2c18413804-us14',
        'list_id_allsubscribers' => 'faf62ef4c2',
        'list_id_actual' => 'c5911bca04',
        'list_id_expired' => 'a11d1e4daf',
        'listId' => '5108776a77', // List name: Предварительная регистрация в онлайн фитнес-школе
    ]
];
