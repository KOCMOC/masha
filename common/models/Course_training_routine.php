<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Course_training_routine extends ActiveRecord
{

	const WorkoutRelation = 1;
	const SetRelation = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_training_routine}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_relation', 'type', 'set', 'sleep'], 'required'],
            [['id_relation', 'type'], 'integer'],
            [['set', 'sleep'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_relation' => 'ID связи с упражнением',
            'type' => 'Тип связи',
            'set' => 'Подход',
            'sleep' => 'Отдых'
        ];
    }

    public function getCourseSets() {
        return $this->hasMany(Course_set::className(), ['id_c_tr_rout' => 'id'])->all();
    }

    public function getExerciseRelation() {    	return $this->hasOne(Workout_exercise::className(), ['id' => 'id_relation'])->where(['type' => 0])->one();    }

    public function clonCopy()
    {    	$response = array('status'=>'error', 'error'=>'', 'set_id'=>'');

    	$last_one = $this->getExerciseRelation()->getCourseRoutines('set desc')->asArray()->one();

    	$transaction = Yii::$app->db->beginTransaction();

    	$clone_routine = new Course_training_routine;
    	$clone_routine->id_relation = $this->id_relation;
    	$clone_routine->type = $this->type;
    	$clone_routine->sleep = $this->sleep;
    	$clone_routine->alert_sleep = $this->alert_sleep;
    	$clone_routine->alert_proc = $this->alert_proc;
    	$clone_routine->set = $last_one['set'] + 1;
    	if($clone_routine->save()) {
    		$clone_routine->refresh();
    		$sets = $this->getCourseSets();
    		if(sizeof($sets) > 0) {               	foreach($sets as $set) {
               		$clone_set = new Course_set;
               		$clone_set->id_set_attr = $set->id_set_attr;
               		$clone_set->id_c_tr_rout = $clone_routine->id;
               		$clone_set->default_val = $set->default_val;
               		$clone_set->trigger = $set->trigger;
               		if($clone_set->save()) {

               		} else {
               			$transaction->rollback();
               			$response['error'] = 'Не удалось сохранить атрибут подхода упражнения';
               		}
               	}    		}
    	} else {
    		$transaction->rollback();
        	$response['error'] = 'Не удалось сохранить подход';
    	}

    	if(!$response['error']) {
    		$response['status'] = 'ok';
    		$response['routine'] = $clone_routine;
    		$transaction->commit();
    	}

		return $response;    }

    public function del() {
    	$transaction = Yii::$app->db->beginTransaction();    	$sets = $this->getCourseSets();
    	foreach($sets as $set) {    		if(!$set->delete()) {    			$transaction->rollback();
    			return false;    		}    	}
    	if(!$this->delete()) {    		$transaction->rollback();
    		return false;    	}
    	$transaction->commit();
    	return true;    }
}