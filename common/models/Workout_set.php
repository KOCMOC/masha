<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Workout_set extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%workout_set}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['sleep'], 'number', 'min'=>0, 'message'=>'Должно быть положительным числом'],
            [['alert_proc', 'alert_sleep'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название сета',
            'sleep' => 'Отдых',
            'alert_proc' => 'Комментарий выполение',
            'alert_sleep' => 'Комментарий отдых'
        ];
    }

    /*public function getWorkout() {
        return $this->hasOne(Workout::className(), ['id' => 'workout_id'])->one();
    }*/

	public function getWorkout_exercise() {
		return $this->hasOne(Workout_exercise::className(), ['exercise_id' => 'id'])->where(['type'=>\common\models\Workout_exercise::Set])->one();
    }

    public function getExercises($order = 'id') {
        return $this->hasMany(Set_exercise::className(), ['set_id' => 'id'])->orderBy($order);
    }

    public function clonCopy() {

    	$response = array('status'=>'error', 'error'=>'', 'set_id'=>'');
    	$transaction = Yii::$app->db->beginTransaction();

    	$clone_workout_set = new Workout_set;
    	$clone_workout_set->title = $this->title;
    	$clone_workout_set->sleep = $this->sleep;
    	$clone_workout_set->alert_sleep = $this->alert_sleep;
    	$clone_workout_set->alert_proc = $this->alert_proc;
    	if($clone_workout_set->save()) {
    		$clone_workout_set->refresh();            $set_exercises = $this->getExercises()->all();
            if(sizeof($set_exercises) > 0) {            	foreach($set_exercises as $set_exercise) {            		$clone_set_exercise = new Set_exercise;
            		$clone_set_exercise->set_id = $clone_workout_set->id;
            		$clone_set_exercise->exercise_id = $set_exercise->exercise_id;
            		$clone_set_exercise->number = $set_exercise->number;
            		if($clone_set_exercise->save()) {                        $clone_set_exercise->refresh();
                        $routines = $set_exercise->getCourseRoutines()->all();
                        if(sizeof($routines) > 0) {                        	foreach($routines as $routine) {                        		$clone_routine = new Course_training_routine;
                        		$clone_routine->id_relation = $clone_set_exercise->id;
                        		$clone_routine->type = $routine->type;
                        		$clone_routine->set = $routine->set;
                        		$clone_routine->sleep = $routine->sleep;
                        		$clone_routine->alert_sleep = $routine->alert_sleep;
                        		$clone_routine->alert_proc = $routine->alert_proc;
                        		if($clone_routine->save()) {                                    $clone_routine->refresh();
                                    $sets = $routine->getCourseSets();
                                    if(sizeof($sets) > 0) {                                    	foreach($sets as $set) {                                    		$clone_set = new Course_set;
                                    		$clone_set->id_set_attr = $set->id_set_attr;
                                    		$clone_set->id_c_tr_rout = $clone_routine->id;
                                    		$clone_set->default_val = $set->default_val;
                                    		$clone_set->trigger = $set->trigger;
                                    		if($clone_set->save()) {                                    		} else {
                                    			$transaction->rollback();                                    			$response['error'] = 'Не удалось сохранить атрибут подхода упражнения сета';                                    		}                                    	}                                    }                        		} else {
                        			$transaction->rollback();                        			$response['error'] = 'Не удалось сохранить подход упражнения сета';                        		}                        	}                        }            		} else {
            			$transaction->rollback();            			$response['error'] = 'Не удалось сохранить упражнение сета';            		}            	}            }    	} else {
    		$transaction->rollback();        	$response['error'] = 'Не удалось сохранить сет';    	}

    	if(!$response['error']) {
    		$response['status'] = 'ok';
    		$response['set'] = $clone_workout_set;    		$transaction->commit();    	}

		return $response;
    }

}