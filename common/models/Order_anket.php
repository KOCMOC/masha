<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Order_anket extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_anket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type', 'order_id'], 'required'],
            ['order_id', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            [['title', 'alias'], 'filter', 'filter' => 'trim'],
            [['title', 'alias'], 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }],
            ['type', 'in', 'range' => [Course_anket::Number, Course_anket::Text, Course_anket::Checkbox], 'allowArray' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'order_id' => 'ID заказа',
            'title' => 'Название поля',
            'type' => 'Тип поля'
        ];
    }

	public function getRelatedAnketFields() {
        return $this->hasMany(Schedule_report::className(), ['related_anket_field' => 'id'])->all();
    }

    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id'])->one();
    }

    public function del() {
     	$transaction = Yii::$app->db->beginTransaction();
    	$related_anket_fields = $this->getRelatedAnketFields();
    	if(sizeof($related_anket_fields) > 0) {
    		foreach($related_anket_fields as $related_anket_field) {
	    		if(!$related_anket_fields->delete()) {
	    			$transaction->rollback();
	    			return false;
	    		}
    		}
    	}
    	if(!$this->delete()) {
    		$transaction->rollback();
    		return false;
    	}
    	$transaction->commit();
    	return true;
    }

}