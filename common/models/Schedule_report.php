<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Schedule_report extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schedule_report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schedule_id', 'workout_report_field_id'], 'required'],
            [['schedule_id', 'workout_report_field_id', 'related_anket_field'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'schedule_id' => 'ID тренировки в курсе',
            'workout_report_field_id' => 'ID поля из справочника',
            'related_anket_field' => 'ID связанного поля анкеты отчёта'
        ];
    }

    public function getField() {
        return $this->hasOne(Workout_report_field::className(), ['id' => 'workout_report_field_id'])->one();
    }

    public function getRelatedAnketField() {
        return $this->hasOne(Order_anket::className(), ['id' => 'related_anket_field'])->one();
    }

    public function getSchedule() {
        return $this->hasOne(Schedule::className(), ['id' => 'schedule_id'])->one();
    }
}