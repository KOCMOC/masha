<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Workout extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%workout}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', /*'description',*/ 'course_id'], 'required'],
            [['title', /*'description'*/], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['course_id', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название тренировки',
            'description' => 'Описание тренировки',
            'course_id' => 'ID курса'
        ];
    }

    public function getCourse() {
        return $this->hasOne(Course::className(), ['id' => 'course_id'])->one();
    }

	public function getExercises() {
        return $this->hasMany(Exercise::className(), ['workout_id' => 'id']);
    }

	public function getWorkoutExercises($order = 'id') {
        return $this->hasMany(Workout_exercise::className(), ['workout_id' => 'id'])->orderBy($order);
    }

	public function getReport() {
        return $this->hasMany(Workout_report_field::className(), ['workout_id' => 'id']);
    }

    public function clonCopy($course_id = false) {
    	$response = array('status'=>'error', 'error'=>'', 'set_id'=>'');

    	$transaction = Yii::$app->db->beginTransaction();

    	if($course_id === false) {
			$last_one = $this->getCourse()->getWorkouts('number desc')->asArray()->one();
		} else {			$last_one = Course::findOne($course_id)->getWorkouts('number desc')->asArray()->one();		}

    	$clone_workout = new Workout;
    	$clone_workout->course_id = ($course_id === false)?$this->course_id:$course_id;
    	$clone_workout->title = $this->title;
    	$clone_workout->first = $this->first;
    	$clone_workout->number = $last_one['number'] + 1;
    	if($clone_workout->save()) {            $clone_workout->refresh();
            $reports = $this->getReport()->all();
            if(sizeof($reports) > 0) {            	foreach($reports as $report) {            		$clone_report_field = new Workout_report_field;
            		$clone_report_field->workout_id = $clone_workout->id;
            		$clone_report_field->title = $report->title;
            		$clone_report_field->type = $report->type;
            		$clone_report_field->related_anket_field = $report->related_anket_field;
            		if(!$clone_report_field->save()) {			    		$transaction->rollback();
			        	$response['error'] = 'Не удалось сохранить поле отчёта';            		}            	}            }
            $exercises = $this->getWorkoutExercises()->all();
            if(sizeof($exercises) > 0) {            	foreach($exercises as $exercise) {            		if($exercise->type == Workout_exercise::Exercise) {            			$clone_exercise = $exercise->clonCopy($clone_workout->id);
            			if($clone_exercise['status'] != 'ok') {				    		$transaction->rollback();
				        	$response['error'] = 'Не удалось создать упражнение';            			}            		} else {
            			$workout_set = $exercise->getSet();
            			if($workout_set) {            				$clon_set = $workout_set->clonCopy();
							if($clon_set['status'] == 'ok') {
		                        $cloned_set = $clon_set['set'];
		                        $clone_workout_exercise = new \common\models\Workout_exercise;
		                        $clone_workout_exercise->workout_id = $clone_workout->id;
		                        $clone_workout_exercise->exercise_id = $cloned_set->id;
		                        $clone_workout_exercise->type = \common\models\Workout_exercise::Set;
		                        $clone_workout_exercise->number = $exercise->number;
		                        if(!$clone_workout_exercise->save()) { print_R($clone_workout_exercise->errors); die('111');		                        	$transaction->rollback();
						        	$response['error'] = 'Не удалось создать связь для сета';
		                        }
			            	} else {
			                	$response['error'] = $clon_set['error'];
			            	}            			}            		}            	}            }    	} else {
    		$transaction->rollback();
        	$response['error'] = 'Не удалось сохранить тренировку';
    	}

    	if(!$response['error']) {
    		$response['status'] = 'ok';
    		$response['workout'] = $clone_workout;
    		$transaction->commit();
    	}

		return $response;
    }

}