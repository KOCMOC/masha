<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Schedule extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schedule}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id','workout_id', 'date'], 'required'],
            [['order_id','workout_id'], 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'order_id' => 'ID заказа',
            'workout_id' => 'ID тренировки',
            'date' => 'Дата и время тренировки'
        ];
    }

    public function getWorkout() {
        return $this->hasOne(Workout::className(), ['id' => 'workout_id'])->one();
    }

	public function getWorkoutExercises($order = 'id') {
        return $this->hasMany(Workout_exercise::className(), ['workout_id' => 'id'])->orderBy($order);
    }

    public function getExercises($order = 'id') {    	return $this->hasMany(Schedule_exercise::className(), ['schedule_id' => 'id'])->orderBy($order);    }

    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id'])->one();
    }

    public function getParent_workout() {
        return $this->hasOne(Workout::className(), ['id' => 'workout_id']);
    }

    public function getParent_order() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

	public function getReports() {
        return $this->hasMany(Report::className(), ['id_schedule' => 'id'])->all();
    }

	public function getWorkoutReport() {
        return $this->hasMany(Schedule_report::className(), ['schedule_id' => 'id'])->all();
    }

    public function del() {    	$exercises = $this->getExercises()->all();
    	if(sizeof($exercises) > 0) {    		foreach($exercises as $exercise) {    			$exercise->del();    		}    	}

    	$reports = $this->getReports();
    	if(sizeof($reports) > 0) {
    		foreach($reports as $report) {
    			$report->delete();
    		}
    	}

    	$wreports = $this->getWorkoutReport();
    	if(sizeof($wreports) > 0) {
    		foreach($wreports as $wreport) {
    			$wreport->delete();
    		}
    	}

    	$this->delete();

    	return true;    }

    public function prepare() {    	//$old_schedule = Schedule::find()->where(['order_id'=>$this->order_id, 'done'=>1])->andWhere('date<"'.$this->date.'"')->asArray()->one();

		$globalKeys = $globalVals = array();
		$order = $this->getOrder();
		$globals = $order->getUserGlobalFields();

		if(sizeof($globals) > 0) {			foreach($globals as $global) {           		array_push($globalKeys, $global->alias);
           		array_push($globalVals, $global->value);			}		}

    	$exercises = $this->getExercises('number')->all();

    	if(sizeof($exercises) > 0) {
    		$transaction = Yii::$app->db->beginTransaction();    		foreach($exercises as $exercise) {    			if($exercise->type == \common\models\Workout_exercise::Exercise) {                	$routines = $exercise->getScheduleRoutines('set')->all();
                	if(sizeof($routines) > 0) {                		foreach($routines as $routine) {                			$sets = $routine->getScheduleSets();
                			if(sizeof($sets) > 0) {                				foreach($sets as $set) {                           			$report = new Report();
                           			$report->id_schedule = $this->id;
                           			$report->id_set = $set->id;
                					/*if(is_numeric($set->default_val)) {
                						$report->default_val = $set->default_val;
                					} else {                                        eval('$report->default_val = '.str_replace($globalKeys, $globalVals, $set->default_val).';');                					}*/
                					$report->default_val = $set->default_val;
                              		$report->save();                				}                			}                		}                	}
                	//foreach($routines)    			} elseif($exercise->type == \common\models\Workout_exercise::Set) {    				$workout_set = $exercise->getSet();
    				if($workout_set) {
    					$set_exercises = $workout_set->getExercises()->all();
    					foreach($set_exercises as $set_exercise) {
		                	$routines = $set_exercise->getScheduleRoutines('set')->all();
		                	if(sizeof($routines) > 0) {
		                		foreach($routines as $routine) {
		                			$sets = $routine->getScheduleSets();
		                			if(sizeof($sets) > 0) {
		                				foreach($sets as $set) {
		                           			$report = new Report();
		                           			$report->id_schedule = $this->id;
		                           			$report->id_set = $set->id;
		                					/*if(is_numeric($set->default_val)) {
		                						$report->default_val = $set->default_val;
		                					} else {
		                                        eval('$report->default_val = '.str_replace($globalKeys, $globalVals, $set->default_val).';');
		                					}*/
		                					$report->default_val = $set->default_val;
		                              		$report->save();
		                				}
		                			}
		                		}
		                	}
	                	}
                	}    			}    		}
    		$transaction->commit();    	}
    }

}