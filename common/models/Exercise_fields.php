<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Exercise_fields extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exercise_fields}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_exercise', 'title'], 'required'],
            [['id_exercise'], 'integer'],
            [['title'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_exercise' => 'ID упражнения',
            'title' => 'Название',
            'value' => 'Текст'
        ];
    }

    public function getExercise() {
        return $this->hasOne(Exercise::className(), ['id' => 'id_exercise'])->one();
    }

}