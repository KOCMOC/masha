<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Discount extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discount}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'period', 'discount'], 'required'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название элемента продажи',
            'period' => 'Период',
            'discount' => 'Скидка'
        ];
    }

}