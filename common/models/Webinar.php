<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%webinar}}".
 *
 * @property string $id
 * @property string $title
 * @property string $preview
 * @property string $short_text
 * @property string $full_text
 * @property string $video_content
 * @property string $date
 */
class Webinar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%webinar}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_text', 'date'], 'required'],
            [['short_text', 'full_text', 'video_content'], 'string'],
            [['date'], 'safe'],
            [['title', 'preview'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название вебинара',
            'preview' => 'Изображение',
            'short_text' => 'Короткий текст вебинара',
            'full_text' => 'Подробный текст вебинара',
            'video_content' => 'Видео контент',
            'date' => 'Дата',
        ];
    }
}
