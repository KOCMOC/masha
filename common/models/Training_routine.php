<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Training_routine extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_routine}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_exercise', 'set', 'sleep'], 'required'],
            [['id_exercise'], 'integer'],
            [['set', 'sleep'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_exercise' => 'ID упражнения',
            'set' => 'Подход',
            'sleep' => 'Отдых'
        ];
    }

    public function getExercise() {
        return $this->hasOne(Exercise::className(), ['id' => 'id_exercise'])->one();
    }

    public function getSets() {
        return $this->hasMany(Set::className(), ['id_tr_rout' => 'id'])->all();
    }

    public function del() {
    	$transaction = Yii::$app->db->beginTransaction();    	$sets = $this->getSets();
    	foreach($sets as $set) {    		if(!$set->delete()) {    			$transaction->rollback();
    			return false;    		}    	}
    	if(!$this->delete()) {    		$transaction->rollback();
    		return false;    	}
    	$transaction->commit();
    	return true;    }
}