<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Schedule_set extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schedule_set}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_set_attr', 'id_s_tr_rout'], 'required'],
            [['id_set_attr', 'id_s_tr_rout'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_set_attr' => 'ID атрибута подхода',
            'id_s_tr_rout' => 'ID подхода',
            'default_val' => 'Значение по умолчанию'
        ];
    }

    public function getSet_attribute() {
        return $this->hasOne(Set_attribute::className(), ['id' => 'id_set_attr'])->one();
    }

    public function getScheduleTraining_routine() {
        return $this->hasOne(Schedule_training_routine::className(), ['id' => 'id_s_tr_rout'])->one();
    }
}