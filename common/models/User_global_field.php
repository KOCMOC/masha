<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class User_global_field extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_global_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['id_user', 'id_global_field'], 'required'],
            ['id_user', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            ['id_global_field', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            //value rule
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_user' => 'ID пользователя',
            'id_global_field' => 'ID глобального значения',
            'value' => 'Значение'
        ];
    }

    public function getGlobal_field() {
        return $this->hasOne(Global_field::className(), ['id' => 'id_global_field'])->one();
    }

}