<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Schedule_workout_set extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schedule_workout_set}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['sleep'], 'number', 'min'=>0, 'message'=>'Должно быть положительным числом'],
            [['alert_proc', 'alert_sleep'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название сета',
            'sleep' => 'Отдых',
            'alert_proc' => 'Комментарий выполнение',
            'alert_sleep' => 'Комментарий отдых'
        ];
    }

    /*public function getWorkout() {
        return $this->hasOne(Workout::className(), ['id' => 'workout_id'])->one();
    }*/

	public function getSchedule_exercise() {
		return $this->hasOne(Schedule_exercise::className(), ['exercise_id' => 'id'])->where(['type'=>\common\models\Schedule_exercise::Set])->one();
    }

    public function getExercises($order = 'id') {
        return $this->hasMany(Schedule_set_exercise::className(), ['set_id' => 'id'])->orderBy($order);
    }

    public function del() {
		$set_exercises = $this->getExercises()->all();
		if(sizeof($set_exercises) > 0) {
			foreach($set_exercises as $set_exercise) {
            	$set_exercise->del();
			}
		}
		$this->delete();
       	return true;
    }

}