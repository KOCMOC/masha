<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Workout_report extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%workout_report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workout_id', 'workout_report_field_id'], 'required'],
            [['workout_id', 'workout_report_field_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'workout_id' => 'ID тренировки в курсе',
            'workout_report_field_id' => 'ID поля из справочника'
        ];
    }

    public function getField() {
        return $this->hasOne(Workout_report_field::className(), ['id' => 'workout_report_field_id'])->one();
    }

    public function getWorkout() {
        return $this->hasOne(Workout::className(), ['id' => 'workout_id'])->one();
    }
}