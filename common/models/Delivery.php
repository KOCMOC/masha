<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Delivery extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'email'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\Delivery', 'message' => 'Запрос уже был отправлен'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'fio' => 'Ф И О',
            'phone' => 'Телефон',
            'email' => 'E-mail'
        ];
    }
}