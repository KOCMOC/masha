<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property integer $tickets_role
 * @property boolean $is_vip
 * @property integer $trainer_id
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_NEW = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;

    const ROLE_SYSTEM = 0;
    const ROLE_ADMIN = 1;
    const ROLE_KURATOR = 2;
    const ROLE_USER = 3;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_ACTIVE, self::STATUS_DELETED]],

            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [self::ROLE_ADMIN, self::ROLE_KURATOR, self::ROLE_USER]],

            ['phone', 'default', 'value' => ''],

            [['is_vip'], 'boolean'],

            [['fio'], 'string'],

            [['trainer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['trainer_id' => 'id']],
            [['tickets_role'], 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['is_trainer' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

      /**
     * Finds user by email
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email, $status = self::STATUS_ACTIVE)
    {

        return static::findOne(['email' => $email, 'status' => $status]);

    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generatePassword()
    {
    	return substr(Yii::$app->security->generateRandomString(), 0, 7);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainer() {
        return $this->hasOne(User::className(), ['trainer_id' => 'id']);
    }

    /**
     * Set moor free trainer or set trainer by id
     * @param null $id
     */
    public function setTrainer($id = null) {
        if(is_null($id)) {
            $row = Yii::$app->db->createCommand("SELECT t.id,
(SELECT COUNT(*) FROM `user` WHERE trainer_id = t.id AND role = 2) AS c_run,
(SELECT COUNT(*) FROM `user` WHERE trainer_id = t.id AND role = 2) AS c_all
FROM user t WHERE role = 1
ORDER BY c_run, c_all")->queryOne();

            $this->trainer_id = $row['id'];
        }
        else {
            $this->trainer_id = $id;
        }
    }


    public function getAnket() {
        return $this->hasOne(UserAnket::className(), ['user_id' => 'id'])->one();
    }

    public function getFinanceAccount() {
    	return $this->hasOne(Finance_account::className(), ['user_id' => 'id'])->one();
    }

    public function getOrders() {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    public function getFinanceTransactions() {
    	return $this->hasMany(Finance_transaction::className(), ['user_id' => 'id']);
    }

    public function getGlobalFields() {
    	return $this->hasMany(User_global_field::className(), ['id_user' => 'id'])->all();
    }

}
