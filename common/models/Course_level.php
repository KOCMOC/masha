<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Course_level extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_level}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название уровня курса',
        ];
    }

}