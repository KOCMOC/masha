<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%diet}}".
 *
 * @property string $id
 * @property string $title
 * @property string $url
 * @property string $content
 */
class Diet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%diet}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url', 'content'], 'required'],
            [['content'], 'string'],
            [['title', 'url'], 'string', 'max' => 255],
            [['url'], 'unique'],
            [['min', 'max'], 'number'],
            [['vegan', 'mother'], 'in', 'range'=>range(0, 1)]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'min' => 'Минимальное значение интервала',
            'max' => 'Максимальное значение интервала',
            'vegan' => 'Вегетарианец',
            'mother' => 'Кормящая мать',
            'title' => 'Название страницы',
            'content' => 'Содержимое страницы',
        ];
    }

    public function clonCopy()
    {    	$diet = new Diet;
    	$diet->title = $this->title;
    	$diet->url = $this->url.'-1';
    	$diet->min = $this->min;
    	$diet->max = $this->max;
    	$diet->vegan = $this->vegan;
    	$diet->mother = $this->mother;
    	$diet->content = $this->content;
    	$diet->save();    }
}
