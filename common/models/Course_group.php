<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Course_group extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','description'], 'required'],
            [['title', 'description'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название группы курса',
            'description' => 'Описание группы курса'
        ];
    }

	public function getCourses() {
        return $this->hasMany(Course::className(), ['course_group_id' => 'id']);
    }

    public function getOrders() {    	return $this->hasMany(Order::className(), ['course_group_id' => 'id']);
    }

	public function getSellItems($order = 'id') {
        return $this->hasMany(Sell_item::className(), ['course_group_id' => 'id'])->orderBy($order);
    }

}