<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Order_global_field extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_global_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['id_order', 'id_global_field'], 'required'],
            ['id_order', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            ['id_global_field', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_order' => 'ID заказа',
            'id_global_field' => 'ID глобального значения'
        ];
    }

}