<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Set_attribute extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%set_attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['name', 'unit'], 'filter', 'filter' => 'trim'],
            [['name', 'unit'], 'string', 'length' => [2, 255]],
            /*['name', 'unique', 'message' => 'Такое название уже существует'],*/
            ['name', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название атрибута',
            'unit' => 'Единица измерения'
        ];
    }

    public static function getAttributesList()
    {		$attributes = self::find()->all();
       	$attributesList = [];
       	foreach($attributes as $attribute) {
       		$attributesList[$attribute->id] = [
       			'name'=>$attribute->name,
       			'unit'=>$attribute->unit
       		];
       	}
       	return $attributesList;    }
}