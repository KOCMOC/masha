<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%ticket_message}}".
 *
 * @property string $id
 * @property string $ticket_id
 * @property string $user_id
 * @property string $message
 * @property string $attachment
 * @property string $creation_datetime
 * @property string $read_datetime
 *
 * @property Ticket $ticket
 */
class TicketMessage extends \yii\db\ActiveRecord
{

    public $attachmentFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ticket_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'user_id', 'message', 'creation_datetime', 'read_datetime'], 'required'],
            [['ticket_id', 'user_id'], 'integer'],
            [['message', 'attachment'], 'string'],
            [['creation_datetime', 'read_datetime'], 'safe'],
            [['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticket_id' => 'id']],
            [['attachmentFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Тикет',
            'user_id' => 'Автор',
            'message' => 'Сообщение',
            'attachment' => 'Прикрепления',
            'creation_datetime' => 'Дата создания',
            'read_datetime' => 'Дата чтения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Ticket::className(), ['id' => 'ticket_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function upload()
    {
        $files = [];
        if ($this->validate()) {
            foreach ($this->attachmentFiles as $file) {
                $path = '/public/tickets/' . md5($file->baseName) . '.' . $file->extension;
                if(@$file->saveAs(Yii::getAlias('@frontend/web' . $path))) {
                    $files[] = [
                        'url' => $path,
                        'filename' => $file->baseName . '.' . $file->extension,
                    ];
                }
            }
        }

        $this->attachmentFiles = null;

        return json_encode($files);
    }

    public function getAttachments()
    {
        return json_decode($this->attachment, true);
    }
}
