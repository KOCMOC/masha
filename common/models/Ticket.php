<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%ticket}}".
 *
 * @property string $id
 * @property string $user_id
 * @property integer $role
 * @property string $title
 * @property integer $status
 * @property string $creation_datetime
 * @property string $read_datetime
 * @property integer $evaluation
 *
 * @property TicketMessage[] $ticketMessages
 */
class Ticket extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 0;
    const STATUS_VIEWERD = 1;
    const STATUS_ANSWERED = 2;
    const STATUS_NEW_ANSWER = 3;
    const STATUS_NEW_ANSWER_VIEWED = 4;
    const STATUS_CLOSED = 5;

    const EVALUATION_NONE = 0;
    const EVALUATION_LIKE = 1;
    const EVALUATION_DISLIKE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ticket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'creation_datetime', 'read_datetime'], 'required'],
            [['user_id', 'role', 'status', 'evaluation'], 'integer'],
            [['creation_datetime', 'read_datetime'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Автор',
            'role' => 'Роль',
            'title' => 'Тема тикета',
            'status' => 'Статус',
            'creation_datetime' => 'Дата создания',
            'read_datetime' => 'Дата чтения',
            'evaluation' => 'Оценка',
        ];
    }

    // TODO Должен возвращать название роли "Тренер, Мастер ..."
    public function getRoleName()
    {
        $role = UserRole::findOne(['id' => $this->role]);
        return (is_null($role)) ? '-' : $role->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id'])->orderBy(['creation_datetime' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getTrainer()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
