<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Schedule_exercise extends ActiveRecord
{

	const Exercise = 0;
	const Set = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schedule_exercise}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schedule_id', 'exercise_id', 'type'], 'required'],
            [['schedule_id', 'exercise_id', 'number', 'type'], 'number', 'min'=>0, 'message'=>'Должно быть положительным числом'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'schedule_id' => 'ID расписания',
            'exercise_id' => 'ID элемента',
            'type' => 'Тип элемента',
            'number' => 'Порядок упражнения в тренировке'
        ];
    }

    public function getSchedule() {
        return $this->hasOne(Schedule::className(), ['id' => 'workout_id'])->one();
    }

    public function getExercise() {
        return $this->hasOne(Exercise::className(), ['id' => 'exercise_id'])->one();
    }

    public function getSet() {
        return $this->hasOne(Schedule_workout_set::className(), ['id' => 'exercise_id'])->one();
    }

    public function getScheduleRoutines($order = 'id') {
    	return $this->hasMany(Schedule_training_routine::className(), ['id_relation' => 'id'])->where(['type'=>\common\models\Schedule_training_routine::WorkoutRelation])->orderBy($order);
    }

    public function del() {
    	if($this->type == \common\models\Workout_exercise::Exercise) {
			$routines = $this->getScheduleRoutines()->all();
			if(sizeof($routines) > 0) {
				foreach($routines as $routine) {
	            	$routine->del();
				}
			}
		} else {			if($set = $this->getSet()) {                $set->del();			}
		}
		$this->delete();
       	return true;
    }
}