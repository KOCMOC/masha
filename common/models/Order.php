<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Order extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','course_group_id'], 'required'],
            [['user_id','course_group_id'], 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            ['active', 'in', 'range'=>range(0, 3)],
            [['level', 'gender', 'expire'], 'default', 'value'=>'0'],
            ['schedule_template', 'default', 'value'=>''],
            ['expire', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'ID пользователя',
            'course_group_id' => 'ID курса',
            'active' => 'Курс активный',
            'expire' => 'Активен до'
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->one();
    }

    public function getAnket() {		return $this->hasMany(Order_anket::className(), ['order_id' => 'id'])->all();
    }

    /*public function getCourse() {
        return $this->hasOne(Course::className(), ['id' => 'course_id'])->one();
    }*/

    public function getCourseGroup() {    	return $this->hasOne(Course_group::className(), ['id' => 'course_group_id'])->one();    }

    public function getUserRelation() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /*public function getCourseRelation() {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }*/

    public function getCourseGroupRelation() {
        return $this->hasOne(Course_group::className(), ['id' => 'course_group_id']);
    }

    public function getSchedule() {
		return $this->hasMany(Schedule::className(), ['order_id' => 'id'])->all();
	}

    public function getScheduleRelation() {
		return $this->hasMany(Schedule::className(), ['order_id' => 'id']);
	}

    public function getTransactions() {    	return $this->hasMany(Finance_transaction::className(), ['subject_id' => 'id'])->where(['subject_type'=>\common\models\Finance_transaction::Subject_order]);    }

    public function getSellItem() {
        return $this->hasOne(Sell_item::className(), ['course_group_id' => 'course_group_id', 'type' => 'type'])->one();
    }

    public function getGlobalFields() {
    	return $this->hasMany(User_global_field::className(), ['id_order' => 'id'])->all();
    }

    public function getUserGlobalFields() {
    	return $this->hasMany(User_global_field::className(), ['id_order' => 'id', 'id_user' => 'user_id'])->all();
    }

    public function del() {
    	$schedule = $this->getSchedule();
    	if(sizeof($schedule) > 0) {
	    	foreach($schedule as $day) {	    		$day->del();	    	}
    	}

    	$anket = $this->getAnket();
    	if(sizeof($anket) > 0) {    		foreach($anket as $field) {    			$field->delete();    		}    	}

    	$this->delete();
    	return true;    }
}