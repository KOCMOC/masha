<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Exercise extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exercise}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','description_cut', 'description'/*, 'number'*/], 'required'],
            [['title', 'description_cut', 'description'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['description_cut', 'string', 'length' => [2, 512]],
            /*['number', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом']*/
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название упражнения',
            'description' => 'Полное описание выполнения упражнения',
            'description_cut' => 'Краткое описание упражнения',
            'number' => 'Позиция'
        ];
    }

    public function getTrainingRoutines($order = 'id') {
        return $this->hasMany(Training_routine::className(), ['id_exercise' => 'id'])->orderBy($order);
    }

    public function getFields() {    	return $this->hasMany(Exercise_fields::className(), ['id_exercise' => 'id']);    }

    public function getGlobalFields() {
    	return $this->hasMany(Global_field::className(), ['id_exercise' => 'id']);
    }

    public function clonCopy() {

		$return = array('status' => 'error', 'id' => 0);

    	$routines = $this->getTrainingRoutines()->all();
    	$fields = $this->getFields()->all();

    	$transaction = Yii::$app->db->beginTransaction();
    	$cloned_exercise = new Exercise;
    	$cloned_exercise->id_tab = 2;
    	$cloned_exercise->id_related = $this->id;
    	$cloned_exercise->workout_id = $this->workout_id;
    	$cloned_exercise->title = 'Копия - '.$this->title;
        $cloned_exercise->description_cut = $this->description_cut;
        $cloned_exercise->video = $this->video;
        $cloned_exercise->description = $this->description;
        $cloned_exercise->number = $this->number;
        if($cloned_exercise->save()) {
        	$cloned_exercise->refresh();
        	$return['id'] = $cloned_exercise->id;            if(sizeof($routines)) {            	foreach($routines as $routine) {                	$cloned_routine = new Training_routine;
                    $cloned_routine->id_exercise = $cloned_exercise->id;
                    $cloned_routine->set = $routine->set;
                    $cloned_routine->sleep = $routine->sleep;
                    $cloned_routine->alert_sleep = $routine->alert_sleep;
                    $cloned_routine->alert_proc = $routine->alert_proc;
                    if($cloned_routine->save()) {
                    	$cloned_routine->refresh();                    	$sets = $routine->getSets();
                    	if(sizeof($sets) > 0) {                    		foreach($sets as $set) {                    			$cloned_set = new Set;
                    			$cloned_set->id_set_attr = $set->id_set_attr;
                    			$cloned_set->id_tr_rout = $cloned_routine->id;
                    			$cloned_set->default_val = $set->default_val;
                    			if(!$cloned_set->save()) {			                    	$transaction->rollback();
									return $return;                    			}                    		}                    	}                    } else {                    	$transaction->rollback();
						return $return;                    }            	}            }

            if(sizeof($fields)) {
            	foreach($fields as $field) {
                	$cloned_field = new Exercise_fields;
                	$cloned_field->id_exercise = $cloned_exercise->id;
                    $cloned_field->title = $field->title;
                    $cloned_field->value = $field->value;
					if(!$cloned_field->save()) {
						$transaction->rollback();
						return $return;
					}
            	}
            }        } else {			$transaction->rollback();
			return $return;        }

        $transaction->commit();
        $return['status'] = 'ok';
        return $return;    }

    public function del() {    	$routines = $this->getTrainingRoutines()->all();
    	$fields = $this->getFields()->all();

    	$transaction = Yii::$app->db->beginTransaction();

    	if(sizeof($routines) > 0) {    		foreach($routines as $routine) {           		if(!$routine->del()) {		    		$transaction->rollback();
		    		return false;           		}    		}    	}

    	if(sizeof($fields) > 0) {    		foreach($fields as $field) {    			if(!$field->delete()) {    				$transaction->rollback();
		    		return false;    			}    		}    	}

    	if($this->delete()) {			$transaction->commit();
			return true;    	} else {			$transaction->rollback();
			return false;    	}    }
}