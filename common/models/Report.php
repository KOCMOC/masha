<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Report extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_schedule','id_set'], 'required'],
            [['id_schedule', 'id_set'], 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_schedule' => 'ID занятия',
            'id_set' => 'ID параметра'
        ];
    }

    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'id_schedule'])->one();
    }

    public function getSet() {
        return $this->hasOne(Set::className(), ['id' => 'id_set'])->one();
    }

    public function getScheduleSet() {
        return $this->hasOne(Schedule_set::className(), ['id' => 'id_set'])->one();
    }

}