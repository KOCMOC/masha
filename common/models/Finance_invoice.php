<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Finance_invoice extends ActiveRecord
{

	const Status_progress = 0;
	const Status_approved = 1;
	const Status_declined = 2;

	const Method_yakassa = 0;
	const Method_cache = 1;
	const Method_electro = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%finance_invoice}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'method'], 'required'],
            ['user_id', 'number', 'integerOnly' => true, 'min' => 1],
            ['updated_by', 'number', 'integerOnly' => true, 'min' => 0],
            ['amount', 'number', 'integerOnly' => false],
            ['method', 'in', 'range' => [self::Method_yakassa, self::Method_cache, self::Method_electro], 'allowArray' => true],
            ['status', 'in', 'range' => [self::Status_progress, self::Status_approved, self::Status_declined], 'allowArray' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'ID пользователя',
            'updated_by' => 'ID оператора',
            'trigger_id' => 'ID триггера',
            'amount' => 'Сумма транзакции',
            'method' => 'Метод оплаты',
            'status' => 'Статус оплаты'
        ];
    }

    public function findByHash($hash) {    	return Finance_invoice::find()->where(['hash'=>$hash])->one();    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->one();
    }

    public function commit(){		/* если всё хорошо, и транзакция по приёму денег прошла */
		$transactions = $this->getUser()->getFinanceTransactions()->orderBy('created_at')->all();
		$stopit = false;
		foreach($transactions as $transaction) if(!$stopit) if(!$transaction->commit()) $stopit = true;    }

    public function rollback(){

    }

}