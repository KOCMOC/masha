<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Course_global_field extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_global_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['id_course', 'id_global_field'], 'required'],
            ['id_course', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            ['id_global_field', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_course' => 'ID курса',
            'id_global_field' => 'ID глобального значения'
        ];
    }

    public function getGlobalField() {
        return $this->hasOne(Global_field::className(), ['id' => 'id_global_field'])->one();
    }

}