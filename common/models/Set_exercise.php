<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Set_exercise extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%set_exercise}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['set_id', 'exercise_id'], 'required'],
            [['set_id', 'exercise_id', 'number'], 'number', 'min'=>0, 'message'=>'Должно быть положительным числом'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'set_id' => 'ID сета',
            'exercise_id' => 'ID упражнения',
            'number' => 'Порядок упражнения в сете'
        ];
    }

    public function getExercise() {
        return $this->hasOne(Exercise::className(), ['id' => 'exercise_id'])->one();
    }

    public function getSet() {
        return $this->hasOne(Workout_set::className(), ['id' => 'set_id'])->one();
    }

    public function getCourseRoutines($order = 'id') {
    	return $this->hasMany(Course_training_routine::className(), ['id_relation' => 'id'])->where(['type'=>\common\models\Course_training_routine::SetRelation])->orderBy($order);
    }

    public function del() {
		$routines = $this->getCourseRoutines()->all();
		$transaction = Yii::$app->db->beginTransaction();
		if(sizeof($routines) > 0) {
			foreach($routines as $routine) {
            	if(!$routine->del()) {
                	$transaction->rollback();
                	return false;
            	}
			}
		}
       	if(!$this->delete()) {
           	$transaction->rollback();
           	return false;
       	}
       	$transaction->commit();
       	return true;
    }

}