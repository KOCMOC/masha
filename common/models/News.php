<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property string $id
 * @property string $title
 * @property string $preview
 * @property string $short_text
 * @property string $full_text
 * @property string $date
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_text', 'date'], 'required'],
            [['short_text', 'full_text'], 'string'],
            [['date'], 'safe'],
            [['title', 'preview'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название новости',
            'preview' => 'Изображение',
            'short_text' => 'Короткий текст новости',
            'full_text' => 'Подробный текст новости',
            'date' => 'Дата',
        ];
    }
}
