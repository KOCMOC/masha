<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Course extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_group_id', 'level', 'gender', 'title','description'], 'required'],
            [['course_group_id', 'level', 'gender'], 'integer', 'min' => 1],
            [['title', 'description'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
        	'course_group_id' => 'ID группы курсов',
            'title' => 'Название курса',
            'description' => 'Описание курса',
            'level' => 'Уровень',
            'gender' => 'Пол',
            'place' => 'Дом/зал'
        ];
    }

    public static function getGenderList()
    {
    	return [
    		'1' => 'муж.',
    		'2' => 'жен.'
    	];
    }

    public static function getPlaceList()
    {
    	return [
    		'1' => 'дом',
    		'2' => 'зал'
    	];
    }

	public function getCourseGroup() {
        return $this->hasOne(Course_group::className(), ['id' => 'course_group_id']);
    }

	public function getWorkouts($order = 'id') {
        return $this->hasMany(Workout::className(), ['course_id' => 'id'])->orderBy($order);
    }

	public function getAnket() {
        return $this->hasMany(Course_anket::className(), ['course_id' => 'id'])->all();
    }

    public function clonCopy() {
    	$ankets = $this->getAnket();
    	$globals = \common\models\Global_field::find()->where(['id_course'=>$this->id])->all();
    	$workouts = $this->getWorkouts()->all();

		$course = new Course;
		$course->course_group_id = $this->course_group_id;
		$course->level = $this->level;
		$course->gender = $this->gender;
		$course->place = $this->place;
		$course->title = 'Копия '.$this->title;
		$course->description = $this->description;
		$course->schedule_template = $this->schedule_template;
		if($course->save()) {
			$course->refresh();
	    	if(sizeof($ankets) > 0) {
				foreach($ankets as $anket) {
					$canket = new Course_anket;
		    		$canket->course_id = $course->id;
		    		$canket->title = $anket->title;
		    		$canket->type = $anket->type;
		    		$canket->alias = $anket->alias;
		    		$canket->save();

				}
	    	}

	    	foreach($globals as $global) {	    		$cglobal = new \common\models\Global_field;
	    		$cglobal->id_course = $course->id;
	    		$cglobal->id_exercise = $global->id_exercise;
	    		$cglobal->title = $global->title;
	    		$cglobal->alias = $global->alias;
	    		$cglobal->default = $global->default;
	    		$cglobal->save();	    	}

	    	if(sizeof($workouts) > 0) {
	    		foreach($workouts as $workout) {
	    			$workout->clonCopy($course->id);
	    		}
	    	}
    	}
    }

    public function order($order, $user_id = false) {
    	$user_id = ($user_id === false)?Yii::$app->user->identity->id:$user_id;

    	$globals = \common\models\Global_field::find()->where(['id_course'=>$this->id])->all();

    	$userOrder = \common\models\Order::find()->where(['user_id'=>$user_id, 'course_group_id'=>$this->course_group_id])->andWhere(['!=', 'active', 2])->one();

    	$transaction = Yii::$app->db->beginTransaction();

    	/*$globalKeys = array();
    	$globalVals = array();*/
    	if(sizeof($globals) > 0)
	    	foreach($globals as $global) {
	    		$user_global = new \common\models\User_global_field;
	    		$user_global->id_order = $userOrder->id;
	    		$user_global->id_user = $user_id;
	    		$user_global->id_exercise = $global->id_exercise;
	    		$user_global->id_global_field = $global->id;
	    		$user_global->title = $global->title;
	    		$user_global->alias = $global->alias;
	    		$user_global->value = $global->default;
	    		if(!$user_global->save()) {
					$transaction->rollback();     die('0');
					return false;
	    		}
            	/*array_push($globalKeys, $global->alias);
            	array_push($globalVals, $global->default);*/
	    	}

    	/*$userOrder = new \common\models\Order();
    	$userOrder->user_id = $user_id;
    	$userOrder->course_group_id = $this->course_group_id;
    	$userOrder->level = $this->level;
    	$userOrder->gender = $this->gender;
    	$userOrder->active = 0;
    	if(!$userOrder->save()) {
    		$transaction->rollback();
    		return false;
    	}
        $userOrder->refresh();  */

		$anket_fields_ass_arr = [];
		$anket_fields = $this->getAnket();
		if(sizeof($anket_fields) > 0)
        foreach($anket_fields as $anket_field) {
	        $orderAnket = new \common\models\Order_anket();
	        $orderAnket->order_id = $userOrder->id;
	        $orderAnket->title = $anket_field->title;
	        $orderAnket->type = $anket_field->type;
	        $orderAnket->alias = $anket_field->alias;
	        if(!$orderAnket->save()) {
	    		$transaction->rollback(); die('1');
	    		return false;
	    	}
	    	$orderAnket->refresh();
	    	$anket_fields_ass_arr[$anket_field->id] = $orderAnket->id;
        }

        $workouts = $this->getWorkouts('number')->all();

        $schdays = explode(',', $userOrder->schedule_template);

        $workouts_arr = $workouts;

        $selected_time = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        $first = false;

        while($selected_time < $userOrder->expire) {
        	if(sizeof($workouts) == 0) $workouts = $workouts_arr;
        	$sch_date = date('Y-m-d', $selected_time);
        	$today = ($sch_date == date('Y-m-d'))?true:false;
        	if(in_array(date('w', $selected_time), $schdays)) {
                $workout = array_shift($workouts);
                if($workout->first == 1) {
                	if($first === false) {
                		$first = true;
                	} else {
                		$workout = array_shift($workouts);
                	}
                }
                if(!$workout) {
                	$selected_time = $userOrder->expire;
                	continue;
                }

				$schedule = new \common\models\Schedule();
				$schedule->order_id = $userOrder->id;
				$schedule->workout_id = $workout->id;
				$schedule->date = $sch_date;
				$schedule->done = 0;
    			if($schedule->save()) {
    				$schedule->refresh();
    				$workout_report_fields = $workout->getReport()->all();
                	if(sizeof($workout_report_fields) > 0)
                	foreach($workout_report_fields as $workout_report_field) {
                		$schedule_report = new \common\models\Schedule_report();
                		$schedule_report->schedule_id = $schedule->id;
                		$schedule_report->workout_report_field_id = $workout_report_field->id;
                		$schedule_report->related_anket_field = isset($anket_fields_ass_arr[$workout_report_field->related_anket_field])?$anket_fields_ass_arr[$workout_report_field->related_anket_field]:0;
                		if(!$schedule_report->save()) {
				    		$transaction->rollback();die('2');
				    		return false;
                        }
                	}
    				$workout_exercises = $workout->getWorkoutExercises('number')->all();
                	if(sizeof($workout_exercises) > 0)
                	foreach($workout_exercises as $workout_exercise) {
                    	$schedule_exercise = new \common\models\Schedule_exercise();
                    	$schedule_exercise->schedule_id = $schedule->id;
                    	$schedule_exercise->exercise_id = $workout_exercise->exercise_id;
                    	$schedule_exercise->type = $workout_exercise->type;
                    	$schedule_exercise->number = $workout_exercise->number;
                    	if($schedule_exercise->save()) {
                    		$schedule_exercise->refresh();
                    		if($workout_exercise->type == \common\models\Workout_exercise::Exercise) {
                                $routines = $workout_exercise->getCourseRoutines('set')->all();
                                if(sizeof($routines) > 0)
                                foreach($routines as $routine) {
                                	$schedule_routine = new \common\models\Schedule_training_routine();
                                	$schedule_routine->id_relation = $schedule_exercise->id;
                                	$schedule_routine->type = $routine->type;
                                	$schedule_routine->set = $routine->set;
                                	$schedule_routine->sleep = $routine->sleep;
                                	$schedule_routine->alert_proc = $routine->alert_proc;
                                	$schedule_routine->alert_sleep = $routine->alert_sleep;
                                	if($schedule_routine->save()) {
                                        $schedule_routine->refresh();
                                        $sets = $routine->getCourseSets();
                                        if(sizeof($sets) > 0)
                                        foreach($sets as $set) {
                                        	$schedule_set = new \common\models\Schedule_set();
                                        	$schedule_set->id_set_attr = $set->id_set_attr;
                                        	$schedule_set->id_s_tr_rout = $schedule_routine->id;
                                        	$schedule_set->default_val = $set->default_val;
                                        	$schedule_set->trigger = $set->trigger;
                                        	$schedule_set->to_global = $set->to_global;
                                        	if(!$schedule_set->save()) {
									    		$transaction->rollback();die('3');
									    		return false;
                                        	}
                                       		if($today === true) {
                                       			$report = new \common\models\Report();
                                       			$report->id_schedule = $schedule->id;
                                       			$report->id_set = $schedule_set->id;
                                       			if($schedule_set->to_global == 1) {
                                       				$report->default_val = $set->default_val;
                                       			} else {
                                       				//if(is_numeric($set->default_val)) {
                                       					$report->default_val = $set->default_val;
                                       				/*} else {
                                       					$default_val = 0;
                                       					$eval_default = '$default_val = '.str_replace($globalKeys, $globalVals, $set->default_val).';';
                                                                 @eval($eval_default);
                                                                 $report->default_val = $default_val;
                                       				}*/
                                       			}
                                       			//$report->trigger = $set->trigger;
                                          		$report->save();
                                       		}
                                        }
                                	} else {
							    		$transaction->rollback();     die('4');
							    		return false;
                                	}
                                }
                    		} elseif($workout_exercise->type == \common\models\Workout_exercise::Set) {
                                $workout_set = $workout_exercise->getSet();
                                $schedule_workout_set = new \common\models\Schedule_workout_set();
                                $schedule_workout_set->title = $workout_set->title;
                                $schedule_workout_set->sleep = $workout_set->sleep;
                                $schedule_workout_set->alert_proc = $workout_set->alert_proc;
                                $schedule_workout_set->alert_sleep = $workout_set->alert_sleep;
                                if($schedule_workout_set->save()) {
                                    $schedule_workout_set->refresh();
                                    $schedule_exercise->exercise_id = $schedule_workout_set->id;
                                    if(!$schedule_exercise->save()) {
							    		$transaction->rollback();die('5');
							    		return false;
                                    }
                                    $set_exercises = $workout_set->getExercises('number')->all();
                                    if(sizeof($set_exercises) > 0)
                                    foreach($set_exercises as $set_exercise) {
	                                    $schedule_set_exercise = new \common\models\Schedule_set_exercise();
	                                    $schedule_set_exercise->set_id = $schedule_workout_set->id;
	                                    $schedule_set_exercise->exercise_id = $set_exercise->exercise_id;
	                                    $schedule_set_exercise->number = $set_exercise->number;
	                                    if($schedule_set_exercise->save()) {
		                                    $schedule_set_exercise->refresh();
		                                    $set_routines = $set_exercise->getCourseRoutines('set')->all();
		                                    if(sizeof($set_routines) > 0)
		                                    foreach($set_routines as $set_routine) {
		                                    	$schedule_routine = new \common\models\Schedule_training_routine();
		                                    	$schedule_routine->id_relation = $schedule_set_exercise->id;
		                                    	$schedule_routine->type = $set_routine->type;
		                                    	$schedule_routine->set = $set_routine->set;
		                                    	$schedule_routine->sleep = $set_routine->sleep;
		                                    	$schedule_routine->alert_proc = $set_routine->alert_proc;
		                                    	$schedule_routine->alert_sleep = $set_routine->alert_sleep;
		                                    	if($schedule_routine->save()) {
		                                    		$schedule_routine->refresh();
			                                        $sets = $set_routine->getCourseSets();
			                                        if(sizeof($sets) > 0)
			                                        foreach($sets as $set) {
			                                        	$schedule_set = new \common\models\Schedule_set();
			                                        	$schedule_set->id_set_attr = $set->id_set_attr;
			                                        	$schedule_set->id_s_tr_rout = $schedule_routine->id;
			                                        	$schedule_set->default_val = $set->default_val;
			                                        	$schedule_set->trigger = $set->trigger;
			                                        	$schedule_set->to_global = $set->to_global;
			                                        	if(!$schedule_set->save()) {
												    		$transaction->rollback();die('6');
												    		return false;
			                                        	}
		                                        		if($today === true) {
		                                        			$report = new \common\models\Report();
		                                        			$report->id_schedule = $schedule->id;
		                                        			$report->id_set = $schedule_set->id;
		                                        			if($schedule_set->to_global == 1) {
		                                        				$report->default_val = $set->default_val;
		                                        			} else {
		                                        				//if(is_numeric($set->default_val)) {
		                                        					$report->default_val = $set->default_val;
		                                        				/*} else {
		                                        					$default_val = 0;
		                                        					$eval_default = '$default_val = '.str_replace($globalKeys, $globalVals, $set->default_val).';';
                                                                    @eval($eval_default);
                                                                    $report->default_val = $default_val;
		                                        				}*/
		                                        			}
		                                        			//$report->trigger = $set->trigger;
		                                           			$report->save();
		                                        		}
			                                        }
				                                } else {
										    		$transaction->rollback();die('7');
										    		return false;
				                                }
		                                    }
		                                } else {
								    		$transaction->rollback();die('8');
								    		return false;
		                                }
                                    }
                                } else {
						    		$transaction->rollback();die('9');
						    		return false;
                                }
                    		}
                    	} else {
				    		$transaction->rollback();die('10');
				    		return false;
                    	}
                	}
    			} else {
    				print_R($schedule->errors);
		    		$transaction->rollback();die('11');
		    		return false;
    			}
			}
        	$selected_time = strtotime($sch_date.' + 1 day');
        }
    	$transaction->commit();
    	$user = \common\models\User::findOne($user_id);
    	$user->course = $userOrder->id;
    	$user->save();
    	return true;
    }

    public function extend($order) {
    	$user_id = $order->user_id;//($user_id === false)?Yii::$app->user->identity->id:$user_id;

    	$globals = \common\models\Global_field::find()->where(['id_course'=>$this->id])->all();
    	$_userGlobals = \common\models\User_global_field::find()->where(['id_order'=>$order->id])->all();
    	$orderAnkets = \common\models\Order_anket::find()->where(['order_id'=>$order->id])->all();
    	$userGlobals = array();

    	$userOrder = $order;//\common\models\Order::find()->where(['user_id'=>$user_id, 'course_group_id'=>$this->course_group_id])->andWhere(['!=', 'active', 2])->one();

    	$transaction = Yii::$app->db->beginTransaction();

    	$order->active = 1;
    	$order->save();

    	foreach($_userGlobals as $_userGlobal) {        	$userGlobals[$_userGlobal->id_exercise.'_'.$_userGlobal->alias] = $_userGlobal;    	}

    	if(sizeof($globals) > 0)
	    	foreach($globals as $global)
	    		if(!isset($userGlobals[$global->id_exercise.'_'.$global->alias])) {
		    		$user_global = new \common\models\User_global_field;
		    		$user_global->id_order = $userOrder->id;
		    		$user_global->id_user = $user_id;
		    		$user_global->id_exercise = $global->id_exercise;
		    		$user_global->id_global_field = $global->id;
		    		$user_global->title = $global->title;
		    		$user_global->alias = $global->alias;
		    		$user_global->value = $global->default;
		    		if(!$user_global->save()) {
						$transaction->rollback();     die('0');
						return false;
		    		}
		    	}

		$anket_fields_ass_arr = [];
		$anket_fields = $this->getAnket();
		if(sizeof($anket_fields) > 0)
        foreach($anket_fields as $anket_field) {
	        foreach($orderAnkets as $orderAnket) {
	    		if($orderAnket->title == $anket_field->title and $orderAnket->alias == $anket_field->alias) {
	    			$anket_fields_ass_arr[$anket_field->id] = $orderAnket->id;
	    		}
	    	}
        }

        $workouts = $this->getWorkouts('number')->all();

        $schdays = explode(',', $userOrder->schedule_template);

        $workouts_arr = $workouts;

        $selected_time = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        $last_schedule = $userOrder->getScheduleRelation()->orderBy(['date' => SORT_DESC])->one();
        $last_workout_number = $last_schedule->getWorkout()->number;

		while($last_workout_number >= $workouts[0]->number) {
        	array_shift($workouts);
        }

        while($selected_time < $userOrder->expire) {
        	if(sizeof($workouts) == 0) $workouts = $workouts_arr;
        	$sch_date = date('Y-m-d', $selected_time);
        	$today = ($sch_date == date('Y-m-d'))?true:false;
        	if(in_array(date('w', $selected_time), $schdays)) {
                $workout = array_shift($workouts);
                if($workout->first == 1) {
                	$workout = array_shift($workouts);
                }
                if(!$workout) {
                	$selected_time = $userOrder->expire;
                	continue;
                }

				$schedule = new \common\models\Schedule();
				$schedule->order_id = $userOrder->id;
				$schedule->workout_id = $workout->id;
				$schedule->date = $sch_date;
				$schedule->done = 0;
    			if($schedule->save()) {
    				$schedule->refresh();
    				$workout_report_fields = $workout->getReport()->all();
                	if(sizeof($workout_report_fields) > 0)
                	foreach($workout_report_fields as $workout_report_field) {
                		$schedule_report = new \common\models\Schedule_report();
                		$schedule_report->schedule_id = $schedule->id;
                		$schedule_report->workout_report_field_id = $workout_report_field->id;
                		$schedule_report->related_anket_field = isset($anket_fields_ass_arr[$workout_report_field->related_anket_field])?$anket_fields_ass_arr[$workout_report_field->related_anket_field]:0;
                		if(!$schedule_report->save()) {
				    		$transaction->rollback();die('2');
				    		return false;
                        }
                	}
    				$workout_exercises = $workout->getWorkoutExercises('number')->all();
                	if(sizeof($workout_exercises) > 0)
                	foreach($workout_exercises as $workout_exercise) {
                    	$schedule_exercise = new \common\models\Schedule_exercise();
                    	$schedule_exercise->schedule_id = $schedule->id;
                    	$schedule_exercise->exercise_id = $workout_exercise->exercise_id;
                    	$schedule_exercise->type = $workout_exercise->type;
                    	$schedule_exercise->number = $workout_exercise->number;
                    	if($schedule_exercise->save()) {
                    		$schedule_exercise->refresh();
                    		if($workout_exercise->type == \common\models\Workout_exercise::Exercise) {
                                $routines = $workout_exercise->getCourseRoutines('set')->all();
                                if(sizeof($routines) > 0)
                                foreach($routines as $routine) {
                                	$schedule_routine = new \common\models\Schedule_training_routine();
                                	$schedule_routine->id_relation = $schedule_exercise->id;
                                	$schedule_routine->type = $routine->type;
                                	$schedule_routine->set = $routine->set;
                                	$schedule_routine->sleep = $routine->sleep;
                                	$schedule_routine->alert_proc = $routine->alert_proc;
                                	$schedule_routine->alert_sleep = $routine->alert_sleep;
                                	if($schedule_routine->save()) {
                                        $schedule_routine->refresh();
                                        $sets = $routine->getCourseSets();
                                        if(sizeof($sets) > 0)
                                        foreach($sets as $set) {
                                        	$schedule_set = new \common\models\Schedule_set();
                                        	$schedule_set->id_set_attr = $set->id_set_attr;
                                        	$schedule_set->id_s_tr_rout = $schedule_routine->id;
                                        	$schedule_set->default_val = $set->default_val;
                                        	$schedule_set->trigger = $set->trigger;
                                        	$schedule_set->to_global = $set->to_global;
                                        	if(!$schedule_set->save()) {
									    		$transaction->rollback();die('3');
									    		return false;
                                        	}
                                       		if($today === true) {
                                       			$report = new \common\models\Report();
                                       			$report->id_schedule = $schedule->id;
                                       			$report->id_set = $schedule_set->id;
                                       			if($schedule_set->to_global == 1) {
                                       				$report->default_val = $set->default_val;
                                       			} else {
                                       				//if(is_numeric($set->default_val)) {
                                       					$report->default_val = $set->default_val;
                                       				/*} else {
                                       					$default_val = 0;
                                       					$eval_default = '$default_val = '.str_replace($globalKeys, $globalVals, $set->default_val).';';
                                                                 @eval($eval_default);
                                                                 $report->default_val = $default_val;
                                       				}*/
                                       			}
                                       			//$report->trigger = $set->trigger;
                                          		$report->save();
                                       		}
                                        }
                                	} else {
							    		$transaction->rollback();     die('4');
							    		return false;
                                	}
                                }
                    		} elseif($workout_exercise->type == \common\models\Workout_exercise::Set) {
                                $workout_set = $workout_exercise->getSet();
                                $schedule_workout_set = new \common\models\Schedule_workout_set();
                                $schedule_workout_set->title = $workout_set->title;
                                $schedule_workout_set->sleep = $workout_set->sleep;
                                $schedule_workout_set->alert_proc = $workout_set->alert_proc;
                                $schedule_workout_set->alert_sleep = $workout_set->alert_sleep;
                                if($schedule_workout_set->save()) {
                                    $schedule_workout_set->refresh();
                                    $schedule_exercise->exercise_id = $schedule_workout_set->id;
                                    if(!$schedule_exercise->save()) {
							    		$transaction->rollback();die('5');
							    		return false;
                                    }
                                    $set_exercises = $workout_set->getExercises('number')->all();
                                    if(sizeof($set_exercises) > 0)
                                    foreach($set_exercises as $set_exercise) {
	                                    $schedule_set_exercise = new \common\models\Schedule_set_exercise();
	                                    $schedule_set_exercise->set_id = $schedule_workout_set->id;
	                                    $schedule_set_exercise->exercise_id = $set_exercise->exercise_id;
	                                    $schedule_set_exercise->number = $set_exercise->number;
	                                    if($schedule_set_exercise->save()) {
		                                    $schedule_set_exercise->refresh();
		                                    $set_routines = $set_exercise->getCourseRoutines('set')->all();
		                                    if(sizeof($set_routines) > 0)
		                                    foreach($set_routines as $set_routine) {
		                                    	$schedule_routine = new \common\models\Schedule_training_routine();
		                                    	$schedule_routine->id_relation = $schedule_set_exercise->id;
		                                    	$schedule_routine->type = $set_routine->type;
		                                    	$schedule_routine->set = $set_routine->set;
		                                    	$schedule_routine->sleep = $set_routine->sleep;
		                                    	$schedule_routine->alert_proc = $set_routine->alert_proc;
		                                    	$schedule_routine->alert_sleep = $set_routine->alert_sleep;
		                                    	if($schedule_routine->save()) {
		                                    		$schedule_routine->refresh();
			                                        $sets = $set_routine->getCourseSets();
			                                        if(sizeof($sets) > 0)
			                                        foreach($sets as $set) {
			                                        	$schedule_set = new \common\models\Schedule_set();
			                                        	$schedule_set->id_set_attr = $set->id_set_attr;
			                                        	$schedule_set->id_s_tr_rout = $schedule_routine->id;
			                                        	$schedule_set->default_val = $set->default_val;
			                                        	$schedule_set->trigger = $set->trigger;
			                                        	$schedule_set->to_global = $set->to_global;
			                                        	if(!$schedule_set->save()) {
												    		$transaction->rollback();die('6');
												    		return false;
			                                        	}
		                                        		if($today === true) {
		                                        			$report = new \common\models\Report();
		                                        			$report->id_schedule = $schedule->id;
		                                        			$report->id_set = $schedule_set->id;
		                                        			if($schedule_set->to_global == 1) {
		                                        				$report->default_val = $set->default_val;
		                                        			} else {
		                                        				//if(is_numeric($set->default_val)) {
		                                        					$report->default_val = $set->default_val;
		                                        				/*} else {
		                                        					$default_val = 0;
		                                        					$eval_default = '$default_val = '.str_replace($globalKeys, $globalVals, $set->default_val).';';
                                                                    @eval($eval_default);
                                                                    $report->default_val = $default_val;
		                                        				}*/
		                                        			}
		                                        			//$report->trigger = $set->trigger;
		                                           			$report->save();
		                                        		}
			                                        }
				                                } else {
										    		$transaction->rollback();die('7');
										    		return false;
				                                }
		                                    }
		                                } else {
								    		$transaction->rollback();die('8');
								    		return false;
		                                }
                                    }
                                } else {
						    		$transaction->rollback();die('9');
						    		return false;
                                }
                    		}
                    	} else {
				    		$transaction->rollback();die('10');
				    		return false;
                    	}
                	}
    			} else {
    				print_R($schedule->errors);
		    		$transaction->rollback();die('11');
		    		return false;
    			}
			}
        	$selected_time = strtotime($sch_date.' + 1 day');
        }
    	$transaction->commit();
    	$user = \common\models\User::findOne($user_id);
    	$user->course = $userOrder->id;
    	$user->save();
    	return true;
    }

    public function del() {
   		$delete = true;
   		$workouts = $this->getWorkouts()->all();
   		if(sizeof($workouts) > 0) {
   			$transaction = Yii::$app->db->beginTransaction();
   			foreach($workouts as $workout) {
    			$workout_exercises = $workout->getWorkoutExercises()->all();
    			if(sizeof($workout_exercises) > 0)
				foreach($workout_exercises as $workout_exercise) {
   					if($workout_exercise->type == \common\models\Workout_exercise::Set) {
   						$set = $workout_exercise->getSet();
                        $set_exercises = $set->getExercises()->all();
                        foreach($set_exercises as $set_exercise) {
                        	if(!$set_exercise->delete()) {
                        		$delete = false;
				    			/*$response['status'] = 'error';
				                $response['error'] = 'Не удалось удалить тренировку';*/
				                $transaction->rollback();
                        	}
                        }
                        if(!$set->delete()) {
                        	$delete = false;
			    			/*$response['status'] = 'error';
			                $response['error'] = 'Не удалось удалить тренировку';*/
                        	$transaction->rollback();
                        }
   					}
					if(!$workout_exercise->delete()) {
						$delete = false;
						/*$response['status'] = 'error';
						$response['error'] = 'Не удалось удалить тренировку';*/
						$transaction->rollback();
					}
   				}
				if(!$workout->delete()) {
					$delete = false;
					/*$response['status'] = 'error';
					$response['error'] = 'Не удалось удалить тренировку';*/
					$transaction->rollback();
				}

   			}
   			$transaction->commit();
   		}
   		if($delete === true) {
   			$ankets = $this->getAnket();
   			if(sizeof($ankets) > 0) {
   				foreach($ankets as $anket) {
   					$anket->del();
   				}
   			}
   			$this->delete();
   			return true;
   		} else {
   			return false;
   		}
    }

}