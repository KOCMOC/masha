<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Workout_exercise extends ActiveRecord
{

	const Exercise = 0;
	const Set = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%workout_exercise}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workout_id', 'exercise_id', 'type'], 'required'],
            [['workout_id', 'exercise_id', 'number', 'type'], 'number', 'min'=>0, 'message'=>'Должно быть положительным числом'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'workout_id' => 'ID тренировки',
            'exercise_id' => 'ID элемента',
            'type' => 'Тип элемента',
            'number' => 'Порядок элемента в тренировке'
        ];
    }

    public function getWorkout() {
        return $this->hasOne(Workout::className(), ['id' => 'workout_id'])->one();
    }

    public function getExercise() {
        return $this->hasOne(Exercise::className(), ['id' => 'exercise_id'])->one();
    }

    public function getSet() {
        return $this->hasOne(Workout_set::className(), ['id' => 'exercise_id'])->one();
    }

    public function getCourseRoutines($order = 'id') {    	return $this->hasMany(Course_training_routine::className(), ['id_relation' => 'id'])->where(['type'=>\common\models\Course_training_routine::WorkoutRelation])->orderBy($order);    }

    public function del() {		$routines = $this->getCourseRoutines()->all();
		$transaction = Yii::$app->db->beginTransaction();
		if(sizeof($routines) > 0) {			foreach($routines as $routine) {            	if(!$routine->del()) {                	$transaction->rollback();
                	return false;            	}			}		}
       	if(!$this->delete()) {
           	$transaction->rollback();
           	return false;
       	}
       	$transaction->commit();
       	return true;    }

    public function clonCopy($workout_id = false) {
    	$response = array('status'=>'error', 'error'=>'', 'set_id'=>'');

    	$transaction = Yii::$app->db->beginTransaction();

		if($workout_id === false) {
    		$last_one = $this->getWorkout()->getWorkoutExercises('number desc')->asArray()->one();
    	}

    	$clone_workout_exercise = new Workout_exercise;
    	$clone_workout_exercise->workout_id = ($workout_id === false)?$this->workout_id:$workout_id;
    	$clone_workout_exercise->exercise_id = $this->exercise_id;
    	$clone_workout_exercise->type = $this->type;
    	$clone_workout_exercise->number = ($workout_id === false)?($last_one['number'] + 1):$this->number;
    	$clone_workout_exercise->trigger_yes = $this->trigger_yes;
    	$clone_workout_exercise->trigger_no = $this->trigger_no;
    	if($clone_workout_exercise->save()) {
    		$clone_workout_exercise->refresh();

            $routines = $this->getCourseRoutines()->all();
            if(sizeof($routines) > 0) {
            	foreach($routines as $routine) {
            		$clone_routine = new Course_training_routine;
            		$clone_routine->id_relation = $clone_workout_exercise->id;
            		$clone_routine->type = $routine->type;
            		$clone_routine->set = $routine->set;
            		$clone_routine->sleep = $routine->sleep;
            		$clone_routine->alert_sleep = $routine->alert_sleep;
            		$clone_routine->alert_proc = $routine->alert_proc;
            		if($clone_routine->save()) {
                        $clone_routine->refresh();
                        $sets = $routine->getCourseSets();
                        if(sizeof($sets) > 0) {
                        	foreach($sets as $set) {
                        		$clone_set = new Course_set;
                        		$clone_set->id_set_attr = $set->id_set_attr;
                        		$clone_set->id_c_tr_rout = $clone_routine->id;
                        		$clone_set->default_val = $set->default_val;
                        		$clone_set->trigger = $set->trigger;
                        		if($clone_set->save()) {

                        		} else {
                        			$transaction->rollback();
                        			$response['error'] = 'Не удалось сохранить атрибут подхода упражнения';
                        		}
                        	}
                        }
            		} else {
            			$transaction->rollback();
            			$response['error'] = 'Не удалось сохранить подход упражнения';
            		}
            	}
            }

    	} else {
    		$transaction->rollback();
        	$response['error'] = 'Не удалось сохранить упражнение';
    	}

    	if(!$response['error']) {
    		$response['status'] = 'ok';
    		$response['exercise'] = $clone_workout_exercise;
    		$transaction->commit();
    	}

		return $response;
    }

}