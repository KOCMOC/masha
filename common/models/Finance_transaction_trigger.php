<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Finance_transaction_trigger extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%finance_transaction_trigger}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['trigger', 'required'],
            ['trigger', 'string', 'length' => [2, 255]],
            ['comment', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'trigger' => 'Триггер',
            'comment' => 'Комментарий'
        ];
    }
           /*
           actions:
           1) set_course
           2) add_course_days // узнать как будет продлить, продлить будет extend
           3)
           */
    public function up()
    {
   		$transaction = Yii::$app->db->beginTransaction();
   		$triggers = json_decode($this->trigger);
        if(is_array($triggers) and sizeof($triggers) > 0) {
        	$status = false;        	foreach($triggers as $trigger) {            	if($trigger->action == 'change_expiration') {
            			$order = \common\models\Order::findOne($trigger->order_id);
               			if($order) {                            $order->expire = strtotime($trigger->length);
                            $order->active = 1;
                            $order->save();
                            $status = true;               			} else {               				$transaction->rollback();
               				return false;               			}
            	} elseif($trigger->action == 'extend_order') {
            			$order = \common\models\Order::findOne($trigger->order_id);
               			if($order) {
                            $order->active = 3;
                            //$order->updated_at = time();
                            $order->save();
               				$status = true;
               			} else {
               				$transaction->rollback();
               				return false;
               			}            	}        	}
        	if($status === true) {
				$transaction->commit();
				return true;
			}        }    }
}