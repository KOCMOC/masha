<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Workout_report_field extends ActiveRecord
{

	const Text = 0;
	const Textarea = 1;
	const Checkbox = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%workout_report_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type', 'workout_id'], 'required'],
            ['workout_id', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            //['title', 'unique', 'message' => 'Такое название уже существует'],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }],
            ['type', 'in', 'range' => [self::Text, self::Textarea, self::Checkbox], 'allowArray' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название поля',
            'type' => 'Тип поля'
        ];
    }

    public static function getTypesList()
    {
		return [
			'Текст' => self::Text,
			'Длинный текст' => self::Textarea,
			'Галочка' => self::Checkbox
		];
    }

}