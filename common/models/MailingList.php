<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * Mailing list item
 */

class MailingList extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mailing_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['user_id'], 'required'],
                ['is_active', 'boolean'],
                ['is_expired', 'boolean'],
        ];
    }

    // public static function get_recent_list() {
    //     $offset = 60 * 60 * 24; // 1 day
    //     $date = date("Y-m-d H:i:s", time() - $offset);
    //     $list = self::find()
    //         ->where(['>=', 'date_update', $date])
    //         ->all();

    //     return $list;
    // }
}