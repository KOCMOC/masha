<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_anket}}".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $gender
 * @property integer $growth
 * @property integer $weight
 * @property integer $breast_size
 * @property integer $waist_size
 * @property integer $hip_size
 * @property integer $operating_weight
 * @property string $goal
 */
class UserAnket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_anket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'gender', 'growth', 'weight', 'breast_size', 'waist_size', 'hip_size', 'operating_weight', 'kal'], 'integer'],
            [['gender', 'growth', 'weight', 'breast_size', 'waist_size', 'hip_size', 'operating_weight', 'goal', 'kal'], 'safe'],
            ['status', 'integer', 'min' => 0],
            [['mother', 'vegan'], 'in', 'range' => [0, 1]],
            ['day', 'in', 'range' => [0, 1, 2, 3, 4, 5, 6, 7]],
            ['week', 'in', 'range' => [0, 1, 2, 3, 4]],
            [['goal', 'insta', 'vk', 'born'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'born' => 'Дата рождения',
            'gender' => 'Пол',
            'growth' => 'Рост',
            'weight' => 'Вес',
            'country' => 'Страна',
            'city' => 'Город',
            'insta' => 'Аккаунт в Instagram',
            'vk' => 'Аккаунт в ВК',
            'kal' => 'Калории',
            'mother' => 'Кормящая мать',
            'vegan' => 'Вегетарианец',
            'breast_size' => 'Размер груди',
            'waist_size' => 'Размер талии',
            'hip_size' => 'Размер бедер',
            'operating_weight' => 'Рабочий вес',
            'goal' => 'Цель',
            'status' => 'Статус',
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->one();
    }
}
