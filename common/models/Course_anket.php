<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Course_anket extends ActiveRecord
{

	const Number = 0;
	const Text = 1;
	const Checkbox = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_anket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type', 'course_id'], 'required'],
            ['course_id', 'number', 'min'=>1, 'message'=>'Должно быть положительным числом'],
            [['title', 'alias'], 'filter', 'filter' => 'trim'],
            [['title', 'alias'], 'string', 'length' => [2, 255]],
            //['title', 'unique', 'message' => 'Такое название уже существует'],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }],
            ['type', 'in', 'range' => [self::Number, self::Text, self::Checkbox], 'allowArray' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'order_id' => 'ID курса',
            'title' => 'Название поля',
            'type' => 'Тип поля',
            'alias' => 'Альяс',
        ];
    }

    public static function getTypesList()
    {
		return [
			'Число' => self::Number,
			'Текст' => self::Text,
			'Галочка' => self::Checkbox
		];
    }

    public static function getAliases()
    {		return [
			'' => 'без альяса',
			'gender' => 'Пол',
			'growth' => 'Рост',
			'weight' => 'Вес',
			'breast_size' => 'Размер груди',
			'waist_size' => 'Размер талии',
			'hip_size' => 'Размер бедер',
			'operating_weight' => 'Рабочий вес',
			'goal' => 'Цель',
		];    }

	public function getRelatedAnketFields() {
        return $this->hasMany(Workout_report_field::className(), ['related_anket_field' => 'id'])->all();
    }

    public function del() {
     	$transaction = Yii::$app->db->beginTransaction();
    	$related_anket_fields = $this->getRelatedAnketFields();
    	if(sizeof($related_anket_fields) > 0) {
    		foreach($related_anket_fields as $related_anket_field) {
	    		if(!$related_anket_fields->delete()) {
	    			$transaction->rollback();
	    			return false;
	    		}
    		}
    	}
    	if(!$this->delete()) {
    		$transaction->rollback();
    		return false;
    	}
    	$transaction->commit();
    	return true;
    }

}