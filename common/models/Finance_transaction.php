<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Finance_transaction extends ActiveRecord
{

	const Status_progress = 0;
	const Status_approved = 1;
	const Status_declined = 2;

	const Subject_invoice = 0;
    const Subject_order = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%finance_transaction}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['user_id', 'number', 'integerOnly' => true, 'min' => 1],
            [['trigger_id', 'subject_id'], 'number', 'integerOnly' => true],
            ['amount', 'number', 'integerOnly' => false],
            ['status', 'in', 'range' => [self::Status_progress, self::Status_approved, self::Status_declined], 'allowArray' => true],
            ['subject_type', 'in', 'range' => [self::Subject_invoice, self::Subject_order], 'allowArray' => true],
            ['comment', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'ID пользователя',
            'trigger_id' => 'ID триггера',
            'amount' => 'Сумма транзакции',
            'status' => 'Статус транзакции',
            'comment' => 'Комментарий'
        ];
    }

    public function getTrigger() {
    	return $this->hasOne(Finance_transaction_trigger::className(), ['id' => 'trigger_id'])->one();
    }

    public function getChild() {
    	return $this->hasOne(Finance_transaction::className(), ['parent'=>'id'])->one();
    }

    public function commit($admin = false){   		$transaction = Yii::$app->db->beginTransaction();
   		$user = \common\models\User::findOne($this->user_id);
   		if($user) {   			$finance_account = $user->getFinanceAccount();
   			if($finance_account) {                if($admin === true or $finance_account->balance + $this->amount >= 0) {
                	if($admin === false) {	                    $finance_account->balance = $finance_account->balance + $this->amount;
                    }
                    if($admin === true or $finance_account->save()) {
                    	if($trigger = $this->getTrigger()) {                        	if(!$trigger->up()) {				   				$transaction->rollback();
				   				return false;                        	}                    	}
                        $this->status = self::Status_approved;
                        if($this->save()) {			   				$transaction->commit();
			   				return true;                        } else {			   				$transaction->rollback();
			   				return false;                        }                    } else {		   				$transaction->rollback();
		   				return false;                    }                } else {	   				$transaction->rollback();
	   				return false;                }   			} else {   				$transaction->rollback();
   				return false;   			}   		} else {   			$transaction->rollback();
   			return false;   		}    }

    public function rollback(){

    }

}