<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Sell_item extends ActiveRecord
{

	const Minimal = 0;
	const Standart = 1;
	const Vip = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sell_item}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_group_id', 'type', 'title', 'amount'], 'required'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }],
            ['amount', 'number', 'integerOnly' => false],
            ['course_group_id', 'number', 'integerOnly' => true],
            ['trigger', 'string'],
            ['type', 'in', 'range' => [self::Minimal, self::Standart, self::Vip], 'allowArray' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название элемента продажи',
            'course_group_id' => 'ID группы курсов',
            'type' => 'Тип аккаунта',
            'amount' => 'Цена',
            'trigger' => 'Триггер',
        ];
    }

    public static function getTypes()
    {    	return [
    		self::Minimal => 'Минимальный',
    		self::Standart => 'Станартный',
    		self::Vip => 'VIP'
    	];    }

    public function getCourseGroup()
    {    	return $this->hasOne(Course_group::className(), ['id' => 'course_group_id'])->one();    }

    public function getOrders()
    {    	return $this->hasOne(Order::className(), ['course_group_id' => 'course_group_id']);    }

    public function buy($discount_id, $promo=false, $order = false)
    {
		$user = \common\models\User::findOne(\Yii::$app->user->identity->id);
		$finance_account = $user->getFinanceAccount();

		$discount = \common\models\Discount::findOne($discount_id);

		if(!$discount) {			$discount_id = 0;
			$period = '1 month';
			$amount = $this->amount;		} else {			$period = $discount->period;
			$q = (int)str_replace(' month', '', $discount->period);
			$amount = ($this->amount - $this->amount * $discount->discount / 100) * $q;		}

		$promocode = 0;
		if($promo == 1) {			$promocode = 1;
			$amount = round($amount * 0.9);  // 10%		} elseif($promo == 2) {			$promocode = 2;
			$amount = 1;  // 1 рубль		} elseif($promo == 3) {
			$promocode = 3;
			$amount = round($amount * 0.7);  // 30%
		} elseif($promo == 4) {
			$promocode = 4;
			$amount = round($amount * 0.7);  // 30%
		} elseif($promo == 5) {
			$promocode = 5;
			$amount = round($amount * 0.5);  // 50%
		} elseif($promo == 6) {
			$promocode = 6;
			$amount = round($amount * 0.85);  // 15%
		} elseif($promo == 7) {
			$promocode = 7;
			$amount = round($amount * 0.8);  // 20%
		} elseif($promo == 8) {
			$promocode = 8;
			$amount = round($amount * 0.7);  // 30%
		}

		$transaction = Yii::$app->db->beginTransaction();

		$ext = false;

		if($order === false) {
			$order = new \common\models\Order();
			$order->course_group_id = $this->course_group_id;
			$order->promocode = $promocode;
			$order->discount = $discount_id;
			$order->user_id = $user->id;
			$order->type = $this->type;
			$order->amount = $amount;
			if(!$order->save()) {
				$transaction->rollback();				return false;
			}
			$pay_text = 'Оплата';
		} else {			$order->amount += $amount;
			$order->promocode = $promocode;
			$order->discount = $discount_id;
			$order->save();
			$pay_text = 'Продление ';
			$ext = true;		}

		$trigger = new \common\models\Finance_transaction_trigger();
		$trigger->trigger = str_replace('{{%order_id}}', $order->id, $this->trigger);
		$trigger->trigger = str_replace('{{%length}}', $period, $trigger->trigger);
		if($ext === true) $trigger->trigger = str_replace('"active_order"', '"extend_order"', $trigger->trigger);
		$trigger->comment = $pay_text.' курса '.$this->title.', ID '.$this->id;
		if(!$trigger->save()) {
			$transaction->rollback();
			return false;
		}
		$trigger->refresh();

		$finance_transaction = new \common\models\Finance_transaction();
		$finance_transaction->user_id = $user->id;
		$finance_transaction->subject_id = $order->id;
		$finance_transaction->subject_type = \common\models\Finance_transaction::Subject_order;
		$finance_transaction->trigger_id = $trigger->id;
		$finance_transaction->amount = $amount * -1;
		$finance_transaction->status = \common\models\Finance_transaction::Status_progress;
		$finance_transaction->comment = $pay_text.' курса '.$this->title.', ID '.$this->id;
		if(!$finance_transaction->save()) {
			$transaction->rollback();
			return false;
		}
		if($finance_account->balance > $amount) {
           	// проводим транзакцию и в ней списываем бабло и проводим триггер
		} else {
            $invoice = new \common\models\Finance_invoice();
            $invoice->user_id = $user->id;
            $invoice->updated_by = 0;  // тут будет юзер системы
            $invoice->amount = $amount - $finance_account->balance;
            $invoice->method = \common\models\Finance_invoice::Method_yakassa;
            $invoice->status = \common\models\Finance_invoice::Status_progress;
			if(!$invoice->save()) {
				$transaction->rollback();
				return false;
			}
            $invoice->refresh();
            $invoice->hash = md5($invoice->id.time());
			$invoice->save();

            $i_finance_transaction = new \common\models\Finance_transaction();
            $i_finance_transaction->parent = $finance_transaction->id;
			$i_finance_transaction->user_id = $user->id;
			$i_finance_transaction->subject_id = $invoice->id;
			$i_finance_transaction->subject_type = \common\models\Finance_transaction::Subject_invoice;
			$i_finance_transaction->trigger_id = 0;
			$i_finance_transaction->amount = $invoice->amount;
			$i_finance_transaction->status = \common\models\Finance_transaction::Status_progress;
			$i_finance_transaction->comment = 'Пополнение счёта для '.$user->email.', ID '.$user->id;
			if(!$i_finance_transaction->save()) {
				$transaction->rollback();
				return false;
			}
		}
		$transaction->commit();
		return true;    }

}