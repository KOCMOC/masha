<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Set extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%set}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_set_attr', 'id_tr_rout'], 'required'],
            [['id_set_attr', 'id_tr_rout'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_set_attr' => 'ID атрибута подхода',
            'id_tr_rout' => 'ID плана тренировки',
            'default_val' => 'Значение по умолчанию'
        ];
    }

    public function getSet_attribute() {
        return $this->hasOne(Set_attribute::className(), ['id' => 'id_set_attr'])->one();
    }

    public function getTraining_routine() {
        return $this->hasOne(Training_routine::className(), ['id' => 'id_tr_rout'])->one();
    }
}