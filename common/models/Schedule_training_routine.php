<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Schedule_training_routine extends ActiveRecord
{

	const WorkoutRelation = 1;
	const SetRelation = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schedule_training_routine}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_relation', 'type', 'set', 'sleep'], 'required'],
            [['id_relation', 'type'], 'integer'],
            [['set', 'sleep'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_relation' => 'ID связи с упражнением',
            'type' => 'Тип связи',
            'set' => 'Подход',
            'sleep' => 'Отдых'
        ];
    }

    public function getScheduleSets() {
        return $this->hasMany(Schedule_set::className(), ['id_s_tr_rout' => 'id'])->all();
    }

    public function getScheduleExercise() {
        return $this->hasOne(Schedule_exercise::className(), ['id' => 'id_relation'])->one();
    }

    public function getScheduleSetExercise() {    	return $this->hasOne(Schedule_set_exercise::className(), ['id' => 'id_relation'])->one();    }

    public function del() {
    	$transaction = Yii::$app->db->beginTransaction();    	$sets = $this->getScheduleSets();
    	foreach($sets as $set) {    		if(!$set->delete()) {    			$transaction->rollback();
    			return false;    		}    	}
    	if(!$this->delete()) {    		$transaction->rollback();
    		return false;    	}
    	$transaction->commit();
    	return true;    }
}