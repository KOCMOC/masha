<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Exercise_tab extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exercise_tab}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required'],
            [['title'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название'
        ];
    }


}