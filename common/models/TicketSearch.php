<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Ticket;

/**
 * TicketSearch represents the model behind the search form about `common\models\Ticket`.
 */
class TicketSearch extends Ticket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'role', 'status', 'evaluation'], 'integer'],
            [['title', 'description', 'creation_datetime', 'read_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->identity->role == User::ROLE_KURATOR) {
            $query = Ticket::find()->joinWith('trainer')
                ->where([
                    'user.trainer_id' => Yii::$app->user->identity->getId(),
                    'ticket.role' => Yii::$app->user->identity->tickets_role,
                ]);
        }
        else {
            $query = Ticket::find()->joinWith('trainer');
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'attributes' => [
                    'status' => [
                        'asc' => ['status' => SORT_DESC, 'creation_datetime' => SORT_DESC],
                        'desc' => ['status' => SORT_ASC, 'creation_datetime' => SORT_ASC],
                    ],
                    'creation_datetime',
                    'is_vip',
                ],
                'defaultOrder' => [
                    'status' => SORT_DESC,
                    'is_vip' => SORT_DESC,
                    'creation_datetime' => SORT_DESC
                ]
            ]
        ]);

        $dataProvider->sort->enableMultiSort = true;

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'role' => $this->role,
            'status' => $this->status,
            //'creation_datetime' => $this->creation_datetime,
            // 'read_datetime' => $this->read_datetime,
            'evaluation' => $this->evaluation,
        ]);

        if (!is_null($this->creation_datetime)) {
            $query->andFilterWhere(['between', 'date({{%ticket}}.creation_datetime)', $this->creation_datetime, $this->creation_datetime]);
        }

        if (!is_null($this->read_datetime)) {
            $query->andFilterWhere(['between', 'date({{%ticket}}.read_datetime)', $this->read_datetime, $this->read_datetime]);
        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
