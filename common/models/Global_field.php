<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Global_field extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%global_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'id_exercise', 'id_course'], 'required'],
            [['id_exercise', 'id_course'], 'integer'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 150]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }],
            //alias rule
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Наименование',
            'ID exercise' => 'Упражнение',
            'alias' => 'Алиас',
            'default' => 'По умолчанию'
        ];
    }

}