<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Заказы ';
$this->params['breadcrumbs'][] = 'Заказы';
?>
<div class="site-index">
    <div class="">
        <h2>Курсы&nbsp;(<?=$count_orders;?>)</h2>
    </div>
    <div><?= Yii::$app->session->getFlash('error'); ?></div>
    <?if(sizeof($orders) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Пользователь</th>
                    <th>E-mail</th>
                    <th>Курс</th>
                    <th>Активный</th>
                    <th>Общая стоимость</th>
                    <th>Заказан / продлён</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?foreach($orders as $order):?>
                <tr id="order_<?=$order['id'] ?>" class="order-row" data-order-id="<?=$order['id'] ?>">
                    <th scope="row"><?=$order['id'];?></th>
                    <td><?=$order['userRelation']['fio']?></td>
                    <td><?=$order['userRelation']['email']?></td>
                    <td style="max-width:300px"><?=$order['courseGroupRelation']['title']?></td>
                    <td><input class="active" type="checkbox"<?=($order['active'])?' checked':'' ?>></td>
                    <td><?=$order['amount']?></td>
                    <td><?=date('d.m.Y', $order['created_at']) ?><?=($order['updated_at'])?' / '.date('d.m.Y', $order['updated_at']):'' ?></td>
                    <td><a href="<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/order/delete/{$order['id']}"]);?>">X</a></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
    <?else:?>
    Нет курсов
    <?endif;?>
</div>
<script type="text/javascript">
	$(document).ready(function(){		$('.order-row input.active').change(function(){
			$.ajax({
			    method: 'POST',
			    url: '/admin/order/change_active',
			    data: { 'order_id': $(this).closest('.order-row').data('order-id'), 'active': $(this).prop('checked'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {

	        		} else {
	        			$('#order_'+result.order_id+' input.active').prop('checked', !$('#order_'+result.order_id+' input.active').prop('checked'));
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось изменить статус заказа');
			    }
			});		});	});
</script>