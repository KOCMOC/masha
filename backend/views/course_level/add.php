<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Уровни курсов: Новый уровень ';
$this->params['breadcrumbs'][] = ['label' => 'Уровни курсов', 'url' => '\admin\course_level'];
$this->params['breadcrumbs'][] = 'Новый уровень';

?>
<div class="site-index">
    <div>
        <h2>Новый уровень курсов</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'add-course-group',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ],
    ]);?>
    <div class="row col-md-10"><?=$form->field($model, 'title')->textInput();?></div>
    </div>
    <div class="row col-sm-10">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
    </div>
    <?php ActiveForm::end();?>
</div>
