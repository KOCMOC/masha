<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Уровни курсов';
$this->params['breadcrumbs'][] = 'Уровни курсов';
?>
<div class="site-index">
    <div class="">
        <h2>Уровни курсов</h2>
    </div>
    <div><?= Yii::$app->session->getFlash('error'); ?></div>
    <div class="btn-group">
        <button id="add" type="button" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новый уровень курсов</button>
    </div>
    <?if(sizeof($levels) > 0):?>
    <div class="body-content">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Название уровня курсов</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($levels as $level):?>
                <tr>
                    <th scope="row"><?=$level['id'];?></th>
                    <td>
                    	<a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/course_level/edit/$level->id"]);?>' class="btn btn-success btn-sm" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                    <td><?=$level['title']?></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <?else:?>
    Нет уровней курсов
    <?endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').click(function(){
        location.href = "<?=\Yii::$app->urlManager->createAbsoluteUrl(['/course_level/add'])?>";
    });
})
</script>