<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Курсы: Редактировать курс ';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => '\admin\course'];
$this->params['breadcrumbs'][] = 'Редактировать курс';

?>
<div class="site-index">
    <div>
        <h2>Редактировать курс</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'edit-course',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ],
    ]);?>
    <?=$form->field($model, 'id')->hiddenInput(['value' => $course_id])->label('');?>
    <div class="row col-md-10"><?=$form->field($model, 'title')->textInput()->label('Название курса');?></div>
    <div class="row col-md-10"><?=$form->field($model, 'course_group_id')->dropDownList($groups, ['prompt'=>'Выберите группу курса']);?></div>
	<div class="row col-md-10"><?=$form->field($model, 'gender')->radioList($genders) ?></div>
    <div class="row col-md-10"><?=$form->field($model, 'level')->dropDownList($levels, ['prompt'=>'Выберите уровень курса']);?></div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'description')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
        'language' => 'ru'
    ]);?>
    </div>
    </div>
    <div class="row col-sm-10">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
    </div>
    <?php ActiveForm::end();?>
	<div class="row col-md-10">
	                <div id="anket"><?php
	                	if(sizeof($anket_fields) > 0)
	                		foreach($anket_fields as $anket_field) : ?>
								<div class="anket-field" id="course_anket_field_<?=$anket_field->id ?>" data-id="<?=$anket_field->id ?>">
									<input type="text" field="title" class="add-field-title" value="<?=$anket_field->title ?>">&nbsp;<select field="type" class="add-field-type"><?php foreach($anketFieldsTypes as $anketFieldKey=>$anketFieldType): ?>
										<option value="<?=$anketFieldType ?>"<?=($anket_field->type == $anketFieldType)?' selected':''; ?>><?=$anketFieldKey ?></option>
									<?php endforeach; ?></select>
									<div class="ex-close">x</div>
								</div>
	                	<?php	endforeach; ?></div>
           			<div class="btn-group">
				        <button type="button" id="course-anket-add" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить поле анкеты</button>
				    </div>
					<div id="anket_field_template" class="anket-field">
						<input type="text" field="title" class="add-field-title" value="Новое поле">&nbsp;<select field="type" class="add-field-type"><?php foreach($anketFieldsTypes as $anketFieldKey=>$anketFieldType): ?>
							<option value="<?=$anketFieldType ?>"><?=$anketFieldKey ?></option>
						<?php endforeach; ?></select>
						<div class="ex-close">x</div>
					</div>
	</div>
</div>
<script type="text/javascript">
  		$('#course-anket-add').click(function(){
			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_course_anket_field',
			    data: { 'course_id': <?=$course_id ?>, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		    $('#anket_field_template').clone().removeAttr('id').show().data('id', result.field_id).attr('id', 'course_anket_field_'+result.field_id).appendTo($('#anket'));
	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось добавить поле анкеты');
			    }
			});

  		});
		$('#anket').delegate('.anket-field > input, .anket-field > select', 'change', function(){
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/edit_course_anket_field',
			    data: { 'course_anket_field_id': $(this).parent().data('id'), 'field': $(this).attr('field'), 'value': $(this).val(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		} else {
	        			$('#course_anket_field_'+result.id+' [field='+result.field+']').val(result.old_value);
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить поле анкеты');
			    }
			});
		});
		$('#anket').delegate('.anket-field .ex-close', 'click', function(){
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/del_course_anket_field',
			    data: { 'course_anket_field_id': $(this).parent().data('id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			$('#course_anket_field_'+result.id).remove();
	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось удалить поле анкеты');
			    }
			});
		});
</script>
