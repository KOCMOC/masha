<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Курсы ';
$this->params['breadcrumbs'][] = 'Курсы';
?>
<div class="site-index">
    <div class="">
        <h2>Заказанные курсы&nbsp;(<?=$count_orders;?>)</h2>
    </div>
    <div><?= Yii::$app->session->getFlash('error'); ?></div>
    <?if(sizeof($orders) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Название курса</th>
                    <th>Описание курса</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($orders as $order): $course_group = $order->getCourseGroupRelation()->one(); ?>
                <tr>
                    <th scope="row"><?=$order['id'];?></th>
                    <td>
                    	<a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/schedule/view/$order->id"]);?>' title='Посмотреть' class='btn btn-success btn-sm' id="a-desc" desc-id="<?=$order['id']?>"><span class="glyphicon glyphicon-eye-open"></span></a>
                    </td>
                    <td><?=$course_group['title']?></td>
                    <td><?=(mb_strlen($course_group['description']) > 55)?mb_substr($course_group['description'], 0, 55).'...':$course_group['description']?></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
    <?else:?>
    Нет курсов
    <?endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').click(function(){
        location.href = "<?=\Yii::$app->urlManager->createAbsoluteUrl(['/course/add'])?>";
    });
})
</script>