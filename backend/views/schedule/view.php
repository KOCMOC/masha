<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Заказанный курс: компоновка ';
$this->params['breadcrumbs'][] = ['label' => 'Заказанные курсы', 'url' => '\admin\course'];
$this->params['breadcrumbs'][] = 'Скомпоновать курс';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<div class="site-index">
    <div class="">
        <h2>Курс</h2>
    </div>
    <div class="body-content">
        <table class="table table-striped">
            <tr>
                <td width="150"><b>Название</b></td>
                <td><?=$course->title?></td>
            </tr>
            <tr>
                <td><b>Описание</b></td>
                <td><?=$course->description?></td>
            </tr>
            <tr>
                <td><b>Анкета курса</b></td>
                <td>
                <?php
                	$anket_fields = $course->getAnket();
                	if(sizeof($anket_fields) > 0)
                	foreach($anket_fields as $anket_key=>$anket_field) {
    					echo '<div>'.($anket_key+1).'. '.$anket_field->title.' ('.array_search($anket_field->type, $anketFieldsTypes).')</div>';
                	}
                ?>
                </td>
            </tr>
            <tr>
            	<td><b>Тренировки</b></td>
            	<td>
            		<div id="workouts-container">
            			<div class="btn-group">
					        <button id="workout_add" type="button" class="btn btn-info" style="z-index:1" data-course-id="<?=$course->id?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить тренировку</button>
					    </div>
					    <div id="workouts"><?php foreach($schedules as $schedule): ?>
	            		<div class="workout" id="workout<?=$schedule->id ?>" data-workout-id="<?=$schedule->id ?>">
	            			Название:&nbsp;<input type="text" value="<?=str_replace('"', '&quot;', $schedule->getWorkout()->title) ?>"><div class="close">X</div><div class="ex-open">V</div>
		            		<div class="workout-fields-container">
		            			<div class="workout-fields"><?php if(sizeof($schedule->getWorkoutReport()) > 0) foreach($schedule->getWorkoutReport() as $workout_report): /*$workout_report_field = $workout_report->getField();*/ ?>
		            				<div class="workout-field" id="workout_report_field_<?=$workout_report->id ?>" data-id="<?=$workout_report->id ?>">
				            			<input type="text" field="title" class="add-field-title" value="<?=$workout_report->getField()->title ?>">&nbsp;<select field="type" class="add-field-type"><?php foreach($reportFieldsTypes as $reportFieldKey=>$reportFieldType): ?>
				            			<option value="<?=$reportFieldType ?>"<?=($workout_report->getField()->type == $reportFieldType)?' selected':'' ?>><?=$reportFieldKey ?></option>
			            				<?php endforeach; ?></select><br />Связано&nbsp;с&nbsp;<select field="related_anket_field" class="add-field-related_anket_field"><option>Выберите поле анкеты</option><?php foreach($anket_fields as $anket_key=>$anket_field):?><option value="<?=$anket_field->id ?>"><?=$anket_key+1 ?>. <?=mb_substr($anket_field->title, 0, 30) ?></option><?php endforeach; ?></select>
			            				<div class="ex-close">x</div>
			            			</div>
		            			<?php endforeach; ?></div>
		            			<div class="btn-group">
							        <button type="button" class="workout-report-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить поле отчёта</button>
							    </div>
	            			</div>
	            			<div class="btn-group">
						        <button type="button" class="set-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить сет</button>
						    </div>
	            			<div class="exercises"><?php foreach($schedule->getExercises('number')->all() as $workout_exercise):
	            			if($workout_exercise->type == \common\models\Workout_exercise::Exercise) :
	            				$exercise = $workout_exercise->getExercise(); ?>
	            				<div class="exercise" id="workout_exercise_<?=$workout_exercise->id ?>" data-exercise-id="<?=$exercise->id ?>" data-relation-id="<?=$workout_exercise->id ?>" data-type="<?=\common\models\Course_training_routine::WorkoutRelation ?>"><?=$exercise->title ?><div class="ex-close">X</div><div class="ex-open">V</div><div class="routines"></div></div>
	            			<?php else:
	            				$set = $workout_exercise->getSet(); ?>
	            				<div class="set" data-set-id="<?=$set->id ?>" id="set<?=$set->id ?>">
		            				Название:&nbsp;<input type="text" class="title" value="<?=str_replace('"', '&quot;', $set->title) ?>" field="title">
									<div class="ex-close">X</div>
		            				<br />
		            				Отдых:&nbsp;<input type="text" value="<?=$set->sleep ?>" class="sleep" field="sleep">
		            				<br />
		            				Коммент выпол:&nbsp;<input type="text" field="alert_proc" value="<?=$set->alert_proc ?>" style="width:68%;margin:0 0 5px 0">
			            			<br />
			            			Коммент отдых:&nbsp;<input type="text" field="alert_sleep" value="<?=$set->alert_sleep ?>" style="width:68%;margin:0">
			            			<div class="set-exercises"><?php foreach($set->getExercises('number')->all() as $set_exercise): $exercise = $set_exercise->getExercise(); ?>
			            				<div class="exercise" id="set_exercise_<?=$set_exercise->id ?>" data-exercise-id="<?=$exercise->id ?>" data-relation-id="<?=$set_exercise->id ?>" data-type="<?=\common\models\Course_training_routine::SetRelation ?>"><?=$exercise->title ?><div class="ex-close">X</div><div class="ex-open">V</div><div class="routines"></div></div>
		            				<?php endforeach; ?></div>
		            			</div>
	            			<?php endif;
	            			endforeach; ?></div>
	            		</div>
					    <?php endforeach; ?></div>
            		</div>
            		<div id="exercises"><?php foreach($exercises as $exercise): ?>
	            		<div class="exercise" list="true" data-exercise-id="<?=$exercise->id ?>"><?=$exercise->title ?><div class="ex-close">X</div><div class="ex-open">V</div><div class="routines"></div></div>
            			<?php endforeach; ?>
            		</div>
            		<div id="workout_template" class="workout">
            			Название:&nbsp;<input type="text"><div class="close">X</div><div class="ex-open">V</div>
            			<div class="workout-fields-container">
	            			<div class="workout-fields"></div>
		            			<div class="btn-group">
							        <button type="button" class="workout-report-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить поле отчёта</button>
							    </div>
			            </div>
           				<div class="btn-group">
					        <button type="button" class="set-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить сет</button>
					    </div>
            			<div class="exercises"></div>
            		</div>
            		<div id="set_template" class="set">
            			Название:&nbsp;<input type="text" class="title" field="title"><div class="ex-close">X</div><br />
            			Отдых:&nbsp;<input type="text" class="sleep" field="sleep"><br />
            			Коммент выпол:&nbsp;<input type="text" field="alert_proc" style="width:68%;margin:0 0 5px 0"><br />
            			Коммент отдых:&nbsp;<input type="text" field="alert_sleep" style="width:68%;margin:0">
                        <div class="set-exercises"></div>
            		</div>
            		<div id="routine_template" class="routine">
            			<div class="close">X</div>
						Подход <span></span>&nbsp;
						Отдых <input type="text" value="0" field="sleep"><br />
						Ком. вып. <input type="text" field="alert_proc"><br />
						Ком. отд. <input type="text" field="alert_sleep">
						<div class="routine_sets"></div>
						<a href="javascript:void(0)" class="add-set">Добавить параметр</a>
            		</div>
            		<div id="routine_set_template" class="routine-set">
						<select field="id_set_attr">
							<option value="0">Выберите параметр</option>
						<?php foreach($attributesList as $attributeOne) : ?>
							<option value="<?=$attributeOne->id ?>"><?=$attributeOne->name ?><?=($attributeOne->unit)?' ('.$attributeOne->unit.')':'' ?></option>
						<?php endforeach; ?>
						</select>
						<input type="text" value="" field="default_val"><div class="close">x</div>
            		</div>
					<div id="workout_field_template" class="workout-field">
						<input type="text" field="title" class="add-field-title" value="Новое поле">&nbsp;<select field="type" class="add-field-type"><?php foreach($reportFieldsTypes as $reportFieldKey=>$reportFieldType): ?>
							<option value="<?=$reportFieldType ?>"><?=$reportFieldKey ?></option>
						<?php endforeach; ?></select><br />Связано&nbsp;с&nbsp;<select field="related_anket_field" class="add-field-related_anket_field"><option>Выберите поле анкеты</option><?php foreach($anket_fields as $anket_key=>$anket_field):?><option value="<?=$anket_field->id ?>"><?=$anket_key+1 ?>. <?=mb_substr($anket_field->title, 0, 30) ?></option><?php endforeach; ?></select>
			            <div class="ex-close">x</div>
					</div>
            	</td>
            </tr>
        </table>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var droppedAr = false;
		var draggedEx = false;		var minExWidth = $('#workouts').innerWidth();
		$( ".exercises" )
			.sortable({
		    	revert: true,
		    	start: function( event, ui ){		    		//$(event.toElement).css({width:'auto', 'min-width': minExWidth+'px'});		    	},
		    	stop: function( event, ui ){
		    		//$(event.toElement).css({height:'auto', 'min-width':'auto'});
		    		if($(event.toElement).hasClass('exercise')) {		    			//$(event.toElement).css({width:'100%'});		    		}
		    		saveExercisesList($(event.toElement).parent().parent().data('workout-id'));
		    	}
			}).droppable({
				greedy: true,
		    	drop: function( event, ui ) {
                    droppedAr = {where:'workout', who:$(event.toElement).data('exercise-id'), to:$(event.target).parent().data('workout-id')};
		     		/*if(event.toElement.hasAttribute('list')) {
		     			$(event.toElement).removeAttr('list').find('div:last-child').show();
		     			addExercise($(event.toElement).data('exercise-id'), $(event.target).parent().data('workout-id'));
		     		}*/
		     	}
		  	});
		$( ".set-exercises" )
			.sortable({				revert: true,
		    	start: function( event, ui ){
		    		//$(event.toElement).css({width:'auto'});
		    	},
		    	stop: function( event, ui ){
		    		//$(event.toElement).css({height:'auto', width:'100%'});
		    		saveSetExercisesList($(event.toElement).parent().parent().data('set-id'));
		    	}			}).droppable({
				greedy: true,
		    	drop: function( event, ui ) {
		    		droppedAr = {where:'set', who:$(event.toElement).data('exercise-id'), to:$(event.target).parent().data('set-id')};
		    		/*if(event.toElement.hasAttribute('list')) {
		     			$(event.toElement).removeAttr('list').find('div:last-child').show();
		     			addSetExercise($(event.toElement).data('exercise-id'), $(event.target).parent().data('set-id'));
		     		}*/
		     	}
		  	});
		$( "#workouts" ).sortable({
			revert: true,
	    	stop: function( event, ui ){
		    	saveWorkoutList();
	    	}
		});
		$( "#exercises .exercise" ).draggable({
		    connectToSortable: ".exercises, .set-exercises",
		    helper: "clone",
		    revert: "invalid",
		    start: function() {
		    	draggedEx = true;
		    	droppedAr = false;		    },
		    stop: function( event, ui ) {
		    	if(draggedEx === true && droppedAr !== false) {
		    		if(event.toElement.hasAttribute('list')) {		    			//$(event.toElement).css({height:'auto', 'min-width':'auto'});
		    			rndVal = Math.random().toString().replace('0.', '');
		     			$(event.toElement).removeAttr('list').attr('rand', rndVal).find('div.ex-close').show();	                	if(droppedAr.where == 'workout') {	                		addExercise(droppedAr.who, droppedAr.to, rndVal);	                	} else
	                	if(droppedAr.where == 'set') {                            addSetExercise(droppedAr.who, droppedAr.to, rndVal);	                	}
	                }		    	}
			    draggedEx = false;		    }
		});
  		$('#workout_add').click(function(){
  			var wlist = [];
  			$('#workouts .workout').each(function() {
  				if($(this).data('workout-id') != undefined && $(this).data('workout-id') > 0) {
  					wlist[$(this).index()] = $(this).data('workout-id');
  				}
  			});
			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_workout',
			    data: { 'course_id': <?=$course->id ?>, 'creation': 1, 'list': wlist, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {			    	if(result && result.status && result.status == 'ok') {			    		var new_workout = $('#workout_template').clone().removeAttr('id').show();
			    		new_workout.find('input[type=text]').val(result.workout_title);
						new_workout.data('workout-id', result.workout_id).attr('id', 'workout'+result.workout_id).find('.exercises')
							.sortable({
						    	revert: true,
						    	start: function( event, ui ){
							    		//$(event.toElement).css({width:'auto', 'min-width': minExWidth+'px'});
							    	},
							    	stop: function( event, ui ){
							    		//$(event.toElement).css({height:'auto', 'min-width':'auto'});
							    		if($(event.toElement).hasClass('exercise')) {
							    			//$(event.toElement).css({width:'100%'});
							    		}
						    		saveExercisesList($(event.toElement).parent().parent().data('workout-id'));
						    	}
						  	}).droppable({
						    	drop: function( event, ui ) {
						    		droppedAr = {where:'workout', who:$(event.toElement).data('exercise-id'), to:$(event.target).parent().data('workout-id')};
						     		/*if(event.toElement.hasAttribute('list')) {
						     			$(event.toElement).removeAttr('list');
						     			addExercise($(event.toElement).data('exercise-id'), $(event.target).parent().data('workout-id'));
						     		}*/
						     	}
						  	});
						$('#workouts').append(new_workout);			    	} else {			    		console.log(result.error);			    	}			    },
			    error: function() {			    	console.log('Не удалось создать тренировку');			    }
			});
  		});
  		$('#workouts').delegate('.set-add', 'click', function(){
  			var workout_id = $(this).parent().parent().data('workout-id');
  			var exlist = [];
  			$('#workout'+workout_id+' .exercise, #workout'+workout_id+' .set').each(function() {
  				if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
  					exlist[$(this).index()] = {id:$(this).data('exercise-id'), type:0};
  				} else if($(this).data('set-id') != undefined && $(this).data('set-id') > 0) {
  					exlist[$(this).index()] = {id:$(this).data('set-id'), type:1};
                }
  			});			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_set',
			    data: { 'workout_id': workout_id, 'creation': 1, 'list': exlist, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {			    	if(result && result.status && result.status == 'ok') {
			    		var new_set = $('#set_template').clone().removeAttr('id').show();
			    		new_set.find('input.title').val(result.set_title);
			    		new_set.find('input.sleep').val('0');
						new_set.data('set-id', result.set_id).attr('id', 'set'+result.set_id).find('.set-exercises')
							.sortable({
								revert: true,
						    	start: function( event, ui ){
						    	},
						    	stop: function( event, ui ){
						    		saveSetExercisesList($(event.toElement).parent().parent().data('set-id'));
						    	}
							}).droppable({
								greedy: true,
						    	drop: function( event, ui ) {
						    		droppedAr = {where:'set', who:$(event.toElement).data('exercise-id'), to:$(event.target).parent().data('set-id')};
						     	}
						  	})
						$('#workout'+result.workout_id+' .exercises').append(new_set);
					} else {
	        			//$('#workout'+result.workout_id+' .exercise[data-exercise-id='+result.exercise_id+']').remove();
	        			console.log(result.error);
	        		}			    },
			    error: function(){			    	console.log('Не удалось создать сет');			    }
			});  		});
  		$('#workouts').delegate('.workout-report-add', 'click', function(){        	console.log($(this).parent().parent().parent().data('workout-id'));
			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_workout_report_field',
			    data: { 'workout_id': $(this).parent().parent().parent().data('workout-id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		    $('#workout_field_template').clone().removeAttr('id').show().data('id', result.field_id).attr('id', 'workout_report_field_'+result.field_id).appendTo($('#workout'+result.workout_id+' .workout-fields'));
	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось добавить поле отчёта');
			    }
			});
  		});
  		/*$('#workouts').delegate('.workout .workout-fields > .workout-field > .ex-close', 'click', function(){  			$(this).parent().remove();  		}); */
  		function addExercise(exercise_id, workout_id, rndVal) {
  			var exlist = [];
  			$('#workout'+workout_id+' .exercise').each(function() {
  				if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {  					exlist[$(this).index()] = $(this).data('exercise-id');
  				}  			});			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_exercise',
			    data: { 'exercise_id': exercise_id, 'workout_id': workout_id, list: exlist, rand: rndVal, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {	        		if(result && result.status && result.status == 'ok') {
	        		    $('#workout'+result.workout_id+' .exercise[rand='+result.rand+']').removeAttr('rand').data('relation-id', result.relation_id).data('type', result.type).attr('id', 'workout_exercise_'+result.relation_id);
	        		} else {
	        			$('#workout'+result.workout_id+' .exercise[rand='+result.rand+']').remove();
	        			console.log(result.error);	        		}			    },
			    error: function() {
			    	$('#workout'+result.workout_id+' .exercise[rand='+result.rand+']').remove();
			    	console.log('Не удалось добавить упражнение');
			    }
			});  		}
  		function saveExercisesList(workout_id) {
  			var exlist = [];
  			$('#workout'+workout_id+' .exercises > .exercise, #workout'+workout_id+' .exercises > .set').each(function() {
  				if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
  					exlist[$(this).index()] = {id:$(this).data('exercise-id'), type:0};
  				} else if($(this).data('set-id') != undefined && $(this).data('set-id') > 0) {
  					exlist[$(this).index()] = {id:$(this).data('set-id'), type:1};
                }
  			});
			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_exercise',
			    data: { 'workout_id': workout_id, list: exlist, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {

	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить список упражнений');
			    }
			});
  		}
  		function saveSetExercisesList(set_id) {
  			var exlist = [];
  			$('#set'+set_id+' .set-exercises .exercise').each(function() {
  				if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
  					exlist[$(this).index()] = {id:$(this).data('exercise-id')};
  				}
  			});
			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_set_exercise',
			    data: { 'set_id': set_id, list: exlist, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {

	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить список упражнений в сете');
			    }
			});
  		}
  		function addSetExercise(exercise_id, set_id, rndVal) {
  			var exlist = [];
  			$('#set'+set_id+' > .set-exercises > .exercise').each(function() {
  				if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
  					exlist[$(this).index()] = $(this).data('exercise-id');
  				}
  			});
			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_set_exercise',
			    data: { 'exercise_id': exercise_id, 'set_id': set_id, rand: rndVal, list: exlist, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
                    	$('#set'+result.set_id+' .exercise[rand='+result.rand+']').removeAttr('rand').data('relation-id', result.relation_id).data('type', result.type).attr('id', 'set_exercise_'+result.relation_id);
	        		} else {
	        			$('#set'+result.set_id+' .exercise[rand='+result.rand+']').remove();
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	$('#set'+result.set_id+' .exercise[rand='+result.rand+']').remove();
			    	console.log('Не удалось добавить упражнение в сет');
			    }
			});
  		}
  		$('#workouts').delegate('.exercise > div.ex-open', 'click', function() {
  			if($(this).parent().find('.routines').html() == '') {
	  			$.ajax({
				    method: 'POST',
				    url: '/admin/exercise/course_routines',
				    data: { 'id': $(this).parent().attr('id'), 'relation_id': $(this).parent().data('relation-id'), 'type':$(this).parent().data('type'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
				    dataType: 'json',
				    success: function(result) {
		        		if(result && result.status && result.status == 'ok') {
                            $('#'+result.id).find('.routines').html(result.html);
		        		} else {
		        			console.log(result.error);
		        		}
				    },
				    error: function() {
				    	console.log('Не удалось удалить упражнение');
				    }
				});
			}
  			$(this).parent().find('.routines').toggle();
  			$(this).parent().toggleClass('opened');  		});
  		$('#workouts').delegate('.exercises > .exercise > div.ex-close', 'click', function() {  			$(this).parent().addClass('deleted').hide();
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/del_exercise',
			    data: { 'workout_id': $(this).parent().parent().parent().data('workout-id'), exercise_id: $(this).parent().data('exercise-id'), number: $(this).parent().index(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			var workout_id = $('#workouts .exercise.deleted').parent().parent().data('workout_id');
                        $('#workouts .exercise.deleted').remove();
                        saveExercisesList(workout_id);
	        		} else {
	        			$('#workouts .exercise.deleted').show().removeClass('deleted');
	        			console.log(result.error);
	        		}
			    },
			    error: function() {					$('#workouts .exercise.deleted').show().removeClass('deleted');
			    	console.log('Не удалось удалить упражнение');
			    }
			});  		});
  		$('#workouts').delegate('.set-exercises > .exercise > div.ex-close', 'click', function() {
  			$(this).parent().addClass('deleted').hide();
  			//console.log($(this).parent().data('exercise-id'));
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/del_set_exercise',
			    data: { 'set_id': $(this).parent().parent().parent().data('set-id'), exercise_id: $(this).parent().data('exercise-id'), number: $(this).parent().index(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			var set_id = $('#workouts .set-exercises > .exercise.deleted').parent().parent().data('set_id');
	        			console.log(set_id);
                        $('#workouts .exercise.deleted').remove();
                        //saveExercisesList(workout_id);
	        		} else {
	        			$('#workouts .exercise.deleted').show().removeClass('deleted');
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
					$('#workouts .set-exercises > .exercise.deleted').show().removeClass('deleted');
			    	console.log('Не удалось удалить упражнение из сета');
			    }
			});
  		});
  		$('#workouts').delegate('.workout > div.close', 'click', function() {
  			if(confirm('Вы действительно хотите удалить тренировку?')) {
	  			$(this).parent().addClass('deleted').hide();
	  			$.ajax({
				    method: 'POST',
				    url: '/admin/course/del_workout',
				    data: { 'workout_id': $(this).parent().data('workout-id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
				    dataType: 'json',
				    success: function(result) {
		        		if(result && result.status && result.status == 'ok') {
	                        $('#workouts .workout.deleted').remove();
		        		} else {
		        			$('#workouts .workout.deleted').show().removeClass('deleted');
		        			console.log(result.error);
		        		}
				    },
				    error: function() {
						$('#workouts .workout.deleted').show().removeClass('deleted');
				    	console.log('Не удалось удалить тренировку');
				    }
				});
			}
  		});
  		$('#workouts').delegate('.workout > div.ex-open', 'click', function() {
  			$(this).parent().find('.workout-fields-container').toggle();
  		});
  		$('#workouts').delegate('.workout > input', 'blur', function() {
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/edit_workout',
			    data: { 'workout_id': $(this).parent().data('workout-id'), title: $(this).val(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		} else {
	        			$('#workout'+result.workout_id).val(result.title);
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить тренировку');
			    }
			});
  		});
  		$('#workouts').delegate('.workout > .exercises > .set > input', 'blur', function() {
  			var dfield = $(this).attr('field');
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/edit_set',
			    data: { 'set_id': $(this).parent().data('set-id'), field: dfield, value: $(this).val(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		} else {
	        			$('#workout'+result.workout_id).val(result.title);
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить тренировку');
			    }
			});
  		});
  		$('#workouts').delegate('.routines > div > div.close', 'click', function() {
  			$(this).parent().addClass('deleted').hide();
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/del_course_routine',
			    data: { 'routine_id': $(this).parent().data('routine-id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			$('#course_routine_'+result.routine_id+'.deleted').remove();
	        		} else {
                        $('#course_routine_'+result.routine_id+'.deleted').removeClass('deleted').show();
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	 $('#workouts .routines .deleted').removeClass('deleted').show();
			    	console.log('Не удалось удалить подход');
			    }
			});
  			$(this).parent().hide();
  		});
  		$('#workouts').delegate('.routines .routine_sets div.close', 'click', function() {
  			$(this).parent().addClass('deleted').hide();
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/del_course_set',
			    data: { 'set_id': $(this).parent().data('set-id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			$('#course_set_'+result.set_id+'.deleted').remove();
	        		} else {
                        $('#course_set_'+result.set_id+'.deleted').removeClass('deleted').show();
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	 $('#workouts .routines .routine_sets .deleted').removeClass('deleted').show();
			    	console.log('Не удалось удалить параметр');
			    }
			});
  			$(this).parent().hide();
  		});
		$('#workouts').delegate('.routines .routine > input', 'change', function() {
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/edit_course_routine',
			    data: { 'routine_id': $(this).parent().data('routine-id'), 'field': $(this).attr('field'), 'value': $(this).val(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		} else {
	        			$('#course_routine_'+result.id+' [field='+result.field+']').val(result.old_value);
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить подход');
			    }
			});
		});
		$('.workout-fields').delegate('.workout-field > input, .workout-field > select', 'change', function(){  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/edit_workout_report_field',
			    data: { 'workout_report_field_id': $(this).parent().data('id'), 'field': $(this).attr('field'), 'value': $(this).val(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		} else {	        			$('#workout_report_field_'+result.id+' [field='+result.field+']').val(result.old_value);
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить поле отчёта');
			    }
			});		});
		$('.workout-fields').delegate('.workout-field .ex-close', 'click', function(){
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/del_workout_report_field',
			    data: { 'workout_report_field_id': $(this).parent().data('id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			$('#workout_report_field_'+result.id).remove();
	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось удалить поле отчёта');
			    }
			});
		});
		$('#workouts').delegate('.routines .routine_sets .routine-set > select, .routines .routine_sets .routine-set > input', 'change', function() {  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/edit_course_set',
			    data: { 'set_id': $(this).parent().data('set-id'), 'field': $(this).attr('field'), 'value': $(this).val(), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        		} else {
	        			$('#course_set_'+result.id+' [field='+result.field+']').val(result.old_value);
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить параметр');
			    }
			});
		});
  		$('#workouts').delegate('.routines .add-routine', 'click', function() {
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_course_routine',
			    data: { 'relation_id': $(this).data('relation-id'), 'type': $(this).data('type'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			var new_routine = $('#routine_template').clone().show().attr('id', 'course_routine_'+result.id).data('routine-id', result.id);
	        			new_routine.find('span').html(result.set);
	        			$('#add_routine_'+result.type+'_'+result.relation_id).before(new_routine);
	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось добавить подход');
			    }
			});
  		});
  		$('#workouts').delegate('.routines .routine .add-set', 'click', function() {
  			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_course_set',
			    data: { 'routine_id': $(this).parent().data('routine-id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {
	        			var new_set = $('#routine_set_template').clone().show().attr('id', 'course_set_'+result.id).data('set-id', result.id);
	        			$('#course_routine_'+result.routine_id+' .routine_sets').append(new_set);
	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось добавить параметр');
			    }
			});
  		});
  		function saveWorkoutList() {
  			var wlist = [];
  			$('#workouts .workout').each(function() {
  				if($(this).data('workout-id') != undefined && $(this).data('workout-id') > 0) {
  					wlist[$(this).index()] = $(this).data('workout-id');
  				}
  			});
			$.ajax({
			    method: 'POST',
			    url: '/admin/course/add_workout',
			    data: { 'course_id': <?=$course->id ?>, 'list': wlist, '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
			    dataType: 'json',
			    success: function(result) {
	        		if(result && result.status && result.status == 'ok') {

	        		} else {
	        			console.log(result.error);
	        		}
			    },
			    error: function() {
			    	console.log('Не удалось сохранить список тренировок');
			    }
			});
  		}
  		$('#workouts').delegate('.workout > .exercises > .set > div.ex-close', 'click', function() {
  			if(confirm('Вы действительно хотите удалить сет?')) {
	  			$(this).parent().addClass('deleted').hide();
	  			$.ajax({
				    method: 'POST',
				    url: '/admin/course/del_set',
				    data: { 'set_id': $(this).parent().data('set-id'), '_csrf-backend': '<?=Yii::$app->request->getCsrfToken() ?>' },
				    dataType: 'json',
				    success: function(result) {
		        		if(result && result.status && result.status == 'ok') {
	                        $('#workouts .set.deleted').remove();
		        		} else {
		        			$('#workouts .set.deleted').show().removeClass('deleted');
		        			console.log(result.error);
		        		}
				    },
				    error: function() {
						$('#workouts .set.deleted').show().removeClass('deleted');
				    	console.log('Не удалось удалить сет');
				    }
				});
			}
  		});
  		var ex_scroll_offset = $('#exercises').offset().top-50;
  		$(window).scroll(function(){  			if($(window).scrollTop() > ex_scroll_offset) {
  				if(!$('#exercises, #workout_add').hasClass('topped')) {
	  				$('#exercises, #workout_add').addClass('topped');	  				$('#exercises').css({position:'fixed', top:'50px', width:$('#exercises').outerWidth(), height:$('#exercises').outerHeight(), 'overflow-y': 'auto', left:$('#exercises').offset().left});
	  				$('#workout_add').css({position:'fixed', top:'50px', left:$('#workout_add').offset().left});  			    }
  			} else {  				if($('#exercises').hasClass('topped')) {
	  				$('#exercises, #workout_add').removeClass('topped');	  				$('#exercises').css({position:'static', top:'auto', width:'47%', height:'auto', 'overflow-y': 'inherit', left:'auto'});
	  				$('#workout_add').css({position:'static', top:'auto', left:'auto'});
	  			}  			}  		});
  		$(window).trigger('scroll');
	});
</script>