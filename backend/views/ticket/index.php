<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тикеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => true,
        'hover' => true,
        // 'pjax' => true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id',
            [
                'label' => 'От кого',
                'attribute' => 'user_id',
                'format' => 'html',
                'value' => function($data) {
                    return Html::a(is_null($data->user) ? '-' : (empty($data->user->fio) ? $data->user->email : $data->user->fio), is_null($data->user) ? '#' : \Yii::$app->urlManager->createAbsoluteUrl(['/trainer/client/' . $data->user->id])) . (($data->user->is_vip) ? ' <div class="btn btn-xs btn-info">VIP</div>' : '');
                }
            ],
            [
                'label' => 'Кому',
                'attribute' => 'role',
                'format' => 'html',
                'value' => function ($data) {
                    $role = \common\models\UserRole::findOne(['id' => $data->role]);
                    return is_null($role) ? '-' : $role->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\UserRole::find()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'title',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a($data->title, \Yii::$app->urlManager->createAbsoluteUrl(["/ticket/$data->id"]));
                }
            ],
            [
                'attribute' => 'creation_datetime',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'creation_datetime',
                    'convertFormat' => true,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'format' => 'yyyy-MM-dd',
                    ],
                ]),
            ],
            /*[
                'attribute' => 'read_datetime',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'read_datetime',
                    'convertFormat' => true,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'format' => 'yyyy-MM-dd',
                    ],
                ]),
            ],*/
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($data) {
                    switch ($data->status) {
                        case(\common\models\Ticket::STATUS_NEW):
                            return 'Не отвечен';
                            break;
                        case(\common\models\Ticket::STATUS_VIEWERD):
                            return 'Просмотрен';
                            break;
                        case(\common\models\Ticket::STATUS_ANSWERED):
                            return 'Отвечен';
                            break;
                        case(\common\models\Ticket::STATUS_NEW_ANSWER):
                            return 'Есть ответ<br><small>(не просмотрено клиентом)</small>';
                            break;
                        case(\common\models\Ticket::STATUS_NEW_ANSWER_VIEWED):
                            return 'Есть ответ<br><small>(просмотрено клиентом)</small>';
                            break;
                        case(\common\models\Ticket::STATUS_CLOSED):
                            return 'Закрыт';
                            break;
                    }
                },
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'filter' => [
                    \common\models\Ticket::STATUS_NEW => 'Не отвечен',
                    \common\models\Ticket::STATUS_VIEWERD => 'Просмотрен',
                    \common\models\Ticket::STATUS_ANSWERED => 'Отвечен',
                    \common\models\Ticket::STATUS_NEW_ANSWER => 'Есть ответ (не просмотрено)',
                    \common\models\Ticket::STATUS_NEW_ANSWER_VIEWED => 'Есть ответ (просмотрено)',
                    \common\models\Ticket::STATUS_CLOSED => 'Закрыт',
                ],
            ],
            /*[
                'attribute' => 'evaluation',
                'format' => 'html',
                'value' => function ($data) {
                    switch ($data->evaluation) {
                        case(\common\models\Ticket::EVALUATION_NONE):
                            return Html::tag('span', '', ['class' => 'glyphicon glyphicon-option-horizontal', 'aria-hidden' => 'true']);
                            break;
                        case(\common\models\Ticket::EVALUATION_LIKE):
                            return Html::tag('span', '', ['class' => 'glyphicon glyphicon-thumbs-up text-success', 'aria-hidden' => 'true']);
                            break;
                        case(\common\models\Ticket::EVALUATION_DISLIKE):
                            return Html::tag('span', '', ['class' => 'glyphicon glyphicon-thumbs-down text-danger', 'aria-hidden' => 'true']);
                            break;
                    }
                },
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'filter' => [
                    \common\models\Ticket::EVALUATION_NONE => 'Нет оценки',
                    \common\models\Ticket::EVALUATION_LIKE => 'Понравилось',
                    \common\models\Ticket::EVALUATION_DISLIKE => 'Не понравилось',
                ],
            ],*/
            [
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a('<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>', ['delete', 'id' => $data->id], [
                        'class' => 'text-danger',
                        'data' => [
                            'confirm' => 'Вы уверены, что хотите удалить этот тикет?',
                            'method' => 'post',
                        ]
                    ]);
                },
                'contentOptions' => ['class' => 'text-center'],
            ]
        ],
    ]); ?>
</div>
