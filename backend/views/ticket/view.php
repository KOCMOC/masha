<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = $model->title . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Тикеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-7">
            <?php if(count($model->ticketMessages)): ?>
                <?php foreach ($model->ticketMessages as $message): ?>

                    <div class="panel panel-<?= ($model->user_id == $message->user_id) ? 'default' : 'info'?>">
                        <div class="panel-heading"><?= Html::a($message->user->email, ['/admin/user#' . $model->user->id]); ?></div>
                        <div class="panel-body">
                            <?=nl2br($message->message) ?>
                        </div>
                        <?php if(count($message->attachments)): ?>
                        <div class="panel-footer">
                            <?php foreach ($message->attachments as $file): ?>
                                <a href="<?= $file['url'] ?>" target="_blank" class="label label-default"><?= $file['filename'] ?></a>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($formModel, 'message')->textarea() ?>

            <?= $form->field($formModel, 'attachmentFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить ответ', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-lg-5">
            <div class="panel panel-default">
                <div class="panel-body">
                	<p>Автор: <span class="pull-right"><?=$model->user->fio ?></span></p>
                    <p>E-mail: <span class="pull-right"><?= Html::a($model->user->email, ['/admin/user#' . $model->user->id]); ?></p>
                    <p>Телефон: <span class="pull-right"><?=$model->user->phone ?></span></p>
                    <p>Кому: <span class="pull-right"><?= $model->roleName ?></span></p>
                    <p>Статус: <span class="pull-right"><?php switch ($model->status) {
                            case(\common\models\Ticket::STATUS_NEW):
                                echo 'Не отвечен';
                                break;
                            case(\common\models\Ticket::STATUS_VIEWERD):
                                echo 'Просмотрен';
                                break;
                            case(\common\models\Ticket::STATUS_ANSWERED):
                                echo 'Отвечен';
                                break;
                            case(\common\models\Ticket::STATUS_NEW_ANSWER):
                                echo 'Есть ответ (не просмотрено)';
                                break;
                            case(\common\models\Ticket::STATUS_NEW_ANSWER_VIEWED):
                                echo 'Есть ответ (просмотрено)';
                                break;
                            case(\common\models\Ticket::STATUS_CLOSED):
                                echo 'Закрыт ';
                                break;
                            } ?></span></p>
                    <!--<p>Оцента: <span class="pull-right"><?php /*switch ($model->evaluation) {
                            case(\common\models\Ticket::EVALUATION_NONE):
                                echo Html::tag('span', '', ['class' => 'glyphicon glyphicon-option-horizontal', 'aria-hidden' => 'true']);
                                break;
                            case(\common\models\Ticket::EVALUATION_LIKE):
                                echo Html::tag('span', '', ['class' => 'glyphicon glyphicon-thumbs-up text-success', 'aria-hidden' => 'true']);
                                break;
                            case(\common\models\Ticket::EVALUATION_DISLIKE):
                                echo Html::tag('span', '', ['class' => 'glyphicon glyphicon-thumbs-down text-danger', 'aria-hidden' => 'true']);
                                break;
                            }*/?></span></p>-->
                    <p>Дата создания: <span class="pull-right"><?= $model->creation_datetime ?></span></p>
                    <p>Дата просмотра: <span class="pull-right"><?= $model->read_datetime ?></span></p>
                </div>
            </div>
            <p class="text-center">
                <?php if($model->status == \common\models\Ticket::STATUS_CLOSED): ?>
                <a href="/admin/ticket/status/<?= $model->id ?>" class="btn btn-success">Открыть тикет</a>
                <?php else: ?>
                <a href="/admin/ticket/status/<?= $model->id ?>" class="btn btn-primary">Закрыть тикет</a>
                <?php endif; ?>
            </p>
        </div>
    </div>

</div>
