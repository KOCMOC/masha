<div class="set" data-set-id="<?=$set->id ?>" id="set<?=$set->id ?>">
    Название:&nbsp;<input type="text" class="title" value="<?=str_replace('"', '&quot;', $set->title) ?>" field="title">
    <div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="workout-expand"><span class="glyphicon glyphicon-download"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div>
    <div class="set-data">
     Отдых:&nbsp;<input type="text" value="<?=$set->sleep ?>" class="sleep" field="sleep">
     <br />
     Коммент выпол:&nbsp;<input type="text" field="alert_proc" value="<?=$set->alert_proc ?>" style="width:100%;margin:0 0 5px 0">
     <br />
     Коммент отдых:&nbsp;<input type="text" field="alert_sleep" value="<?=$set->alert_sleep ?>" style="width:100%;margin:0">
     <div class="set-exercises"><?php foreach($set->getExercises('number')->all() as $set_exercise): $exercise = $set_exercise->getExercise(); ?>
             <div class="exercise" id="set_exercise_<?=$set_exercise->id ?>" data-exercise-id="<?=$exercise->id ?>" data-relation-id="<?=$set_exercise->id ?>" data-type="<?=\common\models\Course_training_routine::SetRelation ?>"><?=$exercise->title ?><div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="routines"></div></div>
         <?php endforeach; ?></div>
    </div>
</div>