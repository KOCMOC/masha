	<div id="course_routine_<?=$routine->id ?>" data-routine-id="<?=$routine->id ?>" class="routine">
		<?php if($routine->type != \common\models\Course_training_routine::SetRelation): ?>
		<div class="close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div>
		<div class="r-clone" style="float:right;margin-right:10px;font-size:20px"><a href="javascript:void(0)">+</a></div>
		Подход <?=$routine->set ?>&nbsp;
		Отдых <input type="text" value="<?=$routine->sleep ?>" field="sleep"><br />
		Ком. вып. <input type="text" value="<?=$routine->alert_proc ?>" field="alert_proc"><br />
		Ком. отд. <input type="text" value="<?=$routine->alert_sleep ?>" field="alert_sleep">
		<?php endif; ?>
		<div class="routine_sets">
		<?php
		$sets = $routine->getCourseSets();
		foreach($sets as $set): ?>
			<div id="course_set_<?=$set->id ?>" data-set-id="<?=$set->id ?>" class="routine-set">
				<select field="id_set_attr">
				<?php if($set->id_set_attr == 0): ?><option value="0">Выберите параметр</option><?php endif; ?>
				<?php foreach($attributesList as $attributeOne) : ?>
					<option value="<?=$attributeOne->id ?>"<?=($attributeOne->id == $set->id_set_attr)?' selected':'' ?>><?=$attributeOne->name ?><?=($attributeOne->unit)?' ('.$attributeOne->unit.')':'' ?></option>
				<?php endforeach; ?>
				</select>
				<input field="default_val" type="text" value="<?=$set->default_val ?>"><input field="to_global" type="checkbox" value="1"<?=($set->to_global == 1)?' checked':'' ?>><a href="javascript:void(0)" class="call-global">G</a><input style="display:none" field="trigger" type="text" value="<?=$set->trigger ?>" placeholder="Триггер"><div class="close">x</div>
			</div>
		<?php endforeach; ?>
		</div>
		<a href="javascript:void(0)" class="add-set">Добавить параметр</a>
	</div>