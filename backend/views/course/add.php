<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Курсы: Новый курс ';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => '\admin\course'];
$this->params['breadcrumbs'][] = 'Новый курс';

?>
<div class="site-index">
    <div>
        <h2>Новый курс</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'add-course',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ],
    ]);?>
    <div class="row col-md-10"><?=$form->field($model, 'title')->textInput()->label('Название курса');?></div>
    <div class="row col-md-10"><?=$form->field($model, 'course_group_id')->dropDownList($groups, ['prompt'=>'Выберите группу курса']);?></div>
	<div class="row col-md-10"><?=$form->field($model, 'gender')->radioList($genders) ?></div>
	<div class="row col-md-10"><?=$form->field($model, 'place')->radioList($places) ?></div>
    <div class="row col-md-10"><?=$form->field($model, 'level')->dropDownList($levels, ['prompt'=>'Выберите уровень курса']);?></div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'description')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
        'language' => 'ru'
    ]);?>
    </div>
    </div>
    <div class="row col-sm-10">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
    </div>
    <?php ActiveForm::end();?>
</div>
