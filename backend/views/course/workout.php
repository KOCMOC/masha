<div class="workout" id="workout<?=$workout->id ?>" data-workout-id="<?=$workout->id ?>">
    Название:&nbsp;<input type="text" value="<?=str_replace('"', '&quot;', $workout->title) ?>"><div class="close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="workout-expand"><span class="glyphicon glyphicon-download"></span></div>
    <div class="workout-fields-container">
        <div class="workout-fields"><?php if($workout->getReport()->count() > 0) foreach($workout->getReport()->all() as $workout_report): /*$workout_report_field = $workout_report->getField();*/ ?>
                <div class="workout-field" id="workout_report_field_<?=$workout_report->id ?>" data-id="<?=$workout_report->id ?>">
                    <input type="text" field="title" class="add-field-title" value="<?=$workout_report->title ?>">&nbsp;<select field="type" class="add-field-type"><?php foreach($reportFieldsTypes as $reportFieldKey=>$reportFieldType): ?>
                            <option value="<?=$reportFieldType ?>"<?=($workout_report->type == $reportFieldType)?' selected':'' ?>><?=$reportFieldKey ?></option>
                        <?php endforeach; ?></select><br />Связано&nbsp;с&nbsp;<select field="related_anket_field" class="add-field-related_anket_field"><option>Выберите поле анкеты</option><?php foreach($anket_fields as $anket_key=>$anket_field):?><option value="<?=$anket_field->id ?>"><?=$anket_key+1 ?>. <?=mb_substr($anket_field->title, 0, 30) ?></option><?php endforeach; ?></select>
                    <div class="ex-close">x</div>
                </div>
            <?php endforeach; ?></div>
        <div class="btn-group">
            <button type="button" class="workout-report-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить поле отчёта</button>
        </div>
    </div>
    <div class="btn-group">
        <button type="button" class="set-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить сет</button>
    </div>
    <div class="exercises"><?php foreach($workout->getWorkoutExercises('number')->all() as $workout_exercise):
            if($workout_exercise->type == \common\models\Workout_exercise::Exercise) :
                $exercise = $workout_exercise->getExercise(); ?>
                <div class="exercise" id="workout_exercise_<?=$workout_exercise->id ?>" data-exercise-id="<?=$exercise->id ?>" data-relation-id="<?=$workout_exercise->id ?>" data-type="<?=\common\models\Course_training_routine::WorkoutRelation ?>"><?=$exercise->title ?><div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="routines"></div></div>
            <?php else:
                $set = $workout_exercise->getSet(); ?>
                <div class="set" data-set-id="<?=$set->id ?>" id="set<?=$set->id ?>">
                    Название:&nbsp;<input type="text" class="title" value="<?=str_replace('"', '&quot;', $set->title) ?>" field="title">
                    <div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="workout-expand"><span class="glyphicon glyphicon-download"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div>
                    <div class="set-data">
                     Отдых:&nbsp;<input type="text" value="<?=$set->sleep ?>" class="sleep" field="sleep">
                     <br />
                     Коммент выпол:&nbsp;<input type="text" field="alert_proc" value="<?=$set->alert_proc ?>" style="width:100%;margin:0 0 5px 0">
                     <br />
                     Коммент отдых:&nbsp;<input type="text" field="alert_sleep" value="<?=$set->alert_sleep ?>" style="width:100%;margin:0">
                     <div class="set-exercises"><?php foreach($set->getExercises('number')->all() as $set_exercise): $exercise = $set_exercise->getExercise(); ?>
                             <div class="exercise" id="set_exercise_<?=$set_exercise->id ?>" data-exercise-id="<?=$exercise->id ?>" data-relation-id="<?=$set_exercise->id ?>" data-type="<?=\common\models\Course_training_routine::SetRelation ?>"><?=$exercise->title ?><div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="routines"></div></div>
                         <?php endforeach; ?></div>
                    </div>
                </div>
            <?php endif;
        endforeach; ?></div>
</div>