<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Курсы: компоновка ';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => '\admin\course'];
$this->params['breadcrumbs'][] = 'Скомпоновать курс';

$this->registerJsFile('/admin/js/jquery.hc-sticky.min.js',  ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/admin/js/course-view.js',  ['position' => yii\web\View::POS_END]);

?>
<div class="site-index">
    <div class="">
        <h2>Курс</h2>
    </div>
    <div class="body-content">
        <table class="table table-striped">
            <tr>
                <td width="150"><b>Название</b></td>
                <td><?=$course->title?></td>
            </tr>
            <tr>
                <td><b>Описание</b></td>
                <td><?=$course->description?></td>
            </tr>
            <tr>
                <td><b>Анкета курса</b></td>
                <td>
                    <?php
                    $anket_fields = $course->getAnket();
                    if(sizeof($anket_fields) > 0)
                        foreach($anket_fields as $anket_key=>$anket_field) {
                            echo '<div>'.($anket_key+1).'. '.$anket_field->title.' ('.array_search($anket_field->type, $anketFieldsTypes).')</div>';
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <td><b>Тренировки</b></td>
                <td></td>
            </tr>
                <td colspan="2">
                    <div id="workouts-container">
                        <div class="btn-group">
                            <button id="workout_add" type="button" class="btn btn-info" style="width: 100%;" data-course-id="<?=$course->id?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить тренировку</button>
                        </div>
                        <div id="workouts"><?php foreach($course->getWorkouts('number')->all() as $workout): ?>
                                <div class="workout" id="workout<?=$workout->id ?>" data-workout-id="<?=$workout->id ?>">
                                    Название:&nbsp;<input type="text" value="<?=str_replace('"', '&quot;', $workout->title) ?>"><div class="close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="workout-expand"><span class="glyphicon glyphicon-download"></span></div>
                                    <div class="workout-fields-container">
                                        <div class="workout-fields"><?php if($workout->getReport()->count() > 0) foreach($workout->getReport()->all() as $workout_report): /*$workout_report_field = $workout_report->getField();*/ ?>
                                                <div class="workout-field" id="workout_report_field_<?=$workout_report->id ?>" data-id="<?=$workout_report->id ?>">
                                                    <input type="text" field="title" class="add-field-title" value="<?=$workout_report->title ?>">&nbsp;<select field="type" class="add-field-type"><?php foreach($reportFieldsTypes as $reportFieldKey=>$reportFieldType): ?>
                                                            <option value="<?=$reportFieldType ?>"<?=($workout_report->type == $reportFieldType)?' selected':'' ?>><?=$reportFieldKey ?></option>
                                                        <?php endforeach; ?></select><br />Связано&nbsp;с&nbsp;<select field="related_anket_field" class="add-field-related_anket_field"><option>Выберите поле анкеты</option><?php foreach($anket_fields as $anket_key=>$anket_field):?><option value="<?=$anket_field->id ?>"><?=$anket_key+1 ?>. <?=mb_substr($anket_field->title, 0, 30) ?></option><?php endforeach; ?></select>
                                                    <div class="ex-close">x</div>
                                                </div>
                                            <?php endforeach; ?></div>
                                        <div class="btn-group">
                                            <button type="button" class="workout-report-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить поле отчёта</button>
                                        </div>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="set-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить сет</button>
                                    </div>
                                    <div class="exercises"><?php foreach($workout->getWorkoutExercises('number')->all() as $workout_exercise):
                                            if($workout_exercise->type == \common\models\Workout_exercise::Exercise) :
                                                $exercise = $workout_exercise->getExercise(); ?>
                                                <div class="exercise" id="workout_exercise_<?=$workout_exercise->id ?>" data-exercise-id="<?=$exercise->id ?>" data-global-exercise-id="<?=($exercise->id_related > 0)?$exercise->id_related:$exercise->id ?>" data-relation-id="<?=$workout_exercise->id ?>" data-type="<?=\common\models\Course_training_routine::WorkoutRelation ?>"><?=$exercise->title ?><div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="ex-trigger" style="display:none">Триггер сделал&nbsp;<input type="text" field="trigger_yes" value="<?=$workout_exercise->trigger_yes ?>"><br />Триггер не сделал&nbsp;<input type="text" field="trigger_no" value="<?=$workout_exercise->trigger_no ?>"></div><div class="routines"></div></div>
                                            <?php else:
                                                $set = $workout_exercise->getSet(); ?>
                                                <div class="set" data-set-id="<?=$set->id ?>" id="set<?=$set->id ?>">
                                                    Название:&nbsp;<input type="text" class="title" value="<?=str_replace('"', '&quot;', $set->title) ?>" field="title">
                                                    <div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="workout-expand"><span class="glyphicon glyphicon-download"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div>
                                                    <div class="set-data">
	                                                    Отдых:&nbsp;<input type="text" value="<?=$set->sleep ?>" class="sleep" field="sleep">
	                                                    <br />
	                                                    Коммент выпол:&nbsp;<input type="text" field="alert_proc" value="<?=$set->alert_proc ?>" style="width:100%;margin:0 0 5px 0">
	                                                    <br />
	                                                    Коммент отдых:&nbsp;<input type="text" field="alert_sleep" value="<?=$set->alert_sleep ?>" style="width:100%;margin:0">
	                                                    <div class="set-exercises"><?php foreach($set->getExercises('number')->all() as $set_exercise): $exercise = $set_exercise->getExercise(); ?>
	                                                            <div class="exercise" id="set_exercise_<?=$set_exercise->id ?>" data-exercise-id="<?=$exercise->id ?>" data-global-exercise-id="<?=($exercise->id_related > 0)?$exercise->id_related:$exercise->id ?>" data-relation-id="<?=$set_exercise->id ?>" data-type="<?=\common\models\Course_training_routine::SetRelation ?>"><?=$exercise->title ?><div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="ex-trigger" style="display:none">Триггер сделал&nbsp;<input type="text" field="trigger_yes" value="<?=$set_exercise->trigger_yes ?>"><br />Триггер не сделал&nbsp;<input type="text" field="trigger_no" value="<?=$set_exercise->trigger_no ?>"></div><div class="routines"></div></div>
	                                                        <?php endforeach; ?></div>
                                                    </div>
                                                </div>
                                            <?php endif;
                                        endforeach; ?></div>
                                </div>
                            <?php endforeach; ?></div>
                    </div>

					<div id="ex-sticky">
						<?php foreach(\common\models\Exercise_tab::find()->all() as $tab): ?><?=$tab->title ?>&nbsp;<input type="checkbox" tab="<?=$tab->id ?>" checked>&nbsp;&nbsp;&nbsp;<?php endforeach; ?>
						<input type="text">
	                    <div id="exercises">
	                        <?php foreach($exercises as $exercise): ?>
	                            <div class="exercise" tab="<?=$exercise->id_tab ?>" list="true" data-exercise-id="<?=$exercise->id ?>" data-global-exercise-id="<?=($exercise->id_related > 0)?$exercise->id_related:$exercise->id ?>"><?=$exercise->title ?><div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="ex-trigger" style="display:none">Триггер сделал&nbsp;<input type="text" field="trigger_yes" value=""><br />Триггер не сделал&nbsp;<input type="text" field="trigger_no" value=""></div><div class="routines"></div></div>
	                        <?php endforeach; ?>
	                    </div>
                    </div>
                    <div id="workout_template" class="workout">
                        Название:&nbsp;<input type="text"><div class="close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div><div class="ex-open"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div><div class="workout-expand"><span class="glyphicon glyphicon-download"></span></div>
                        <div class="workout-fields-container">
                            <div class="workout-fields"></div>
                            <div class="btn-group">
                                <button type="button" class="workout-report-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить поле отчёта</button>
                            </div>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="set-add btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить сет</button>
                        </div>
                        <div class="exercises"></div>
                    </div>
                    <div id="set_template" class="set">
                        Название:&nbsp;<input type="text" class="title" field="title"><div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div><div class="workout-expand"><span class="glyphicon glyphicon-download"></span></div><div class="ex-clone"><span class="glyphicon glyphicon-plus"></span></div>
                        <div class="set-data">
	                        Отдых:&nbsp;<input type="text" class="sleep" field="sleep"><br />
	                        Коммент выпол:&nbsp;<input type="text" field="alert_proc" style="width:100%;margin:0 0 5px 0"><br />
	                        Коммент отдых:&nbsp;<input type="text" field="alert_sleep" style="width:100%;margin:0">
	                        <div class="set-exercises"></div>
                        </div>
                    </div>
                    <div id="routine_template" class="routine">
                        <div class="close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div>
                        Подход <span></span>&nbsp;
                        Отдых <input type="text" value="0" field="sleep"><br />
                        Ком. вып. <input type="text" field="alert_proc"><br />
                        Ком. отд. <input type="text" field="alert_sleep">
                        <div class="routine_sets"></div>
                        <a href="javascript:void(0)" class="add-set">Добавить параметр</a>
                    </div>
                    <div id="routine_set_template" class="routine-set">
                        <select field="id_set_attr">
                            <option value="0">Выберите параметр</option>
                            <?php foreach($attributesList as $attributeOne) : ?>
                                <option value="<?=$attributeOne->id ?>"><?=$attributeOne->name ?><?=($attributeOne->unit)?' ('.$attributeOne->unit.')':'' ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="text" value="" field="default_val"><a href="javascript:void(0)" class="call-global">Глобалки</a></a><input style="display:none" type="text" value="" field="trigger" placeholder="Триггер"><div class="close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div>
                    </div>
                    <div id="workout_field_template" class="workout-field">
                        <input type="text" field="title" class="add-field-title" value="Новое поле">&nbsp;<select field="type" class="add-field-type"><?php foreach($reportFieldsTypes as $reportFieldKey=>$reportFieldType): ?>
                                <option value="<?=$reportFieldType ?>"><?=$reportFieldKey ?></option>
                            <?php endforeach; ?></select><br />Связано&nbsp;с&nbsp;<select field="related_anket_field" class="add-field-related_anket_field"><option>Выберите поле анкеты</option><?php foreach($anket_fields as $anket_key=>$anket_field):?><option value="<?=$anket_field->id ?>"><?=$anket_key+1 ?>. <?=mb_substr($anket_field->title, 0, 30) ?></option><?php endforeach; ?></select>
                        <div class="ex-close"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="globalfield_template" style="display:none">
	<input type="text" field="title" value="">&nbsp;<input type="text" field="alias" value="">&nbsp;<input type="text" field="default" value="">&nbsp;<a href="javascript:void(0)" class="global-update">V</a>&nbsp;<a href="javascript:void(0)" class="global-delete">X</a>
</div>
<div id="globals_list" data-current-exid="0" style="display:none;position:absolute;padding:10px;background:#fff;border:1px solid #e5e5e5;border-radius:10px;margin-top:20px">
	<div onclick="$(this).parent().hide();" style="color: #fff; cursor: pointer; background: #f00; text-align: center; height: 20px; width: 20px; position: absolute; right: 0px; margin-top: -11px; border-radius: 0px 10px 0px 0px;">X</div>
	Глобальные переменные:
	<div style="padding: 15px; border: 1px solid #e5e5e5; border-radius: 10px;margin-bottom:5px" id="global_fields"><?php if(sizeof($globals) > 0) foreach($globals as $global): ?>
	<?php
		/*$default = false;
		foreach($course_globals as $course_global) {			if($course_global->getGlobalField()->id == $global->id) {				$default = $course_global->default;			}		} */
	?>
	<div <?/*php if($default !== false) echo 'style="background:#cfc"'; */?> id="global_field_<?=$global->id ?>" class="global_field" data-id="<?=$global->id ?>" data-exercise-id="<?=$global->id_exercise ?>"><input type="text" field="title" value="<?=$global->title ?>">&nbsp;<input type="text" field="alias" value="<?=$global->alias ?>">&nbsp;<input type="text" field="default" value="<?=/*($default !== false)?$default:*/$global->default ?>"><?/*<button class="btn btn-info" id="add-course-global-field" type="button" style="margin-left:10px;"><span class="glyphicon glyphicon-plus"></span></button>*/?>&nbsp;<a href="javascript:void(0)" class="global-update">V</a>&nbsp;<a href="javascript:void(0)" class="global-delete">X</a></div>
	<?php endforeach; ?></div>
	Наименование:&nbsp;<input type="text" id="global-field-title">&nbsp;&nbsp;&nbsp;Алиас:&nbsp;<input type="text" id="global-field-alias"><br />По умолчанию:&nbsp;<input type="text" id="global-field-default"><button class="btn btn-info" id="add-global-field" type="button" style="margin-left:10px;"><span class="glyphicon glyphicon-plus"></span></button>
</div>
<script type="text/javascript">
    course_id = <?=$course->id; ?>;
    $(document).ready(function(){
    	$('body').append($('#globals_list'));
		//$('#add-global-field').click(function(){
		$(document).delegate('#add-global-field', 'click', function(){
            $.ajax({
                method: 'POST',
                url: '/admin/course/add_global_field',
                data: {'id_course': course_id, 'id_exercise': $('#globals_list').data('current-exid'), 'title': $('#global-field-title').val(), 'alias': $('#global-field-alias').val(), 'default': $('#global-field-default').val() },
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                    	var newGlobal = $('#globalfield_template').clone();
                    	newGlobal.find('input[field=title]').val(result.title);
                        newGlobal.find('input[field=alias]').val(result.alias);
                        newGlobal.find('input[field=default]').val(result.default);
                        newGlobal.data('id', result.id).data('exercise-id', $('#globals_list').data('current-exid')).removeAttr('id').attr('id', 'global_field_'+result.id).show();
                        $('#global_fields').append(newGlobal);
                    } else {
                        console.log(result.error);
                    }
                },
                error: function() {
                    console.log('Не удалось создать глобальную переменную');
                }
            });
            $('#global-field-title, #global-field-alias, #global-field-default').val('');
		});
		$('#globals_list').delegate('.global-delete', 'click', function(){
			if(confirm("Удалить клобальную переменную " + $(this).parent().find('input[field=title]').val()+'?')) {
	            $.ajax({
	                method: 'POST',
	                url: '/admin/course/del_global_field',
	                data: {'id': $(this).parent().data('id')},
	                dataType: 'json',
	                success: function(result) {
	                    if(result && result.status && result.status == 'ok') {
	                    	$('#global_field_'+result.id).remove();
	                    } else {
	                        console.log(result.error);
	                    }
	                },
	                error: function() {
	                    console.log('Не удалось удалить глобальную переменную');
	                }
	            });
	         }
		});
		$('#globals_list').delegate('.global-update', 'click', function(){
            $.ajax({
                method: 'POST',
                url: '/admin/course/update_global_field',
                data: {'id': $(this).parent().data('id'), 'title': $(this).parent().find('input[field=title]').val(), 'alias': $(this).parent().find('input[field=alias]').val(), 'default': $(this).parent().find('input[field=default]').val() },
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                    	$('#global_field_'+result.id+' input[field=title]').val(result.title);
                    	$('#global_field_'+result.id+' input[field=alias]').val(result.alias);
                    	$('#global_field_'+result.id+' input[field=default]').val(result.default);
                    } else {
                        console.log(result.error);
                    }
                },
                error: function() {
                    console.log('Не удалось сохранить глобальную переменную');
                }
            });
		});
		$(document).delegate('#add-course-global-field', 'click', function(){
            $.ajax({
                method: 'POST',
                url: '/admin/course/add_global_field',
                data: {'id_course': course_id, 'id_global': $(this).parent().data('id'), 'default': $(this).parent().find('input[field=default]').val()},
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                    	$('#global_field_'+result.id).css({'background':'#cfc', 'opacity':0.5});
                    	$('#global_field_'+result.id).animate({opacity:1}, 1000);
                    	/*var newGlobal = $('#globalfield_template').clone();
                    	newGlobal.find('input[field=title]').val(result.title);
                        newGlobal.find('input[field=alias]').val(result.alias);
                        newGlobal.find('input[field=default]').val(result.default);
                        newGlobal.data('id', result.id).removeAttr('id').show();
                        $('#global_fields').append(newGlobal);*/
                    } else {
                        console.log(result.error);
                    }
                },
                error: function() {
                    console.log('Не удалось создать глобальную переменную для курса');
                }
            });
		});
		$(document).delegate('.call-global', 'click', function(){
			var exid = $(this).parents('.exercise').data('global-exercise-id');
			$('#globals_list .global_field').each(function(){				if($(this).data('exercise-id') == exid) {					$(this).show();				} else {					$(this).hide();				}
			});
			$('#globals_list').data('current-exid', exid).css({top: $(this).offset().top, left: $(this).offset().left}).show();
		});
    });
</script>