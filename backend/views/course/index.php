<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Курсы ';
$this->params['breadcrumbs'][] = 'Курсы';
?>
<div class="site-index">
    <div class="">
        <h2>Курсы&nbsp;(<?=$count_courses;?>)</h2>
    </div>
    <div><?= Yii::$app->session->getFlash('error'); ?></div>
    <div class="btn-group">
        <button id="add" type="button" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новый курс</button>
    </div>
    <?if(sizeof($courses) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Название курса</th>
                    <th>Описание курса</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?foreach($courses as $course):?>
                <tr>
                    <th scope="row"><?=$course['id'];?></th>
                    <td>
                    	<a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/course/edit/$course->id"]);?>' class="btn btn-success btn-sm" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>
                    	<a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/course/view/$course->id"]);?>' title='Посмотреть' class='btn btn-success btn-sm' id="a-desc" desc-id="<?=$course['id']?>"><span class="glyphicon glyphicon-eye-open"></span></a>
                    </td>
                    <td><?=$course['title']?></td>
                    <td><?=(mb_strlen($course['description']) > 55)?mb_substr($course['description'], 0, 55).'...':$course['description']?></td>
                    <td>
                    	<a href="<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/course/clone/$course->id"]);?>" class="btn btn-success btn-sm" title="Клонировать" onclick="if(!confirm('Клонировать курс?')) return false;"><span class="glyphicon glyphicon-plus-sign"></span></a>
	                    <a href="<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/course/delete/$course->id"]);?>" onclick="if(!confirm('Удалить курс?')) return false;">Удалить</a>
	                </td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
    <?else:?>
    Нет курсов
    <?endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').click(function(){
        location.href = "<?=\Yii::$app->urlManager->createAbsoluteUrl(['/course/add'])?>";
    });
})
</script>