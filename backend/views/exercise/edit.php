<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Упражнения: Редактировать упражнение ';
$this->params['breadcrumbs'][] = ['label' => 'Упражнения', 'url' => '\admin\exercise'];
$this->params['breadcrumbs'][] = 'Редактировать упражнение';

?>
<div class="site-index">
    <div>
        <h2>Редактировать упражнение</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'edit-exercise',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ],
    ]);?>
    <?=$form->field($model, 'id')->hiddenInput(['value' => $exercise_id])->label('');?>
    <div class="row col-md-10"><?=$form->field($model, 'title')->textInput()?></div>
	<div class="row col-md-10"><?=$form->field($model, 'id_tab')->dropDownList($tabs); ?></div>
	<div class="row col-md-10"><?=$form->field($model, 'id_related')->textInput(); ?></div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'description_cut')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'elfinder/connector',
        ],
        'language' => 'ru'
    ]);?>
    </div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'video')->widget(TinyMce::className(), [
    	'settings'=> [
    		'menubar' => false,
    		'toolbar' => 'media code preview',
	    	'plugins' => ['media', 'code', 'preview'],
	    	'toolbar_items_size' => 'large'
	  	],
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'elfinder/connector',
        ],
        'language' => 'ru',
    ]);?>
    </div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'description')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'elfinder/connector',
        ],
        'language' => 'ru'
    ]);?>
    </div>
<?php foreach($fields as $field): ?>
<div class="row col-md-10 ex-fields">
	<div>
		<div class="close">X</div>
		<input type="text" class="form-control" name="ExerciseForm[ex_names][ex<?=$field->id ?>]" value="<?=$field->title ?>">
		<textarea name="ExerciseForm[ex_fields][ex<?=$field->id ?>]"><?=$field->value ?></textarea>
	</div>
</div>
<?php endforeach; ?>
    <div class="row col-md-10">
		<button class="btn btn-info" id="add-ex-field" type="button" style="margin-bottom:10px;"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новое поле</button>
    </div>
    <input type="hidden" name="ExerciseForm[del_sets]" id="del-sets" value="">
    <div class="row col-md-10">
	    <div id="sets">
	    	<?php if(isset($sets) and is_array($sets) and sizeof($sets) > 0) foreach($sets as $set): ?>
			<div>
		    	<div class="set-item old" data-edited="no" style="border:1px solid #e5e5e5; border-radius:4px; padding:10px; margin-bottom: 10px">
		    		<div style="display:none" class="set-id"><?=$set->id ?></div>
		    		Отдых: <input type="text" class="sleep" value="<?=$set->sleep ?>"><a href='javascript:void(0)' style="margin-left:10px" class="btn btn-danger btn-sm" title="Удалить"><span class="glyphicon glyphicon-remove"></span></a><br />
                    Комментарий выполнение: <input type="text" class="alert-proc-field" style="width:500px" value="<?=$set->alert_proc ?>"><br />
                    Комментарий отдых: <input type="text" class="alert-sleep-field" style="width:500px" value="<?=$set->alert_sleep ?>">
		    		<input type="hidden" name="ExerciseForm[old_sets][]" value="">
		    		<div class="attributes">
		    			<?php
		    			$attrs = $set->getSets();
		    			if(sizeof($attrs) > 0)
		    			foreach($attrs as $attr):
		    			$attrData = $attr->getSet_attribute();
		    			?>
		    			<div class="attr-item">
		    				<select class="attr-id" style="margin-right:10px;">
			    				<? foreach($attributes as $attribute): ?>
			    				<option value="<?=$attribute->id ?>"<?=($attrData->id == $attribute->id)?' selected':'' ?>><?=$attribute->name.($attribute->unit?' ('.$attribute->unit.')':'') ?></option>
			    				<? endforeach; ?>
		    				</select>
                            <input type="text" class="attr-default" style="margin-right:10px;" value="<?=$attr->default_val ?>">
                            <?/*<a href="javascript:void(0)" class="call-global">Глобалки</a>*/?>
                            <input type="text" class="attr-trigger" style="margin-right:10px;display:none" value="<?=$attr->trigger ?>" placeholder="Триггер">
		    				<a href='javascript:void(0)' class="btn btn-danger btn-sm" title="Удалить"><span class="glyphicon glyphicon-remove"></span></a>
		    			</div>
                        <?php endforeach; ?>
		    		</div>
		    		<button class="btn btn-info add-set-attr" type="button"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новый параметр</button>
		    	</div>
		    </div>
		    <?php endforeach; ?>
	    </div>
	    <button class="btn btn-info" type="button" id="add-set"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новый подход</button>
	</div>
    <div class="row col-sm-10">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
    </div>
    <?php ActiveForm::end();?>
</div>
<div id="set_template" style="display:none">
   	<div class="set-item new" style="border:1px solid #e5e5e5; border-radius:4px; padding:10px; margin-bottom: 10px">
   		Отдых: <input type="text"><a href='javascript:void(0)' class="btn btn-danger btn-sm" style="margin-left:10px" title="Удалить"><span class="glyphicon glyphicon-remove"></span></a><br />
   		Комментарий выполнение: <input type="text" class="alert-proc-field" style="width:500px"><br />
   		Комментарий отдых: <input type="text" class="alert-sleep-field" style="width:500px">
   		<input type="hidden" name="ExerciseForm[sets][]" value="">
   		<div class="attributes">
   			<div class="attr_template attr-item">
   				<select class="attr-id" style="margin-right:10px;">
    				<option value="" selected>Выберите параметр</option>
    				<? foreach($attributes as $attribute): ?>
    				<option value="<?=$attribute->id ?>"><?=$attribute->name.($attribute->unit?' ('.$attribute->unit.')':'') ?></option>
    				<? endforeach; ?>
   				</select>
   				<input type="text" class="attr-default" style="margin-right:10px;">
   				<?/*<a href="javascript:void(0)" class="call-global">Глобалки</a>*/?>
   				<input type="text" class="attr-trigger" style="margin-right:10px;display:none" placeholder="Триггер">
   				<a href='javascript:void(0)' class="btn btn-danger btn-sm" title="Удалить"><span class="glyphicon glyphicon-remove"></span></a>
   			</div>
   		</div>
   		<button class="btn btn-info add-set-attr" type="button"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новый параметр</button>
   	</div>
</div>
<div id="attr_template" class="attr-item" style="display:none">
	<select class="attr-id" style="margin-right:10px;">
		<option value="" selected>Выберите параметр</option>
		<? foreach($attributes as $attribute): ?>
		<option value="<?=$attribute->id ?>"><?=$attribute->name.($attribute->unit?' ('.$attribute->unit.')':'') ?></option>
		<? endforeach; ?>
	</select>
	<input type="text" class="attr-default" style="margin-right:10px;">
	<?/*<a href="javascript:void(0)" class="call-global">Глобалки</a>*/?>
	<input type="text" class="attr-trigger" style="margin-right:10px;display:none" placeholder="Триггер">
	<a href='javascript:void(0)' class="btn btn-danger btn-sm" title="Удалить"><span class="glyphicon glyphicon-remove"></span></a>
</div>
<div id="exfield_template" class="row col-md-10 ex-fields">
	<div>
		<div class="close">X</div>
		<input type="text" class="form-control" name="ExerciseForm[ex_names][]">
		<textarea name="ExerciseForm[ex_fields][]"></textarea>
	</div>
</div>
<div id="globalfield_template" style="display:none">
	<input type="text" field="title" value="">&nbsp;<input type="text" field="alias" value="">&nbsp;<input type="text" field="default" value="" style="width:50px">
</div>
<?/*<div id="globals_list" style="display:none;position:absolute;padding:10px;background:#fff;border:1px solid #e5e5e5;border-radius:10px;margin-top:20px">
	<div onclick="$(this).parent().hide();" style="color: #fff; cursor: pointer; background: #f00; text-align: center; height: 20px; width: 20px; position: absolute; right: 0px; margin-top: -11px; border-radius: 0px 10px 0px 0px;">X</div>
	Глобальные переменные:
	<div style="padding: 15px; border: 1px solid #e5e5e5; border-radius: 10px;margin-bottom:5px" id="global_fields"><?php if(sizeof($globals) > 0) foreach($globals as $global): ?>
	<div data-id="<?=$global->id ?>"><input type="text" field="title" value="<?=$global->title ?>">&nbsp;<input type="text" field="alias" value="<?=$global->alias ?>">&nbsp;<input type="text" field="default" value="<?=$global->default ?>" style="width:50px"></div>
	<?php endforeach; ?></div>
	Наименование:&nbsp;<input type="text" id="global-field-title">&nbsp;&nbsp;&nbsp;Алиас:&nbsp;<input type="text" id="global-field-alias"><br >По умолчанию:&nbsp;<input type="text" id="global-field-default"><button class="btn btn-info" id="add-global-field" type="button" style="margin-left:10px;"><span class="glyphicon glyphicon-plus"></span></button>
</div>*/?>
<script type="text/javascript">
	$(document).ready(function(){
		<?/*$('body').append($('#globals_list'));*/?>
		var nextMce = 3;
		var set_data;
		$('.ex-fields:visible textarea').each(function(){
			$(this).attr('id', 'new_ex_field'+nextMce);
			$('#new_ex_field'+nextMce).tinymce({"language":"ru","plugins":["advlist autolink lists link image charmap print preview hr anchor pagebreak","searchreplace visualblocks visualchars code fullscreen","insertdatetime media nonbreaking save table contextmenu directionality","template paste textcolor"],"toolbar":"insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor","toolbar_items_size":"small","image_advtab":true,"relative_urls":false,"spellchecker_languages":"+Русский=ru","script_url":"/admin/assets/624a6fc2/tiny_mce.js","file_browser_callback":        function(field_name, url, type, win) {
			    var aFieldName = field_name, aWin = win;
			    var el = $("#elfd"+nextMce);
			    if(el.length == 0) {
			        el = $("<div/>").attr("id", "elfd"+nextMce);
			        $("body").append(el);
			        el.elfinder({"places":"","rememberLastDir":false,"url":"/admin/elfinder/connector","lang":"ru-RU","dialog":{"width":900,"modal":true,"title":"Files"},"editorCallback":            function(url) {
			        aWin.document.getElementById(field_name).value = url;
			        if (type == "image" && aFieldName=="src" && aWin.ImageDialog.showPreviewImage)
			            aWin.ImageDialog.showPreviewImage(url);
			    },"closeOnEditorCallback":true});
			        //place it above tinymce dialogue
			        el[0].elfinder.dialog.closest('.ui-dialog').css({'z-index':4000001});
			    }
			    else {
			        el.elfinder("open","/admin/elfinder/connector");
			    }
			}});
			nextMce++;		});
		$('#add-ex-field').click(function(){			var new_mce = $('#exfield_template').clone();
			new_mce.show().removeAttr('id').show();
			new_mce.find('textarea').attr('id', 'new_ex_field'+nextMce);
			//var currMce = nextMce - 1;
			var mces = $('.mce-tinymce:visible');
			console.log(mces.eq(mces.length-1).parent().parent().attr('id'));
			mces.eq(mces.length-1).parent().parent().after(new_mce);

			$('#new_ex_field'+nextMce).tinymce({"language":"ru","plugins":["advlist autolink lists link image charmap print preview hr anchor pagebreak","searchreplace visualblocks visualchars code fullscreen","insertdatetime media nonbreaking save table contextmenu directionality","template paste textcolor"],"toolbar":"insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor","toolbar_items_size":"small","image_advtab":true,"relative_urls":false,"spellchecker_languages":"+Русский=ru","script_url":"/admin/assets/624a6fc2/tiny_mce.js","file_browser_callback":        function(field_name, url, type, win) {
			    var aFieldName = field_name, aWin = win;
			    var el = $("#elfd"+nextMce);
			    if(el.length == 0) {
			        el = $("<div/>").attr("id", "elfd"+nextMce);
			        $("body").append(el);
			        el.elfinder({"places":"","rememberLastDir":false,"url":"/admin/elfinder/connector","lang":"ru-RU","dialog":{"width":900,"modal":true,"title":"Files"},"editorCallback":            function(url) {
			        aWin.document.getElementById(field_name).value = url;
			        if (type == "image" && aFieldName=="src" && aWin.ImageDialog.showPreviewImage)
			            aWin.ImageDialog.showPreviewImage(url);
			    },"closeOnEditorCallback":true});
			        //place it above tinymce dialogue
			        el[0].elfinder.dialog.closest('.ui-dialog').css({'z-index':4000001});
			    }
			    else {
			        el.elfinder("open","/admin/elfinder/connector");
			    }
			}});
			nextMce++;		});
		$('#add-global-field').click(function(){
            $.ajax({
                method: 'POST',
                url: '/admin/exercise/add_global_field',
                data: { 'id_exercise': '<?=$exercise_id ?>', 'title': $('#global-field-title').val(), 'alias': $('#global-field-alias').val(), 'default': $('#global-field-default').val() },
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                    	var newGlobal = $('#globalfield_template').clone();
                    	newGlobal.find('input[field=title]').val(result.title);
                        newGlobal.find('input[field=alias]').val(result.alias);
                        newGlobal.data('id', result.id).removeAttr('id').show();
                        $('#global_fields').append(newGlobal);
                    } else {
                        console.log(result.error);
                    }
                },
                error: function() {
                    console.log('Не удалось создать глобальную переменную');
                }
            });
            $('#global-field-title, #global-field-alias, #global-field-default').val('');
		});
		<?/*$(document).delegate('.call-global', 'click', function(){			$('#globals_list').css({top: $(this).offset().top, left: $(this).offset().left}).show();		});*/?>
		$(document).delegate('.ex-fields .close', 'click', function(){			$(this).parent().parent().remove();		});
		$('#edit-exercise').on('beforeSubmit', function(e) {
			$('#sets .set-item').each(function(){
				if($(this).hasClass('old') && $(this).data('edited') == 'yes') {
					set_data = '{"id":'+$(this).find('.set-id').html()+', "sleep":'+$(this).find('> input[type=text]').val()+', "alert_proc":"'+$(this).find('> .alert-proc-field').val().replace(/\"/g, '&quot;')+'", "alert_sleep":"'+$(this).find('> .alert-sleep-field').val().replace(/\"/g, '&quot;')+'", "sets":[';
					$(this).find('.attr-item').each(function(){
						set_data += '{"id":'+$(this).find('.attr-id').val()+',"default_val":"'+$(this).find('.attr-default').val()+'","trigger":"'+$(this).find('.attr-trigger').val()+'"},';
					});
					set_data = set_data.substring(0, set_data.length-1);
					set_data += ']}';
					$(this).find('> input[type=hidden]').val(set_data);
				}
				if($(this).hasClass('new')) {					set_data = '{"sleep":'+$(this).find('> input[type=text]').val()+', "alert_proc":"'+$(this).find('> .alert-proc-field').val().replace(/\"/g, '&quot;')+'", "alert_sleep":"'+$(this).find('> .alert-sleep-field').val().replace(/\"/g, '&quot;')+'", "sets":[';
					$(this).find('.attr-item').each(function(){
						set_data += '{"id":'+$(this).find('.attr-id').val()+',"default_val":"'+$(this).find('.attr-default').val()+'","trigger":"'+$(this).find('.attr-trigger').val()+'"},';
					});
					set_data = set_data.substring(0, set_data.length-1);
					set_data += ']}';
					$(this).find('> input[type=hidden]').val(set_data);				}
			});
		    return true;
		});
		$('#add-set').click(function(){
			var new_set = $('#set_template').clone().removeAttr('id').show();
			$('#sets').append(new_set);
		});
		$(document).delegate('.add-set-attr', 'click', function(){
			var new_attr = $('#attr_template').clone().removeAttr('id').show();
			$(this).parent().find('.attributes').append(new_attr);
			if($(this).closest('.set-item').hasClass('old')) {				$(this).closest('.set-item.old').data('edited', 'yes');
			}
		});
		$(document).delegate('.attr-item > .btn-danger, .set-item > .btn-danger', 'click', function(){
			if($(this).closest('.set-item').hasClass('old')) {				if($(this).parent().hasClass('attr-item')) {
					$(this).closest('.set-item.old').data('edited', 'yes');
				} else {
					var dels = ($('#del-sets').val() != '')?$('#del-sets').val().split(','):[];					dels.push($(this).parent().find('.set-id').html());
					$('#del-sets').val(dels.join(','));				}			}
			$(this).parent().remove();
		});
		$(document).delegate('.set-item.old input[type="text"], .set-item.old select', 'change', function(){
			$(this).closest('.set-item.old').data('edited', 'yes');
		});
	});
</script>