<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Упражнения ';
$this->params['breadcrumbs'][] = 'Упражнения';
?>
<div class="site-index">

    <div class="">
        <h2>Упражнения&nbsp;(<?=$count_exercises;?>)</h2>
    </div>
    <?if(sizeof($exercises) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <div class="btn-group">
            <button id="add" type="button" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новое упражнение</button>
        </div>
        <div class="btn-group">
        	<form>
		        <input type="text" style="width:300px;margin-left:30px;display:inline-block" name="search" class="form-control" value="<?=$search ?>">
		        <button id="search" type="button" class="btn btn-info" onclick="submit()"><span class="glyphicon glyphicon-plus"></span>&nbsp;Найти</button>
	        </form>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Название</th>
                    <th>Краткое описание</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($exercises as $key=>$exercise):?>
                <tr>
                    <th scope="row"><?=$exercise->id;?></th>
                    <td width="165">
                        <a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/exercise/edit/$exercise->id"]);?>' class="btn btn-success btn-sm" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/exercise/view/$exercise->id"]);?>' title='Посмотреть' class='btn btn-success btn-sm' id="a-desc" desc-id="<?=$key?>"><span class="glyphicon glyphicon-eye-open"></span></a>
                        <a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/exercise/clone/$exercise->id"]);?>' title='Клонировать' class='btn btn-success btn-sm' id="a-desc" desc-id="<?=$key?>"><span class="glyphicon glyphicon-plus-sign"></span></a>
                    	<a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/exercise/delete/$exercise->id"]);?>' title='Удалить' class='btn btn-danger btn-sm' id="a-desc" desc-id="<?=$key?>" onclick="if(!confirm('Удалить упражнение?')) return false;"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                    <td><?=$exercise->title?></td>
                    <td><?=$exercise->description_cut?></td>
                    <td></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
  </div>
    <?else:?>
    Нет упражнений
    <?endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').click(function(){
        location.href = "<?=\Yii::$app->urlManager->createAbsoluteUrl(['/exercise/add'])?>";
    });
})
</script>