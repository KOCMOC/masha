<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Упражнения: просмотреть ';
$this->params['breadcrumbs'][] = ['label' => 'Упражнения', 'url' => '\admin\exercise'];
$this->params['breadcrumbs'][] = 'Просмотреть упражнение';
?>
<div class="site-index">

    <div class="">
        <h2>Упражнение</h2>
    </div>
    <div class="body-content">
        <table class="table table-striped">
            <tr>
                <td width="150"><b>Название</b></td>
                <td><?=$exercise->title?></td>
            </tr>
            <tr>
                <td><b>Краткое описание</b></td>
                <td><?=$exercise->description_cut?></td>
            </tr>
            <tr>
                <td><b>Полное описание</b></td>
                <td><?=$exercise->description?></td>
            </tr>
            <tr>
                <td><b>Видео</b></td>
                <td><?=$exercise->video?></td>
            </tr>
        </table>
    </div>
</div>