<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Элементы продаж: Редактировать элемент продаж ';
$this->params['breadcrumbs'][] = ['label' => 'Элементы продаж', 'url' => '\admin\sell_item'];
$this->params['breadcrumbs'][] = 'Редактировать элемент продаж';

?>
<div class="site-index">
    <div>
        <h2>Редактировать Элемент продаж</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'edit-sell-item',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ],
    ]);?>
    <?=$form->field($model, 'id')->hiddenInput(['value' => $sell_item_id])->label('');?>
    <div class="row col-md-10"><?=$form->field($model, 'title')->textInput();?></div>
	<div class="row col-md-10"><?=$form->field($model, 'course_group_id')->dropDownList($groups, ['prompt'=>'Выберите группу курса']);?></div>
	<div class="row col-md-10"><?=$form->field($model, 'type')->dropDownList($types, ['prompt'=>'Выберите тип аккаунта']);?></div>
    <div class="row col-md-10"><?=$form->field($model, 'amount')->textInput();?></div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'trigger')->textArea(['rows' => '6']);?>
    </div>
    <div class="row col-sm-10">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
    </div>
    <?php ActiveForm::end();?>
</div>