<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Элементы продажи';
$this->params['breadcrumbs'][] = 'Элементы продажи';
?>
<div class="site-index">
    <div class="">
        <h2>Элементы продажи&nbsp;(<?=$count_items;?>)</h2>
    </div>
    <div><?= Yii::$app->session->getFlash('error'); ?></div>
    <div class="btn-group">
        <button id="add" type="button" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новый элемент продажи</button>
    </div>
    <?if(sizeof($items) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Название элемента продажи</th>
                    <th>Группа курсов</th>
                    <th>Тип аккаунта</th>
                    <th>Цена</th>
                    <th>Создан</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($items as $item):?>
                <tr>
                    <th scope="row"><?=$item->id;?></th>
                    <td>
                    	<a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/sell_item/edit/$item->id"]);?>' class="btn btn-success btn-sm" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                    <td><?=$item->title?></td>
                    <td><?=$item->getCourseGroup()->title ?></td>
                    <td><?=$types[$item->type] ?></td>
                    <td><?=$item->amount ?></td>
                    <td><?=date('d.m.Y', $item->created_at) ?></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
    <?else:?>
    Нет элементов продажи
    <?endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').click(function(){
        location.href = "<?=\Yii::$app->urlManager->createAbsoluteUrl(['/sell_item/add'])?>";
    });
})
</script>