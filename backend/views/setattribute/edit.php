<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Атрибуты упражнений: Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'Атрибуты упражнений', 'url' => '\admin\setattribute'];
$this->params['breadcrumbs'][] = 'Новый атрибут';

?>
<div class="site-index">
    <div>
        <h2>Редактировать курс</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'edit-setattribute',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ]
    ]);?>
    <?=$form->field($model, 'id')->hiddenInput(['value' => $setattribute_id])->label('');?>
    <div class="row col-md-10"><?=$form->field($model, 'name')->textInput();?></div>
    <div class="row col-md-10"><?=$form->field($model, 'unit')->textInput();?></div>
</div>
<div class="row col-sm-10">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
</div>
<?php ActiveForm::end();?>
</div>
