<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Атрибуты упражнений: Новый';
$this->params['breadcrumbs'][] = ['label' => 'Атрибуты упражнений', 'url' => '\admin\setattribute'];
$this->params['breadcrumbs'][] = 'Новый атрибут';

?>
<div class="site-index">
    <div>
        <h2>Новый атрибут</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'add-setattribute',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ]
    ]);?>
    <div class="row col-md-10"><?=$form->field($model, 'name')->textInput();?></div>
    <div class="row col-md-10"><?=$form->field($model, 'unit')->textInput();?></div>
</div>
<div class="row col-sm-10">
    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
</div>
<?php ActiveForm::end();?>
</div>
