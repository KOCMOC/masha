<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Атрибуты упражнений ';
$this->params['breadcrumbs'][] = 'Атрибуты упражнений';
?>
<div class="site-index">

    <div class="">
        <h2>Атрибуты упражнений&nbsp;(<?=$count_setattributes;?>)</h2>
    </div>
    <?if(sizeof($setattributes) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <div class="btn-group">
            <button id="add" type="button" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новый атрибут</button>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Наименование атрибута</th>
                    <th>Единицы</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($setattributes as $set):?>
                <tr>
                    <th scope="row"><?=$set->id;?></th>
                    <td><a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/setattribute/edit/$set->id"]);?>' class="btn btn-success btn-sm" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><?=$set->name?></td>
                    <td><?=$set->unit?></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
    <?else:?>
    Нет атрибутов
    <?endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').click(function(){
        location.href = "<?=\Yii::$app->urlManager->createAbsoluteUrl(['/setattribute/add'])?>";
    });
})
</script>