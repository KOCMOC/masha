<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Питание';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {copy} {delete}',
                'buttons' => [
				    'copy' => function ($url, $model, $key) {
				        return Html::a('<span class="glyphicon glyphicon-plus-sign"></span>', ['copy', 'id'=>$model->id]);
				    },
				],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['style' => 'width: 10%'],
            ],
        ],
    ]); ?>
</div>
