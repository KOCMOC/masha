<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\ArrayHelper;
use kartik\sidenav\SideNav;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
        NavBar::begin([
            'brandLabel' => 'FitChampions',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        $menuItems = [];
       /* $menuItems = [
            ['label' => 'Главная', 'url' => ['/site/index']],
        ];*/
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
        } else {
            $menuItems = ArrayHelper::merge($menuItems, [[
                'label' => 'Выйти (' . Yii::$app->user->identity->email . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']
            ],]);
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems
        ]);
        NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>

        <div class="row">
            <?php if(!Yii::$app->user->isGuest): ?>
            <div class="col-lg-3">
                <?php

                echo SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'heading' => 'Основное меню',
                    'items' => [
                        ['label' => 'Пользователи', 'icon' => 'user', 'url' => ['/user']],
                        ['label' => 'Заказы', 'icon' => 'shopping-cart', 'url' => ['/order']],
                        ['label' => 'Тикеты', 'icon' => 'comment', 'url' => ['/ticket']],
                        ['label' => 'Новости', 'icon' => 'list', 'url' => ['/news']],
                        ['label' => 'Вебинары', 'icon' => 'list', 'url' => ['/webinar']],
                        ['label' => 'Питание', 'icon' => 'file', 'url' => ['/diet']],
                        ['label' => 'Статические страницы', 'icon' => 'file', 'url' => ['/page']],
                        ['label' => 'F.A.Q.', 'icon' => 'comment', 'url' => ['/faq']],
                        ['label' => 'Группы курсов', 'icon' => 'folder-close', 'url' => ['/course_group']],
                        ['label' => 'Курсы', 'icon' => 'list-alt', 'url' => ['/course']],
                        /*['label' => 'Тренировки', 'url' => ['/workout']],*/
                        ['label' => 'Упражнения', 'icon' => 'list', 'url' => ['/exercise']],
                        ['label' => 'Атрибуты', 'icon' => 'th-list', 'url' => ['/setattribute']],
                        ['label' => 'Элементы продажи', 'icon' => 'list', 'url' => ['/sell_item']],
                        ['label' => 'Подписчики', 'icon' => 'envelope', 'url' => ['/delivery']],
                        ['label' => 'Уровни курсов', 'icon' => 'list', 'url' => ['/course_level']],

                    ],
                ]);

                echo SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'heading' => 'Служебное меню',
                    'items' => [
                        ['label' => 'Роли', 'icon' => 'user', 'url' => ['/role']],
                        ['label' => 'Деплой', 'icon' => 'download', 'url' => ['/deploy']],
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-9">
            <?php else: ?>
            <div class="col-lg-12">
            <?php endif; ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; FitChampions <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
