<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'question')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'elfinder/connector',
        ],
        'language' => 'ru'
    ]) ?>

    <?= $form->field($model, 'answer')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'elfinder/connector',
        ],
        'language' => 'ru'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
