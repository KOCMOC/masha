<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Результат выполнения';
$this->params['breadcrumbs'][] = ['label' => 'Деплой', 'url' => ['deploy/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<pre style="background-color: #000000; color: #FFFFFF; padding: 10px;">
 .  ____  .    ____________________________
 |/      \|   |                            |
[| <span style="color: #FF0000;">&hearts;    &hearts;</span> |]  | Git Deployment Script v0.1 |
 |___==___|  /              &copy; oodavid 2012 |
              |____________________________|

<?php echo $output; ?>
</pre>