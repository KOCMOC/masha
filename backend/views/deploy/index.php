<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Деплой';
$this->params['breadcrumbs'][] = $this->title;

?>

<p><?= Html::a('Запустить деплой', ['deploy/run'], ['class' => 'btn btn-primary']) ?></p>
<p>Деплой выполнит следующие комманды на сервере:</p>
<pre style="background-color: #000000; color: #FFFFFF; padding: 10px;">
<?php
foreach ($commands as $command) {
    echo $command . "\n";
}
?>
</pre>