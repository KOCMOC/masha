<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'FitChampions: cms - Подписчики';
$this->params['breadcrumbs'][] = 'Подписчики';
?>
<div class="site-index">
    <div class="">
        <h2>Подписчики&nbsp;(<?=$count_subscribers;?>)</h2>
        <p><?= Html::a('Экспортировать в Mailchimp', ['export'], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => 'Запустить экспорт списка?',
                    'method' => 'post',
                ]
            ]); ?></p>
    </div>
    <div><?= Yii::$app->session->getFlash('error'); ?></div>
    <?if(sizeof($subscribers) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ФИО</th>
                    <th>E-mail</th>
                    <th>Телефон</th>
                    <th>Когда подписался</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($subscribers as $subscriber):?>
                <tr>
                    <th scope="row"><?=$subscriber['id'];?></th>
                    <td><?=$subscriber['fio']?></td>
                    <td><?=$subscriber['email']?></td>
                    <td><?=$subscriber['phone']?></td>
                    <td><?=date('d.m.Y H:i:s', $subscriber['created_at']) ?></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
    <?else:?>
    Нет подписчиков
    <?endif;?>
</div>