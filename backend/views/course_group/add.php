<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Группы курсов: Новая группа ';
$this->params['breadcrumbs'][] = ['label' => 'Группы курсов', 'url' => '\admin\course_group'];
$this->params['breadcrumbs'][] = 'Новая группа';

?>
<div class="site-index">
    <div>
        <h2>Новая группа курсов</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'add-course-group',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ],
    ]);?>
    <div class="row col-md-10"><?=$form->field($model, 'title')->textInput();?></div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'description')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
        'language' => 'ru'
    ]);?>
    </div>
    </div>
    <div class="row col-sm-10">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
    </div>
    <?php ActiveForm::end();?>
</div>
