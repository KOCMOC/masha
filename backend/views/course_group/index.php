<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'FitChampions: cms - Группы курсов';
$this->params['breadcrumbs'][] = 'Группы курсов';
?>
<div class="site-index">
    <div class="">
        <h2>Группы курсов&nbsp;(<?=$count_courses;?>)</h2>
    </div>
    <div><?= Yii::$app->session->getFlash('error'); ?></div>
    <div class="btn-group">
        <button id="add" type="button" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span>&nbsp;Новая группа курсов</button>
    </div>
    <?if(sizeof($courses) > 0):?>
    <div class="body-content">
        <div>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Название группы курсов</th>
                    <th>Описание группы курсов</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($courses as $course):?>
                <tr>
                    <th scope="row"><?=$course['id'];?></th>
                    <td>
                    	<a href='<?php echo \Yii::$app->urlManager->createAbsoluteUrl(["/course_group/edit/$course->id"]);?>' class="btn btn-success btn-sm" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                    <td><?=$course['title']?></td>
                    <td><?=$course['description']?></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div>
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
    </div>
    <?else:?>
    Нет групп курсов
    <?endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').click(function(){
        location.href = "<?=\Yii::$app->urlManager->createAbsoluteUrl(['/course_group/add'])?>";
    });
})
</script>