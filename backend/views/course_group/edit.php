<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use yii\widgets\Breadcrumbs;

$this->title = 'FitChampions: cms - Курсы: Редактировать группу курсов ';
$this->params['breadcrumbs'][] = ['label' => 'Группы курсов', 'url' => '\admin\course_group'];
$this->params['breadcrumbs'][] = 'Редактировать группу курсов';

?>
<div class="site-index">
    <div>
        <h2>Редактировать группу курсов</h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'edit-course-group',
        'options' => [
            'class' => 'form-horizontal col-lg-11',
            'role' => 'form',
        ],
    ]);?>
    <?=$form->field($model, 'id')->hiddenInput(['value' => $course_group_id])->label('');?>
    <div class="row col-md-10"><?=$form->field($model, 'title')->textInput();?></div>
    <div class="row col-md-10">
    <?php echo $form->field($model, 'description')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
        'language' => 'ru'
    ]);?>
    </div>
    </div>
    <div class="row col-sm-10">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'delivery-button']) ?>
    </div>
    <?php ActiveForm::end();?>
</div>