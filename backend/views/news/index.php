<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'title',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a($data->title, \Yii::$app->urlManager->createAbsoluteUrl(["news/update", 'id' => $data->id]));
                }
            ],
            'short_text:ntext',
            'date',
            [
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a('<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>', ['delete', 'id' => $data->id], [
                        'class' => 'text-danger',
                        'data' => [
                            'confirm' => 'Вы уверены, что хотите удалить эту новость?',
                            'method' => 'post',
                        ]
                    ]);
                },
                'contentOptions' => ['class' => 'text-center'],
            ]
        ],
    ]); ?>
</div>
