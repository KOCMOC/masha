<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'fio',
            [
                'attribute' => 'email',
                'format' => 'html',
                'value' => function($data) {
                    return Html::a($data->email, \Yii::$app->urlManager->createAbsoluteUrl(["/user/update/$data->id"]));
                }
            ],
            'phone',
            // 'born',
            // 'growth',
            [
                'label' => 'Статус',
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($data) {
                    switch($data->status){
                        case (1):
                            return '<div class="btn btn-xs btn-primary">Активный</div>'.
                                (($data->is_vip) ? ' <div class="btn btn-xs btn-info">VIP</div>' : '')
                                .'<small><a href="#" class="send-reg-data pull-right" data-user-id="' . $data->id . '">Выслать рег данные</a></small>';
                            break;
                        case (0):
                            return '<div class="btn btn-xs btn-success">Новый</div>'.
                                (($data->is_vip) ? ' <div class="btn btn-xs btn-info">VIP</div>' : '')
                                .'<small>' . Html::a('Выслать рег данные', \Yii::$app->urlManager->createAbsoluteUrl(['/user/resend', 'id' => $data->id]), ['class' => 'text-xs pull-right']) . '</small>';
                        case (2):
                            return '<div class="btn btn-xs btn-danger">Удален</div>'.
                                (($data->is_vip) ? ' <div class="btn btn-xs btn-info">VIP</div>' : '');


                    }
                },
                'contentOptions' => ['class' => 'text-center'],
            ],
            /*[
                'label' => 'Дата регистрации',
                'attribute' => 'created_at',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'creation_datetime',
                    'convertFormat' => true,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'format' => 'yyyy-MM-dd',
                    ],
                ]),
            ],*/
            [
                'label' => 'Дата регистрации',
                'attribute' => 'created_at',
                'format' => 'html',
                'value' => function($data) {
                    return date('d.m.Y H:i:s', $data->created_at);
                }
            ],
            // 'updated_at',
            // 'role',
            // 'is_trainer',
            // 'trainer_id',
            [
                'label' => 'Действия',
                'format' => 'html',
                'value' => function($data){
                    return Html::a('Анкета', \Yii::$app->urlManager->createAbsoluteUrl(['/user/anket', 'user_id' => $data->id])).'&nbsp;'.Html::a('Заказы', \Yii::$app->urlManager->createAbsoluteUrl(['/user/orders', 'user_id' => $data->id])/*Html::a('Расписание', \Yii::$app->urlManager->createAbsoluteUrl(['/schedule', 'user_id' => $data->id])*/);
                }
            ],
            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php
$url = Url::toRoute(['resend']);
$script = <<< JS
    $(document).ready(function(){
		$('.send-reg-data').click(function(){
		    var user_id = $(this).data('user-id');
		    alert(user_id);
			$.ajax({
	            method: 'POST',
	            url: '$url',
	            data: { 'id': user_id },
	            dataType: 'json',
	            success: function(result) {
	                if(result && result.status && result.status == 'ok') {
                    	alert('Успешно отправлено');
	                } else {
                        alert('Не удалось отправить: ' + result.error);
	                }
	            },
	            error: function() {
                	alert('Не удалось отправить - ошибка сервера');
	            }
	        });

			return false;
		});
	});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>