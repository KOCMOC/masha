<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\UserRole;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->label('ФИО')->textInput() ?>
    <?= $form->field($model, 'email')->label('Email')->textInput() ?>
    <?= $form->field($model, 'phone')->label('Номер телефона')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->label('Роль')->dropDownList([
        User::ROLE_ADMIN => 'Администрация',
        User::ROLE_KURATOR => 'Куратор',
        User::ROLE_USER => 'Клиент',
    ]) ?>

    <?= $form->field($model, 'trainer_id')->label('Привязон к тренеру')->dropDownList(
        ArrayHelper::map(User::find()->where(['tickets_role' => '2'])->all(), 'id', 'fio'),
        [
        'prompt' => ''
    ]) ?>

    <?= $form->field($model, 'is_vip')->checkbox(['label' => 'VIP клиент']) ?>

    <?= $form->field($model, 'tickets_role')->label('Тикет-роль')->dropDownList(
        ArrayHelper::map(UserRole::find()->all(), 'id', 'name'),
        [
        'prompt' => ''
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
