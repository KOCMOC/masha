<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="user-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <!--<p>
        <?/*= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'fio',
                'email:email',
                'phone',
                // 'born',
                // 'growth',
                /*[
                    'label' => 'Дата регистрации',
                    'attribute' => 'created_at',
                    'filter' => \kartik\date\DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'creation_datetime',
                        'convertFormat' => true,
                        'removeButton' => false,
                        'pluginOptions' => [
                            'format' => 'yyyy-MM-dd',
                        ],
                    ]),
                ],*/
                [
                    'label' => 'Дата регистрации',
                    'attribute' => 'created_at',
                    'format' => 'html',
                    'value' => function($data) {
                        return date('d.m.Y H:i:s', $data->created_at);
                    }
                ],
                // 'updated_at',
                // 'role',
                // 'is_trainer',
                // 'trainer_id',
                [
                    'label' => 'Действия',
                    'format' => 'html',
                    'value' => function($data){
                        return Html::a('Расписание', \Yii::$app->urlManager->createAbsoluteUrl(['/schedule', 'user_id' => $data->id]));
                    }
                ],
                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>