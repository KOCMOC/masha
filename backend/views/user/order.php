<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$user = $model->getUser();

$this->title = 'Заказ: '.$model->getCourseGroup()->title.' для ' . $user->email;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Заказы '.$user->email, 'url' => ['orders', ['user_id' => $user->id, 'order_id'=>$model->id]]];

$schs = [
	0 => 'Вс',
	1 => 'Пн',
	2 => 'Вт',
	3 => 'Ср',
	4 => 'Чт',
	5 => 'Пт',
	6 => 'Сб'
];

?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <div>Дата старта: <input type="text" name="start" value="<?=($model->datestart > 0)?date('d.m.Y', $model->datestart):'Не запланирована' ?>"?></div>
    <div>Дата окончания: <?=date('d.m.Y', $model->expire) ?></div>
    <div>Дни тренировок: <?=str_replace(array_keys($schs), $schs, $model->schedule_template)?></div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div><input type="button" id="freeze" value="Заморозить"> на <select id="days">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    </select> дней</div>


	<script type="text/javascript">
		$('#freeze').click(function(){
            $.ajax({
                method: 'POST',
                url: '/admin/user/freezeorder',
                data: {'id': <?=$model->id ?>, 'days': $('#days').val() },
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                        window.location.href=window.location.href;
                    } else {
                        console.log(result.error);
                    }
                },
                error: function() {
                    console.log('Не удалось заморозить курс');
                }
            });
		});
	</script>

	<?/*<div class="user-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'fio')->label('ФИО')->textInput() ?>
	    <?= $form->field($model, 'born')->label('Дата рождения')->textInput() ?>
	    <?= $form->field($model, 'country')->label('Страна')->textInput() ?>
	    <?= $form->field($model, 'city')->label('Город')->textInput() ?>
	    <?= $form->field($model, 'insta')->label('Аккаунт в Instagram')->textInput() ?>
	    <?= $form->field($model, 'vk')->label('Аккаунт в ВК')->textInput() ?>
	    <?= $form->field($model, 'gender')->checkbox(['label' => 'Женщина']) ?>
	    <?= $form->field($model, 'growth')->label('Рост')->textInput() ?>
	    <?= $form->field($model, 'weight')->label('Вес')->textInput() ?>
	    <?= $form->field($model, 'breast_size')->label('Объём груди')->textInput() ?>
	    <?= $form->field($model, 'waist_size')->label('Обхват талии')->textInput() ?>
	    <?= $form->field($model, 'hip_size')->label('Обхват бёдер')->textInput() ?>
	    <?= $form->field($model, 'goal')->label('Цель')->dropDownList([
	        0 => 'Похудеть',
	        1 => 'Набрать вес',
	        2 => 'Рельеф',
	    ]) ?>
		<?= $form->field($model, 'vegan')->checkbox(['label' => 'Вегетарианец']) ?>
	    <?= $form->field($model, 'mother')->checkbox(['label' => 'Кормящая мать']) ?>
	    <?= $form->field($model, 'kal')->label('Калории')->textInput() ?>
	    <?= $form->field($model, 'day')->label('День')->dropDownList([
	        0, 1, 2, 3, 4, 5, 6, 7
	    ]) ?>
	    <?= $form->field($model, 'week')->label('Неделя')->dropDownList([
	        0, 1, 2, 3, 4
	    ]) ?>
	    <div><?=$diet->title ?></div>
	    <?= $form->field($model, 'status')->checkbox(['label' => 'Активна']) ?>

	    <div class="form-group">
	        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>*/?>

</div>
