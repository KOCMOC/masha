<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Заказы: ' . $user->email;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Заказы '.$user->email, 'url' => ['orders', 'user_id' => $user->id]];

?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <table>
    <?php foreach($orders as $order): ?>
    	<tr><td><?=Html::a($order->getCourseGroup()->title, \Yii::$app->urlManager->createAbsoluteUrl(['/user/order', 'user_id' => $user->id, 'order_id' => $order->id])) ?></td></tr>
    <?php endforeach; ?>
    </table>

	<?/*<div class="user-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'fio')->label('ФИО')->textInput() ?>
	    <?= $form->field($model, 'born')->label('Дата рождения')->textInput() ?>
	    <?= $form->field($model, 'country')->label('Страна')->textInput() ?>
	    <?= $form->field($model, 'city')->label('Город')->textInput() ?>
	    <?= $form->field($model, 'insta')->label('Аккаунт в Instagram')->textInput() ?>
	    <?= $form->field($model, 'vk')->label('Аккаунт в ВК')->textInput() ?>
	    <?= $form->field($model, 'gender')->checkbox(['label' => 'Женщина']) ?>
	    <?= $form->field($model, 'growth')->label('Рост')->textInput() ?>
	    <?= $form->field($model, 'weight')->label('Вес')->textInput() ?>
	    <?= $form->field($model, 'breast_size')->label('Объём груди')->textInput() ?>
	    <?= $form->field($model, 'waist_size')->label('Обхват талии')->textInput() ?>
	    <?= $form->field($model, 'hip_size')->label('Обхват бёдер')->textInput() ?>
	    <?= $form->field($model, 'goal')->label('Цель')->dropDownList([
	        0 => 'Похудеть',
	        1 => 'Набрать вес',
	        2 => 'Рельеф',
	    ]) ?>
		<?= $form->field($model, 'vegan')->checkbox(['label' => 'Вегетарианец']) ?>
	    <?= $form->field($model, 'mother')->checkbox(['label' => 'Кормящая мать']) ?>
	    <?= $form->field($model, 'kal')->label('Калории')->textInput() ?>
	    <?= $form->field($model, 'day')->label('День')->dropDownList([
	        0, 1, 2, 3, 4, 5, 6, 7
	    ]) ?>
	    <?= $form->field($model, 'week')->label('Неделя')->dropDownList([
	        0, 1, 2, 3, 4
	    ]) ?>
	    <div><?=$diet->title ?></div>
	    <?= $form->field($model, 'status')->checkbox(['label' => 'Активна']) ?>

	    <div class="form-group">
	        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>*/?>

</div>
