<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-lg-8">
            <?php /*echo $form->field($model, 'role')
                ->label('Роль')
                ->dropDownList([
                User::ROLE_ADMIN => 'Администратор',
                User::ROLE_KURATOR => 'Тренер',
                User::ROLE_USER => 'Клиент',
            ],
            [
                'prompt' => ''
            ]) */?>
            <?php echo $form->field($model, 'trainer_id')
                ->label('Тренер')
                ->dropDownList(ArrayHelper::map(User::find()->where(['tickets_role' => 2])->all(), 'id', 'fio'),[
                    'prompt' => ''
                ]) ?>
        </div>
        <div class="col-lg-2">
            <?php /*echo $form->field($model, 'is_trainer')->checkbox() */?>
            <?php echo $form->field($model, 'is_vip')->checkbox(['label' => 'VIP клиенты']) ?>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
