<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Webinar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="webinar-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <div class="row">
        <div class="col-lg-8">

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'full_text')->widget(TinyMce::className(), [
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'elfinder/connector',
        ],
        'language' => 'ru'
    ]) ?>

    <?= $form->field($model, 'video_content')->widget(TinyMce::className(), [
        'settings'=> [
            'menubar' => false,
            'toolbar' => 'media code preview',
            'plugins' => ['media', 'code', 'preview'],
            'toolbar_items_size' => 'large'
        ],
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'elfinder/connector',
        ],
        'language' => 'ru',
    ]); ?>

        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'date')->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'value' => $model->isNewRecord ? date("Y-m-d") : $model->date,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>

            <?= $form->field($model, 'preview')->fileInput() ?>

            <?php if(!empty($model->preview)): ?>
                <p><img src="<?= $model->preview ?>" class="img-thumbnail img-responsive"></p>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
