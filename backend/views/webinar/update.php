<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Webinar */

$this->title = 'Обновить вебинар: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Вебинары', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="webinar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
