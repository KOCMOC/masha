<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SetAttributeForm extends Model
{
    public $id;
    public $name;
    public $unit;

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'unit'], 'filter', 'filter' => 'trim'],
            [['name', 'unit'], 'string', 'length' => [2, 255]],
            [['id'], 'number','min'=>1],
            ['name', 'is_unique_add', 'on' => 'add'],
            ['name', 'is_unique_edit', 'on' => 'edit'],
            ['name', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название атрибута',
            'unit' => 'Единица измерения'
        ];
    }

    public function is_unique_add($attribute, $param)
    {
        /*$res = \common\models\Set_attribute::find()
                ->where(['name'=>$this->name])
                ->all();
        if(sizeof($res) > 0) {
            $this->addError( $attribute, 'Такое название уже есть' );
        }      */
    }

    public function is_unique_edit($attribute, $param=array())
    {
        /*$res = \common\models\Set_attribute::find()
                ->where('name = :name and id != :id', ['name'=>$this->name, 'id'=>$this->id])
                ->all();
        if(sizeof($res) > 0) {
            $this->addError( $attribute, 'Название "'.$this->name.'" уже есть' );
        }*/
    }

    public function add()
    {
        $setattribute = new \common\models\Set_attribute();
        $setattribute->name = $this->name;
        $setattribute->unit = $this->unit;
        return $setattribute->save();
    }

    public function edit($id)
    {
        $setattribute = \common\models\Set_attribute::findOne($id);
        if($setattribute) {
            $setattribute->name = $this->name;
            $setattribute->unit = $this->unit;
            return $setattribute->save();
        }
        return false;
    }
}
