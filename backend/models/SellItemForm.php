<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SellItemForm extends Model
{
    public $id;
    public $title;
    public $course_group_id;
    public $type;
    public $amount;
    public $trigger;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_group_id', 'type', 'title', 'amount'], 'required'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }],
            ['amount', 'number', 'integerOnly' => false],
            ['course_group_id', 'number', 'integerOnly' => true],
            ['trigger', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название элемента продажи',
            'course_group_id' => 'ID группы курсов',
            'type' => 'Тип аккаунта',
            'amount' => 'Цена',
            'trigger' => 'Триггер'
        ];
    }

    public function add()
    {
        $item = new \common\models\Sell_item();
        $item->title = $this->title;
        $item->course_group_id = $this->course_group_id;
        $item->type = $this->type;
        $item->amount = $this->amount;
        $item->trigger = $this->trigger;
        return $item->save();
    }

    public function edit($id)
    {
        $item = \common\models\Sell_item::findOne($id);
        if($item) {
	        $item->title = $this->title;
	        $item->course_group_id = $this->course_group_id;
	        $item->type = $this->type;
	        $item->amount = $this->amount;
	        $item->trigger = $this->trigger;
	        return $item->save();
        }
        return false;
    }
}
