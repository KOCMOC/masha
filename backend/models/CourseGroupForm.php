<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CourseGroupForm extends Model
{
    public $id;
    public $title;
    public $description;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','description'], 'required'],
            [['title', 'description'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }],
            ['description', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название группы курсов',
            'description' => 'Описание группы курсов'
        ];
    }

    public function add()
    {
        $course_group = new \common\models\Course_group();
        $course_group->title = $this->title;
        $course_group->description = $this->description;
        return $course_group->save();
    }

    public function edit($id)
    {
        $course_group = \common\models\Course_group::findOne($id);
        if($course_group) {
            $course_group->title = $this->title;
            $course_group->description = $this->description;
            return $course_group->save();
        }
        return false;
    }
}
