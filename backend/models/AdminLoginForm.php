<?php
namespace backend\models;

use common\models\User;
use common\models\LoginFormBase;
use yii\helpers\ArrayHelper;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class AdminLoginForm extends LoginFormBase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['email', 'validateUser'],
        ]);
    }

    public function validateUser($attribute, $params)
    {
        $user = $this->getUser();
        if (!$this->hasErrors()) {
            if ($user->status == User::STATUS_DELETED) {
                $this->addError($attribute, 'Ваш аккаунт был удален');
            }
        } 
        if (!$this->hasErrors()) {
            if ($user->role != User::ROLE_ADMIN) {
                $this->addError($attribute, 'Вход только администраторам');
            }
        }
    }
}
