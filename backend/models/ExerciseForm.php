<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ExerciseForm extends Model
{
    public $id;
    public $title;
    public $id_tab;
    public $id_related;
    public $description_cut;
    public $video;
    public $description;
    public $sets;
    public $del_sets;
    public $old_sets;
    public $ex_fields;
    public $ex_names;

    public function rules()
    {
        return [
            [['title','description_cut', 'video', 'description'], 'required'],
            [['title', 'description_cut', 'video', 'description'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['description_cut', 'string', 'length' => [2, 512]],
            [['id', 'id_tab'], 'number','min'=>1],
            ['id_related', 'number', 'min'=>0],
            ['id_related', 'default', 'value'=>0],
            ['title', 'is_unique_add', 'on' => 'add'],
            ['title', 'is_unique_edit', 'on' => 'edit'],
            ['sets', 'required', 'on' => 'add'],
            ['sets', 'is_correct_sets', 'on' => 'add'],
            [['sets', 'old_sets'], 'is_correct_sets', 'on' => 'edit'],
            ['del_sets', 'string', 'on' => 'edit'],
            [['ex_fields','ex_names'], 'is_correct_array']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название упражнения',
            'id_tab' => 'Категория',
            'id_related' => 'Связаное упражнение',
            'description' => 'Полное описание выполнения упражнения',
            'video' => 'Демонстрационное видео',
            'description_cut' => 'Краткое описание упражнения'
        ];
    }

    public function is_unique_add($attribute)
    {
        $res = \common\models\Exercise::find()
                ->where(['title'=>$this->title, 'id_tab'=>$this->id_tab])
                ->all();
        if(sizeof($res) > 0) {
            $this->addError( $attribute, 'Такое название упражнения уже есть' );
        }
    }

    public function is_unique_edit($attribute)
    {
        $res = \common\models\Exercise::find()
                ->where('title = :title and id != :id and id_tab = :id_tab', ['title'=>$this->title, 'id'=>$this->id, 'id_tab'=>$this->id_tab])
                ->all();
        if(sizeof($res) > 0) {
            $this->addError( $attribute, 'Название упражнения "'.$this->title.'" уже есть' );
        }
    }

    public function is_correct_sets($attribute)
    {
    	if(is_array($this->$attribute) and sizeof($this->$attribute) > 0) {    		foreach($this->$attribute as $attr) if($attr) {		    	$json = json_decode($attr);
		    	if(
		    		$json and
		    		is_object($json) and
		    		property_exists($json, 'sleep') and
		    		property_exists($json, 'sets') and
		    		is_array($json->sets) and
		    		sizeof($json->sets) > 0
		    	) {		    	} else  {		    		$this->addError( $attribute, 'Неверно составлены подходы' );		    	}
	    	}
    	} else  {    		$this->addError( $attribute, 'Неверно составлены подходы' );    	}    }

    public function is_correct_array($attribute) {    	if(!is_array($this->$attribute)) {        	$this->addError( $attribute, 'Не массив' );    	}    }

    public function add()
    {
		$transaction = Yii::$app->db->beginTransaction();

        $exercise = new \common\models\Exercise();
        $exercise->title = $this->title;
        $exercise->id_tab = $this->id_tab;
  		$exercise->id_related = $this->id_related;
        $exercise->description_cut = $this->description_cut;
        $exercise->video = $this->video;
        $exercise->description = $this->description;
        if($exercise->save()) {
        	$exercise->refresh();

			if(sizeof($this->ex_fields) > 0)
           	foreach($this->ex_fields as $ex_key=>$ex_field) {
       			$nField = new \common\models\Exercise_fields();
       			$nField->id_exercise = $exercise->id;
       			$nField->title = $this->ex_names[$ex_key];
       			$nField->value = $ex_field;
       			if(!$nField->save()) {
         			$transaction->rollback();
 					return false;
       			}
           	}

        	$set_cntr = 1;
        	foreach($this->sets as $set) {        		$set = json_decode($set);        		$tr = new \common\models\Training_routine();
        		$tr->id_exercise = $exercise->id;
        		$tr->set = $set_cntr;
        		$tr->sleep = $set->sleep;
        		$tr->alert_proc = $set->alert_proc;
        		$tr->alert_sleep = $set->alert_sleep;
        		$set_cntr++;
        		if($tr->save()) {
        			$tr->refresh();        			$attrs = $set->sets;
        			foreach($attrs as $attr) {        				$at = new \common\models\Set();
        				$at->id_set_attr = $attr->id;
        				$at->default_val = $attr->default_val;
        				$at->trigger = $attr->trigger;
        				$at->id_tr_rout = $tr->id;        				if(!$at->save()) {        					$transaction->rollback();
        					return false;        				}
        			}        		} else {
        			$transaction->rollback();        			return false;        		}        	}        } else {
        	$transaction->rollback();			return false;        }
		$transaction->commit();
		return true;
    }

    public function edit($id)
    {

    	$exercise = \common\models\Exercise::findOne($id);
    	$dels = explode(',', $this->del_sets);
    	$olds = [];
    	if(sizeof($this->old_sets) > 0)
    	foreach($this->old_sets as $oset) if($oset) {    		$oset = json_decode($oset);
    		$olds[$oset->id] = $oset;    	}

    	$transaction = Yii::$app->db->beginTransaction();

        $exercise = \common\models\Exercise::findOne($id);
        if($exercise) {
            $exercise->title = $this->title;
            $exercise->id_tab = $this->id_tab;
            $exercise->id_related = $this->id_related;
            $exercise->description_cut = $this->description_cut;
            $exercise->video = $this->video;
            $exercise->description = $this->description;
            if($exercise->save()) {
            	$eFields = $exercise->getFields()->all();
            	if(sizeof($this->ex_fields) > 0)
            	foreach($eFields as $eField) {            		if(array_key_exists('ex'.$eField->id, $this->ex_fields)) {            			$eField->title = $this->ex_names['ex'.$eField->id];
            			$eField->value = $this->ex_fields['ex'.$eField->id];
            			if(!$eField->save()) {        					$transaction->rollback();
        					return false;            			}            		} else {            			if(!$eField->delete()) {
        					$transaction->rollback();
        					return false;
            			}            		}            	}
            	if(sizeof($this->ex_fields) > 0)
            	foreach($this->ex_fields as $ex_key=>$ex_field) {            		if(substr($ex_key, 0, 2) != 'ex') {               			$nField = new \common\models\Exercise_fields();
               			$nField->id_exercise = $exercise->id;
               			$nField->title = $this->ex_names[$ex_key];
               			$nField->value = $ex_field;
               			if(!$nField->save()) {	               			$transaction->rollback();
        					return false;               			}            		}            	}
		    	$eSets = $exercise->getTrainingRoutines()->all();
		    	$set_cntr = 1;
		    	foreach($eSets as $eSet) {
		        	if(in_array($eSet->id, $dels)) {
		        		$eSet->del();
		        	} else {		        		if(array_key_exists($eSet->id, $olds)) {			        		$sAttrs = $eSet->getSets();
			        		$oSets = $olds[$eSet->id]->sets;
			        		foreach($sAttrs as $sAttr) {
			        			$toDel = true;
			        			foreach($oSets as $oKey=>$oSet) if($oSet !== false) {			        				if($oSet->id == $sAttr->id_set_attr) {			        					$sAttr->default_val = $oSet->default_val;
			        					$sAttr->trigger = $oSet->trigger;
				        				if(!$sAttr->save()) {
				        					$transaction->rollback();
				        					return false;
				        				}
			        					$toDel = false;
			        					$oSets[$oKey] = false;
			        					break;			        				}			        			}
			        			if($toDel === true) {			        				if(!$sAttr->delete()) {
			        					$transaction->rollback();
			        					return false;
			        				}			        			}
					    	}
					    	foreach($oSets as $oSet) if($oSet !== false) {					    		$oat = new \common\models\Set();
                                $oat->id_set_attr = $oSet->id;
                                $oat->id_tr_rout = $eSet->id;
                                $oat->default_val = $oSet->default_val;
                                $oat->trigger = $oSet->trigger;
		        				if(!$oat->save()) {
		        					$transaction->rollback();
		        					return false;
		        				}					    	}
					    	$sSet = $olds[$eSet->id];
	         				$eSet->sleep = $sSet->sleep;
	         				$eSet->alert_proc = $sSet->alert_proc;
	         				$eSet->alert_sleep = $sSet->alert_sleep;
			 			}
			 			$eSet->set = $set_cntr;
			 			if(!$eSet->save()) {
		        			$transaction->rollback();
        					return false;
		        		}
		        		$set_cntr++;
		        	}
		    	}
	            if(sizeof($this->sets) > 0)
	            foreach($this->sets as $set) {
	        		$set = json_decode($set);
	        		$tr = new \common\models\Training_routine();
	        		$tr->id_exercise = $exercise->id;
	        		$tr->set = $set_cntr;
	        		$tr->sleep = $set->sleep;
	        		$tr->alert_proc = $set->alert_proc;
	        		$tr->alert_sleep = $set->alert_sleep;
	        		$set_cntr++;
	        		if($tr->save()) {
	        			$tr->refresh();
	        			$attrs = $set->sets;
	        			foreach($attrs as $attr) {
	        				$at = new \common\models\Set();
	        				$at->id_set_attr = $attr->id;
	        				$at->default_val = $attr->default_val;
	        				$at->trigger = $attr->trigger;
	        				$at->id_tr_rout = $tr->id;
	        				if(!$at->save()) {
	        					$transaction->rollback();
	        					return false;
	        				}
	        			}
	        		} else {
	        			$transaction->rollback();
	        			return false;
	        		}
	        	}            } else {            	$transaction->rollback();
				return false;            }
        } else {        	$transaction->rollback();
			return false;        }
        $transaction->commit();
        return true;
    }
}
