<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CourseForm extends Model
{
    public $id;
    public $course_group_id;
    public $title;
    public $description;
    public $level;
    public $gender;
    public $place;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_group_id', 'level', 'gender', 'title', 'description'], 'required'],
            [['course_group_id', 'level', 'gender', 'place'],  'integer', 'min' => 1, 'message'=>'Не выбрано {attribute}'],
            [['title', 'description'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }],
            ['description', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return $value;
                }
            }]
        ];
    }

    public function attributeLabels()
    {
        return [
        	'course_group_id' => 'ID группы курсов',
            'title' => 'Название курса',
            'description' => 'Описание курса',
            'level' => 'Уровень',
            'gender' => 'Пол',
            'Место тренировки'
        ];
    }

    public function add()
    {
        $course = new \common\models\Course();
        $course->course_group_id = $this->course_group_id;
        $course->title = $this->title;
        $course->description = $this->description;
        $course->level = $this->level;
        $course->gender = $this->gender;
        $course->place = $this->place;
        return $course->save();
    }

    public function edit($id)
    {
        $course = \common\models\Course::findOne($id);
        if($course) {
        	$course->course_group_id = $this->course_group_id;
            $course->title = $this->title;
            $course->description = $this->description;
	        $course->level = $this->level;
	        $course->gender = $this->gender;
	        $course->place = $this->place;
            return $course->save();
        }
        return false;
    }
}
