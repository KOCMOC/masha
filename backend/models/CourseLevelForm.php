<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CourseLevelForm extends Model
{
    public $id;
    public $title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'filter', 'filter' => 'trim'],
            ['title', 'string', 'length' => [2, 255]],
            ['title', 'filter', 'filter' => function ($value) {
                if($value == '') {
                    return '';
                } else {
                    return strip_tags($value);
                }
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название уровня курсов',
        ];
    }

    public function add()
    {
        $course_level = new \common\models\Course_level();
        $course_level->title = $this->title;
        return $course_level->save();
    }

    public function edit($id)
    {
        $course_level = \common\models\Course_level::findOne($id);
        if($course_level) {
            $course_level->title = $this->title;
            return $course_level->save();
        }
        return false;
    }
}
