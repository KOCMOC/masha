<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SetattributeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }
   
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $setattributes = \common\models\Set_attribute::find();
        $count_setattributes = clone $setattributes;
        $pages = new Pagination(['totalCount' => $count_setattributes->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;
        $setattributes = $setattributes->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'setattributes' => $setattributes,
            'pages' => $pages,
            'count_setattributes' => $count_setattributes->count(),
        ]);
     }

    public function actionAdd()
    {
        $model = new \backend\models\SetAttributeForm();
        $model->scenario = 'add';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->add()){
                $this->redirect('index');
            }
        }
        return $this->render('add', ['model'=>$model]);
    }

    public function actionEdit()
    {
    	$id = Yii::$app->request->get('setattribute_id');
        if($id > 0) {
            $model = new \backend\models\SetAttributeForm();
            $model->scenario = 'edit';
            if($model->load(Yii::$app->request->post()) && $model->validate()) {
                if($model->edit(Yii::$app->request->post('SetAttributeForm')['id'])){
                    $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl(["/setattribute"]));
                }
            }
            if($setattribute = \common\models\Set_attribute::findOne($id)) {
                $model->attributes = $setattribute->attributes;
            } else {
                return $this->render('/site/error', [
                    'message' => 'Такой атрубут не существует! Может кто-то форму подменил?!',
                    'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/setattribute"])
                ]);
            }
            return $this->render('edit', ['model'=>$model, 'setattribute_id'=>$id]);
        } else {
            return $this->render('/site/error', [
                'message' => 'Id атрибута не передан - редактирование невозможно!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/setattribute"])
            ]);
        }
    }
}
