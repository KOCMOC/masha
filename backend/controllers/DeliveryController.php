<?php
namespace backend\controllers;

use sammaye\mailchimp\Mailchimp;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\Pagination;
use common\models\Delivery;


/**
 * Site controller
 */
class DeliveryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $subscribers = Delivery::find();
        $count_subscribers = clone $subscribers;
        $pages = new Pagination(['totalCount' => $count_subscribers->count(), 'pageSize' => 50]);
        $pages->pageSizeParam = false;
        $subscribers = $subscribers->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->asArray()
            ->all();

        return $this->render('index', [
            'subscribers' => $subscribers,
            'pages' => $pages,
            'count_subscribers' => $count_subscribers->count(),
        ]);
    }

    public function actionExport()
    {
        $result = [];

        $mc = new Mailchimp(['apikey' => Yii::$app->params['mailchimp']['apiKey']]);
        $mcList = new \Mailchimp_Lists($mc->mailChimp);

        $subscribers = Delivery::find()->all();
        foreach ($subscribers as $subscriber) {

            try {
                $data = [
                    'email' => $subscriber->email,
                    'firstname' => $subscriber->fio,
                ];

                // TODO Включить когда скажут
                $subscriberResult = true; //$mcList->subscribe(Yii::$app->params['mailchimp']['listId'], $data);

            } catch (\Exception $e) {
                $subscriberResult = false;
            }

            if($subscriberResult!== false)
                $result[] = $subscriber->email;
        }

        return $this->render('export', [
            'result' => $result,
        ]);
    }
}
