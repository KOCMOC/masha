<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\Pagination;


/**
 * Site controller
 */
class Sell_itemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $items = \common\models\Sell_item::find();
        $count_items = clone $items;
        $pages = new Pagination(['totalCount' => $count_items->count(), 'pageSize' => 50]);
        $pages->pageSizeParam = false;
        $items = $items->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $types = \common\models\Sell_item::getTypes();

        return $this->render('index', [
            'items' => $items,
            'pages' => $pages,
            'count_items' => $count_items->count(),
            'types' => $types
        ]);
     }

    public function actionAdd()
    {
        $model = new \backend\models\SellItemForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->add()){
                $this->redirect('index');
            }
        }
        $groups = \common\models\Course_group::find()->asArray()->all();
        $groupsArray = [];
        if(sizeof($groups) > 0) {
        	foreach($groups as $group){
				$groupsArray[$group['id']] = $group['title'];
        	}
        }
        $types = \common\models\Sell_item::getTypes();
        return $this->render('add', ['model'=>$model, 'groups'=>$groupsArray, 'types'=>$types]);
    }

    public function actionEdit()
    {
    	$id = Yii::$app->request->get('sell_item_id');
        $model = new \backend\models\SellItemForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->edit(Yii::$app->request->post('SellItemForm')['id'])){
                $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl(["/sell_item"]));
            }
        }
        if($course = \common\models\Sell_item::findOne($id)) {
            $model->attributes = $course->attributes;
        } else {
            return $this->render('/site/error', [
                'message' => 'Id элемента продаж не передан - редактирование невозможно!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/sell_item"])
            ]);
        }
        $groups = \common\models\Course_group::find()->asArray()->all();
        $groupsArray = [];
        if(sizeof($groups) > 0) {
        	foreach($groups as $group){
				$groupsArray[$group['id']] = $group['title'];
        	}
        }
        $types = \common\models\Sell_item::getTypes();
        return $this->render('edit', ['model'=>$model, 'sell_item_id'=>$id, 'types'=>$types, 'groups'=>$groupsArray]);
    }

}
