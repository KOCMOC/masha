<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Ticket;
use yii\web\Controller;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class DeployController extends Controller
{
    /**
     * The commands
     * @var array
     */
    public $commands = [
        'echo $PWD',
        'whoami',
        'git reset --hard', // против переписывающегося composer.lock
        'git pull https://KOCMOC:Pi9nHm1f8d@bitbucket.org/KOCMOC/fitchampions.git',
        'git status',
        'git submodule sync',
        'git submodule update',
        'git submodule status',
    ];

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    
    public function actionIndex()
    {
        return $this->render('index', [
            'commands' => $this->commands
        ]);
    }

    /**
     * @return string
     */
    public function actionRun()
    {

        // Run the commands for output
        $output = '';
        foreach($this->commands AS $command){
            // Run it
            $tmp = shell_exec($command);
            // Output
            $output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
            $output .= htmlentities(trim($tmp)) . "\n";
        }

        return $this->render('deploy', [
            'output' => $output,
        ]);
    }
}
