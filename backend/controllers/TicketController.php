<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use common\models\Ticket;
use common\models\TicketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\TicketMessage;
use yii\web\UploadedFile;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->identity->role == User::ROLE_KURATOR) {
            $this->layout = 'main_trainer';
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->identity->role == User::ROLE_KURATOR) {
            return $this->render('kurator_index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Ticket model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if($model->creation_datetime == $model->read_datetime) {
            $model->read_datetime = date("Y-m-d H:i:s");
            $model->status = Ticket::STATUS_VIEWERD;
            $model->save();
        }

        if($model->status == Ticket::STATUS_NEW_ANSWER) {
            $model->status = Ticket::STATUS_NEW_ANSWER_VIEWED;
            $model->save();
        }

        if($model->status == Ticket::STATUS_NEW) {
            $model->read_datetime = date("Y-m-d H:i:s");
            $model->status = Ticket::STATUS_VIEWERD;
            $model->save();
        }


        $formModel = new TicketMessage();

        if ($formModel->load(Yii::$app->request->post())) {
            $formModel->ticket_id = $id;
            $formModel->user_id = Yii::$app->user->identity->getId();
            $formModel->creation_datetime = date("Y-m-d H:i:s");
            $formModel->read_datetime = date("Y-m-d H:i:s");

            $formModel->attachmentFiles = UploadedFile::getInstances($formModel, 'attachmentFiles');

            if ($formModel->save()) {
                $model->status = Ticket::STATUS_ANSWERED;
                $model->save();

                $formModel->attachment = $formModel->upload();
                if($formModel->save()) {	                $mailer = Yii::$app->get('mail');
			        $mailer->compose()
			            ->setTo($model->getUser()->one()->email)
			            ->setFrom([Yii::$app->params['adminEmail']=>Yii::$app->params['adminEmail']])
			            ->setSubject('Новый ответ на сайте '.Yii::$app->params['siteData']['name'])
			            ->setHtmlBody($this->renderPartial('/ticket/mail', ['siteData'=>Yii::$app->params['siteData'], 'ticket'=>$model, 'message'=>$formModel->message]))
			            ->send();                }
                $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('view', [
            'model' => $model,
            'formModel' => $formModel,
        ]);
    }

    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionStatus($id)
    {
        $ticket = $this->findModel($id);
        $ticket->status = ($ticket->status == Ticket::STATUS_CLOSED) ? Ticket::STATUS_NEW : Ticket::STATUS_CLOSED;
        $ticket->save();

        return $this->redirect(['view', 'id' => $ticket->id]);
    }

    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
