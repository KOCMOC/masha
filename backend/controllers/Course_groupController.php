<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Response;


/**
 * Site controller
 */
class Course_groupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $courses = \common\models\Course_group::find();
        $count_courses = clone $courses;
        $pages = new Pagination(['totalCount' => $count_courses->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;
        $courses = $courses->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'courses' => $courses,
            'pages' => $pages,
            'count_courses' => $count_courses->count(),
        ]);
     }

    public function actionAdd()
    {
        $model = new \backend\models\CourseGroupForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->add()){
                $this->redirect('index');
            }
        }
        return $this->render('add', ['model'=>$model]);
    }

    public function actionEdit()
    {
    	$id = Yii::$app->request->get('course_group_id');
        $model = new \backend\models\CourseGroupForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->edit(Yii::$app->request->post('CourseGroupForm')['id'])){
                $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl(["/course_group"]));
            }
        }
        if($course = \common\models\Course_group::findOne($id)) {
            $model->attributes = $course->attributes;
        } else {
            return $this->render('/site/error', [
                'message' => 'Id группы курсов не передан - редактирование невозможно!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/course_group"])
            ]);
        }
        return $this->render('edit', ['model'=>$model, 'course_group_id'=>$id]);
    }

}
