<?php

namespace backend\controllers;

use common\models\Ticket;
use common\models\TicketSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TrainerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->layout = 'main_trainer';
        return parent::beforeAction($action);
    }

    /**
     * Trainer cabinet main page
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    /**
     * Clients page
     */
    public function actionClients()
    {
        return $this->render('clients', []);
    }
}