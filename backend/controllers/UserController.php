<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->identity->role == User::ROLE_KURATOR) {
            $this->layout = 'main_trainer';
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->identity->role == User::ROLE_KURATOR) {
            return $this->render('index_trainer', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionResend()
    {
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = array('status'=>'error', 'error'=>'');
            $user_id = Yii::$app->request->post('id');

            $user = User::findOne($user_id);
            if($user and $user->status == User::STATUS_ACTIVE) {
                $password = $user->generatePassword();
                $user->setPassword($password);
                $user->save();
                $mailer = Yii::$app->get('mail');
                $mailer->compose()
                    ->setTo($user->email)
                    ->setFrom([Yii::$app->params['adminEmail']=>Yii::$app->params['adminEmail']])
                    ->setSubject('Регистрация на проекте '.Yii::$app->params['siteData']['name'])
                    ->setHtmlBody($this->renderPartial('@frontend/views/mail/registration', ['promoCode'=>false, 'siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$password]))
                    ->send();
                $response['status'] = 'ok';
            } else {
                $response['error'] = 'Пользователь не найден';
            }
            return $response;
        }
    }

    public function actionAnket() {		$user_id = Yii::$app->request->get('user_id');

		$saved = '';
        $model = \common\models\UserAnket::find()->where(['user_id'=>$user_id])->one();
        if(is_array(Yii::$app->request->post()) and sizeof(Yii::$app->request->post()) > 0) {
        	//print_R(Yii::$app->request->post()); die();
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	            $saved = 'Изменения успешно сохранены';
	        } else {
	        	$saved = 'Не удалось сохранить изменения';
	        }
        }

        $diet = \common\models\Diet::find()->where(['<', 'min', $model->kal])->andWhere(['>', 'max', $model->kal])->andWhere(['vegan'=>$model->vegan])->andWhere(['like', 'url', '%_'.$model->week.'_'.$model->day, false])->one();

  		return $this->render('anket', [
            'model' => $model,
            'saved' => $saved,
            'diet' => $diet
        ]);    }

    public function actionOrders() {
		$user_id = Yii::$app->request->get('user_id');

		$user = User::findOne($user_id);
        $orders = \common\models\Order::find()->where(['user_id'=>$user_id])->all();

  		return $this->render('orders', [
            'orders' => $orders,
            'user' => $user
        ]);
    }

    public function actionOrder() {
		$user_id = Yii::$app->request->get('user_id');
		$order_id = Yii::$app->request->get('order_id');

		$model = \common\models\Order::findOne($order_id);

		if(is_array(Yii::$app->request->post()) and sizeof(Yii::$app->request->post()) > 0) {			$start = Yii::$app->request->post('start');
			$start = strtotime($start);
			$model->datestart = $start;
			$model->save();
		}

  		return $this->render('order', [
            'model' => $model
        ]);
    }

    public function actionFreezeorder() {
    	if(Yii::$app->request->isAjax) {    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');
			$id = Yii::$app->request->post('id');
			$days = Yii::$app->request->post('days');

			$model = \common\models\Order::findOne($id);

			if($model->schedule_template == '') {				$model->datestart = $model->datestart + 86400 * $days;
				$model->expire = $model->expire + 86400 * $days;
				if($model->save()) {
					$response['status'] = 'ok';
				}			} elseif($model->active == 1) {				//$model->datestart = date('d.m.Y', $model->datestart + 86400 * $days);
				$model->expire = $model->expire + 86400 * $days;
				if($model->save()) {
					$response['status'] = 'ok';
				}

				//сюда внести чтобы трени перенеслись			}


	  		return $response;
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
