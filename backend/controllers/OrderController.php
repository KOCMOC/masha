<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\web\Response;
use common\models\Order;


/**
 * Site controller
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($user_id = false)
    {
        $role = 3;
        $orders = Order::find();
        $count_orders = clone $orders;
        $pages = new Pagination(['totalCount' => $count_orders->count(), 'pageSize' => 50]);
        $pages->pageSizeParam = false;
        $orders = $orders->orderby(['created_at' => SORT_DESC])
        	->with('courseGroupRelation')
        	->with('userRelation')
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->asArray()
            ->all();

        return $this->render('index', [
            'orders' => $orders,
            'pages' => $pages,
            'count_orders' => $count_orders->count()
        ]);
    }

    public function actionChange_active() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$order_id = Yii::$app->request->post('order_id');
    		$active = Yii::$app->request->post('active');

    		$response['order_id'] = $order_id;

			$order = Order::findOne($order_id);
    		if($order) {
    			if($order->active == 0 and $order->expire == 0) {                	$transaction = \common\models\Finance_transaction::find()->where(['subject_type'=>\common\models\Finance_transaction::Subject_order, 'subject_id'=>$order->id])->one();    			    if($transaction->status == 0) {    			    	$transaction->commit(true);
    				}
    			}
    			$order->active = ($active == 'false')?0:1;

                if(!$order->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось изменить активность заказа';
                }
                /*$order->active = ($active == 'true')?1:0;
                if(!$order->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось изменить активность заказа';
                }*/
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти заказ';
    		}

    		return $response;
    	}
    }

    public function actionDelete()
    {
    	$id = Yii::$app->request->get('order_id');
    	if($order = \common\models\Order::findOne($id)) {
        	$order->del();
    	}
        return $this->redirect('\admin\order');
    }
}
