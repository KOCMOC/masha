<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;


/**
 * Site controller
 */
class Course_levelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $levels = \common\models\Course_level::find()->all();

        return $this->render('index', [
            'levels' => $levels
        ]);
     }

    public function actionAdd()
    {
        $model = new \backend\models\CourseLevelForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->add()){
                $this->redirect('index');
            }
        }
        return $this->render('add', ['model'=>$model]);
    }

    public function actionEdit()
    {
    	$id = Yii::$app->request->get('course_level_id');
        $model = new \backend\models\CourseLevelForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->edit(Yii::$app->request->post('CourseLevelForm')['id'])){
                $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl(["/course_level"]));
            }
        }
        if($course = \common\models\Course_level::findOne($id)) {
            $model->attributes = $course->attributes;
        } else {
            return $this->render('/site/error', [
                'message' => 'Id уровня курсов не передан - редактирование невозможно!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/course_level"])
            ]);
        }
        return $this->render('edit', ['model'=>$model, 'course_level_id'=>$id]);
    }

}
