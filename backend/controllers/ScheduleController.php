<?php
namespace backend\controllers;

use common\models\User;
use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Response;


/**
 * Site controller
 */
class ScheduleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->identity->role == User::ROLE_KURATOR) {
            $this->layout = 'main_trainer';
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
    	$user_id = Yii::$app->request->get('user_id');

        $orders = \common\models\Order::find()->where(['user_id'=>$user_id]);
        $count_orders = clone $orders;
        $pages = new Pagination(['totalCount' => $count_orders->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;
        $orders = $orders->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'orders' => $orders,
            'pages' => $pages,
            'count_orders' => $count_orders->count(),
        ]);
     }

    public function actionView()
    {
    	$order_id = Yii::$app->request->get('order_id');
        if($order = \common\models\Order::findOne($order_id)) {
        	$exercises = \common\models\Exercise::find()->orderBy('title')->all();
        	$attributesList = \common\models\Set_attribute::find()->all();
        	$anketFieldsTypes = \common\models\Course_anket::getTypesList();
        	$reportFieldsTypes = \common\models\Workout_report_field::getTypesList();
        	$course = \common\models\Course::find()->where(['gender'=>$order->gender, 'level'=>$order->level, 'course_group_id'=>$order->course_group_id])->one();
            $schedules = \common\models\Schedule::find()->where(['order_id'=>$order->id])->all();
            return $this->render('view', ['schedules' => $schedules, 'order' => $order, 'course' => $course, 'exercises' => $exercises, 'attributesList' => $attributesList, 'anketFieldsTypes' => $anketFieldsTypes, 'reportFieldsTypes' => $reportFieldsTypes]);
        } else {
            return $this->render('/site/error', [
                'message' => 'Такой курс не существует! Может кто-то форму подменил?!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/course"])
            ]);
        }
        return $this->render('view', ['course' => $course]);
    }

    public function actionAdd_workout()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_id = Yii::$app->request->post('course_id');
    		$creation  = Yii::$app->request->post('creation');
    		$list = Yii::$app->request->post('list');

			if($creation) {
	    		$nworkout = new \common\models\Workout();
	    		$nworkout->course_id = $course_id;
	    		$nworkout->title = 'Новая тренировка';
	    		if($nworkout->save()) {
	    			$nworkout->refresh();
	    			$response['workout_id'] = $nworkout->id;
	    			$response['workout_title'] = $nworkout->title;
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось создать тренировку';
	    		}
    		}

    		if($response['status'] == 'ok') {
    			$count = 1;
    			\common\models\Workout::updateAll(['number'=>0], ['course_id'=>$course_id]);
    			if(sizeof($list) > 0) {
    				foreach($list as $wid) if($wid) {
    					$workout = \common\models\Workout::find()->where(['id'=>$wid])->one();
    					$workout->number = $count;
    					$count++;
		    			if(!$workout->save()) {
			    			$response['status'] = 'error';
			    			$response['error'] = 'Не удалось сохранить список тренировок';
			    		}
		    		}
	    		}
	    		if($creation) {
	    			$nworkout->number = $count;
	    			$nworkout->save();
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionAdd_exercise()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$list = Yii::$app->request->post('list');
    		$response['rand'] = Yii::$app->request->post('rand');

    		$response['workout_id'] = $workout_id;
    		$response['exercise_id'] = $exercise_id;

    		if($exercise_id) {
    			$exercise = \common\models\Exercise::findOne($exercise_id);
    			if($exercise) {
    				$transaction = Yii::$app->db->beginTransaction();

	    			$workout_exercise = new \common\models\Workout_exercise();
	                $workout_exercise->workout_id = $workout_id;
	                $workout_exercise->exercise_id = $exercise_id;
	                $workout_exercise->type = \common\models\Workout_exercise::Exercise;
	                if($workout_exercise->save()) {
	                	$workout_exercise->refresh();
	                	$response['relation_id'] = $workout_exercise->id;
	                	$response['type'] = \common\models\Course_training_routine::WorkoutRelation;
	                	$routines = $exercise->getTrainingRoutines('set')->all();
	                	if(sizeof($routines) > 0)
	                	foreach($routines as $routine) {
			    			$course_training_routine = new \common\models\Course_training_routine();
			                $course_training_routine->id_relation = $workout_exercise->id;
			                $course_training_routine->type = \common\models\Course_training_routine::WorkoutRelation;
			                $course_training_routine->set = $routine->set;
			                $course_training_routine->sleep = $routine->sleep;
			                if($course_training_routine->save()) {
			                	$course_training_routine->refresh();
	                        	$sets = $routine->getSets();
	                        	if(sizeof($sets) > 0)
	                        	foreach($sets as $set) {
                                	$course_set = new \common\models\Course_set();
                                	$course_set->id_set_attr = $set->id_set_attr;
                                	$course_set->id_c_tr_rout = $course_training_routine->id;
                                	$course_set->default_val = $set->default_val;
                                	if(!$course_set->save()) {
						    			$response['status'] = 'error';
						    			$response['error'] = 'Не удалось добавить упражнение';
						    			$transaction->rollback();
                                	}
	                        	}
                        	} else {
				    			$response['status'] = 'error';
				    			$response['error'] = 'Не удалось добавить упражнение';
				    			$transaction->rollback();
                        	}
	                	}
	                } else {
		    			$response['status'] = 'error';
		    			$response['error'] = 'Не удалось добавить упражнение';
		    			$transaction->rollback();
	                }

	                $transaction->commit();
                } else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось найти  упражнение';
                }
    		}

    		if($response['status'] == 'ok') {
            	\common\models\Workout_exercise::updateAll(['number'=>0], ['workout_id'=>$workout_id]);
            	$count = 1;
    			if(sizeof($list) > 0) {
    				foreach($list as $exid) if(is_array($exid) and isset($exid['id']) and $exid['id'] > 0) {
    					$workout_exercise = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id, 'exercise_id'=>$exid['id'], 'type'=>$exid['type'], 'number'=>0])->one();
    					if($workout_exercise) {
	    					$workout_exercise->number = $count;
	    					$count++;
			    			if(!$workout_exercise->save()) {
				    			$response['status'] = 'error';
				    			$response['error'] = 'Не удалось сохранить список упражнений';
				    		}
			    		}
		    		}
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionDel_exercise() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$number = Yii::$app->request->post('number');
    		$number++;

    		$type = ($exercise_id)?\common\models\Workout_exercise::Exercise:\common\models\Workout_exercise::Set;

    		$workout_exercise = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id, 'exercise_id'=>$exercise_id, 'number'=>$number, 'type'=>$type])->one();
    		if($workout_exercise) {
    			if(!$workout_exercise->del()) {
	    			$response['status'] = 'error';
	                $response['error'] = 'Не удалось удалить упражнение/сет';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти упражнение/сет';
    		}

			if($response['status'] == 'ok') {
    			$workout_exercises = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id])->orderBy('number')->all();
    			$count = 1;
    			foreach($workout_exercises as $wex) {
    				$wex->number = $count;
    				$wex->save();
                	$count++;
    			}
    		}

    		return $response;
    	}
    }

    public function actionAdd_set_exercise()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$list = Yii::$app->request->post('list');
    		$response['rand'] = Yii::$app->request->post('rand');

    		$response['set_id'] = $set_id;
    		$response['exercise_id'] = $exercise_id;

    		if($exercise_id) {
    			$exercise = \common\models\Exercise::findOne($exercise_id);
    			if($exercise) {
	    			$transaction = Yii::$app->db->beginTransaction();

	    			$workout_exercise = new \common\models\Set_exercise();
	                $workout_exercise->set_id = $set_id;
	                $workout_exercise->exercise_id = $exercise_id;
	                if($workout_exercise->save()) {
	                    $workout_exercise->refresh();
	                	$response['relation_id'] = $workout_exercise->id;
	                	$response['type'] = \common\models\Course_training_routine::SetRelation;

		    			$course_training_routine = new \common\models\Course_training_routine();
		                $course_training_routine->id_relation = $workout_exercise->id;
		                $course_training_routine->type = \common\models\Course_training_routine::SetRelation;
		                $course_training_routine->set = 1;
		                $course_training_routine->sleep = 0;

		                if($course_training_routine->save()) {
		                	$course_training_routine->refresh();

		                	$routine = $exercise->getTrainingRoutines('set')->one();
		                	$sets = ($routine)?$routine->getSets():[];
                        	if(sizeof($sets) > 0)
                        	foreach($sets as $set) {
                               	$course_set = new \common\models\Course_set();
                               	$course_set->id_set_attr = $set->id_set_attr;
                               	$course_set->id_c_tr_rout = $course_training_routine->id;
                               	$course_set->default_val = $set->default_val;
                               	if(!$course_set->save()) {
					    			$response['status'] = 'error';
					    			$response['error'] = 'Не удалось добавить упражнение в сет';
					    			$transaction->rollback();
                               	}
	                        }
                       	} else {
			    			$response['status'] = 'error';
			    			$response['error'] = 'Не удалось добавить упражнение в сет';
			    			$transaction->rollback();
                       	}
	                } else {
	                    $transaction->rollback();
		    			$response['status'] = 'error';
		    			$response['error'] = 'Не удалось добавить упражнение в сет';
	                }

	                $transaction->commit();
                } else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось найти  упражнение';
                }
    		}

    		if($response['status'] == 'ok') {
            	\common\models\Set_exercise::updateAll(['number'=>0], ['set_id'=>$set_id]);
    			if(sizeof($list) > 0) {
    				$count = 1;
    				foreach($list as $exid) if(is_array($exid) and isset($exid['id']) and $exid['id'] > 0) {
    					$set_exercise = \common\models\Set_exercise::find()->where(['set_id'=>$set_id, 'exercise_id'=>$exid['id'], 'number'=>0])->one();
    					$set_exercise->number = $count;
    					$count++;
		    			if(!$set_exercise->save()) {
			    			$response['status'] = 'error';
			    			$response['error'] = 'Не удалось сохранить список упражнений в сете';
			    		}
		    		}
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionDel_set_exercise() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$number = Yii::$app->request->post('number');
    		$number++;

    		$set_exercise = \common\models\Set_exercise::find()->where(['set_id'=>$set_id, 'exercise_id'=>$exercise_id, 'number'=>$number])->one();
    		if($set_exercise) {
    			if(!$set_exercise->delete()) {
	    			$response['status'] = 'error';
	                $response['error'] = 'Не удалось удалить упражнение из сета';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти упражнение из сета';
    		}

			if($response['status'] == 'ok') {
    			$set_exercises = \common\models\Set_exercise::find()->where(['set_id'=>$set_id])->orderBy('number')->all();
    			$count = 1;
    			foreach($set_exercises as $sex) {
    				$sex->number = $count;
    				$sex->save();
                	$count++;
    			}
    		}

    		return $response;
    	}
    }

    public function actionDel_workout() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');

    		$workout = \common\models\Workout::find()->where(['id'=>$workout_id])->one();

    		if($workout) {
    			$transaction = Yii::$app->db->beginTransaction();
    			$workout_exercises = $workout->getWorkoutExercises()->all();
				foreach($workout_exercises as $workout_exercise) {
   					if($workout_exercise->type == \common\models\Workout_exercise::Set) {
   						$set = $workout_exercise->getSet();
                        $set_exercises = $set->getExercises()->all();
                        foreach($set_exercises as $set_exercise) {
                        	if(!$set_exercise->delete()) {
				    			$response['status'] = 'error';
				                $response['error'] = 'Не удалось удалить тренировку';
				                $transaction->rollback();
				                return $response;
                        	}
                        }
                        if(!$set->delete()) {
			    			$response['status'] = 'error';
			                $response['error'] = 'Не удалось удалить тренировку';
                        	$transaction->rollback();
                        	return $response;
                        }
   					}
					if(!$workout_exercise->delete()) {
						$response['status'] = 'error';
						$response['error'] = 'Не удалось удалить тренировку';
						$transaction->rollback();
						return $response;
					}
   				}
				if(!$workout->delete()) {
					$response['status'] = 'error';
					$response['error'] = 'Не удалось удалить тренировку';
					$transaction->rollback();
					return $response;
				}
    			$transaction->commit();
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти тренировку';
    		}

    		return $response;
    	}
    }

    public function actionDel_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');

    		$set = \common\models\Workout_set::find()->where(['id'=>$set_id])->one();
    		if($set) {
    			$transaction = Yii::$app->db->beginTransaction();
				$set_exercises = $set->getExercises()->all();
				$workout_exercise = $set->getWorkout_exercise();
				foreach($set_exercises as $set_exercise) {
					if(!$set_exercise->delete()) {
						$response['status'] = 'error';
						$response['error'] = 'Не удалось удалить сет';
						$transaction->rollback();
						return $response;
					}
				}
				if(!$workout_exercise->delete()) {
					$response['status'] = 'error';
					$response['error'] = 'Не удалось удалить сет';
					$transaction->rollback();
					return $response;
				}
				if(!$set->delete()) {
					$response['status'] = 'error';
					$response['error'] = 'Не удалось удалить сет';
					$transaction->rollback();
					return $response;
				}
    			$transaction->commit();
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти сет';
    		}

    		return $response;
    	}
    }

    public function actionEdit_workout() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$title = Yii::$app->request->post('title');

    		$workout = \common\models\Workout::find()->where(['id'=>$workout_id])->one();

    		if($workout) {
    			$old_title = $workout->title;
    			$workout->title = $title;
    			if(!$workout->save()) {
	    			$response['status'] = 'error';
	    			$response['title'] = $old_title;
	                $response['error'] = 'Не удалось сохоранить тренировку';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти тренировку';
    		}

    		return $response;
    	}
    }

    public function actionAdd_set()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$creation  = Yii::$app->request->post('creation');
    		$list = Yii::$app->request->post('list');

			if($creation) {
	    		$set = new \common\models\Workout_set();
	    		$set->title = 'Новый сет';
	    		$set->sleep = 0;
	    		$set->alert_proc = '';
	    		$set->alert_sleep = '';
	    		if($set->save()) {
	    			$set->refresh();
	    			$response['set_id'] = $set->id;
	    			$response['set_title'] = $set->title;
	    			$response['workout_id'] = $workout_id;
	    			$set_exercise = new \common\models\Workout_exercise();
	                $set_exercise->workout_id = $workout_id;
	                $set_exercise->exercise_id = $set->id;
	                $set_exercise->type = \common\models\Workout_exercise::Set;
	                if(!$set_exercise->save()) {
		    			$response['status'] = 'error';
		    			$response['error'] = 'Не удалось добавить сет в тренировку';
	                }
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось создать сет';
	    		}
    		}

    		if($response['status'] == 'ok') {
    			$count = 1;
            	\common\models\Workout_exercise::updateAll(['number'=>0], ['workout_id'=>$workout_id]);
    			if(sizeof($list) > 0) {
    				foreach($list as $exid) if(is_array($exid) and isset($exid['id']) and $exid['id'] > 0) {
    					$workout_exercise = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id, 'exercise_id'=>$exid['id'], 'type'=>$exid['type'], 'number'=>0])->one();
    					if($workout_exercise) {
	    					$workout_exercise->number = $count;
	    					$count++;
			    			if(!$workout_exercise->save()) {
				    			$response['status'] = 'error';
				    			$response['error'] = 'Не удалось сохранить список упражнений';
				    		}
			    		}
		    		}
	    		}
	    		if($creation) {
	    			$set_exercise->number = $count;
	    			$set_exercise->save();
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionEdit_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$set = \common\models\Workout_set::find()->where(['id'=>$set_id])->one();

    		if($set) {
    			$old_value = $set->$field;
    			$set->$field = str_replace('"', '&quot;', $value);
    			if(!$set->save()) {
	    			$response['status'] = 'error';
	    			$response['value'] = $old_title;
	    			$response['field'] = $field;
	                $response['error'] = 'Не удалось сохранить сет';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти сет';
    		}

    		return $response;
    	}
    }

    public function actionDel_course_routine() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$routine_id = Yii::$app->request->post('routine_id');

    		$routine = \common\models\Course_training_routine::findOne($routine_id);

    		$relation = $routine->id_relation;
    		$type = $routine->type;

    		if($routine->del()) {
    			$routines = \common\models\Course_training_routine::find()->where(['id_relation'=>$relation, 'type'=>$type])->orderBy('set')->all();
    			if(sizeof($routines) > 0) {
    				$set = 1;
                	foreach($routines as $routine) {
                  		$routine->set = $set;
                  		$routine->save();
                  		$set++;
                	}
    			}
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти сет';
    		}

    		$response['routine_id'] = $routine_id;

    		return $response;
    	}
    }

    public function actionDel_course_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');

    		$set = \common\models\Course_set::findOne($set_id);

    		if(!$set->delete()) {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти параметр';
    		}

    		$response['set_id'] = $set_id;

    		return $response;
    	}
    }

    public function actionAdd_course_routine() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$relation_id = Yii::$app->request->post('relation_id');
    		$type = Yii::$app->request->post('type');

    		$relation = false;
            if($type == \common\models\Course_training_routine::WorkoutRelation) {
    			$relation = \common\models\Workout_exercise::findOne($relation_id);
    		} elseif($type == \common\models\Course_training_routine::SetRelation) {
    			$relation = \common\models\Set_exercise::findOne($relation_id);
    		}

    		if($relation and $relation_id and $type) {
    			$maxSet = $relation->getCourseRoutines()->select(['set'=>'IF(ISNULL(MAX(`set`)), 1, MAX(`set`))'])->one()->toArray();
    			$routine = new \common\models\Course_training_routine();
    			$routine->id_relation = $relation_id;
    			$routine->type = $type;
    			$routine->sleep = 0;
    			$routine->set = $maxSet['set'] + 1;
	    		if($routine->save()) {
	    			$routine->refresh();
	    			$response['id'] = $routine->id;
	    			$response['relation_id'] = $relation_id;
	    			$response['type'] = $type;
	    			$response['set'] = $maxSet['set'] + 1;
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось добавить подход';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось найти связь или недопустимая связь';
    		}

    		return $response;
    	}
    }

    public function actionAdd_course_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$routine_id = Yii::$app->request->post('routine_id');

    		if($routine_id) {
    			$set = new \common\models\Course_set();
    			$set->id_c_tr_rout = $routine_id;
    			$set->id_set_attr = 0;
    			$set->default_val = '';
	    		if($set->save()) {
	    			$set->refresh();
	    			$response['id'] = $set->id;
	    			$response['routine_id'] = $routine_id;
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось добавить параметр';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось добавить параметр';
    		}

    		return $response;
    	}
    }

    public function actionEdit_course_routine() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$routine_id = Yii::$app->request->post('routine_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$routine = \common\models\Course_training_routine::findOne($routine_id);

    		$response['id'] = $routine_id;
    		$response['field'] = $field;
    		$response['old_value'] = $routine->$field;

    		if($routine) {
    			$routine->$field = str_replace('"', '&quot;', $value);
	    		if(!$routine->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить подход';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить подход';
    		}

    		return $response;
    	}
    }

    public function actionEdit_course_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$set = \common\models\Course_set::findOne($set_id);

    		$response['id'] = $set_id;
    		$response['field'] = $field;
    		$response['old_value'] = $set->$field;

    		if($set) {
    			$set->$field = $value;
	    		if(!$set->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить параметр';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить параметр';
    		}

    		return $response;
    	}
    }

    public function actionAdd_workout_report_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');

    		$field = new \common\models\Workout_report_field();
    		$field->workout_id = $workout_id;
    		$field->title = 'Новое поле';
    		$field->type = 0;
    		if($field->save()) {
    			$field->refresh();
	    		$response['workout_id'] = $workout_id;
	    		$response['field_id'] = $field->id;
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось добавить поле отчёта';
    		}

    		return $response;
    	}
    }

    public function actionEdit_workout_report_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_report_field_id = Yii::$app->request->post('workout_report_field_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$workout_report_field = \common\models\Workout_report_field::findOne($workout_report_field_id);

    		$response['id'] = $workout_report_field_id;
    		$response['field'] = $field;

    		if($workout_report_field) {
	    		$response['old_value'] = $workout_report_field->$field;
    			$workout_report_field->$field = $value;
	    		if(!$workout_report_field->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить поле отчёта';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить поле отчёта';
    		}

    		return $response;
    	}
    }

    public function actionDel_workout_report_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_report_field_id = Yii::$app->request->post('workout_report_field_id');

    		$workout_report_field = \common\models\Workout_report_field::findOne($workout_report_field_id);

    		if($workout_report_field) {
    			$response['id'] = $workout_report_field->id;
	    		if(!$workout_report_field->delete()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось удалить поле отчёта';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось удалить поле отчёта';
    		}

    		return $response;
    	}
    }

    public function actionAdd_course_anket_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_id = Yii::$app->request->post('course_id');

    		$field = new \common\models\Course_anket();
    		$field->course_id = $course_id;
    		$field->title = 'Новое поле';
    		$field->type = 0;
    		if($field->save()) {
    			$field->refresh();
	    		$response['field_id'] = $field->id;
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось добавить поле анкеты';
    		}

    		return $response;
    	}
    }

    public function actionEdit_course_anket_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_anket_field_id = Yii::$app->request->post('course_anket_field_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$course_anket_field = \common\models\Course_anket::findOne($course_anket_field_id);

    		$response['id'] = $course_anket_field_id;
    		$response['field'] = $field;

    		if($course_anket_field) {
    			$response['old_value'] = $course_anket_field->$field;
    			$course_anket_field->$field = $value;
	    		if(!$course_anket_field->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить поле анкеты';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить поле анкеты';
    		}

    		return $response;
    	}
    }

    public function actionDel_course_anket_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_anket_field_id = Yii::$app->request->post('course_anket_field_id');

    		$course_anket_field = \common\models\Course_anket::findOne($course_anket_field_id);

    		if($course_anket_field) {
    			$response['id'] = $course_anket_field->id;
	    		if(!$course_anket_field->delete()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось удалить поле анкеты';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось удалить поле анкеты';
    		}

    		return $response;
    	}
    }

}
