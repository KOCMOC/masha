<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\Set_attribute;
use common\models\Global_field;

/**
 * Site controller
 */
class ExerciseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $exercises = \common\models\Exercise::find();

        if($search = Yii::$app->request->get('search')) {        	$exercises->where(['like', 'title', '%'.$search.'%', false]);        }

        $count_exercises = clone $exercises;
        $pages = new Pagination(['totalCount' => $count_exercises->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;
        $exercises = $exercises->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'exercises' => $exercises,
            'pages' => $pages,
            'count_exercises' => $count_exercises->count(),
            'search' => $search
        ]);
    }

    public function actionView()
    {
    	$id = Yii::$app->request->get('exercise_id');
        if($exercise = \common\models\Exercise::findOne($id)) {
            return $this->render('view', ['exercise' => $exercise]);
        } else {
            return $this->render('/site/error', [
                'message' => 'Такое упражнение не существует! Может кто-то форму подменил?!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/exercise"])
            ]);
        }
        return $this->render('view', ['exercise' => $exercise]);
    }

    public function actionAdd()
    {
        $model = new \backend\models\ExerciseForm();
        $model->scenario = 'add';

        $attributes = Set_attribute::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->add()){
                $this->redirect('index');
            }
        } //else {print_R($model->errors);die('asd');}
        $globals = Global_field::find()->all();
        $attributes = Set_attribute::find()->all();
        $tabsArr = array();
        foreach(\common\models\Exercise_tab::find()->all() as $tab) $tabsArr[$tab->id] = $tab->title;
        return $this->render('add', ['model'=>$model, 'attributes'=>$attributes, 'globals'=>$globals, 'tabs'=>$tabsArr]);
    }

    public function actionEdit()
    {
    	$id = Yii::$app->request->get('exercise_id');
        if($id > 0) {
            $model = new \backend\models\ExerciseForm();
            $model->scenario = 'edit';
            if($model->load(Yii::$app->request->post()) && $model->validate()) {
                if($model->edit(Yii::$app->request->post('ExerciseForm')['id'])){
                    //$this->redirect(\Yii::$app->urlManager->createAbsoluteUrl(["/exercise"]));
                }
            } //else {print_R($model->errors);die('chesnok');}
            if($exercise = \common\models\Exercise::findOne($id)) {
                $model->attributes = $exercise->attributes;
                $fields = $exercise->getFields()->all();
                $sets = $exercise->getTrainingRoutines()->all();
            } else {
                return $this->render('/site/error', [
                    'message' => 'Такое упражнение не существует! Может кто-то форму подменил?!',
                    'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/exercise"])
                ]);
            }
            $globals = $exercise->getGlobalFields()->all();
            $attributes = Set_attribute::find()->all();
            $tabsArr = array();
            foreach(\common\models\Exercise_tab::find()->all() as $tab) $tabsArr[$tab->id] = $tab->title;
            return $this->render('edit', ['model'=>$model, 'exercise_id'=>$id, 'attributes'=>$attributes, 'fields'=>$fields, 'sets'=>$sets, 'globals'=>$globals, 'tabs'=>$tabsArr]);
        } else {
            return $this->render('/site/error', [
                'message' => 'Id упражнения не передан - редактирование невозможно!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/exercise"])
            ]);
        }
    }

    public function actionClone()
    {
        $id = Yii::$app->request->get('exercise_id');

    	if($id and $exercise = \common\models\Exercise::findOne($id)) {
        	$cloned_exercise = $exercise->clonCopy();
        	if($cloned_exercise['status'] == 'ok') {                return $this->redirect('\admin\exercise\edit\\'.$cloned_exercise['id']);        	}
    	}
        return $this->redirect('\admin\exercise');
    }

    public function actionDelete()
    {
        $id = Yii::$app->request->get('exercise_id');

    	if($id and $exercise = \common\models\Exercise::findOne($id)) {
        	if(!$exercise->del()) {        		die('Не удалось удалить упражнение');        	}
    	}
        return $this->redirect('\admin\exercise');
    }

    public function actionCourse_routines() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'', 'html'=>'');

			$relation_id = Yii::$app->request->post('relation_id');
    		$type = Yii::$app->request->post('type');
    		$id = Yii::$app->request->post('id');

            $routines = \common\models\Course_training_routine::find()->where(['id_relation'=>$relation_id, 'type'=>$type])->all();
            $attributesList = \common\models\Set_attribute::find()->all();

            $response['html'] = $this->renderPartial('/course/routines', ['routines'=>$routines, 'attributesList'=>$attributesList, 'type'=>$type, 'relation_id'=>$relation_id]);
            $response['id'] = $id;

			return $response;
		}
    }

    public function actionAdd_global_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

    		$id_exercise = Yii::$app->request->post('id_exercise');
    		$title = Yii::$app->request->post('title');
    		$alias = Yii::$app->request->post('alias');
    		$default = Yii::$app->request->post('default');

   			$global = new Global_field;
   			$global->id_exercise = $id_exercise;
   			$global->title = $title;
   			$global->alias = $alias;
   			$global->default = $default;
   			if($global->save()) {
   				$global->refresh();   				$response['status'] = 'ok';
                $response['title'] = $title;
                $response['alias'] = $alias;
                $response['default'] = $default;
                $response['id'] = $global->id;   			} else {                $response['error'] = $global->errors;   			}

			return $response;
		}
    }

}
