<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Response;


/**
 * Site controller
 */
class CourseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $courses = \common\models\Course::find();
        $count_courses = clone $courses;
        $pages = new Pagination(['totalCount' => $count_courses->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;
        $courses = $courses->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'courses' => $courses,
            'pages' => $pages,
            'count_courses' => $count_courses->count(),
        ]);
     }

    public function actionAdd()
    {
        $model = new \backend\models\CourseForm();
        $groups = \common\models\Course_group::find()->asArray()->all();
        $groupsArray = [];
        if(sizeof($groups) > 0) {
        	foreach($groups as $group){
				$groupsArray[$group['id']] = $group['title'];
        	}
        }
        $levels = \common\models\Course_level::find()->asArray()->all();
        $levelsArray = [];
        if(sizeof($levels) > 0) {
        	foreach($levels as $level){
				$levelsArray[$level['id']] = $level['title'];
        	}
        }
        $genders = \common\models\Course::getGenderList();
        $places = \common\models\Course::getPlaceList();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->add()){
                $this->redirect('/admin/course');
            }
        } else {print_R($model->errors); }
        return $this->render('add', ['model'=>$model, 'groups'=>$groupsArray, 'levels'=>$levelsArray, 'genders'=>$genders, 'places'=>$places]);
    }

    public function actionEdit()
    {
    	$id = Yii::$app->request->get('course_id');
        $model = new \backend\models\CourseForm();
        $anket_fields = [];
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->edit(Yii::$app->request->post('CourseForm')['id'])){
                $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl(["/course"]));
            }
        }
        $groups = \common\models\Course_group::find()->asArray()->all();
        $groupsArray = [];
        if(sizeof($groups) > 0) {
        	foreach($groups as $group){
				$groupsArray[$group['id']] = $group['title'];
        	}
        }
        $levels = \common\models\Course_level::find()->asArray()->all();
        $levelsArray = [];
        if(sizeof($levels) > 0) {
        	foreach($levels as $level){
				$levelsArray[$level['id']] = $level['title'];
        	}
        }
        $genders = \common\models\Course::getGenderList();
        $places = \common\models\Course::getPlaceList();
        if($course = \common\models\Course::findOne($id)) {
            $model->attributes = $course->attributes;
            $anket_fields = $course->getAnket();
        } else {
            return $this->render('/site/error', [
                'message' => 'Id курса не передан - редактирование невозможно!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/course"])
            ]);
        }
        $anketFieldsTypes = \common\models\Course_anket::getTypesList();
        return $this->render('edit', ['model'=>$model, 'course_id'=>$id, 'groups'=>$groupsArray, 'levels'=>$levelsArray, 'genders'=>$genders, 'places'=>$places, 'anketFieldsTypes'=>$anketFieldsTypes, 'anket_fields'=>$anket_fields]);
    }

    public function actionDelete()
    {
    	$id = Yii::$app->request->get('course_id');
    	if($course = \common\models\Course::findOne($id)) {
        	$course->del();
    	}
        return $this->redirect('\admin\course');
    }

    public function actionView()
    {
    	$id = Yii::$app->request->get('course_id');
        if($course = \common\models\Course::findOne($id)) {
        	$exercises = \common\models\Exercise::find()->orderBy('title')->all();
        	$attributesList = \common\models\Set_attribute::find()->all();
        	$anketFieldsTypes = \common\models\Course_anket::getTypesList();
        	$reportFieldsTypes = \common\models\Workout_report_field::getTypesList();
        	$globals = \common\models\Global_field::find()->where(['id_course'=>$id])->all();
        	//$course_globals = \common\models\Course_global_field::find()->all();

        	//$course
            return $this->render('view', ['course' => $course, 'exercises' => $exercises, 'attributesList' => $attributesList, 'anketFieldsTypes' => $anketFieldsTypes, 'reportFieldsTypes' => $reportFieldsTypes, 'globals' => $globals/*, 'course_globals' => $course_globals*/]);
        } else {
            return $this->render('/site/error', [
                'message' => 'Такой курс не существует! Может кто-то форму подменил?!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/course"])
            ]);
        }
        return $this->render('view', ['course' => $course]);
    }

    public function actionClone()
    {
    	$id = Yii::$app->request->get('course_id');
    	if($course = \common\models\Course::findOne($id)) {
    		$course->clonCopy();
    	} else {
            return $this->render('/site/error', [
                'message' => 'Такой курс не существует! Может кто-то форму подменил?!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/course"])
            ]);
        }
    	$this->redirect('/admin/course');
    }

    public function actionAdd_global_field()
    {
    	if(Yii::$app->request->isAjax) {
    		/*Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

			$id_course = Yii::$app->request->post('id_course');
    		$id_global_field = Yii::$app->request->post('id_global');
    		$default = Yii::$app->request->post('default');

    		if($global_field = \common\models\Global_field::findOne($id_global_field)) {
    			$global = \common\models\Course_global_field::find()->where(['id_course'=>$id_course, 'id_global_field'=>$id_global_field])->one();
	   			if(!$global) {
		   			$global = new \common\models\Course_global_field;
		   			$global->id_course = $id_course;
		   			$global->id_exercise = $global_field->id_exercise;
		   			$global->id_global_field = $global_field->id;
	   			}
	   			$global->default = $default;
	   			if($global->save()) {
	   				$response['status'] = 'ok';
	   				$response['id'] = $global_field->id;
	   			} else {
	                $response['error'] = $global->errors;
	   			}
   			} else {
   				$response['error'] = 'Не удалось найти глобальную переменную';
   			}

			return $response; */

    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

    		$id_exercise = Yii::$app->request->post('id_exercise');
    		$id_course = Yii::$app->request->post('id_course');
    		$title = Yii::$app->request->post('title');
    		$alias = Yii::$app->request->post('alias');
    		$default = Yii::$app->request->post('default');

   			$global = new \common\models\Global_field;
   			$global->id_exercise = $id_exercise;
   			$global->id_course = $id_course;
   			$global->title = $title;
   			$global->alias = $alias;
   			$global->default = $default;
   			if($global->save()) {
   				$global->refresh();
   				$response['status'] = 'ok';
                $response['title'] = $title;
                $response['alias'] = $alias;
                $response['default'] = $default;
                $response['id'] = $global->id;
   			} else {
                $response['error'] = $global->errors;
   			}

			return $response;
		}
    }

    public function actionDel_global_field()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

    		$id = Yii::$app->request->post('id');

   			$global = \common\models\Global_field::findOne($id);
   			if($global) {
   				if($global->delete()) {
	   				$response['status'] = 'ok';
	                $response['id'] = $id;
	   			} else {
	                $response['error'] = $global->errors;
	   			}
	   		} else {
   				$response['error'] = 'Не удалось найти глобальную переменную';
	   		}

			return $response;
		}
    }

    public function actionUpdate_global_field()
    {
    	if(Yii::$app->request->isAjax) {

    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

    		$id = Yii::$app->request->post('id');
    		$title = Yii::$app->request->post('title');
    		$alias = Yii::$app->request->post('alias');
    		$default = Yii::$app->request->post('default');

   			$global = \common\models\Global_field::findOne($id);
   			if($global) {
	   			$global->title = $title;
	   			$global->alias = $alias;
	   			$global->default = $default;
	   			if($global->save()) {
	   				$global->refresh();
	   				$response['status'] = 'ok';
	                $response['title'] = $title;
	                $response['alias'] = $alias;
	                $response['default'] = $default;
	                $response['id'] = $global->id;
	   			} else {
	                $response['error'] = $global->errors;
	   			}
	   		} else {
   				$response['error'] = 'Не удалось найти глобальную переменную';
	   		}

			return $response;
		}
    }

    public function actionAdd_workout()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_id = Yii::$app->request->post('course_id');
    		$creation  = Yii::$app->request->post('creation');
    		$list = Yii::$app->request->post('list');

			if($creation) {
	    		$nworkout = new \common\models\Workout();
	    		$nworkout->course_id = $course_id;
	    		$nworkout->title = 'Новая тренировка';
	    		if($nworkout->save()) {
	    			$nworkout->refresh();
	    			$response['workout_id'] = $nworkout->id;
	    			$response['workout_title'] = $nworkout->title;
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось создать тренировку';
	    		}
    		}

    		if($response['status'] == 'ok') {
    			$count = 1;
    			\common\models\Workout::updateAll(['number'=>0], ['course_id'=>$course_id]);
    			if(sizeof($list) > 0) {
    				foreach($list as $wid) if($wid) {
    					$workout = \common\models\Workout::find()->where(['id'=>$wid])->one();
    					$workout->number = $count;
    					$count++;
		    			if(!$workout->save()) {
			    			$response['status'] = 'error';
			    			$response['error'] = 'Не удалось сохранить список тренировок';
			    		}
		    		}
	    		}
	    		if($creation) {
	    			$nworkout->number = $count;
	    			$nworkout->save();
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionAdd_exercise()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$list = Yii::$app->request->post('list');
    		$response['rand'] = Yii::$app->request->post('rand');

    		$response['workout_id'] = $workout_id;
    		$response['exercise_id'] = $exercise_id;

    		if($exercise_id) {
    			$exercise = \common\models\Exercise::findOne($exercise_id);
    			if($exercise) {
    				$transaction = Yii::$app->db->beginTransaction();

	    			$workout_exercise = new \common\models\Workout_exercise();
	                $workout_exercise->workout_id = $workout_id;
	                $workout_exercise->exercise_id = $exercise_id;
	                $workout_exercise->type = \common\models\Workout_exercise::Exercise;
	                if($workout_exercise->save()) {
	                	$workout_exercise->refresh();
	                	$response['relation_id'] = $workout_exercise->id;
	                	$response['type'] = \common\models\Course_training_routine::WorkoutRelation;
	                	$routines = $exercise->getTrainingRoutines('set')->all();
	                	if(sizeof($routines) > 0)
	                	foreach($routines as $routine) {
			    			$course_training_routine = new \common\models\Course_training_routine();
			                $course_training_routine->id_relation = $workout_exercise->id;
			                $course_training_routine->type = \common\models\Course_training_routine::WorkoutRelation;
			                $course_training_routine->set = $routine->set;
			                $course_training_routine->sleep = $routine->sleep;
			                if($course_training_routine->save()) {
			                	$course_training_routine->refresh();
	                        	$sets = $routine->getSets();
	                        	if(sizeof($sets) > 0)
	                        	foreach($sets as $set) {
                                	$course_set = new \common\models\Course_set();
                                	$course_set->id_set_attr = $set->id_set_attr;
                                	$course_set->id_c_tr_rout = $course_training_routine->id;
                                	$course_set->default_val = $set->default_val;
                                	$course_set->trigger = $set->trigger;
                                	if(!$course_set->save()) {
						    			$response['status'] = 'error';
						    			$response['error'] = 'Не удалось добавить упражнение';
						    			$transaction->rollback();
                                	}
	                        	}
                        	} else {
				    			$response['status'] = 'error';
				    			$response['error'] = 'Не удалось добавить упражнение';
				    			$transaction->rollback();
                        	}
	                	}
	                } else {
		    			$response['status'] = 'error';
		    			$response['error'] = 'Не удалось добавить упражнение';
		    			$transaction->rollback();
	                }

	                $transaction->commit();
                } else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось найти  упражнение';
                }
    		}

    		if($response['status'] == 'ok') {
            	\common\models\Workout_exercise::updateAll(['number'=>0], ['workout_id'=>$workout_id]);
            	$count = 1;
    			if(sizeof($list) > 0) {
    				foreach($list as $exid) if(is_array($exid) and isset($exid['id']) and $exid['id'] > 0) {
    					$workout_exercise = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id, 'exercise_id'=>$exid['id'], 'type'=>$exid['type'], 'number'=>0])->one();
    					if($workout_exercise) {
	    					$workout_exercise->number = $count;
	    					$count++;
			    			if(!$workout_exercise->save()) {
				    			$response['status'] = 'error';
				    			$response['error'] = 'Не удалось сохранить список упражнений';
				    		}
			    		}
		    		}
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionDel_exercise() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$number = Yii::$app->request->post('number');
    		$number++;

    		$type = ($exercise_id)?\common\models\Workout_exercise::Exercise:\common\models\Workout_exercise::Set;

    		$workout_exercise = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id, 'exercise_id'=>$exercise_id, 'number'=>$number, 'type'=>$type])->one();
    		if($workout_exercise) {
    			if(!$workout_exercise->del()) {
	    			$response['status'] = 'error';
	                $response['error'] = 'Не удалось удалить упражнение/сет';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти упражнение/сет';
    		}

			if($response['status'] == 'ok') {
    			$workout_exercises = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id])->orderBy('number')->all();
    			$count = 1;
    			foreach($workout_exercises as $wex) {
    				$wex->number = $count;
    				$wex->save();
                	$count++;
    			}
    		}

    		return $response;
    	}
    }

    public function actionAdd_set_exercise()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$list = Yii::$app->request->post('list');
    		$response['rand'] = Yii::$app->request->post('rand');

    		$response['set_id'] = $set_id;
    		$response['exercise_id'] = $exercise_id;

    		if($exercise_id) {
    			$exercise = \common\models\Exercise::findOne($exercise_id);
    			if($exercise) {
	    			$transaction = Yii::$app->db->beginTransaction();

	    			$workout_exercise = new \common\models\Set_exercise();
	                $workout_exercise->set_id = $set_id;
	                $workout_exercise->exercise_id = $exercise_id;
	                if($workout_exercise->save()) {
	                    $workout_exercise->refresh();
	                	$response['relation_id'] = $workout_exercise->id;
	                	$response['type'] = \common\models\Course_training_routine::SetRelation;

		    			$course_training_routine = new \common\models\Course_training_routine();
		                $course_training_routine->id_relation = $workout_exercise->id;
		                $course_training_routine->type = \common\models\Course_training_routine::SetRelation;
		                $course_training_routine->set = 1;
		                $course_training_routine->sleep = 0;

		                if($course_training_routine->save()) {
		                	$course_training_routine->refresh();

		                	$routine = $exercise->getTrainingRoutines('set')->one();
		                	$sets = ($routine)?$routine->getSets():[];
                        	if(sizeof($sets) > 0)
                        	foreach($sets as $set) {
                               	$course_set = new \common\models\Course_set();
                               	$course_set->id_set_attr = $set->id_set_attr;
                               	$course_set->id_c_tr_rout = $course_training_routine->id;
                               	$course_set->default_val = $set->default_val;
                               	$course_set->trigger = $set->trigger;
                               	if(!$course_set->save()) {
					    			$response['status'] = 'error';
					    			$response['error'] = 'Не удалось добавить упражнение в сет';
					    			$transaction->rollback();
                               	}
	                        }
                       	} else {
			    			$response['status'] = 'error';
			    			$response['error'] = 'Не удалось добавить упражнение в сет';
			    			$transaction->rollback();
                       	}
	                } else {
	                    $transaction->rollback();
		    			$response['status'] = 'error';
		    			$response['error'] = 'Не удалось добавить упражнение в сет';
	                }

	                $transaction->commit();
                } else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось найти  упражнение';
                }
    		}

    		if($response['status'] == 'ok') {
            	\common\models\Set_exercise::updateAll(['number'=>0], ['set_id'=>$set_id]);
    			if(sizeof($list) > 0) {
    				$count = 1;
    				foreach($list as $exid) if(is_array($exid) and isset($exid['id']) and $exid['id'] > 0) {
    					$set_exercise = \common\models\Set_exercise::find()->where(['set_id'=>$set_id, 'exercise_id'=>$exid['id'], 'number'=>0])->one();
     					$set_exercise->number = $count;
	    				$count++;
		    			if(!$set_exercise->save()) {
			    			$response['status'] = 'error';
			    			$response['error'] = 'Не удалось сохранить список упражнений в сете';
			    		}
		    		}
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionDel_set_exercise() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$exercise_id = Yii::$app->request->post('exercise_id');
    		$number = Yii::$app->request->post('number');
    		$number++;

    		$set_exercise = \common\models\Set_exercise::find()->where(['set_id'=>$set_id, 'exercise_id'=>$exercise_id, 'number'=>$number])->one();
    		if($set_exercise) {
    			if(!$set_exercise->del()) {
	    			$response['status'] = 'error';
	                $response['error'] = 'Не удалось удалить упражнение из сета';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти упражнение из сета';
    		}

			if($response['status'] == 'ok') {
    			$set_exercises = \common\models\Set_exercise::find()->where(['set_id'=>$set_id])->orderBy('number')->all();
    			$count = 1;
    			foreach($set_exercises as $sex) {
    				$sex->number = $count;
    				$sex->save();
                	$count++;
    			}
    		}

    		return $response;
    	}
    }

    public function actionDel_workout() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');

    		$workout = \common\models\Workout::find()->where(['id'=>$workout_id])->one();

    		if($workout) {
    			$transaction = Yii::$app->db->beginTransaction();
    			$workout_exercises = $workout->getWorkoutExercises()->all();
				foreach($workout_exercises as $workout_exercise) {
   					if($workout_exercise->type == \common\models\Workout_exercise::Set) {
   						$set = $workout_exercise->getSet();
                        $set_exercises = $set->getExercises()->all();
                        foreach($set_exercises as $set_exercise) {
                        	if(!$set_exercise->delete()) {
				    			$response['status'] = 'error';
				                $response['error'] = 'Не удалось удалить тренировку';
				                $transaction->rollback();
				                return $response;
                        	}
                        }
                        if(!$set->delete()) {
			    			$response['status'] = 'error';
			                $response['error'] = 'Не удалось удалить тренировку';
                        	$transaction->rollback();
                        	return $response;
                        }
   					}
					if(!$workout_exercise->delete()) {
						$response['status'] = 'error';
						$response['error'] = 'Не удалось удалить тренировку';
						$transaction->rollback();
						return $response;
					}
   				}
				if(!$workout->delete()) {
					$response['status'] = 'error';
					$response['error'] = 'Не удалось удалить тренировку';
					$transaction->rollback();
					return $response;
				}
    			$transaction->commit();
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти тренировку';
    		}

    		return $response;
    	}
    }

    public function actionDel_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');

    		$set = \common\models\Workout_set::find()->where(['id'=>$set_id])->one();
    		if($set) {
    			$transaction = Yii::$app->db->beginTransaction();
				$set_exercises = $set->getExercises()->all();
				$workout_exercise = $set->getWorkout_exercise();
				$response['workout_id'] = $set->getWorkout_exercise()->workout_id;
				foreach($set_exercises as $set_exercise) {
					if(!$set_exercise->delete()) {
						$response['status'] = 'error';
						$response['error'] = 'Не удалось удалить сет';
						$transaction->rollback();
						return $response;
					}
				}
				if(!$workout_exercise->delete()) {
					$response['status'] = 'error';
					$response['error'] = 'Не удалось удалить сет';
					$transaction->rollback();
					return $response;
				}
				if(!$set->delete()) {
					$response['status'] = 'error';
					$response['error'] = 'Не удалось удалить сет';
					$transaction->rollback();
					return $response;
				}
    			$transaction->commit();
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти сет';
    		}

    		return $response;
    	}
    }

    public function actionEdit_workout() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$title = Yii::$app->request->post('title');

    		$workout = \common\models\Workout::find()->where(['id'=>$workout_id])->one();

    		if($workout) {
    			$old_title = $workout->title;
    			$workout->title = $title;
    			if(!$workout->save()) {
	    			$response['status'] = 'error';
	    			$response['title'] = $old_title;
	                $response['error'] = 'Не удалось сохоранить тренировку';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти тренировку';
    		}

    		return $response;
    	}
    }

    public function actionAdd_set()
    {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');
    		$creation  = Yii::$app->request->post('creation');
    		$list = Yii::$app->request->post('list');

			if($creation) {
	    		$set = new \common\models\Workout_set();
	    		$set->title = 'Новый сет';
	    		$set->sleep = 0;
	    		$set->alert_proc = '';
	    		$set->alert_sleep = '';
	    		if($set->save()) {
	    			$set->refresh();
	    			$response['set_id'] = $set->id;
	    			$response['set_title'] = $set->title;
	    			$response['workout_id'] = $workout_id;
	    			$set_exercise = new \common\models\Workout_exercise();
	                $set_exercise->workout_id = $workout_id;
	                $set_exercise->exercise_id = $set->id;
	                $set_exercise->type = \common\models\Workout_exercise::Set;
	                if(!$set_exercise->save()) {
		    			$response['status'] = 'error';
		    			$response['error'] = 'Не удалось добавить сет в тренировку';
	                }
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось создать сет';
	    		}
    		}

    		if($response['status'] == 'ok') {
    			$count = 1;
            	\common\models\Workout_exercise::updateAll(['number'=>0], ['workout_id'=>$workout_id]);
    			if(sizeof($list) > 0) {
    				foreach($list as $exid) if(is_array($exid) and isset($exid['id']) and $exid['id'] > 0) {
    					$workout_exercise = \common\models\Workout_exercise::find()->where(['workout_id'=>$workout_id, 'exercise_id'=>$exid['id'], 'type'=>$exid['type'], 'number'=>0])->one();
    					if($workout_exercise) {
	    					$workout_exercise->number = $count;
	    					$count++;
			    			if(!$workout_exercise->save()) {
				    			$response['status'] = 'error';
				    			$response['error'] = 'Не удалось сохранить список упражнений';
				    		}
			    		}
		    		}
	    		}
	    		if($creation) {
	    			$set_exercise->number = $count;
	    			$set_exercise->save();
	    		}
	    	}

    		return $response;
    	}
    }

    public function actionEdit_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$set = \common\models\Workout_set::find()->where(['id'=>$set_id])->one();

    		if($set) {
    			$old_value = $set->$field;
    			$set->$field = str_replace('"', '&quot;', $value);
    			if(!$set->save()) {
	    			$response['status'] = 'error';
	    			$response['value'] = $old_title;
	    			$response['field'] = $field;
	                $response['error'] = 'Не удалось сохранить сет';
                }
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти сет';
    		}

    		return $response;
    	}
    }

	public function actionClone_workout() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

			$workout_id = Yii::$app->request->post('workout_id');

			$workout = \common\models\Workout::findOne($workout_id);

			if($workout) {
            	$clon_workout = $workout->clonCopy();
            	if($clon_workout['status'] == 'ok') {
            		$cloned_workout = $clon_workout['workout'];
                   	$response['status'] = 'ok';
                   	$response['workout'] = $this->renderPartial('workout', ['workout' => $cloned_workout, 'anket_fields' => $workout->getCourse()->getAnket(), 'reportFieldsTypes' => \common\models\Workout_report_field::getTypesList()]);
                   	//$this->renderPartial('/mail/registration', ['promoCode'=>false, 'siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$password])
            	} else {
                	$response['error'] = $clon_workout['error'];
            	}
			} else {
				$response['error'] = 'Не удалось найти тренировку';
			}

    		return $response;
    	}
	}

	public function actionClone_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

			$set_id = Yii::$app->request->post('set_id');
			$workout_id = Yii::$app->request->post('workout_id');

			$set = \common\models\Workout_set::findOne($set_id);
			$workout = \common\models\Workout::findOne($workout_id);

			if($workout) {
				if($set) {
	            	$clon_set = $set->clonCopy();
	            	if($clon_set['status'] == 'ok') {
                        $last_one = $workout->getWorkoutExercises('number desc')->asArray()->one();
                        $cloned_set = $clon_set['set'];
                        $clone_workout_exercise = new \common\models\Workout_exercise;
                        $clone_workout_exercise->workout_id = $workout->id;
                        $clone_workout_exercise->exercise_id = $cloned_set->id;
                        $clone_workout_exercise->type = \common\models\Workout_exercise::Set;
                        $clone_workout_exercise->number = $last_one['number'] + 1;
                        if($clone_workout_exercise->save()) {
                        	$response['status'] = 'ok';
                        	$response['workout_id'] = $workout->id;
                        	$response['set'] = $this->renderPartial('set', ['set' => $cloned_set]);
                        	//$this->renderPartial('/mail/registration', ['promoCode'=>false, 'siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$password])
                        }
	            	} else {
	                	$response['error'] = $clon_set['error'];
	            	}
				} else {
					$response['error'] = 'Не удалось найти сет';
				}
			} else {
				$response['error'] = 'Не удалось найти упражнение';
			}

    		return $response;
    	}
	}

	public function actionClone_exercise() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

			$relation_id = Yii::$app->request->post('relation_id');

			$workout_exercise = \common\models\Workout_exercise::findOne($relation_id);

			if($workout_exercise) {
            	$clon_exercise = $workout_exercise->clonCopy();
            	if($clon_exercise['status'] == 'ok') {
            		$cloned_exercise = $clon_exercise['exercise'];
                   	$response['status'] = 'ok';
                   	$response['workout_id'] = $cloned_exercise->workout_id;
                   	$response['exercise'] = $this->renderPartial('exercise', ['workout_exercise' => $cloned_exercise]);
                   	//$this->renderPartial('/mail/registration', ['promoCode'=>false, 'siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$password])
            	} else {
                	$response['error'] = $clon_exercise['error'];
            	}
			} else {
				$response['error'] = 'Не удалось найти упражнение в этой тренировке';
			}

    		return $response;
    	}
	}

	public function actionClone_routine() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'error', 'error'=>'');

			$routine_id = Yii::$app->request->post('routine');

			$routine = \common\models\Course_training_routine::findOne($routine_id);

			if($routine) {
            	$clon_routine = $routine->clonCopy();
            	if($clon_routine['status'] == 'ok') {
            		$attributesList = \common\models\Set_attribute::find()->all();

            		$cloned_routine = $clon_routine['routine'];
                   	$response['status'] = 'ok';
                   	$response['id_relation'] = $cloned_routine->id_relation;
                   	$response['routine'] = $this->renderPartial('routine', ['routine' => $cloned_routine, 'attributesList' => $attributesList]);
                   	//$this->renderPartial('/mail/registration', ['promoCode'=>false, 'siteData'=>Yii::$app->params['siteData'], 'user'=>$user, 'pass'=>$password])
            	} else {
                	$response['error'] = $clon_exercise['error'];
            	}
			} else {
				$response['error'] = 'Не удалось найти подход в этом упражнении';
			}

    		return $response;
    	}
	}

    public function actionDel_course_routine() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$routine_id = Yii::$app->request->post('routine_id');

    		$routine = \common\models\Course_training_routine::findOne($routine_id);

    		$relation = $routine->id_relation;
    		$type = $routine->type;

    		if($routine->del()) {
    			$routines = \common\models\Course_training_routine::find()->where(['id_relation'=>$relation, 'type'=>$type])->orderBy('set')->all();
    			if(sizeof($routines) > 0) {
    				$set = 1;
                	foreach($routines as $routine) {
                  		$routine->set = $set;
                  		$routine->save();
                  		$set++;
                	}
    			}
    		} else {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти сет';
    		}

    		$response['routine_id'] = $routine_id;

    		return $response;
    	}
    }

    public function actionDel_course_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');

    		$set = \common\models\Course_set::findOne($set_id);

    		if(!$set->delete()) {
    			$response['status'] = 'error';
    			$response['error'] = 'Не удалось найти параметр';
    		}

    		$response['set_id'] = $set_id;

    		return $response;
    	}
    }

    public function actionAdd_course_routine() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$relation_id = Yii::$app->request->post('relation_id');
    		$type = Yii::$app->request->post('type');

    		$relation = false;
            if($type == \common\models\Course_training_routine::WorkoutRelation) {
    			$relation = \common\models\Workout_exercise::findOne($relation_id);
    		} elseif($type == \common\models\Course_training_routine::SetRelation) {
    			$relation = \common\models\Set_exercise::findOne($relation_id);
    		}

    		if($relation and $relation_id and $type) {
    			$maxSet = $relation->getCourseRoutines()->select(['set'=>'IF(ISNULL(MAX(`set`)), 1, MAX(`set`))'])->one()->toArray();
    			$routine = new \common\models\Course_training_routine();
    			$routine->id_relation = $relation_id;
    			$routine->type = $type;
    			$routine->sleep = 0;
    			$routine->set = $maxSet['set'] + 1;
	    		if($routine->save()) {
	    			$routine->refresh();
	    			$response['id'] = $routine->id;
	    			$response['relation_id'] = $relation_id;
	    			$response['type'] = $type;
	    			$response['set'] = $maxSet['set'] + 1;
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось добавить подход';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось найти связь или недопустимая связь';
    		}

    		return $response;
    	}
    }

    public function actionAdd_course_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$routine_id = Yii::$app->request->post('routine_id');

    		if($routine_id) {
    			$set = new \common\models\Course_set();
    			$set->id_c_tr_rout = $routine_id;
    			$set->id_set_attr = 0;
    			$set->default_val = '';
    			$set->trigger = '';
	    		if($set->save()) {
	    			$set->refresh();
	    			$response['id'] = $set->id;
	    			$response['routine_id'] = $routine_id;
	    		} else {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось добавить параметр';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось добавить параметр';
    		}

    		return $response;
    	}
    }

    public function actionEdit_exercise_trigger() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$relation_id = Yii::$app->request->post('relation_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$exercise = \common\models\Workout_exercise::findOne($relation_id);

    		$response['id'] = $relation_id;
    		$response['field'] = $field;
    		$response['old_value'] = $exercise->$field;

    		if($exercise) {
    			$exercise->$field = str_replace('"', '&quot;', $value);
	    		if(!$exercise->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить триггер';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить триггер';
    		}

    		return $response;
    	}
    }

    public function actionEdit_set_exercise_trigger() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$relation_id = Yii::$app->request->post('relation_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$exercise = \common\models\Set_exercise::findOne($relation_id);

    		$response['id'] = $relation_id;
    		$response['field'] = $field;
    		$response['old_value'] = $exercise->$field;

    		if($exercise) {
    			$exercise->$field = str_replace('"', '&quot;', $value);
	    		if(!$exercise->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить триггер';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить триггер';
    		}

    		return $response;
    	}
    }

    public function actionEdit_course_routine() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$routine_id = Yii::$app->request->post('routine_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$routine = \common\models\Course_training_routine::findOne($routine_id);

    		$response['id'] = $routine_id;
    		$response['field'] = $field;
    		$response['old_value'] = $routine->$field;

    		if($routine) {
    			$routine->$field = str_replace('"', '&quot;', $value);
	    		if(!$routine->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить подход';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить подход';
    		}

    		return $response;
    	}
    }

    public function actionEdit_course_set() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$set_id = Yii::$app->request->post('set_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$set = \common\models\Course_set::findOne($set_id);

    		$response['id'] = $set_id;
    		$response['field'] = $field;
    		$response['old_value'] = $set->$field;

    		if($set) {
    			$set->$field = $value;
	    		if(!$set->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить параметр';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить параметр';
    		}

    		return $response;
    	}
    }

    public function actionAdd_workout_report_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_id = Yii::$app->request->post('workout_id');

    		$field = new \common\models\Workout_report_field();
    		$field->workout_id = $workout_id;
    		$field->title = 'Новое поле';
    		$field->type = 0;
    		if($field->save()) {
    			$field->refresh();
	    		$response['workout_id'] = $workout_id;
	    		$response['field_id'] = $field->id;
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось добавить поле отчёта';
    		}

    		return $response;
    	}
    }

    public function actionEdit_workout_report_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_report_field_id = Yii::$app->request->post('workout_report_field_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$workout_report_field = \common\models\Workout_report_field::findOne($workout_report_field_id);

    		$response['id'] = $workout_report_field_id;
    		$response['field'] = $field;

    		if($workout_report_field) {
	    		$response['old_value'] = $workout_report_field->$field;
    			$workout_report_field->$field = $value;
	    		if(!$workout_report_field->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить поле отчёта';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить поле отчёта';
    		}

    		return $response;
    	}
    }

    public function actionDel_workout_report_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$workout_report_field_id = Yii::$app->request->post('workout_report_field_id');

    		$workout_report_field = \common\models\Workout_report_field::findOne($workout_report_field_id);

    		if($workout_report_field) {
    			$response['id'] = $workout_report_field->id;
	    		if(!$workout_report_field->delete()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось удалить поле отчёта';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось удалить поле отчёта';
    		}

    		return $response;
    	}
    }

    public function actionAdd_course_anket_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_id = Yii::$app->request->post('course_id');

    		$field = new \common\models\Course_anket();
    		$field->course_id = $course_id;
    		$field->title = 'Новое поле';
    		$field->type = 0;
    		if($field->save()) {
    			$field->refresh();
	    		$response['field_id'] = $field->id;
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось добавить поле анкеты';
    		}

    		return $response;
    	}
    }

    public function actionEdit_course_anket_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_anket_field_id = Yii::$app->request->post('course_anket_field_id');
    		$field = Yii::$app->request->post('field');
    		$value = Yii::$app->request->post('value');

    		$course_anket_field = \common\models\Course_anket::findOne($course_anket_field_id);

    		$response['id'] = $course_anket_field_id;
    		$response['field'] = $field;

    		if($course_anket_field) {
    			$response['old_value'] = $course_anket_field->$field;
    			$course_anket_field->$field = $value;
	    		if(!$course_anket_field->save()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось сохранить поле анкеты';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось сохранить поле анкеты';
    		}

    		return $response;
    	}
    }

    public function actionDel_course_anket_field() {
    	if(Yii::$app->request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		$response = array('status'=>'ok', 'error'=>'');

    		$course_anket_field_id = Yii::$app->request->post('course_anket_field_id');

    		$course_anket_field = \common\models\Course_anket::findOne($course_anket_field_id);

    		if($course_anket_field) {
    			$response['id'] = $course_anket_field->id;
	    		if(!$course_anket_field->delete()) {
	    			$response['status'] = 'error';
	    			$response['error'] = 'Не удалось удалить поле анкеты';
	    		}
    		} else {
    			$response['status'] = 'error';
	    		$response['error'] = 'Не удалось удалить поле анкеты';
    		}

    		return $response;
    	}
    }

}
