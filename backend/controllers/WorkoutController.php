<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\Set_attribute;

/**
 * Site controller
 */
class WorkoutController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $workouts = \common\models\Exercise::find();
        $count_exercises = clone $exercises;
        $pages = new Pagination(['totalCount' => $count_exercises->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;
        $exercises = $exercises->orderby(['id' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'exercises' => $exercises,
            'pages' => $pages,
            'count_exercises' => $count_exercises->count(),
        ]);
    }

    public function actionView()
    {
    	$id = Yii::$app->request->get('exercise_id');
        if($exercise = \common\models\Exercise::findOne($id)) {
            return $this->render('view', ['exercise' => $exercise]);
        } else {
            return $this->render('/site/error', [
                'message' => 'Такое упражнение не существует! Может кто-то форму подменил?!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/exercise"])
            ]);
        }
        return $this->render('view', ['exercise' => $exercise]);
    }

    public function actionAdd()
    {
        $model = new \backend\models\ExerciseForm();
        $model->scenario = 'add';

        $attributes = Set_attribute::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->add()){
                $this->redirect('index');
            }
        }
        return $this->render('add', ['model'=>$model, 'attributes'=>$attributes]);
    }

    public function actionEdit()
    {
    	$id = Yii::$app->request->get('exercise_id');
        if($id > 0) {
            $model = new \backend\models\ExerciseForm();
            $model->scenario = 'edit';
            if($model->load(Yii::$app->request->post()) && $model->validate()) {
                if($model->edit(Yii::$app->request->post('ExerciseForm')['id'])){
                    //$this->redirect(\Yii::$app->urlManager->createAbsoluteUrl(["/exercise"]));
                }
            }
            if($exercise = \common\models\Exercise::findOne($id)) {
                $model->attributes = $exercise->attributes;
                $sets = $exercise->getTrainingRoutines()->all();
            } else {
                return $this->render('/site/error', [
                    'message' => 'Такое упражнение не существует! Может кто-то форму подменил?!',
                    'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/exercise"])
                ]);
            }
            $attributes = Set_attribute::find()->all();
            return $this->render('edit', ['model'=>$model, 'exercise_id'=>$id, 'attributes'=>$attributes, 'sets'=>$sets]);
        } else {
            return $this->render('/site/error', [
                'message' => 'Id упражнения не передан - редактирование невозможно!',
                'back' => \Yii::$app->urlManager->createAbsoluteUrl(["/exercise"])
            ]);
        }
    }

}
