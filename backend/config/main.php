<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'controllerNamespace' => 'backend\controllers',
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            	'admin/elfinder/connector' => 'admin/elfinder/connector',
                'course/edit/<course_id:\d+>' => 'course/edit',
                'course/delete/<course_id:\d+>' => 'course/delete',
                'course/view/<course_id:\d+>' => 'course/view',
                'course/clone/<course_id:\d+>' => 'course/clone',
                'schedule/view/<order_id:\d+>' => 'schedule/view',
                'course_group/edit/<course_group_id:\d+>' => 'course_group/edit',
                'course_level/edit/<course_level_id:\d+>' => 'course_level/edit',
                'setattribute/edit/<setattribute_id:\d+>' => 'setattribute/edit',
                'exercise/edit/<exercise_id:\d+>' => 'exercise/edit',
                'exercise/view/<exercise_id:\d+>' => 'exercise/view',
                'exercise/delete/<exercise_id:\d+>' => 'exercise/delete',
                'exercise/clone/<exercise_id:\d+>' => 'exercise/clone',
                'sell_item/edit/<sell_item_id:\d+>' => 'sell_item/edit',
            	'order/<user_id:\d+>' => 'order/index',
            	'order/delete/<order_id:\d+>' => 'order/delete',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
