$(document).ready(function(){
    var droppedAr = false;
    var draggedEx = false;
    var minExWidth = $('#workouts').innerWidth();
    $( ".exercises" )
        .sortable({
            revert: true,
            start: function( event, ui ){
                //$(ui.helper).css({width:'auto', 'min-width': minExWidth+'px'});
            },
            stop: function( event, ui ){
                //$(ui.helper).css({height:'auto', 'min-width':'auto'});

                if($(ui.helper).hasClass('exercise')) {
                    //$(ui.helper).css({width:'100%'});
                }

                saveExercisesList($(event.target).parent().data('workout-id'));

            }
        }).droppable({
        greedy: true,
        drop: function( event, ui ) {
            droppedAr = {where:'workout', who:$(ui.helper).data('exercise-id'), to:$(event.target).parent().data('workout-id')};
            /*if(ui.helper.hasAttribute('list')) {
             $(ui.helper).removeAttr('list').find('div:last-child').show();
             addExercise($(ui.helper).data('exercise-id'), $(event.target).parent().data('workout-id'));
             }*/
        }
    });
    $( ".set-exercises" )
        .sortable({
            revert: true,
            start: function( event, ui ){
                //$(ui.helper).css({width:'auto'});
            },
            stop: function( event, ui ){
                //$(ui.helper).css({height:'auto', width:'100%'});
                saveSetExercisesList($(event.target).parent().parent().data('set-id'));
            }
        }).droppable({
        greedy: true,
        drop: function( event, ui ) {
            droppedAr = {where:'set', who:$(ui.helper).data('exercise-id'), to:$(event.target).parent().parent().data('set-id')};
            /*if(ui.helper.hasAttribute('list')) {
             $(ui.helper).removeAttr('list').find('div:last-child').show();
             addSetExercise($(ui.helper).data('exercise-id'), $(event.target).parent().data('set-id'));
             }*/
        }
    });
    $( "#workouts" ).sortable({
        revert: true,
        stop: function( event, ui ){
            saveWorkoutList();
        }
    });
    $( "#exercises .exercise" ).draggable({
        connectToSortable: ".exercises, .set-exercises",
        revert: "invalid",
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        start: function() {
            draggedEx = true;
            droppedAr = false;
        },
        stop: function( event, ui ) {
            if(draggedEx === true && droppedAr !== false) {
                if(ui.helper.attr('list') === 'true') {
                    //$(ui.helper).css({height:'auto', 'min-width':'auto'});
                    rndVal = Math.random().toString().replace('0.', '');
                    $(ui.helper).removeAttr('list').attr('rand', rndVal).find('div.ex-close').show();
                    if(droppedAr.where == 'workout') {
                        addExercise(droppedAr.who, droppedAr.to, rndVal);
                    } else
                    if(droppedAr.where == 'set') {
                        addSetExercise(droppedAr.who, droppedAr.to, rndVal);
                    }
                }
            }
            draggedEx = false;
        }
    });
    $('#workout_add').click(function(){
        var wlist = [];
        $('#workouts .workout').each(function() {
            if($(this).data('workout-id') != undefined && $(this).data('workout-id') > 0) {
                wlist[$(this).index()] = $(this).data('workout-id');
            }
        });
        $.ajax({
                method: 'POST',
                url: '/admin/course/add_workout',
                data: { 'course_id': course_id, 'creation': 1, 'list': wlist },
        dataType: 'json',
            success: function(result) {
            if(result && result.status && result.status == 'ok') {
                var new_workout = $('#workout_template').clone().removeAttr('id').show();
                new_workout.find('input[type=text]').val(result.workout_title);
                new_workout.data('workout-id', result.workout_id).attr('id', 'workout'+result.workout_id).find('.exercises')
                    .sortable({
                        revert: true,
                        start: function( event, ui ){
                            //$(ui.helper).css({width:'auto', 'min-width': minExWidth+'px'});
                        },
                        stop: function( event, ui ){
                            //$(ui.helper).css({height:'auto', 'min-width':'auto'});
                            if($(ui.helper).hasClass('exercise')) {
                                //$(ui.helper).css({width:'100%'});
                            }
                            saveExercisesList($(event.target).parent().data('workout-id'));
                        }
                    }).droppable({
                    drop: function( event, ui ) {
                        droppedAr = {where:'workout', who:$(ui.helper).data('exercise-id'), to:$(event.target).parent().data('workout-id')};
                        /*if(ui.helper.hasAttribute('list')) {
                         $(ui.helper).removeAttr('list');
                         addExercise($(ui.helper).data('exercise-id'), $(event.target).parent().data('workout-id'));
                         }*/
                    }
                });
                $('#workouts').append(new_workout);
            } else {
                console.log(result.error);
            }
        },
        error: function() {
            console.log('Не удалось создать тренировку');
        }
    });
    });
    $('#workouts').delegate('.set-add', 'click', function(){
        var workout_id = $(this).parent().parent().data('workout-id');
        var exlist = [];
        $('#workout'+workout_id+' .exercise, #workout'+workout_id+' .set').each(function() {
            if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
                exlist[$(this).index()] = {id:$(this).data('exercise-id'), type:0};
            } else if($(this).data('set-id') != undefined && $(this).data('set-id') > 0) {
                exlist[$(this).index()] = {id:$(this).data('set-id'), type:1};
            }
        });
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_set',
            data: { 'workout_id': workout_id, 'creation': 1, 'list': exlist },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    var new_set = $('#set_template').clone().removeAttr('id').show();
                    new_set.find('input.title').val(result.set_title);
                    new_set.find('input.sleep').val('0');
                    new_set.data('set-id', result.set_id).attr('id', 'set'+result.set_id).find('.set-exercises')
                        .sortable({
                            revert: true,
                            start: function( event, ui ){
                            },
                            stop: function( event, ui ){
                                saveSetExercisesList($(event.target).parent().parent().data('set-id'));
                            }
                        }).droppable({
                        greedy: true,
                        drop: function( event, ui ) {
                            droppedAr = {where:'set', who:$(ui.helper).data('exercise-id'), to:$(event.target).parent().parent().data('set-id')};
                        }
                    })
                    $('#workout'+result.workout_id+' .exercises').append(new_set);
                    saveExercisesList(result.workout_id);
                } else {
                    //$('#workout'+result.workout_id+' .exercise[data-exercise-id='+result.exercise_id+']').remove();
                    console.log(result.error);
                }
            },
            error: function(){
                console.log('Не удалось создать сет');
            }
        });
    });
    $('#workouts').delegate('.workout-report-add', 'click', function(){
        console.log($(this).parent().parent().parent().data('workout-id'));
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_workout_report_field',
            data: { 'workout_id': $(this).parent().parent().parent().data('workout-id') },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    $('#workout_field_template').clone().removeAttr('id').show().data('id', result.field_id).attr('id', 'workout_report_field_'+result.field_id).appendTo($('#workout'+result.workout_id+' .workout-fields'));
                } else {
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось добавить поле отчёта');
            }
        });

    });
    /*$('#workouts').delegate('.workout .workout-fields > .workout-field > .ex-close', 'click', function(){
     $(this).parent().remove();
     }); */
    function addExercise(exercise_id, workout_id, rndVal) {
        var exlist = [];
        $('#workout'+workout_id+' .exercise').each(function() {
            if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
                exlist[$(this).index()] = $(this).data('exercise-id');
            }
        });
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_exercise',
            data: { 'exercise_id': exercise_id, 'workout_id': workout_id, list: exlist, rand: rndVal },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    $('#workout'+result.workout_id+' .exercise[rand='+result.rand+']').removeAttr('rand').data('relation-id', result.relation_id).data('type', result.type).attr('id', 'workout_exercise_'+result.relation_id);
                } else {
                    $('#workout'+result.workout_id+' .exercise[rand='+result.rand+']').remove();
                    console.log(result.error);
                }
            },
            error: function() {
                $('#workout'+result.workout_id+' .exercise[rand='+result.rand+']').remove();
                console.log('Не удалось добавить упражнение');
            }
        });
    }
    function saveExercisesList(workout_id) {
        var exlist = [];
        $('#workout'+workout_id+' .exercises > .exercise, #workout'+workout_id+' .exercises > .set').each(function() {
            if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
                exlist[$(this).index()] = {id:$(this).data('exercise-id'), type:0};
            } else if($(this).data('set-id') != undefined && $(this).data('set-id') > 0) {
                exlist[$(this).index()] = {id:$(this).data('set-id'), type:1};
            }
        });
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_exercise',
            data: { 'workout_id': workout_id, list: exlist },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {

                } else {
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить список упражнений');
            }
        });
    }
    function saveSetExercisesList(set_id) {
        var exlist = [];
        $('#set'+set_id+' .set-exercises .exercise').each(function() {
            if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
                exlist[$(this).index()] = {id:$(this).data('exercise-id')};
            }
        });
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_set_exercise',
            data: { 'set_id': set_id, list: exlist },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {

                } else {
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить список упражнений в сете');
            }
        });
    }
    function addSetExercise(exercise_id, set_id, rndVal) {
        var exlist = [];
        $('#set'+set_id+' .set-exercises > .exercise').each(function() {
            if($(this).data('exercise-id') != undefined && $(this).data('exercise-id') > 0) {
                exlist[$(this).index()] = $(this).data('exercise-id');
            }
        });
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_set_exercise',
            data: { 'exercise_id': exercise_id, 'set_id': set_id, rand: rndVal, list: exlist },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    $('#set'+result.set_id+' .exercise[rand='+result.rand+']').removeAttr('rand').data('relation-id', result.relation_id).data('type', result.type).attr('id', 'set_exercise_'+result.relation_id);
                } else {
                    $('#set'+result.set_id+' .exercise[rand='+result.rand+']').remove();
                    console.log(result.error);
                }
            },
            error: function() {
                $('#set'+result.set_id+' .exercise[rand='+result.rand+']').remove();
                console.log('Не удалось добавить упражнение в сет');
            }
        });
    }
    $('#workouts').delegate('.exercise > div.ex-open', 'click', function() {
        if($(this).parent().find('.routines').html() == '') {
            $.ajax({
                method: 'POST',
                url: '/admin/exercise/course_routines',
                data: { 'id': $(this).parent().attr('id'), 'relation_id': $(this).parent().data('relation-id'), 'type':$(this).parent().data('type') },
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                        $('#'+result.id).find('.routines').html(result.html);
                    } else {
                        console.log(result.error);
                    }
                },
                error: function() {
                    console.log('Не удалось удалить упражнение');
                }
            });
        }
        $(this).parent().find('.routines, .ex-trigger').toggle();
        $(this).parent().toggleClass('opened');
    });
    $('#workouts').delegate('.exercises > .exercise > div.ex-close', 'click', function() {
        $(this).parent().addClass('deleted').hide();
        $.ajax({
            method: 'POST',
            url: '/admin/course/del_exercise',
            data: { 'workout_id': $(this).parent().parent().parent().data('workout-id'), exercise_id: $(this).parent().data('exercise-id'), number: $(this).parent().index() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    var workout_id = $('#workouts .exercise.deleted').parent().parent().data('workout_id');
                    $('#workouts .exercise.deleted').remove();
                    saveExercisesList(workout_id);
                } else {
                    $('#workouts .exercise.deleted').show().removeClass('deleted');
                    console.log(result.error);
                }
            },
            error: function() {
                $('#workouts .exercise.deleted').show().removeClass('deleted');
                console.log('Не удалось удалить упражнение');
            }
        });
    });
    $('#workouts').delegate('.set-exercises > .exercise > div.ex-close', 'click', function() {
        $(this).parent().addClass('deleted').hide();
        //console.log($(this).parent().data('exercise-id'));
        $.ajax({
            method: 'POST',
            url: '/admin/course/del_set_exercise',
            data: { 'set_id': $(this).parents('.set').data('set-id'), exercise_id: $(this).parent().data('exercise-id'), number: $(this).parent().index() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    var set_id = $('#workouts .set-exercises > .exercise.deleted').parents('.set').data('set_id');
                    $('#workouts .exercise.deleted').remove();
                    //saveExercisesList(workout_id);
                } else {
                    $('#workouts .exercise.deleted').show().removeClass('deleted');
                    console.log(result.error);
                }
            },
            error: function() {
                $('#workouts .set-exercises > .exercise.deleted').show().removeClass('deleted');
                console.log('Не удалось удалить упражнение из сета');
            }
        });
    });
    $('#workouts').delegate('.workout > div.close', 'click', function() {
        if(confirm('Вы действительно хотите удалить тренировку?')) {
            $(this).parent().addClass('deleted').hide();
            $.ajax({
                method: 'POST',
                url: '/admin/course/del_workout',
                data: { 'workout_id': $(this).parent().data('workout-id') },
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                        $('#workouts .workout.deleted').remove();
                    } else {
                        $('#workouts .workout.deleted').show().removeClass('deleted');
                        console.log(result.error);
                    }
                },
                error: function() {
                    $('#workouts .workout.deleted').show().removeClass('deleted');
                    console.log('Не удалось удалить тренировку');
                }
            });
        }
    });
    $('#workouts').delegate('.workout > div.ex-open', 'click', function() {
        $(this).parent().find('.workout-fields-container').toggle();
    });
    $('#workouts').delegate('.workout > input', 'blur', function() {
        $.ajax({
            method: 'POST',
            url: '/admin/course/edit_workout',
            data: { 'workout_id': $(this).parent().data('workout-id'), title: $(this).val() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                } else {
                    $('#workout'+result.workout_id).val(result.title);
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить тренировку');
            }
        });
    });
    $('#workouts').delegate('.workout > .exercises > .set > input, .workout > .exercises > .set > .set-data > input', 'blur', function() {
        var dfield = $(this).attr('field');
        $.ajax({
            method: 'POST',
            url: '/admin/course/edit_set',
            data: { 'set_id': $(this).parents('.set').data('set-id'), field: dfield, value: $(this).val() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                } else {
                    $('#workout'+result.workout_id).val(result.title);
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить тренировку');
            }
        });
    });
    $('#workouts').delegate('.routines > div > div.close', 'click', function() {
        $(this).parent().addClass('deleted').hide();
        $.ajax({
            method: 'POST',
            url: '/admin/course/del_course_routine',
            data: { 'routine_id': $(this).parent().data('routine-id') },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    $('#course_routine_'+result.routine_id+'.deleted').remove();
                } else {
                    $('#course_routine_'+result.routine_id+'.deleted').removeClass('deleted').show();
                    console.log(result.error);
                }
            },
            error: function() {
                $('#workouts .routines .deleted').removeClass('deleted').show();
                console.log('Не удалось удалить подход');
            }
        });
        $(this).parent().hide();
    });
    $('#workouts').delegate('.routines .routine_sets div.close', 'click', function() {
        $(this).parent().addClass('deleted').hide();
        $.ajax({
            method: 'POST',
            url: '/admin/course/del_course_set',
            data: { 'set_id': $(this).parent().data('set-id') },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    $('#course_set_'+result.set_id+'.deleted').remove();
                } else {
                    $('#course_set_'+result.set_id+'.deleted').removeClass('deleted').show();
                    console.log(result.error);
                }
            },
            error: function() {
                $('#workouts .routines .routine_sets .deleted').removeClass('deleted').show();
                console.log('Не удалось удалить параметр');
            }
        });
        $(this).parent().hide();
    });
    $('#workouts').delegate('.exercise > .ex-trigger > input', 'change', function() {
        $.ajax({
            method: 'POST',
            url: '/admin/course/edit_exercise_trigger',
            data: { 'relation_id': $(this).parent().parent().data('relation-id'), 'field': $(this).attr('field'), 'value': $(this).val() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                } else {
                    $('#workout_exercise_'+result.id+' .ex-trigger[field='+result.field+']').val(result.old_value);
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить триггер');
            }
        });
    });
    $('#workouts').delegate('.set .exercise > .ex-trigger > input', 'change', function() {
        $.ajax({
            method: 'POST',
            url: '/admin/course/edit_set_exercise_trigger',
            data: { 'relation_id': $(this).parent().parent().data('relation-id'), 'field': $(this).attr('field'), 'value': $(this).val() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                } else {
                    $('#set_exercise_'+result.id+' .ex-trigger[field='+result.field+']').val(result.old_value);
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить триггер');
            }
        });
    });
    $('#workouts').delegate('.routines .routine > input', 'change', function() {
        $.ajax({
            method: 'POST',
            url: '/admin/course/edit_course_routine',
            data: { 'routine_id': $(this).parent().data('routine-id'), 'field': $(this).attr('field'), 'value': $(this).val() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                } else {
                    $('#course_routine_'+result.id+' [field='+result.field+']').val(result.old_value);
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить подход');
            }
        });
    });
    $('.workout-fields').delegate('.workout-field > input, .workout-field > select', 'change', function(){
        $.ajax({
            method: 'POST',
            url: '/admin/course/edit_workout_report_field',
            data: { 'workout_report_field_id': $(this).parent().data('id'), 'field': $(this).attr('field'), 'value': $(this).val() },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                } else {
                    $('#workout_report_field_'+result.id+' [field='+result.field+']').val(result.old_value);
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить поле отчёта');
            }
        });
    });
    $('.workout-fields').delegate('.workout-field .ex-close', 'click', function(){
        $.ajax({
            method: 'POST',
            url: '/admin/course/del_workout_report_field',
            data: { 'workout_report_field_id': $(this).parent().data('id') },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    $('#workout_report_field_'+result.id).remove();
                } else {
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось удалить поле отчёта');
            }
        });
    });
    $('#workouts').delegate('.routines .routine_sets .routine-set > select, .routines .routine_sets .routine-set > input', 'change', function() {
        var val = ($(this).attr('type') == 'checkbox')?($(this).prop('checked')?1:0):$(this).val();
        $.ajax({
            method: 'POST',
            url: '/admin/course/edit_course_set',
            data: { 'set_id': $(this).parent().data('set-id'), 'field': $(this).attr('field'), 'value': val },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                } else {
                    $('#course_set_'+result.id+' [field='+result.field+']').val(result.old_value);
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось сохранить параметр');
            }
        });
    });
    $('#workouts').delegate('.routines .add-routine', 'click', function() {
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_course_routine',
            data: { 'relation_id': $(this).data('relation-id'), 'type': $(this).data('type') },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    var new_routine = $('#routine_template').clone().show().attr('id', 'course_routine_'+result.id).data('routine-id', result.id);
                    new_routine.find('span').html(result.set);
                    $('#add_routine_'+result.type+'_'+result.relation_id).before(new_routine);
                } else {
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось добавить подход');
            }
        });
    });
    $('#workouts').delegate('.routines .routine .add-set', 'click', function() {
        $.ajax({
            method: 'POST',
            url: '/admin/course/add_course_set',
            data: { 'routine_id': $(this).parent().data('routine-id') },
            dataType: 'json',
            success: function(result) {
                if(result && result.status && result.status == 'ok') {
                    var new_set = $('#routine_set_template').clone().show().attr('id', 'course_set_'+result.id).data('set-id', result.id);
                    $('#course_routine_'+result.routine_id+' .routine_sets').append(new_set);
                } else {
                    console.log(result.error);
                }
            },
            error: function() {
                console.log('Не удалось добавить параметр');
            }
        });
    });
    function saveWorkoutList() {
        var wlist = [];
        $('#workouts .workout').each(function() {
            if($(this).data('workout-id') != undefined && $(this).data('workout-id') > 0) {
                wlist[$(this).index()] = $(this).data('workout-id');
            }
        });
        $.ajax({
                method: 'POST',
                url: '/admin/course/add_workout',
                data: { 'course_id': course_id, 'list': wlist },
        dataType: 'json',
            success: function(result) {
            if(result && result.status && result.status == 'ok') {

            } else {
                console.log(result.error);
            }
        },
        error: function() {
            console.log('Не удалось сохранить список тренировок');
        }
    });
    }
    $('#workouts').delegate('.workout > .exercises > .set > div.ex-close', 'click', function() {
        if(confirm('Вы действительно хотите удалить сет?')) {
            $(this).parent().addClass('deleted').hide();
            $.ajax({
                method: 'POST',
                url: '/admin/course/del_set',
                data: { 'set_id': $(this).parent().data('set-id') },
                dataType: 'json',
                success: function(result) {
                    if(result && result.status && result.status == 'ok') {
                        $('#workouts .set.deleted').remove();console.log(result.workout_id);
                        saveExercisesList(result.workout_id);
                    } else {
                        $('#workouts .set.deleted').show().removeClass('deleted');
                        console.log(result.error);
                    }
                },
                error: function() {
                    $('#workouts .set.deleted').show().removeClass('deleted');
                    console.log('Не удалось удалить сет');
                }
            });
        }
    });

    	$('#workouts').delegate('.workout > div.workout-expand', 'click', function() {
    		if($(this).find('span').hasClass('glyphicon-upload')) {
    			$(this).find('span').removeClass('glyphicon-upload').addClass('glyphicon-download');
                $(this).parent().find('>.btn-group, >.exercises').hide();
    		} else {
    			$(this).find('span').removeClass('glyphicon-downupload').addClass('glyphicon-upload');
                $(this).parent().find('>.btn-group, >.exercises').show();
    		}
    	});
    	$('#workouts').delegate('.workout > .exercises > .set > div.workout-expand', 'click', function() {
    		if($(this).find('span').hasClass('glyphicon-upload')) {
    			$(this).find('span').removeClass('glyphicon-upload').addClass('glyphicon-download');
                $(this).parent().find('>.set-data').hide();
    		} else {
    			$(this).find('span').removeClass('glyphicon-downupload').addClass('glyphicon-upload');
                $(this).parent().find('>.set-data').show();
    		}
    	});
    	$('#workouts').delegate('.workout > div.ex-clone', 'click', function() {
    		if(confirm('Клонировать тренировку?')) {
	            $.ajax({
	                method: 'POST',
	                url: '/admin/course/clone_workout',
	                data: { 'workout_id': $(this).parent().data('workout-id') },
	                dataType: 'json',
	                success: function(result) {
	                    if(result && result.status && result.status == 'ok') {
	                        $('#workouts').append(result.workout);
	                    } else {
	                        console.log(result.error);
	                    }
	                },
	                error: function() {
	                    console.log('Не удалось клонировать тренировку');
	                }
	            });
    		};
    	});
    	$('#workouts').delegate('.workout > .exercises > .set > div.ex-clone', 'click', function() {
    		if(confirm('Клонировать сет?')) {
	            $.ajax({
	                method: 'POST',
	                url: '/admin/course/clone_set',
	                data: { 'set_id': $(this).parent().data('set-id'), 'workout_id': $(this).parents('.workout').data('workout-id') },
	                dataType: 'json',
	                success: function(result) {
	                    if(result && result.status && result.status == 'ok') {
	                        $('#workout'+result.workout_id+' .exercises').append(result.set);
	                    } else {
	                        console.log(result.error);
	                    }
	                },
	                error: function() {
	                    console.log('Не удалось клонировать сет');
	                }
	            });
    		};
    	});
    	$('#workouts').delegate('.workout > .exercises > .exercise > .routines div.r-clone', 'click', function() {
    		//if(confirm('Клонировать подход?')) {
	            $.ajax({
	                method: 'POST',
	                url: '/admin/course/clone_routine',
	                data: { 'routine': $(this).parent().data('routine-id') },
	                dataType: 'json',
	                success: function(result) {
	                    if(result && result.status && result.status == 'ok') {
	                        $('#workout_exercise_'+result.id_relation+' .routines').append(result.routine);
	                    } else {
	                        console.log(result.error);
	                    }
	                },
	                error: function() {
	                    console.log('Не удалось клонировать подход');
	                }
	            });
    		//};
    	});

    	$('#workouts').delegate('.workout > .exercises > .exercise > div.ex-clone', 'click', function() {
    		if(confirm('Клонировать упражнение?')) {
	            $.ajax({
	                method: 'POST',
	                url: '/admin/course/clone_exercise',
	                data: { 'relation_id': $(this).parent().data('relation-id') },
	                dataType: 'json',
	                success: function(result) {
	                    if(result && result.status && result.status == 'ok') {
	                        $('#workout'+result.workout_id+' .exercises').append(result.exercise);
	                    } else {
	                        console.log(result.error);
	                    }
	                },
	                error: function() {
	                    console.log('Не удалось клонировать упражнение');
	                }
	            });
    		};
    	});

    $('#ex-sticky').hcSticky({
        top: 100,
        bottomEnd: 10,
        noContainer: true
    });

    $('#exercises, #workout_add').addClass('topped');
    $('#exercises').css({
        width: $('#exercises').outerWidth(),
        height: $(window).height()-170,
        'overflow': 'auto',
        left: '0px',
        right: 'auto' //$('#exercises').offset().left
    });

    /* <поиск> */
   	$('#ex-sticky > input[type=checkbox]').change(function(){
   		var tab = $(this).attr('tab'), checked = $(this).prop('checked');
   		$('#exercises .exercise').each(function(){
   			if($(this).attr('tab') == tab) {
   				if(checked) {
   					$(this).show();
   				} else {   					$(this).hide();   				}
   			}
   		});
    });
   	$('#ex-sticky > input[type=text]').keyup(function(){
   		$('#exercises .exercise').each(function(){
   			if($(this).html().replace(/<(.*)>/, '').toLowerCase().indexOf($('#ex-sticky > input[type=text]').val().toLowerCase()) == -1) {
   				$(this).hide();
   			} else {
   				$(this).show();
   			}
   		});
    });
    /* </поиск> */

});